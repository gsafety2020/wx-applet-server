package com.gsafety.gemp.wxapplet.infection.controller;

import com.gsafety.gemp.wxapplet.healthcode.contract.dto.Result;
import com.gsafety.gemp.wxapplet.infection.contract.dto.SuffererInfoDetailDTO;
import com.gsafety.gemp.wxapplet.infection.contract.iface.SuffererInfoDetailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author dusiwei
 */
@Api(value = "病患信息接口", tags = {"病患信息接口"})
@RestController
@RequestMapping("/infection/info")
@Slf4j
public class SuffererDetailController {

    @Autowired
    private SuffererInfoDetailService suffererInfoDetailService;

    @GetMapping(value = "/suffererinfo/detail")
    @ApiOperation(value = "患者详情")
    public Result<SuffererInfoDetailDTO> findPage(@ApiParam(value = "身份证号", required = true ,name = "idNo") @RequestParam("idNo") String idNo){
        if (StringUtils.isBlank(idNo)) {
            return new Result<SuffererInfoDetailDTO>().fail("身份证号不能为空！");
        }
        SuffererInfoDetailDTO dto = suffererInfoDetailService.findSuffererDetail(idNo);
        if (null==dto) {
            return new Result<SuffererInfoDetailDTO>().fail("未查询到数据！");
        }
        return new Result<SuffererInfoDetailDTO>().success("查询成功!",dto);
    }

}
