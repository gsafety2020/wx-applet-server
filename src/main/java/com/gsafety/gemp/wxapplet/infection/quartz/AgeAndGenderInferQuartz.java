package com.gsafety.gemp.wxapplet.infection.quartz;

import cn.hutool.core.util.IdcardUtil;
import com.gsafety.gemp.wxapplet.infection.contract.iface.SuffererInfoService;
import com.gsafety.gemp.wxapplet.infection.dao.po.SuffererInfoPO;
import com.gsafety.gemp.wxapplet.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @className  SuffererAgeAndGenderInferQuartz
 * @description 隔离点患者信息根据身份证号码生成年龄和性别并更新数据
 * @date: 2020/3/9 20:52
 * @author: zxiao
 */
@Configuration      
@EnableScheduling 
@Slf4j
public class AgeAndGenderInferQuartz {

	@Resource
	private SuffererInfoService suffererInfoService;

	/**
	 * 每天上午8点至下午5点每半个小时执行身份证号映射年龄性别
	 */
//	@Scheduled(cron="0/10 * * * * ?")
	@Scheduled(cron="0 0/30 8-17 * * ?")
	private void ageAndGenderInfer(){
		List<SuffererInfoPO> all = suffererInfoService.findAll();
		if (CollectionUtils.isEmpty(all)) {
			log.info("定时任务【隔离点疫情人员身份证计算年龄和性别】由于【没有数据】取消执行........[{}]", DateUtils.getCurrentDateTime());
			return;
		}

		for(SuffererInfoPO po : all) {
			if(IdcardUtil.isValidCard(po.getSuffererCard())) {
				if(!StringUtils.isEmpty(po.getSuffererGender())) {
					log.info("定时任务【隔离点疫情人员身份证计算年龄和性别】由于【已更新】取消执行........[{}]", DateUtils.getCurrentDateTime());
					return;
				}

				log.info("定时任务【隔离点疫情人员身份证计算年龄和性别】开始执行........[{}]", DateUtils.getCurrentDateTime());
				break;
			}
		}

		if (!StringUtils.isEmpty(all.get(0).getSuffererGender())) {
			log.info("定时任务【隔离点疫情人员身份证计算年龄和性别】由于【已更新】取消执行........[{}]", DateUtils.getCurrentDateTime());
			return;
		}

		List<SuffererInfoPO> valid = all.stream()
				.filter(item -> !StringUtils.isEmpty(item.getSuffererCard()) && IdcardUtil.isValidCard(item.getSuffererCard()))
				.collect(Collectors.toList());

		valid.forEach(item -> {
					String idCard = item.getSuffererCard();
					item.setSuffererAge(Float.valueOf(IdcardUtil.getAgeByIdCard(idCard)));
					item.setSuffererGender(IdcardUtil.getGenderByIdCard(idCard) == 0 ? "女" : "男");
		});


		suffererInfoService.saveAll(valid);
		log.info("定时任务【隔离点疫情人员身份证计算年龄和性别】执行成功........[{}]", DateUtils.getCurrentDateTime());
	}

}
