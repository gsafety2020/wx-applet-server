package com.gsafety.gemp.wxapplet.infection.contract.iface;

import com.gsafety.gemp.wxapplet.infection.dao.po.SiteStaffPO;

/**
 * 
* @ClassName: SiteStaffService 
* @Description: 操作人员服务类 
* @author luoxiao
* @date 2020年3月1日 下午7:16:35 
*
 */
public interface SiteStaffService {

	/**
	 * 
	* @Title: getSiteStaffByOpenId 
	* @Description: 通过OpenId查询系统用户
	* @param @param openId
	* @param @return    设定文件 
	* @return SiteStaffPO    返回类型 
	* @throws
	 */
	public SiteStaffPO getSiteStaffByOpenId(String openId);
	
	/**
	 * 
	* @Title: getSiteStaffById 
	* @Description: 通过主键查询系统用户
	* @param @param id
	* @param @return    设定文件 
	* @return SiteStaffPO    返回类型 
	* @throws
	 */
	public SiteStaffPO getSiteStaffById(String id);
	
	/**
	 * 
	* @Title: saveSiteStaff 
	* @Description: 授权保存
	* @param @param po    设定文件 
	* @return void    返回类型 
	* @throws
	 */
	public void saveSiteStaff(SiteStaffPO po);

}
