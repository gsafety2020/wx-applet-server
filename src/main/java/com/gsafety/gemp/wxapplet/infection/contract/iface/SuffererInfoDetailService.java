package com.gsafety.gemp.wxapplet.infection.contract.iface;

import com.gsafety.gemp.wxapplet.infection.contract.dto.SuffererInfoDetailDTO;

/**
 * @author dusiwei
 */
public interface SuffererInfoDetailService {

    /**
     * 查询患者详情
     * @param idNo @return
     */
    SuffererInfoDetailDTO findSuffererDetail(String idNo);
}
