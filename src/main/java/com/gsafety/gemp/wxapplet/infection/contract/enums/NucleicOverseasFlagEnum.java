package com.gsafety.gemp.wxapplet.infection.contract.enums;

public enum NucleicOverseasFlagEnum {
    TYPE_ALL("0","否"),
    TYPE_LINCHUANG("1","是");

    private String code;
    private String name;
    NucleicOverseasFlagEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static String getNameByCode(String code) {
        for (NucleicOverseasFlagEnum caseTypeEnum : values()) {
            if (caseTypeEnum.code.equals(code)){
                return caseTypeEnum.name;
            }
        }
        return null;
    }
}
