package com.gsafety.gemp.wxapplet.infection.contract.enums;

public enum PointGenderEnum {

    MALE("1","男"),
    FEMALE("2","女");

    private String code;
    private String name;
    PointGenderEnum(String code,String name){
        this.code = code;
        this.name = name;
    }

    public static String getNameByCode(String code) {
        for (PointGenderEnum dataTypeEnum : values()) {
            if (dataTypeEnum.code.equals(code)){
                return dataTypeEnum.name;
            }
        }
        return null;
    }
}
