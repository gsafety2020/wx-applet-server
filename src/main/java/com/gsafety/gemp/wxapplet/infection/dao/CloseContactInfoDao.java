package com.gsafety.gemp.wxapplet.infection.dao;

import com.gsafety.gemp.wxapplet.infection.dao.po.CloseContactInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


/**
 * @author fanlx
 */
public interface CloseContactInfoDao extends JpaRepository<CloseContactInfo, String>, JpaSpecificationExecutor<CloseContactInfo> {

    List<CloseContactInfo> findAllByDataDate(String dataDate);

    int countBySuffererIdno(String suffererIdno);

    int countBySuffererIdnoAndCcIdnoIsNotNull(String suffererIdno);

    List<CloseContactInfo> findBySuffererIdno(String suffererIdno);

    @Query(value = "select t.SUFFERER_IDNO from close_contact_info t where exists(select 1 from cdc_sufferer_info c where c.ID_NO = t.SUFFERER_IDNO) and t.CC_IDNO is not null   group by t.SUFFERER_IDNO order by count(1) desc ",
    countQuery = "select count(1) from close_contact_info t where exists(select 1 from cdc_sufferer_info c where c.ID_NO = t.SUFFERER_IDNO)  and t.CC_IDNO is not null group by t.SUFFERER_IDNO",nativeQuery = true)
    Page<String> findSuperSpreaderList(Pageable pageable);

}
