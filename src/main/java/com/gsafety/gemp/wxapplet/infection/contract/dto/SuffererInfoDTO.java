package com.gsafety.gemp.wxapplet.infection.contract.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.gsafety.gemp.common.excel.ExcelAttribute;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


/**
 * 病患信息表dto
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SuffererInfoDTO {

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "来自那个区编码")
    private String fromAreaCode;

    @ApiModelProperty(value = "转入区地址")
    private String fromAddress;

    @ApiModelProperty(value = "区编码")
    private String areaCode;

    @ApiModelProperty(value = "区名称")
    private String areaName;

    @ApiModelProperty(value = "患者姓名")
    @ExcelAttribute(name="患者姓名")
    private String suffererName;

    @ApiModelProperty(value = "病人类型编码：1.密接人员；2.发热患者；3.疑似患者；4.确诊;5.其他")
    private Integer suffererType;

    @ApiModelProperty(value = "病人类型名称")
    private String suffererTypeName;

    @ApiModelProperty(value = "病人类型改变时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
    private Date suffererTypeTime;

    @ApiModelProperty(value = "病患状态编码：1.隔离点在管（进入）；2.在院治疗（进入）；3.治愈出院（离开）；4.病亡（离开）；5.转出（离开）；6.其他")
    private Integer suffererStatus;

    @ApiModelProperty(value = "病患状态名称")
    private String suffererStatusName;

    @ApiModelProperty(value = "病患状态改变时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd hh:mm")
    private Date suffererStatusTime;

    @ApiModelProperty(value = "病情. 1.治愈；2死亡；3.危重 4.重症 5.其他")
    private Integer illnessState;

    @ApiModelProperty(value = "病情. 1.治愈；2死亡；3.危重 4.重症 5.其他")
    private String illnessStateName;

    @ApiModelProperty(value = "病情情况时间记录")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
    private Date illnessStateTime;

    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd hh:mm")
    @ApiModelProperty(value = "病情时间")
    private Date illnessBeginTime;

    @ApiModelProperty(value = "年龄")
    private Float suffererAge;

    @ApiModelProperty(value = "性别")
    private String suffererGender;

    @ApiModelProperty(value = "身份证号")
    private String suffererCard;

    @ApiModelProperty(value = "隔离者联系电话")
    private String suffererTel;

    @ApiModelProperty(value = "隔离者住址")
    private String suffererAddress;

    @ApiModelProperty(value = "当前所在隔离点或医院ID")
    private String curSiteId;

    @ApiModelProperty(value = "当前位置")
    private String curSiteName;

    @ApiModelProperty(value = "房间号")
    private String roomNumber;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "序号")
    private Integer seqNumber;

    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd hh:mm")
    @ApiModelProperty(value = "入住时间")
    private Date moveIntoTime;

    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd hh:mm")
    @ApiModelProperty(value = "转出时间")
    private Date turnOutTime;

    @ApiModelProperty(value = "转出目标地类型编码。1：隔离点；2.医院；3.自家；4.其他")
    private Integer turnOutToPlaceType;

    @ApiModelProperty(value = "转出目标地类型名称。1：隔离点；2.医院；3.自家；4.其他")
    private String turnOutToPlaceTypeName;

    @ApiModelProperty(value = "转出目标地名称")
    private String turnOutToPlace;

    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd hh:mm")
    @ApiModelProperty(value = "预计解除时间")
    private Date turnOutPlanTime;

    @ApiModelProperty(value = "死亡时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
    private Date deathDate;

}
