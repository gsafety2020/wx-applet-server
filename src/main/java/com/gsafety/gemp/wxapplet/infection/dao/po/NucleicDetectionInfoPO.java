package com.gsafety.gemp.wxapplet.infection.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "nucleic_detection_info", schema = "wx_applet", catalog = "")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NucleicDetectionInfoPO {

    @Id
    @Column(name = "ID")
    private String id;

    /** 编号**/
    @Column(name = "NUMBER")
    private String number;

    /** 委托机构**/
    @Column(name = "ENTRUST_ORG")
    private String entrustOrg;

    /** 隔离点名称**/
    @Column(name = "ISOLATION_POINT")
    private String isolationPoint;

    /** 隔离点类型**/
    @Column(name = "ISOLATION_TYPE")
    private String isolationType;

    /** 患者姓名**/
    @Column(name = "SUFFERER_NAME")
    private String suffererName;

    /** 有效身份证号**/
    @Column(name = "ID_NO")
    private String idNo;

    /** 年龄**/
    @Column(name = "AGE")
    private String age;

    /** 性别**/
    @Column(name = "GENDER")
    private String gender;

    /** 联系电话**/
    @Column(name = "TELPHONE")
    private String telphone;

    /** 患者ID**/
    @Column(name = "SUFFERER_ID")
    private String suffererId;

    /** 病例类型及检测次数**/
    @Column(name = "CASE_TYPE_DETECTION")
    private String caseTypeDetection;

    /** 样本类型**/
    @Column(name = "SAMPLE_TYPE")
    private String sampleType;

    /** 送样日期**/
    @Column(name = "SEND_SAMPLE_TIME")
    private Date sendSampleTime;

    /** 第几次检测**/
    @Column(name = "DETECTION_NUM")
    private String detectionNum;

    /** 检测结果**/
    @Column(name = "DETECTION_RESULT")
    private String detectionResult;

    /** 检查机构**/
    @Column(name = "DETECTION_ORG")
    private String detectionOrg;

    /** 是否境外返（来）汉人员**/
    @Column(name = "OVERSEAS_RETURN_FLAG")
    private String overseasReturnFlag;

    /** 备注**/
    @Column(name = "NOTE")
    private String note;

    /** 创建时间**/
    @Column(name="CREATE_TIME")
    private Date createTime;

    /** 更新时间**/
    @Column(name="UPDATE_TIME")
    private Date updateTime;
}
