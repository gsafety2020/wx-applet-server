package com.gsafety.gemp.wxapplet.healthcode.contract.dto.in;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
* @ClassName: PestHealthCodeDTO 
* @Description: 健康码入参
* @author luoxiao
* @date 2020年2月21日 下午6:48:11 
*
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("健康码保存信息入参")
public class PestHealthCodeReportDTO implements Serializable{
	
	
	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value="小程序sessionKey")
	private String sessionKey;
	
	@ApiModelProperty(value="手机号")
	private String mobile;
	
	@ApiModelProperty(value="用户姓名")
	private String userName;
	
	@ApiModelProperty(value="地址")
	private String address;
	
	@ApiModelProperty(value="身份证")
	private String identityCard;
	
	@ApiModelProperty(value="地区编码")
	private String areaCode;
	
	@ApiModelProperty(value="当前是否在[城市](1:是，0:否)")
	private String currentCity;
	
	@ApiModelProperty(value="您最近14天是否离开过[城市](1:是，0:否)")
	private String fornightLeaveCity;
	
	@ApiModelProperty(value="您所住楼道是否有确诊病人或者密切接触者(1:是，0:否)")
	private String nearHavePatient;
	
	@ApiModelProperty(value="你接触过发热或呼吸道症状病人(1:是，0:否)")
	private String touchPatient;
	
	@ApiModelProperty(value="当前的健康状态是否正常(1:是，0:否)")
	private String normalHealth;
	
	
}
