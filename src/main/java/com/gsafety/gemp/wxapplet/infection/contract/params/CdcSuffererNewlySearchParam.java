package com.gsafety.gemp.wxapplet.infection.contract.params;

import com.gsafety.gemp.wxapplet.infection.contract.common.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description="cdc新增病患查询条件")
public class CdcSuffererNewlySearchParam  extends PageReq implements Serializable {

    @ApiModelProperty(value = "数据日期 yyyy-MM-dd")
    private String dataDate;
    @ApiModelProperty(value = "新增病患类型 1，临床诊断 2，确诊 3，阳性检测 4，疑似 5.死亡 6.转入,7.转出")
    private String type;
    private String sourceType;

}
