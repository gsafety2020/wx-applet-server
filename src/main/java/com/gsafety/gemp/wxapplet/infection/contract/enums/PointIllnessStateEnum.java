package com.gsafety.gemp.wxapplet.infection.contract.enums;

import com.gsafety.gemp.wxapplet.infection.contract.dto.CaseTypeDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.PointCommonDTO;

import java.util.ArrayList;
import java.util.List;

public enum PointIllnessStateEnum {

    ALL("0","全部"),
    SERVERE("1","重症"),
    DANGER("2","危重"),
    OTHER("99","其他");

    private String code;
    private String name;
    PointIllnessStateEnum(String code,String name){
        this.code = code;
        this.name = name;
    }

    public static String getNameByCode(String code) {
        for (PointIllnessStateEnum dataTypeEnum : values()) {
            if (dataTypeEnum.code.equals(code)){
                return dataTypeEnum.name;
            }
        }
        return null;
    }

    public static List<PointCommonDTO> getList(){
        List<PointCommonDTO> list=new ArrayList<>();
        PointCommonDTO pointCommonDTO=null;
        for (PointIllnessStateEnum commonEnum : values()) {
            PointCommonDTO commonDTO=new PointCommonDTO();
            commonDTO.setCode(commonEnum.code);
            commonDTO.setName(commonEnum.name);
            list.add(commonDTO);
        }
        return list;
    }
}
