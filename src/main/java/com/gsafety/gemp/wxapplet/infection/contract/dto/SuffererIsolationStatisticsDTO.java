package com.gsafety.gemp.wxapplet.infection.contract.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SuffererIsolationStatisticsDTO {

    @ApiModelProperty(value = "统计名称")
    private String name;

    @ApiModelProperty(value = "数量")
    private Integer num;
}
