package com.gsafety.gemp.wxapplet.infection.controller;

import com.gsafety.gemp.wxapplet.infection.service.CdcStaticsService;
import com.gsafety.gemp.wxapplet.utils.DateUtils;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.gsafety.gemp.wxapplet.healthcode.contract.dto.Result;
import com.gsafety.gemp.wxapplet.infection.contract.dto.SuffererInfoDTO;
import com.gsafety.gemp.wxapplet.infection.contract.iface.SuffererInfoService;

import lombok.extern.slf4j.Slf4j;

@Api(value = "病患追溯信息cdc统计接口", tags = {"病患追溯信息cdc统计接口"})
@RestController
@RequestMapping("/infection/analysis/cdc")
@Slf4j
public class SuffererCdcAnalysisController {

	@Autowired
	private SuffererInfoService suffererInfoService;

	@Autowired
	private CdcStaticsService cdcStaticsService;

	@ApiOperation(value = "江夏区累计统计数据")
	@PostMapping(value = "/total")
	public Result<?> analysisSuffererStatusTotal(@ApiParam(value = "统计日期") @RequestParam(required = false)  String date){
		Result<?> result = new Result<JSONObject>();
		if (StringUtils.isEmpty(date)) {
			date = DateUtils.getCurrentDateTime(DateUtils.DATE_PATTERN);
		}
		try{
			JSONObject json = cdcStaticsService.analyseCDCSuffererData(date);
			return result.success(json);
		}catch(Exception e){
			log.error("江夏区累计统计数据失败!",e);
			return result.fail(e.getMessage());
		}
	}

	@ApiOperation(value = "江夏区昨日新增数据统计")
	@PostMapping(value = "/newAdd")
	public Result<?> analysisSuffererStatusNewAdd(){
		Result<?> result = new Result<JSONObject>();
		try{
			JSONObject json = suffererInfoService.analyseCDCSuffererToday();
			return result.success(json);
		}catch(Exception e){
			log.error("江夏区昨日新增数据统计失败!",e);
			return result.fail(e.getMessage());
		}
	}

	@ApiOperation(value = "江夏区昨日新增数据状态列表")
	@PostMapping(value = "/list")
	@ApiImplicitParams({
		@ApiImplicitParam(name="type",value="1:传入状态为病情状态,2:传入状态为管制状态，3.传入状态为病人类型",dataType="Integer", paramType = "query"),
		@ApiImplicitParam(name="status",value="若type为1:对应的status值(病情. 1.治愈；3.危重 4.重症 5.其他),若type为2:对应的status(:病患状态：1.隔离点在管（进入）；2.在院治疗（进入）；3.治愈出院（离开）；4.病亡（离开）；5.转出（离开）；6.其他)；，若type为3:对应的status值(病人类型：1.密接人员；2.发热患者；3.疑似患者；4.确诊; 5.其他;)",dataType="Integer", paramType = "query"),
		@ApiImplicitParam(name="page",value="页码 以0开始",dataType="Integer", paramType = "query"),
		@ApiImplicitParam(name="size",value="每页最大数量",dataType="Integer", paramType = "query"),
	})
	public Result<Page<SuffererInfoDTO>> list(Integer type,Integer status,Integer page,Integer size){
		Result<Page<SuffererInfoDTO>> result = new Result<>();
		 if(page == null || page < 0){
	            page = 0;
	     }
	     if(size == null || size < 1){
	         size = 10;
	     }
	     PageRequest pageRequest = PageRequest.of(page, size);
		try{
			Page<SuffererInfoDTO> pagePo = null;

			if(type == 1){
				pagePo = suffererInfoService.getSuffererInfoTodayByIllnessState(status, pageRequest);
			}else if(type == 2){
				pagePo = suffererInfoService.getSuffererInfoTodayBySuffererStatus(status,pageRequest);
			}else if(type == 3){
				pagePo = suffererInfoService.getSuffererInfoTodayBySuffererType(status, pageRequest);
			}else{
				throw new RuntimeException("参数错误!");
			}
			return result.success("查询成功!",pagePo);
		}catch(Exception e){
			log.error("查询江夏区昨日新增数据状态列表失败,",e);
			return result.fail(e.getMessage());
		}
	}
}
