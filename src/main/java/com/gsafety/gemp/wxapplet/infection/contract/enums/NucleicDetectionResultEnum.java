package com.gsafety.gemp.wxapplet.infection.contract.enums;

import com.gsafety.gemp.wxapplet.infection.contract.dto.PointCommonDTO;

import java.util.ArrayList;
import java.util.List;

public enum NucleicDetectionResultEnum {

    DETECTION_RESULT_0("0","全部"),
    DETECTION_RESULT_1("1","阴性"),
    DETECTION_RESULT_2("2","阳性"),
    DETECTION_RESULT_3("3","可疑请结合临床"),
    DETECTION_RESULT_4("4","无结果（检测失败）"),
    DETECTION_RESULT_5("5","无结果（无标本）"),
    DETECTION_RESULT_6("6","无结果（采样不规范）");

    private String code;
    private String name;
    NucleicDetectionResultEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public String getCode(){
        return this.code;
    }

    public static String getNameByCode(String code) {
        for (NucleicDetectionResultEnum dataTypeEnum : values()) {
            if (dataTypeEnum.code.equals(code)){
                return dataTypeEnum.name;
            }
        }
        return null;
    }

    public static List<PointCommonDTO> getList(){
        List<PointCommonDTO> list=new ArrayList<>();
        PointCommonDTO pointCommonDTO=null;
        for (NucleicDetectionResultEnum commonEnum : values()) {
            PointCommonDTO commonDTO=new PointCommonDTO();
            commonDTO.setCode(commonEnum.code);
            commonDTO.setName(commonEnum.name);
            list.add(commonDTO);
        }
        return list;
    }
}
