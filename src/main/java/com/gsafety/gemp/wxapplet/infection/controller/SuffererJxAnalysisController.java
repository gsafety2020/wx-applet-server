package com.gsafety.gemp.wxapplet.infection.controller;

import com.gsafety.gemp.wxapplet.healthcode.contract.dto.Result;
import com.gsafety.gemp.wxapplet.infection.contract.dto.SuffererInfoDTO;
import com.gsafety.gemp.wxapplet.infection.contract.iface.SuffererInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Api(value = "隔离点数据统计分析", tags = {"隔离点数据统计分析"})
@RestController
@RequestMapping("/infection/analysis/jx")
@Slf4j
public class SuffererJxAnalysisController {

	@Autowired
	private SuffererInfoService suffererInfoService;

	@ApiOperation(value = "隔离点数据统计分析")
	@PostMapping(value = "/suffererCensus")
	public Result<?> analysisSuffererStatusTotal(){
		try{
			return new Result().success(suffererInfoService.analyseJxSuffererData());
		}catch(Exception e){
			log.error("隔离点数据统计分析失败!",e);
			return new Result().fail(e.getMessage());
		}
	}

	@ApiOperation(value = "隔离点数据统计分析新增数据统计")
	@PostMapping(value = "/suffererNewCensus")
	@ApiImplicitParam(name="totalDate",value="统计日期",dataType="String", paramType = "query")
	public Result<?> analysisSuffererStatusNewAdd(String totalDate){
		try{
			return new Result().success(suffererInfoService.analyseJxSuffererToday(totalDate));
		}catch(Exception e){
			log.error("隔离点数据统计分析新增数据统计失败!",e);
			return new Result().fail(e.getMessage());
		}
	}

	@ApiOperation(value = "隔离点数据统计数据状态列表")
	@PostMapping(value = "/suffererNewsPage")
	@ApiImplicitParams({
		@ApiImplicitParam(name="type",value="传入病人类型：1—密接人员；2—发热人员",dataType="Integer", paramType = "query"),
		@ApiImplicitParam(name="page",value="页码 以0开始",dataType="Integer", paramType = "query"),
		@ApiImplicitParam(name="size",value="每页最大数量",dataType="Integer", paramType = "query"),
			@ApiImplicitParam(name="totalDate",value="统计日期",dataType="String", paramType = "query")
	})
	public Result<Page<SuffererInfoDTO>> suffererNewsPage(Integer type, Integer page, Integer size,String totalDate){
		 if(page == null || page < 0){
	            page = 0;
	     }
	     if(size == null || size < 1){
	         size = 10;
	     }
		Pageable pageable = PageRequest.of(page, size);
		try{
			Page<SuffererInfoDTO> pagePo = null;
			pagePo = suffererInfoService.getJxSuffererInfoTodayBySuffererType(type, pageable,totalDate);
			return new Result().success("查询成功!",pagePo);
		}catch(Exception e){
			log.error("隔离点数据统计数据状态列表失败,",e);
			return new Result().fail(e.getMessage());
		}
	}

	@ApiOperation(value = "隔离点数据统计数据导出")
	@PostMapping(value = "/export")
	@ApiImplicitParams({
			@ApiImplicitParam(name="totalDate",value="统计日期",dataType="String", paramType = "query")
	})
	public void export(HttpServletResponse response, HttpServletRequest request, String totalDate) throws Exception {
		suffererInfoService.exportStat(response, request,totalDate);
	}

}
