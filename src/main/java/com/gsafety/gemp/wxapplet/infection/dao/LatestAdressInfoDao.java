package com.gsafety.gemp.wxapplet.infection.dao;

import com.gsafety.gemp.wxapplet.infection.dao.po.LatestAdressInfoPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface LatestAdressInfoDao extends
        JpaRepository<LatestAdressInfoPO, String>, JpaSpecificationExecutor<LatestAdressInfoPO> {

//    LatestAdressInfoPO findByIdNo(String idNo);

}
