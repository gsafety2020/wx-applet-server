package com.gsafety.gemp.wxapplet.infection.contract.dto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;



@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CloseContactInfoDTO {
    private String id;
    @ApiModelProperty(value = "患者姓名")
    private String suffererName;
    @ApiModelProperty(value = "患者身份证号")
    private String suffererIdno;
    @ApiModelProperty(value = "密接人员姓名")
    private String ccName;
    @ApiModelProperty(value = "密接人员性别")
    private String ccSex;
    @ApiModelProperty(value = "密接人员年龄")
    private Integer ccAge;
    @ApiModelProperty(value = "与患者关系")
    private String ccRelation;
    @ApiModelProperty(value = "联系电话")
    private String ccTelphone;
    @ApiModelProperty(value = "当前住址")
    private String ccAddress;
    @ApiModelProperty(value = "最后暴露时间")
    private String ccExposeTime;
    @ApiModelProperty(value = "备注")
    private String remark;
    @ApiModelProperty(value = "当前位置")
    private String ccCurLocation;
    @ApiModelProperty(value = "密接人员身份证号")
    private String ccIdno;
    @ApiModelProperty(value = "感染状态 1是 0否")
    private String infectedType;
}
