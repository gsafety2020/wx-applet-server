package com.gsafety.gemp.wxapplet.infection.service;

import java.util.*;
import java.util.stream.Collectors;

import cn.hutool.core.util.ObjectUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.gsafety.gemp.wxapplet.infection.contract.dto.SiteSuffererInfoStatisticDTO;
import com.gsafety.gemp.wxapplet.infection.contract.enums.SuffererTypeEnum;
import com.gsafety.gemp.wxapplet.infection.contract.iface.SiteSuffererInfoService;
import com.gsafety.gemp.wxapplet.infection.dao.SiteInfoDao;
import com.gsafety.gemp.wxapplet.infection.dao.SuffererInfoDao;
import com.gsafety.gemp.wxapplet.infection.dao.po.SiteInfoPO;
import com.gsafety.gemp.wxapplet.infection.dao.po.SuffererInfoPO;
import com.gsafety.gemp.wxapplet.utils.DateUtils;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;

/**
 * 
* @ClassName: SiteSuffererInfoServiceImpl 
* @Description: 隔离点统计实现类
* @author luoxiao
* @date 2020年3月7日 下午10:10:31 
*
 */
@Service("siteSuffererInfoService")
public class SiteSuffererInfoServiceImpl implements SiteSuffererInfoService{

	@Autowired
	private SiteInfoDao siteInfoDao;
	
	@Autowired
	private SuffererInfoDao suffererInfoDao;
	
	@Override
	public Map<String, Object> statisticIsolatedInfo() {
		List<SiteSuffererInfoStatisticDTO> list = new ArrayList<SiteSuffererInfoStatisticDTO>();
		//获取隔离点总数
		SiteSuffererInfoStatisticDTO totalDto = getIsolateTotal();
		list.add(totalDto);

		//根据患者病情获取隔离点信息
		List<SuffererInfoPO> suffererInfos =  suffererInfoDao.findAll();
		Map<Integer,List<SuffererInfoPO>> map = null;
		if(!CollectionUtils.isEmpty(suffererInfos)){
			map = suffererInfos.stream().collect(Collectors.groupingBy(b -> b.getSuffererType()));
		}
		List<SiteSuffererInfoStatisticDTO> typeDto = getStatisticSiteSuffererInfoStatisticDTO(map);
		list.addAll(typeDto);
		
		Map<String, Object> mapResult = new HashMap<String, Object>();
		if(CollectionUtils.isEmpty(suffererInfos)){
			mapResult.put("date", suffererInfos.get(0).getDataDate());
		}
		mapResult.put("list", list);
		return mapResult;
	}
	
	private List<SiteSuffererInfoStatisticDTO> getStatisticSiteSuffererInfoStatisticDTO(Map<Integer,List<SuffererInfoPO>> map){
		List<SiteSuffererInfoStatisticDTO> result = new ArrayList<>();
		SiteSuffererInfoStatisticDTO dto = null;
		for(SuffererTypeEnum type : SuffererTypeEnum.values()){
			List<SuffererInfoPO> list = null;
			if(map != null){
				list = map.get(type.getCode());
			}
			dto = getIsolateInfoBySuffererType(list);
			dto.setName(type.getValue());
			result.add(dto);
		}
		return result;
	}
	
	private SiteSuffererInfoStatisticDTO getIsolateInfoBySuffererType(List<SuffererInfoPO> list){
		SiteSuffererInfoStatisticDTO dto = SiteSuffererInfoStatisticDTO.builder().isolateNum(0).bedNum(0).useBedNum(0).isolatePersonNum(0).noUserBedNum(0).build();
		if(!CollectionUtils.isEmpty(list)){
			//隔离点个数
			Integer isolateNum = list.stream().collect(
												collectingAndThen(
													toCollection(() -> new TreeSet<>(Comparator.comparing(SuffererInfoPO::getCurSiteId))), ArrayList::new)).size();
			//床位总数
			Integer bedNum = list.size();
			//已用床位数
			Integer useBedNum = list.stream().filter(po -> ObjectUtil.isNull(po.getTurnOutTime())).collect(Collectors.toList()).size();
			//隔离人数
			Integer isolatePersonNum = useBedNum;
			//未使用床位数
			Integer noUserBedNum = bedNum - useBedNum;

			dto = SiteSuffererInfoStatisticDTO.builder().isolateNum(isolateNum).bedNum(bedNum).useBedNum(useBedNum).isolatePersonNum(isolatePersonNum).noUserBedNum(noUserBedNum).build();

//			List<String> sites = list.stream().filter(a->StringUtils.isNotEmpty(a.getCurSiteId())).map(SuffererInfoPO::getCurSiteId).collect(Collectors.toList());
//			List<String> curSites = list.stream().filter(a -> StringUtils.isNotEmpty(a.getCurSiteId())&& ObjectUtil.isNull(a.getTurnOutTime())).map(SuffererInfoPO::getCurSiteId).collect(Collectors.toList());
//			List<SiteInfoPO> siteInfos = siteInfoDao.findByIdIn(sites);
//			List<SiteInfoPO> cursiteInfos = siteInfoDao.findByIdIn(curSites);
//			dto = statisticData(list.size(), siteInfos, cursiteInfos);
		}

		return dto;
	}

	
	private SiteSuffererInfoStatisticDTO getIsolateTotal(){
		List<SiteInfoPO> sites = siteInfoDao.findAll();
		Integer isolateNum = sites.size();
		Integer bedNum = sites.stream().filter(a->a.getBedTotal() != null).mapToInt(SiteInfoPO::getBedTotal).sum();
		Integer useBedNum = sites.stream().filter(a->a.getBedUsed() != null).mapToInt(SiteInfoPO::getBedUsed).sum();
		Integer isolatePersonNum = useBedNum;
		Integer noUserBedNum = sites.stream().filter(a->a.getBedFree() != null).mapToInt(SiteInfoPO::getBedFree).sum(); 
		SiteSuffererInfoStatisticDTO dto = SiteSuffererInfoStatisticDTO.builder().name("总计").isolateNum(isolateNum)
				.bedNum(bedNum).useBedNum(useBedNum).isolatePersonNum(isolatePersonNum).noUserBedNum(noUserBedNum).build();
		return dto;
	}
	
	private SiteSuffererInfoStatisticDTO statisticData(Integer personNum,List<SiteInfoPO> sites, List<SiteInfoPO> curSites){
		Integer isolateNum = sites.size();
		Integer bedNum = curSites.stream().filter(a->a.getBedTotal() != null).mapToInt(SiteInfoPO::getBedTotal).sum();
		Integer useBedNum = curSites.stream().filter(a->a.getBedUsed() != null).mapToInt(SiteInfoPO::getBedUsed).sum();
		Integer isolatePersonNum = useBedNum;
		Integer noUserBedNum = bedNum - useBedNum;
		SiteSuffererInfoStatisticDTO dto = SiteSuffererInfoStatisticDTO.builder().isolateNum(isolateNum)
				.bedNum(bedNum).useBedNum(useBedNum).isolatePersonNum(isolatePersonNum).noUserBedNum(noUserBedNum).build();
		return dto;
	}
}
