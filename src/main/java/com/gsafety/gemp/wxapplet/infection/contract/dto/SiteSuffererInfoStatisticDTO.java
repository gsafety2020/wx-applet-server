package com.gsafety.gemp.wxapplet.infection.contract.dto;

import java.io.Serializable;
import java.util.Date;

import com.gsafety.gemp.wxapplet.infection.contract.dto.SuffererAnalysisExportDTO.SuffererAnalysisExportDTOBuilder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
* @ClassName: SiteSuffererInfoStatisticDTO 
* @Description: 隔离点人员统计dto
* @author luoxiao
* @date 2020年3月7日 下午9:59:33 
*
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SiteSuffererInfoStatisticDTO implements Serializable{

	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;
	
	/**统计名称*/
	private String name;
	
	/**隔离点个数*/
	private Integer isolateNum;
	
	/**床位总数*/
	private Integer bedNum;
	
	/**床位数*/
	private Integer useBedNum;
	
	/**隔离人数*/
	private Integer isolatePersonNum;
	
	/**未用床位数*/
	private Integer noUserBedNum;

	
}
