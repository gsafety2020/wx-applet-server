package com.gsafety.gemp.wxapplet.infection.contract.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 
* @ClassName: SuffererTraceStatus 
* @Description: 状态变更记录
* @author luoxiao
* @date 2020年3月1日 下午5:54:21 
*
 */
public class SuffererTraceStatus {

	@Getter
	@AllArgsConstructor
	public enum SiteChangeTypeEnum{
		
		ORIGINAL(0,"原始(信息入库时的状态)"),SHIFT_TO(1,"转入"),ROLL_OUT(2,"转出");
		
		private Integer code;
		
	    private String value;
	}
	
	@Getter
	@AllArgsConstructor
	public enum StatusChangeTypeEnum{
		
		ORIGINAL(0,"原始(信息入库时的状态)"),CHANGED(1,"状态改变");
		
		private Integer code;
		
	    private String value;
	}
	
	@Getter
	@AllArgsConstructor
	public enum TypeChangeTypeEnum{
		
		ORIGINAL(0,"原始(信息入库时的状态)"),CHANGED(1,"状态改变");
		
		private Integer code;
		
	    private String value;
	}
}
