package com.gsafety.gemp.wxapplet.infection.contract.iface;

import com.gsafety.gemp.wxapplet.infection.dao.po.OperateLogPO;

/**
 * @author lzx
 * @version 1.0
 * @date 2020/3/1 19:33
 */
public interface SysLogAspectService {
    /**
     * 新增
     * @param dto
     */
    void save(OperateLogPO dto);

}
