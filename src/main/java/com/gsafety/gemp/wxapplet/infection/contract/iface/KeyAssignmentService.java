package com.gsafety.gemp.wxapplet.infection.contract.iface;


import com.gsafety.gemp.wxapplet.infection.contract.dto.SiteNameDTO;

import java.util.List;

/**
 * @author lzx
 * @version 1.0
 * @date 2020/2/28 1:13
 */
public interface KeyAssignmentService {

    /**
     *
     * 隔离点名称
     * @param
     * @return
     */
    List<SiteNameDTO> findSiteName();

}
