package com.gsafety.gemp.wxapplet.infection.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "site_info")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SiteInfoPO implements Serializable {
    private static final long serialVersionUID = 1896093017964792372L;
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "area_code")
    private String areaCode;

    @Column(name = "area_name")
    private String areaName;

    @Column(name = "site_type")
    private Integer siteType;

    @Column(name = "site_address")
    private String siteAddress;

    @Column(name = "site_name")
    private String siteName;

    @Column(name = "observation_type")
    private Integer observationType;

    @Column(name = "isolation_type")
    private Integer isolationType;

    @Column(name = "site_contacts_name")
    private String siteContactsName;

    @Column(name = "site_contacts_tel")
    private String siteContactsTel;

    @Column(name = "open_date_plan")
    private java.sql.Date openDatePlan;

    @Column(name = "open_date")
    private java.sql.Date openDate;

    @Column(name = "doctor_number")
    private Integer doctorNumber;

    @Column(name = "nurse_number")
    private Integer nurseNumber;

    @Column(name = "social_worker_number")
    private Integer socialWorkerNumber;

    @Column(name = "security_staff_number")
    private Integer securityStaffNumber;

    @Column(name = "bed_total")
    private Integer bedTotal;

    @Column(name = "bed_used")
    private Integer bedUsed;

    @Column(name = "bed_free")
    private Integer bedFree;

    @Column(name = "remark")
    private String remark;

    @Column(name = "create_by")
    private String createBy;

    @Column(name = "update_by")
    private String updateBy;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;
}
