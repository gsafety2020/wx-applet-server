package com.gsafety.gemp.wxapplet.infection.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gsafety.gemp.wxapplet.healthcode.contract.dto.Result;
import com.gsafety.gemp.wxapplet.infection.contract.iface.SiteSuffererInfoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Api(value = "隔离点统计接口(统计局)", tags = {"隔离点统计接口(统计局)"})
@RestController
@RequestMapping("/sufferer/site/tjj")
@Slf4j
public class SiteSuffererInfoStatisticController {

	@Autowired
	private SiteSuffererInfoService siteSuffererInfoService;
	
	@ApiOperation(value = "按病情类型隔离点信息统计")
	@PostMapping(value = "/isolate")
	public Result<Map<String,Object>> statisticBedData(){
		Result<Map<String,Object>> result = new Result<>();
		try{
			Map<String,Object> map = siteSuffererInfoService.statisticIsolatedInfo();
			return result.success(map);
		}catch(Exception e){
			log.error("按病情类型隔离点信息统计失败[{}]",e.getMessage());
			return result.success("统计失败!");
		}
	}
}
