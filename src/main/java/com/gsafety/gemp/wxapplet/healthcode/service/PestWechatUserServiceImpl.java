package com.gsafety.gemp.wxapplet.healthcode.service;

import java.util.Optional;

import com.gsafety.gemp.wxapplet.healthcode.contract.dto.PestWechatUserDTO;
import com.gsafety.gemp.wxapplet.healthcode.contract.params.DimQueryParam;
import com.gsafety.gemp.wxapplet.healthcode.dao.po.PestHealthCodePO;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.gsafety.gemp.wxapplet.healthcode.constant.PestWechatUserStatus;
import com.gsafety.gemp.wxapplet.healthcode.contract.dto.PestWechatUserDTO;
import com.gsafety.gemp.wxapplet.healthcode.contract.service.PestWechatUserService;
import com.gsafety.gemp.wxapplet.healthcode.controller.WechatAuthController.WechatUserinfo;
import com.gsafety.gemp.wxapplet.healthcode.dao.PestWechatUserDao;
import com.gsafety.gemp.wxapplet.healthcode.dao.po.PestWechatUserPO;

import lombok.extern.slf4j.Slf4j;


import static com.gsafety.gemp.wxapplet.healthcode.specification.PestWechatUserSpecifications.*;
import static com.gsafety.gemp.wxapplet.healthcode.wrapper.PestWechatUserWrapper.*;


/**
 * 
* @ClassName: PestWechatUserService 
* @Description: 微信用户服务层
* @author luoxiao
* @date 2020年2月26日 上午12:04:22 
*
 */
@EnableAsync
@Service("pestWechatUserService")
@Slf4j
public class PestWechatUserServiceImpl implements PestWechatUserService{

	@Autowired
	private PestWechatUserDao pestWechatUserDao;
	
	@Override
	public void saveUpdate(PestWechatUserPO po) {
		vaildProperty(po);
		Optional<PestWechatUserPO> optional = pestWechatUserDao.findById(po.getOpenId());
		if(!optional.isPresent()){
			pestWechatUserDao.saveAndFlush(po);
		}
	}
	
	@Override
	public void authorizeUser(PestWechatUserPO po,WechatUserinfo userInfo) {
		if(po == null){
			throw new RuntimeException("用户登陆失败!");
		}
		if(StringUtils.isEmpty(po.getOpenId())){
			throw new RuntimeException("主键为空!");
		}
		po.setWithCredentials(PestWechatUserStatus.AuthorizeEnum.AUTORIZED.getCode());
		po.setNickName(userInfo.getNickName());
		po.setGender(userInfo.getGender());
		po.setCity(userInfo.getCity());
		po.setProvince(userInfo.getProvince());
		po.setCountry(userInfo.getCountry());
		po.setAvatarUrl(userInfo.getAvatarUrl());
		po.setUnionId(userInfo.getUnionId());
		pestWechatUserDao.saveAndFlush(po);
	}
	
	@Override
	public PestWechatUserPO getPestWechatUserById(String openId) {
		if(StringUtils.isEmpty(openId)){
			throw new RuntimeException("主键为空!");
		}
		return pestWechatUserDao.getOne(openId);
	}

	@Override
	public Page<PestWechatUserDTO> findWechatInfoPage(DimQueryParam queryParam, Pageable pageable) {
		Page<PestWechatUserPO> page = pestWechatUserDao.findAll(nickNameLike(queryParam.getKeywords()), pageable);
		return toPageDTO(page);
	}

	public void vaildProperty(PestWechatUserPO po){
		if(po == null){
			throw new RuntimeException("用户登陆失败!");
		}
		if(StringUtils.isEmpty(po.getOpenId())){
			throw new RuntimeException("主键为空!");
		}
		if(StringUtils.isEmpty(po.getGender())){
			throw new RuntimeException("性别为空!");
		}
		if(StringUtils.isEmpty(po.getWithCredentials())){
			throw new RuntimeException("授权为空!");
		}
		if(po.getFirstAccessTime() == null){
			throw new RuntimeException("首次访问时间为空!");
		}
		if(po.getUpdateTime() == null){
			throw new RuntimeException("最后更新时间为空!");
		}
		if(StringUtils.isEmpty(po.getAppid())){
			throw new RuntimeException("appid为空!");
		}
	}

	@Override
	@Async
	@Transactional(rollbackOn = Exception.class)
	public void recordLogin(PestWechatUserDTO dto) {
		try {
			// 按openid查询用户是否存在
			Optional<PestWechatUserPO> optional = this.pestWechatUserDao.findById(dto.getOpenId());
			/*
			 * 1. 存在则结束
			 *
			 * 2. 若不存在则新增
			 */
			if (!optional.isPresent()) {
				PestWechatUserPO po = new PestWechatUserPO();
				po.setOpenId(dto.getOpenId());
				po.setGender(dto.getGender());
				po.setWithCredentials(dto.getWithCredentials());
				po.setFirstAccessTime(dto.getFirstAccessTime());
				po.setUpdateTime(dto.getUpdateTime());
				po.setAppid(dto.getAppid());
				this.pestWechatUserDao.save(po);
				log.info("记录用户login[{}]成功", dto);
			}
		} catch (Exception e) {
			log.error("记录用户login[{}]失败", dto, e);
		}
	}



}
