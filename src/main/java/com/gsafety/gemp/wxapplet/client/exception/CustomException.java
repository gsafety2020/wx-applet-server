/**
 * 
 */
package com.gsafety.gemp.wxapplet.client.exception;

/**
 * @author yangbo
 *
 */
public class CustomException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	private final int errcode;

	/**
	 * 
	 */
	private final String errmsg;

	/**
	 * 
	 * @param errcode
	 * @param errmsg
	 */
	public CustomException(int errcode, String errmsg) {
		super("CustomException[" + errcode + "][" + errmsg + "]");
		this.errcode = errcode;
		this.errmsg = errmsg;
	}

	/**
	 * @return the errcode
	 */
	public int getErrcode() {
		return errcode;
	}

	/**
	 * @return the errmsg
	 */
	public String getErrmsg() {
		return errmsg;
	}

	@Override
	public String toString() {
		return "CustomException [errcode=" + errcode + ", errmsg=" + errmsg + ", getMessage()=" + getMessage() + "]";
	}

}