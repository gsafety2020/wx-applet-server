/**
 * 
 */
package com.gsafety.gemp.wxapplet.client;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * @author yangbo
 * 
 */
public class ClientFactory {

	/**
	 * 
	 */
	private static final String DEFAULT_CHARSET = "utf-8";

	/**
	 * 
	 */
	private static ClientFactory unqiue;

	/**
	 * 
	 */
	private RestTemplate restTemplate;

	/**
	 * 
	 */
	static {
		unqiue = new ClientFactory();
	}

	/**
	 * 
	 */
	private ClientFactory() {
		super();
		init();
	}

	/**
	 * 
	 */
	protected void init() {
		List<HttpMessageConverter<?>> converters = new ArrayList<>();
		StringHttpMessageConverter converter = new StringHttpMessageConverter(Charset.forName(DEFAULT_CHARSET));
		converters.add(converter);
		AllEncompassingFormHttpMessageConverter allEncompassingFormHttpMessageConverter = new AllEncompassingFormHttpMessageConverter();
		allEncompassingFormHttpMessageConverter.setCharset(Charset.forName(DEFAULT_CHARSET));
		converters.add(allEncompassingFormHttpMessageConverter);

		this.restTemplate = new RestTemplate(converters);
	}

	/**
	 * 
	 * @return
	 */
	public static ClientFactory defaultClient() {
		return unqiue;
	}

	/**
	 * @return the restTemplate
	 */
	public RestTemplate getRestTemplate() {
		return restTemplate;
	}

}