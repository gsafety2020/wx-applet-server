package com.gsafety.gemp.wxapplet.infection.dao;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import com.gsafety.gemp.wxapplet.infection.dao.po.SuffererInfoPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gsafety.gemp.wxapplet.infection.dao.po.SuffererInfoAllPO;

/**
 * 
* @ClassName: SuffererInfoAllDao 
* @Description: 统计局缓则DAO
* @author luoxiao
* @date 2020年3月5日 下午6:54:30 
*
 */
public interface SuffererInfoAllDao extends JpaRepository<SuffererInfoAllPO, String>,
JpaSpecificationExecutor<SuffererInfoAllPO>{

	List<SuffererInfoAllPO> findByDataDate(String dataDate);
	
	SuffererInfoAllPO findFirstByOrderByDataDateDesc();
	
	List<SuffererInfoAllPO> findByCreateTimeBetween(Date beginTime,Date endTime);
	
	@Transactional
	void deleteByDataDate(String dataDate);

	List<SuffererInfoAllPO> findBySuffererTypeAndDataDate(Integer suffererType,String date);

	List<SuffererInfoPO> findByTurnOutTime(Date date);
}
