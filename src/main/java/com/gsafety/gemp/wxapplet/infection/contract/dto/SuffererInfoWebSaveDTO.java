package com.gsafety.gemp.wxapplet.infection.contract.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
* @ClassName: SuffererInfoSaveDTO 
* @Description: 患者信息保存
* @author luoxiao
* @date 2020年3月1日 下午4:52:38 
*
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("患者信息Web端保存DTO")
public class SuffererInfoWebSaveDTO implements Serializable{

	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "转入区编码")
	private String fromAreaCode;
	
	@ApiModelProperty(value = "区编码")
	private String areaCode;
	
	@ApiModelProperty(value = "区名称")
	private String areaName;
	
	@ApiModelProperty(value = "转入区地址")
	private String fromAddress;
	
	@ApiModelProperty(value = "隔离者，患者姓名")
	private String suffererName;
	
	@ApiModelProperty(value = "病人类型：1.密接人员；2.发热患者；3.疑似患者；4.确诊; 5.其他;")
	private Integer suffererType;
	
	@JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd hh:mm")
	@ApiModelProperty(value = "病人类型变更时间转出时间(时间格式yyyy-MM-dd hh:mm)")
	private Date suffererTypeTime;
	
	@ApiModelProperty(value = "病患状态：1.隔离点在管（进入）；2.在院治疗（进入）；3.治愈出院（离开）；4.病亡（离开）；5.转出（离开）；6.其他；")
	private Integer suffererStatus;
	
	@JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd hh:mm")
	@ApiModelProperty(value = "病患状态改变时间转出时间(时间格式yyyy-MM-dd hh:mm)")
	private Date suffererStatusTime;
	
	@ApiModelProperty(value = "病情. 1.治愈；2死亡；3.危重 4.重症 5.其他")
	private Integer illnessState;
	
	@ApiModelProperty(value = "年龄")
	private Float suffererAge;
	
	@ApiModelProperty(value = "性别")
	private String suffererGender;
	
	@ApiModelProperty(value = "身份证")
	private String suffererCard;
	
	@ApiModelProperty(value = "隔离者联系电话")
	private String suffererTel;
	
	@ApiModelProperty(value = "隔离者住址")
	private String suffererAddress;
	
	@ApiModelProperty(value = "当前所在隔离点或医Id")
	private String curSiteId;
	
	@ApiModelProperty(value = "当前所在隔离点或医院名称")
	private String curSiteName;
	
	@ApiModelProperty(value = "房号")
	private String roomNumber;
	
	@ApiModelProperty(value = "入住时间(格式yyyy-MM-dd hh:mm)")
	@JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd hh:mm")
	private Date moveIntoTime;
	
	@ApiModelProperty(value = "备注")
	private String remark;
	
	@JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "预计解除时间(格式yyyy-MM-dd)")
	private Date turnOutPlanTime;

}
