package com.gsafety.gemp.wxapplet.healthcode.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.gsafety.gemp.wxapplet.healthcode.contract.dto.PestUserAuthorizeDTO;
import com.gsafety.gemp.wxapplet.healthcode.dao.po.PestUserAuthorizePO;


/**
 * 
* @Description: 用户权限 po 与 dto互转
* @author xuelq
* @date 2020年2月27日 下午4:10:58 
*
 */
public class UserAuthorizeWrapper {
	
	public static PestUserAuthorizePO dtoToPo(PestUserAuthorizeDTO dto){
		PestUserAuthorizePO po = new PestUserAuthorizePO();
		BeanUtils.copyProperties(dto, po);
		return po;
	}
	
	public static PestUserAuthorizeDTO poToDto(PestUserAuthorizePO po){
		PestUserAuthorizeDTO dto = new PestUserAuthorizeDTO();
		BeanUtils.copyProperties(po, dto);
		return dto;
	}

	public static List<PestUserAuthorizeDTO> listPoToDto(List<PestUserAuthorizePO> pos){
		List<PestUserAuthorizeDTO> dtos = new ArrayList<PestUserAuthorizeDTO>();
		for(PestUserAuthorizePO po : pos){
			PestUserAuthorizeDTO dto = poToDto(po);
			dtos.add(dto);
		}
		return dtos;
	}
}
