package com.gsafety.gemp.wxapplet.infection.controller;

import cn.hutool.core.util.ObjectUtil;
import com.gsafety.gemp.wxapplet.healthcode.contract.dto.Result;
import com.gsafety.gemp.wxapplet.infection.contract.dto.PointCommonDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.PointHospitalInfoPageDTO;
import com.gsafety.gemp.wxapplet.infection.contract.enums.PointIllnessStateEnum;
import com.gsafety.gemp.wxapplet.infection.contract.enums.PointSuffererStatusEnum;
import com.gsafety.gemp.wxapplet.infection.contract.enums.PointSuffererTypeEnum;
import com.gsafety.gemp.wxapplet.infection.contract.iface.PointHospitalInfoService;
import com.gsafety.gemp.wxapplet.infection.dao.po.PointHospitalInfoPO;
import com.gsafety.gemp.wxapplet.infection.wrapper.PointHospitalInfoWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(value = "定点医院信息接口", tags = {"定点医院信息接口"})
@RestController
@RequestMapping("/infection/point/hospital")
@Slf4j
public class PointHospitalInfoController {

    @Autowired
    private PointHospitalInfoService pointHospitalInfoService;

    @ApiOperation(value="定点医院信息分页查询接口")
    @PostMapping("/list")
    public Result list(@RequestBody PointHospitalInfoPageDTO dto){
        if(dto.getPage() == null || dto.getPage() <= 0){
            dto.setPage(0);
        }else{
            dto.setPage(dto.getPage() - 1);
        }

        if(dto.getSize() == null || dto.getSize() < 1){
            dto.setSize(10);
        }
        Sort sort = Sort.by(Sort.Direction.DESC, "updateTime");
        PageRequest pageRequest = PageRequest.of(dto.getPage(), dto.getSize(),sort);
        return new Result().success(pointHospitalInfoService.search(dto,pageRequest));
    }

    @ApiOperation(value="定点医院信息详情接口")
    @GetMapping("/detail")
    public Result sufferDetail(@ApiParam(value = "患者ID") @RequestParam String id){
        PointHospitalInfoPO po = pointHospitalInfoService.findOne(id);
        if (ObjectUtil.isNull(po)) {
            return new Result().fail("数据查找失败");
        }

        return new Result().success(PointHospitalInfoWrapper.toDTO(po));
    }

    /**
     *
     * @param
     * @return
     */
    @ApiOperation(value = "类型下拉框")
    @GetMapping(value = "/findPointSuffererType")
    public Result findPointSuffererType(){
        List<PointCommonDTO> dto= PointSuffererTypeEnum.getList();
        return new Result().success(dto);
    }

    /**
     *
     * @param
     * @return
     */
    @ApiOperation(value = "病情下拉框")
    @GetMapping(value = "/findPointIllnessState")
    public Result findPointIllnessState(){
        List<PointCommonDTO> dto= PointIllnessStateEnum.getList();
        return new Result().success(dto);
    }

    /**
     *
     * @param
     * @return
     */
    @ApiOperation(value = "状态下拉框")
    @GetMapping(value = "/findPointSuffererStatus")
    public Result findPointSuffererStatus(){
        List<PointCommonDTO> dto= PointSuffererStatusEnum.getList();
        return new Result().success(dto);
    }

}
