package com.gsafety.gemp.wxapplet.infection.contract.dto;


import com.gsafety.gemp.common.excel.ExcelAttribute;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CdcSuffererTracesDTO {

    @ApiModelProperty("收治医院")
    @ExcelAttribute(name = "医疗/隔离点名称")
    private String hospitalName;
    @ApiModelProperty("变更类型")
    @ExcelAttribute(name = "收治/类型/状态/病情")
    private String changeType;
    @ApiModelProperty("变更描述")
    @ExcelAttribute(name = "数据")
    private String changeInfo;
    @ApiModelProperty("备注")
    private String remark;
    @ApiModelProperty("变更时间")
    @ExcelAttribute(name = "日期时间")
    private String changeTime;

}
