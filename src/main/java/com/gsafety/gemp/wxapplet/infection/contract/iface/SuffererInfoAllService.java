package com.gsafety.gemp.wxapplet.infection.contract.iface;

/**
 * 
* @ClassName: SuffererInfoAllService 
* @Description: 统计局患者服务接口
* @author luoxiao
* @date 2020年3月5日 下午6:56:49 
*
 */
public interface SuffererInfoAllService {

	void  bakYesterdaySuffererInfoData();
}
