package com.gsafety.gemp.wxapplet.healthcode.controller;


import com.gsafety.gemp.wxapplet.healthcode.constant.AppConstant;
import com.gsafety.gemp.wxapplet.healthcode.contract.dto.PestHealthCodeDTO;
import com.gsafety.gemp.wxapplet.healthcode.contract.dto.Result;
import com.gsafety.gemp.wxapplet.healthcode.contract.params.DimQueryParam;
import com.gsafety.gemp.wxapplet.healthcode.contract.service.PestHealthCodeService;
import com.gsafety.gemp.wxapplet.healthcode.contract.service.PestWechatUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 用户微信相关信息
 * @author wangwenhai
 * @date 2020-02-25 15:47
 * @Description
 */
@Api(value = "用户微信相关信息", tags = {"用户微信相关信息"})
@RestController
@RequestMapping("/wuhan/ncov/wechat")
public class WechatDataController {


    @Resource
    private PestHealthCodeService pestHealthCodeService;

    @Resource
    private PestWechatUserService pestWechatUserService;

    /**
     * 健康码列表
     * @author wangwenhai
     * @since 2020年2月25日 上午11:39:08
     * @return
     */
    @ApiOperation(value = "健康码列表")
    @PostMapping("/healthCodePage")
    public Result healthCodePage(@RequestBody DimQueryParam queryParam) {
        Sort sort = Sort.by(Sort.Direction.ASC, "healthCodeId");
        Pageable pageable = PageRequest.of( null == queryParam.getCurrentPage() ? AppConstant.DEFAULT_CURRENT_PAGE : queryParam.getCurrentPage() - 1,
                null == queryParam.getPageSize() ? AppConstant.DEAULT_PAGE_SIZE : queryParam.getPageSize(),
                sort);

        return new Result().success(pestHealthCodeService.findHealthCodePage(queryParam, pageable));
    }

    /**
     * 更新健康码
     * @author wangwenhai
     * @since 2020年2月26日 下午9:02:08
     * @return
     */
    @ApiOperation(value = "更新健康码")
    @PostMapping("/updateHealthCode")
    public Result updateLocationDim(@RequestBody PestHealthCodeDTO dto) {
        pestHealthCodeService.save(dto);

        return new Result().success("更新成功");
    }


    /**
     * 微信用户信息查询模块
     * @author wangwenhai
     * @since 2020年2月27日 下午3:20:08
     * @return
     */
    @ApiOperation(value = "微信用户信息查询模块")
    @PostMapping("/wechatInfoPage")
    public Result wechatInfoPage(@RequestBody DimQueryParam queryParam) {
        Sort sort = Sort.by(Sort.Direction.ASC, "openId");
        Pageable pageable = PageRequest.of( null == queryParam.getCurrentPage() ? AppConstant.DEFAULT_CURRENT_PAGE : queryParam.getCurrentPage() - 1,
                null == queryParam.getPageSize() ? AppConstant.DEAULT_PAGE_SIZE : queryParam.getPageSize(),
                sort);

        return new Result().success(pestWechatUserService.findWechatInfoPage(queryParam, pageable));
    }

}
