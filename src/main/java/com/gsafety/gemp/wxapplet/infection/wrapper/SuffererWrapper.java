package com.gsafety.gemp.wxapplet.infection.wrapper;

import cn.hutool.core.util.ObjectUtil;
import com.gsafety.gemp.wxapplet.infection.contract.dto.SuffererAnalysisExportDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.SuffererInfoDTO;
import com.gsafety.gemp.wxapplet.infection.contract.enums.*;
import com.gsafety.gemp.wxapplet.infection.dao.po.SuffererInfoPO;
import com.gsafety.gemp.wxapplet.utils.DateUtils;
import com.gsafety.gemp.wxapplet.utils.EnumUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.ArrayList;
import java.util.List;


public class SuffererWrapper {

	public static SuffererInfoDTO toDTO(SuffererInfoPO po) {
		SuffererInfoDTO dto = new SuffererInfoDTO();
		BeanUtils.copyProperties(po, dto);

		if(ObjectUtil.isNotNull(po.getSuffererType())) {
			dto.setSuffererTypeName(SuffererTypeEnum.getValueByCode(po.getSuffererType()));
		}

		if(ObjectUtil.isNotNull(po.getSuffererStatus())) {
			SuffererStatusEnum ssEnum = EnumUtils.getEnumByIntegerCode(po.getSuffererStatus(), SuffererStatusEnum.class);
			dto.setSuffererStatusName(ObjectUtil.isNotNull(ssEnum) ? ssEnum.getValue() : "");
		}

		if(ObjectUtil.isNotNull(po.getIllnessState())) {
			IllnessStateEnum illEnum = EnumUtils.getEnumByIntegerCode(po.getIllnessState(), IllnessStateEnum.class);
			dto.setIllnessStateName(ObjectUtil.isNotNull(illEnum) ? illEnum.getValue() : "");
		}

		if(ObjectUtil.isNotNull(po.getTurnOutToPlaceType())) {
			DestinationTypeEnum dtEnum = EnumUtils.getEnumByCode(po.getTurnOutToPlaceType(), DestinationTypeEnum.class);
			dto.setTurnOutToPlaceTypeName(ObjectUtil.isNotNull(dtEnum) ? dtEnum.getName() : "");
		}

		return dto;
	}

	public static SuffererInfoPO toPO(SuffererInfoDTO dto) {
		SuffererInfoPO po = new SuffererInfoPO();
		BeanUtils.copyProperties(dto,po);
		return po;
	}


	public static List<SuffererInfoDTO> toDTOList(List<SuffererInfoPO> records) {
		List<SuffererInfoDTO> list = new ArrayList<>();
		records.stream().forEach(record -> list.add(toDTO(record)));
		return list;
	}


	public static Page<SuffererInfoDTO> toPageDTO(Page<SuffererInfoPO> page){
		List<SuffererInfoDTO> content = toDTOList(page.getContent());
		return new PageImpl<>(content, page.getPageable(), page.getTotalElements());
	}

	public static List<SuffererAnalysisExportDTO> toAnalysisExportDTOList(List<SuffererInfoPO> poList) {
		List<SuffererAnalysisExportDTO> dtoList = new ArrayList<>();
		poList.stream().forEach(po -> {
			SuffererAnalysisExportDTO dto = new SuffererAnalysisExportDTO();
			BeanUtils.copyProperties(po,dto);
			if(ObjectUtil.isNotNull(po.getSuffererStatus())) {
				SuffererStatusEnum ssEnum = EnumUtils.getEnumByIntegerCode(po.getSuffererStatus(), SuffererStatusEnum.class);
				dto.setSuffererStatusName(ObjectUtil.isNotNull(ssEnum) ? ssEnum.getValue() : "");
			}
			if(ObjectUtil.isNotNull(po.getMoveIntoTime())) {
				dto.setMoveIntoTimeStr(DateUtils.format(po.getMoveIntoTime(),"yyyy-MM-dd hh:mm"));
			}
			dtoList.add(dto);
		});
		return dtoList;
	}
}
