package com.gsafety.gemp.wxapplet.healthcode.dao.po;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
* @ClassName: PestHealthCodePO 
* @Description: 健康码PO类
* @author luoxiao
* @date 2020年2月21日 下午12:22:15 
*
 */
@Entity
@Table(name = "pest_health_code")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PestHealthCodePO implements Serializable{

	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "health_code_id")
	/**主键UUID*/
	private String healthCodeId;
	
	@Column(name = "open_id")
	/**小程序openId*/
	private String openId;
	
	@Column(name = "image_url")
	/**微信小程序url*/
	private String imageUrl;
	
	@Column(name = "nick_name")
	/**简称*/
	private String nickName;
	
	@Column(name = "user_name")
	/**用户姓名*/
	private String userName;
	
	@Column(name = "mobile")
	/**手机号*/
	private String mobile;
	
	@Column(name = "address")
	/**地址*/
	private String address;
	
	@Column(name = "identity_card")
	/**身份证*/
	private String identityCard;
	
	@Column(name = "province_code")
	/**省编码*/
	private String provinceCode;
	
	@Column(name = "area_code")
	/**地区编码*/
	private String areaCode;
	
	@Column(name = "parent_code")
	/**地区父节点编码*/
	private String parentCode;
	
	@Column(name = "area_name")
	/**地区名称*/
	private String areaName;
	
	@Column(name = "province_name")
	/**省名称*/
	private String provinceName;
	
	@Column(name = "current_city")
	/**当前是否在[城市](1:是，0:否)*/
	private String currentCity;
	
	@Column(name = "fortnight_leave_city")
	/**您最近14天是否离开过[城市](1:是，0:否)*/
	private String fornightLeaveCity;
	
	@Column(name = "near_have_patient")
	/**您所住楼道是否有确诊病人或者密切接触者(1:是，0:否)*/
	private String nearHavePatient;
	
	@Column(name = "touch_patient")
	/**你接触过发热或呼吸道症状病人(1:是，0:否)*/
	private String touchPatient;
	
	@Column(name = "normal_health")
	/**当前的健康状态是否正常(1:是，0:否)*/
	private String normalHealth;
	
	@Column(name = "health_code_color")
	/**健康码颜色(后台自动生成,red：红色，green：绿色，yellow：黄色)*/
	private String healthCodeColor;
	
	@Column(name = "health_code_url")
	/**二维码路径*/
	private String healthCodeUrl;
	
	@Column(name = "score")
	/**总分*/
	private Integer score;
	
	@Column(name = "is_pest")
	/**是否疫区(0:否，1：是)*/
	private String isPest;
	
	@Column(name = "create_time")
	/**创建时间*/
	private Date createTime;
	
	@Column(name = "update_time")
	/**更新时间*/
	private Date updateTime;
	
	@Column(name = "image_file_path")
	/**二维码本地存储路径*/
	private String imageFilePath;
	
	@Column(name = "qr_code_text")
	/**二维码内容*/
	private String qrCodeText;
	
	@Column(name = "is_delete")
	/**逻辑删除标识(0:未删除 1:已删除)*/
	private String isDelete;

}
