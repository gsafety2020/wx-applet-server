package com.gsafety.gemp.wxapplet.infection.dao;

import com.gsafety.gemp.wxapplet.infection.dao.po.CdcSuffererTrace;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface CdcSuffererTraceDao extends JpaRepository<CdcSuffererTrace, String>, JpaSpecificationExecutor<CdcSuffererTrace> {


    List<CdcSuffererTrace> findAllBySuffererIdNoOrderByChangeTimeAscChangeTypeDesc(String suffererIdNo);

    List<CdcSuffererTrace> findAllBySuffererIdNoOrderByChangeTimeDescChangeTypeAsc(String suffererIdNo);

    Integer countCdcSuffererTraceByChangeTimeAndChangeType(String changeTime,String changeType);


    int countBySuffererIdNoAndSourceType(String suffererIdNo,String sourceType);
    @Transactional
    void deleteBySourceTypeAndSuffererIdNo(String sourceType,String suffererIdno);
    @Transactional
    void deleteByRemarkAndSourceType(String remark,String sourceType);
    @Transactional
    void deleteBySourceTypeAndSuffererIdNoAndHospitalId(String sourceType,String suffererIdno,String hospital);

    @Transactional
    void deleteAllBySourceTypeAndSuffererIdNoAndHospitalNameAndChangeTimeAndChangeType(String sourceType,String suffererIdno,String hospital,String changeTime,String changeType);

    @Query(value = "select count(*) from CdcSuffererTrace t where t.changeTime like :changeTime% and t.changeType=:changeType and t.sourceType='3'")
    int queryCountByChangeTimeAndChangeType(@Param("changeTime") String changeTime, @Param("changeType") String changeType);
    @Transactional
    void deleteByChangeTimeAndSourceTypeAndSuffererIdNo(String changeTime,String sourceType,String suffererIdNo);

    @Transactional
    void deleteByChangeTypeAndSuffererIdNo(String changeType,String suffererIdNo);

    int countBySuffererIdNoAndChangeTypeAndSourceType(String suffererIdNo,String changeType,String sourceType);


}
