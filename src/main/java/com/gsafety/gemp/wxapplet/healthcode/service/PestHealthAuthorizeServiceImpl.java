package com.gsafety.gemp.wxapplet.healthcode.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.gsafety.gemp.wxapplet.healthcode.constant.PestHealthAuthorizeStatus;
import com.gsafety.gemp.wxapplet.healthcode.contract.dto.PestHealthAuthorizeDTO;
import com.gsafety.gemp.wxapplet.healthcode.contract.service.PestHealthAuthorizeService;
import com.gsafety.gemp.wxapplet.healthcode.dao.PestHealthAuthorizeDao;
import com.gsafety.gemp.wxapplet.healthcode.dao.po.PestHealthAuthorizePO;
import com.gsafety.gemp.wxapplet.healthcode.wrapper.PestHealthAuthorizeWrapper;

/**
 * 
* @ClassName: PestHealthAuthorizeServiceImpl 
* @Description:  扫码授权服务实现层
* @author luoxiao
* @date 2020年2月26日 下午5:26:32 
*
 */
@Service("pestHealthAuthorizeService")
public class PestHealthAuthorizeServiceImpl implements PestHealthAuthorizeService{

	@Autowired
	private PestHealthAuthorizeDao pestHealthAuthorizeDao;
	
	@Override
	public PestHealthAuthorizeDTO getAuthorizeInfo(String deviceId) {
		if(StringUtils.isEmpty(deviceId)){
			throw new RuntimeException("设备Id为空!");
		}
		PestHealthAuthorizePO po = pestHealthAuthorizeDao.findByDeviceIdAndIsDelete(deviceId, PestHealthAuthorizeStatus.DeleteEnum.NOT_DELETE.getCode());
		if(po == null){
			return null;
		}
		PestHealthAuthorizeDTO dto = new PestHealthAuthorizeDTO();
		dto = PestHealthAuthorizeWrapper.poToDto(po);
		return dto;
	}

}
