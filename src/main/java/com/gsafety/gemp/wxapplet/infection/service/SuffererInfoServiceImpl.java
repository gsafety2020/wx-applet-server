package com.gsafety.gemp.wxapplet.infection.service;

import com.alibaba.fastjson.JSONObject;
import com.gsafety.gemp.common.excel.ExcelExecutor;
import com.gsafety.gemp.common.excel.ExcelResult;
import com.gsafety.gemp.common.excel.ExcelUtils;
import com.gsafety.gemp.common.result.Result;
import com.gsafety.gemp.wxapplet.healthcode.contract.dto.CodeBasDistrictDTO;
import com.gsafety.gemp.wxapplet.healthcode.controller.WechatAuthController.WechatUserinfo;
import com.gsafety.gemp.wxapplet.healthcode.dao.CodeBasDistrictDao;
import com.gsafety.gemp.wxapplet.healthcode.dao.po.CodeBasDistrictPO;
import com.gsafety.gemp.wxapplet.healthcode.factory.WechatUserManager;
import com.gsafety.gemp.wxapplet.healthcode.factory.WechatUserManagerFactory;
import com.gsafety.gemp.wxapplet.healthcode.wrapper.CodeBasDistrictWrapper;
import com.gsafety.gemp.wxapplet.infection.contract.common.SuffererCensusJx;
import com.gsafety.gemp.wxapplet.infection.contract.dto.*;
import com.gsafety.gemp.wxapplet.infection.contract.enums.*;
import com.gsafety.gemp.wxapplet.infection.contract.iface.SiteStaffService;
import com.gsafety.gemp.wxapplet.infection.contract.iface.SuffererExcelService;
import com.gsafety.gemp.wxapplet.infection.contract.iface.SuffererInfoService;
import com.gsafety.gemp.wxapplet.infection.dao.*;
import com.gsafety.gemp.wxapplet.infection.dao.po.*;
import com.gsafety.gemp.wxapplet.infection.wrapper.SuffererWrapper;
import com.gsafety.gemp.wxapplet.utils.DateUtils;
import com.gsafety.gemp.wxapplet.utils.SheetExecutor;
import com.gsafety.gemp.wxapplet.utils.UUIDUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Transient;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static com.gsafety.gemp.wxapplet.infection.wrapper.SuffererWrapper.toDTOList;
import static com.gsafety.gemp.wxapplet.infection.wrapper.SuffererWrapper.toPageDTO;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;


/**
 * 病患追溯信息服务实现类
 * @author liyanhong
 * @since 2020/2/27  10:44
 */
@Service("suffererInfoService")
@Slf4j
public class SuffererInfoServiceImpl implements SuffererInfoService {

	@Resource
	private SiteInfoDao siteInfoDao;

    @Resource
    private SuffererInfoDao suffererInfoDao;

    @Resource
	private SuffererInfoAllDao suffererInfoAllDao;

    @Resource
    private SuffererTraceDao suffererTraceDao;

    @Resource
    private CodeBasDistrictDao codeBasDistrictDao;

    @Resource
    private SiteStaffRelationDao siteStaffRelationDao;

    @Resource
    private SuffererExcelService suffererExcelService;

    @Resource
    private SiteStaffService siteStaffService;

    @Autowired
	private JdbcTemplate jdbcTemplate;

    private final static String[] parentCodes = {"420100"};

    @Override
    public Page<SuffererBaseDTO> searchSuffererInfo(String staffId,String key, PageRequest pageRequest) {
        List<SuffererBaseDTO> resultList = new ArrayList<>();
        if(StringUtils.isEmpty(staffId)){
			throw new RuntimeException("用户Id为空!");
		}
		SiteStaffPO staffPo = siteStaffService.getSiteStaffById(staffId);
		if(staffPo == null){
			throw new RuntimeException("操作用户不存在!");
		}
		Page<SuffererInfoPO> poPage;
	    Long total = 0L;
		if(SiteStaffRelationStatus.GroupSiteEnum.ALL.getCode().equals(staffPo.getSiteIdGroup())){
			if(StringUtils.isBlank(key)){
				Sort sort = Sort.by(Sort.Direction.DESC, "updateTime");
				pageRequest = PageRequest.of(pageRequest.getPageNumber(), pageRequest.getPageSize(),sort);
				poPage = suffererInfoDao.findAll(pageRequest);
		        total = suffererInfoDao.count();
			}else{
				poPage = suffererInfoDao.findBySuffererNameLikeOrSuffererCardLikeOrSuffererTelLikeOrderByUpdateTimeDesc(key + "%","%"+ key + "%", "%"+ key + "%", pageRequest);
	            total = suffererInfoDao.countBySuffererNameLikeOrSuffererCardLikeOrSuffererTelLike(key + "%", "%"+ key + "%", "%"+ key + "%");
			}
		}else{
			List<SiteStaffRelationPO> relations = siteStaffRelationDao.findByStaffId(staffId);
			if(CollectionUtils.isEmpty(relations)){
				throw new RuntimeException("用户操作站点不存在!");
			}
			List<String> relationsSite = relations.stream().map(SiteStaffRelationPO::getSiteId).collect(Collectors.toList());
			if(StringUtils.isBlank(key)){
	            poPage = suffererInfoDao.findByCurSiteIdInOrderByUpdateTimeDesc(relationsSite,pageRequest);
	            total = suffererInfoDao.countByCurSiteIdIn(relationsSite);
	        }else {
	            poPage = suffererInfoDao.findByCurSiteIdInAndSuffererNameLikeOrSuffererCardLikeOrSuffererTelLikeOrderByUpdateTimeDesc(relationsSite,key + "%", "%"+ key + "%", "%"+ key + "%", pageRequest);
	            total = suffererInfoDao.countByCurSiteIdInAndSuffererNameLikeOrSuffererCardLikeOrSuffererTelLike(relationsSite,key + "%", "%"+ key + "%", "%"+ key + "%");
	        }
		}
        SuffererBaseDTO dto = null;
        for(SuffererInfoPO po : poPage.getContent()){
            dto = new SuffererBaseDTO();
            BeanUtils.copyProperties(po, dto);
            dto.setSuffererAge(po.getSuffererAge() == null? "" : String.valueOf(po.getSuffererAge().intValue()));
            resultList.add(dto);
        }
        return new PageImpl<SuffererBaseDTO>(resultList, pageRequest, total);
    }



	@Override
	public Page<SuffererInfoDTO> searchWebSuffererInfo(String staffId, SuffererInfoWebPageDTO dto, PageRequest pageRequest) {
        if(StringUtils.isEmpty(staffId)){
			throw new RuntimeException("用户Id为空!");
		}
		SiteStaffPO staffPo = siteStaffService.getSiteStaffById(staffId);
		if(staffPo == null){
			throw new RuntimeException("操作用户不存在!");
		}

	    List<String> relationsSite = null;
	    if(!SiteStaffRelationStatus.GroupSiteEnum.ALL.getCode().equals(staffPo.getSiteIdGroup())){
	    	List<SiteStaffRelationPO> relations = siteStaffRelationDao.findByStaffId(staffId);
			if(CollectionUtils.isEmpty(relations)){
				throw new RuntimeException("用户操作站点不存在!");
			}
			relationsSite = relations.stream().map(SiteStaffRelationPO::getSiteId).collect(Collectors.toList());
		}

	    Specification<SuffererInfoPO>  spec = concatSpecification(dto, relationsSite);
	    Page<SuffererInfoPO> poPage = suffererInfoDao.findAll(spec, pageRequest);
		return toPageDTO(poPage);
	}

	@SuppressWarnings("serial")
	private Specification<SuffererInfoPO> concatSpecification(SuffererInfoWebPageDTO dto,List<String> sites){
		Specification<SuffererInfoPO> spec = new Specification<SuffererInfoPO>(){

			@Override
			public Predicate toPredicate(Root<SuffererInfoPO> root, CriteriaQuery<?> query,
					CriteriaBuilder cb) {
				List<Predicate> predicates = new ArrayList<>(); //所有的断言
				if(!CollectionUtils.isEmpty(sites)){
					Predicate curSiteIdIn = cb.in(root.get("curSiteId")).value(sites);
					predicates.add(curSiteIdIn);
				}
				if(dto != null){
					if(StringUtils.isNotEmpty(dto.getSuffererName())){
						Predicate suffererNameLike = cb.like(root.get("suffererName"), "%"+dto.getSuffererName()+"%");
						predicates.add(suffererNameLike);
					}
					if(StringUtils.isNotEmpty(dto.getSuffererCard())){
						Predicate suffererCardLike = cb.like(root.get("suffererCard"), "%"+dto.getSuffererCard()+"%");
						predicates.add(suffererCardLike);
					}
					if(StringUtils.isNotEmpty(dto.getSuffererTel())){
						Predicate suffererTelLike = cb.like(root.get("suffererTel"), "%"+dto.getSuffererTel()+"%");
						predicates.add(suffererTelLike);
					}
					if(dto.getIllnessState() != null){
						Predicate illnessStateEquals = cb.equal(root.get("illnessState"), dto.getIllnessState());
						predicates.add(illnessStateEquals);
					}
					if(dto.getSuffererType() != null&&dto.getSuffererType()!=0){
						Predicate suffererTypeEquals = cb.equal(root.get("suffererType"), dto.getSuffererType());
						predicates.add(suffererTypeEquals);
					}

					if(dto.getSuffererStatus() != null){
						Predicate suffererStatusEquals = cb.equal(root.get("suffererStatus"), dto.getSuffererStatus());
						predicates.add(suffererStatusEquals);
					}

				}
				return cb.and(predicates.toArray(new Predicate[predicates.size()]));
			}

		};
		return spec;
	}

	private Specification<SuffererInfoPO> getSpecification(SuffererInfoWebPageDTO dto){
		Specification<SuffererInfoPO> spec = new Specification<SuffererInfoPO>(){

			@Override
			public Predicate toPredicate(Root<SuffererInfoPO> root, CriteriaQuery<?> query,
										 CriteriaBuilder cb) {
				List<Predicate> predicates = new ArrayList<>(); //所有的断言
				if(dto != null){
					if(StringUtils.isNotEmpty(dto.getSuffererName())){
						Predicate suffererNameLike = cb.like(root.get("suffererName"), "%"+dto.getSuffererName()+"%");
						predicates.add(suffererNameLike);
					}
					if(StringUtils.isNotEmpty(dto.getSuffererCard())){
						Predicate suffererCardLike = cb.like(root.get("suffererCard"), "%"+dto.getSuffererCard()+"%");
						predicates.add(suffererCardLike);
					}
					if(StringUtils.isNotEmpty(dto.getSuffererTel())){
						Predicate suffererTelLike = cb.like(root.get("suffererTel"), "%"+dto.getSuffererTel()+"%");
						predicates.add(suffererTelLike);
					}
					if(dto.getIllnessState() != null){
						Predicate illnessStateEquals = cb.equal(root.get("illnessState"), dto.getIllnessState());
						predicates.add(illnessStateEquals);
					}
					if(dto.getSuffererType() != null&&dto.getSuffererType()!=0){
						Predicate suffererTypeEquals = cb.equal(root.get("suffererType"), dto.getSuffererType());
						predicates.add(suffererTypeEquals);
					}

					if(dto.getSuffererStatus() != null){
						Predicate suffererStatusEquals = cb.equal(root.get("suffererStatus"), dto.getSuffererStatus());
						predicates.add(suffererStatusEquals);
					}

				}
				return cb.and(predicates.toArray(new Predicate[predicates.size()]));
			}

		};
		return spec;
	}
    @Override
    public SuffererDetailDTO getSuffererInfoById(String id) {

        SuffererDetailDTO dto = new SuffererDetailDTO();
        Optional<SuffererInfoPO> poOpt = suffererInfoDao.findById(id);
        if(poOpt.isPresent()){
            BeanUtils.copyProperties(poOpt.get(), dto);
            dto.setSuffererAge(poOpt.get().getSuffererAge() == null? "" : String.valueOf(poOpt.get().getSuffererAge().intValue()));
            dto.setSuffererType(SuffererTypeEnum.getValueByCode(poOpt.get().getSuffererType()));
            dto.setSuffererStatus(SuffererStatusEnum.getValueByCode(poOpt.get().getSuffererStatus()));
            String illnessName = IllnessStateEnum.getValueByCode(dto.getIllnessState()) == StringUtils.EMPTY?"":IllnessStateEnum.getValueByCode(dto.getIllnessState());
            dto.setIllnessName(illnessName);
        }
        return dto;
    }

	@Override
	public SuffererInfoDTO getSuffererWebById(String id) {
		Optional<SuffererInfoPO> poOpt = suffererInfoDao.findById(id);
		if(poOpt.isPresent()){
			return SuffererWrapper.toDTO(poOpt.get());
		}

		return null;
	}

	private SuffererWebDetailDTO getSuffererInfoWebById(String id) {

    	SuffererWebDetailDTO dto = new SuffererWebDetailDTO();
        Optional<SuffererInfoPO> poOpt = suffererInfoDao.findById(id);
        if(poOpt.isPresent()){
            BeanUtils.copyProperties(poOpt.get(), dto);
            dto.setSuffererAge(poOpt.get().getSuffererAge() == null? "" : String.valueOf(poOpt.get().getSuffererAge().intValue()));
            dto.setSuffererTypeName(SuffererTypeEnum.getValueByCode(poOpt.get().getSuffererType()));
            dto.setSuffererStatusName(SuffererStatusEnum.getValueByCode(poOpt.get().getSuffererStatus()));
            String illnessName = IllnessStateEnum.getValueByCode(dto.getIllnessState()) == StringUtils.EMPTY?"":IllnessStateEnum.getValueByCode(dto.getIllnessState());
            dto.setIllnessName(illnessName);
        }
        return dto;
    }

    @Override
    public SuffererDetailDTO updateSuffererStatus(SuffererEditDTO dto) {

        SuffererDetailDTO resultDto = new SuffererDetailDTO();

        Optional<SuffererInfoPO> poOpt = suffererInfoDao.findById(dto.getId());

        if(!poOpt.isPresent()){
            return resultDto;
        }

        SuffererInfoPO suffererInfoPO = poOpt.get();

        BeanUtils.copyProperties(dto, suffererInfoPO);
        suffererInfoPO.setUpdateTime(new Date());

        suffererInfoDao.saveAndFlush(suffererInfoPO);

        return getSuffererInfoById(suffererInfoPO.getId());
    }

	@Override
	public List<SiteInfoSelectDTO> getSiteInfoSelectByStaff(SiteStaffPO staffPo) {
		List<SiteInfoPO> sites = null;
		if(SiteStaffRelationStatus.GroupSiteEnum.ALL.getCode().equals(staffPo.getSiteIdGroup())){
			sites = siteInfoDao.findAll();
		}else{
			//永远都是使用siteIdGroup获取siteId
			List<SiteStaffRelationPO> relations = siteStaffRelationDao.findBySiteIdGroup(staffPo.getSiteIdGroup());
			if(CollectionUtils.isEmpty(relations)){
				throw new RuntimeException("用户操作站点不存在!");
			}
			List<String> relationsSite = relations.stream().map(SiteStaffRelationPO::getSiteId).collect(Collectors.toList());
			sites = siteInfoDao.findByIdIn(relationsSite);
		}
		if(!CollectionUtils.isEmpty(sites)){
			SiteInfoSelectDTO dto = null;
			List<SiteInfoSelectDTO> selectDto = new ArrayList<>();
			for(SiteInfoPO po : sites){
				dto = new SiteInfoSelectDTO();
				BeanUtils.copyProperties(po, dto);
				selectDto.add(dto);
			}
			return selectDto;
		}
		return null;
	}

	@Override
	@Transactional
	public SuffererDetailDTO updateSuffererType(SuffererTypeUpdateDTO dto) {
		vaildSuffererTypeProperty(dto);
		SuffererDetailDTO resultDto = new SuffererDetailDTO();
		Optional<SuffererInfoPO> optional = suffererInfoDao.findById(dto.getId());
		if(!optional.isPresent()){
			return resultDto;
		}
		//修改病人类型
		SuffererInfoPO oldPo = optional.get();
		if(oldPo.getSuffererTypeTime() != null){
			if(dto.getSuffererType() == oldPo.getSuffererType() && dto.getSuffererTypeTime().compareTo(oldPo.getSuffererTypeTime()) == 0){
				throw new RuntimeException("病人类型状态和时间没有任何改变!");
			}
		}

		WechatUserinfo userInfo = getWechatUserinfoBySession(dto.getSessionKey());
		if(userInfo == null){
			throw new RuntimeException("登陆失效，请重新登陆!");
		}
		SiteStaffPO staffPo = siteStaffService.getSiteStaffByOpenId(userInfo.getOpenid());
		//记录患者类型
		SuffererInfoPO curPo = new SuffererInfoPO();
		BeanUtils.copyProperties(dto, curPo);
		curPo.setUpdateBy(staffPo.getRealname());
		saveChangeTypeTrack(curPo,oldPo);

		//更新数据
		oldPo.setSuffererType(dto.getSuffererType());
		oldPo.setSuffererTypeTime(dto.getSuffererTypeTime());
		oldPo.setUpdateBy(staffPo.getRealname());
		oldPo.setUpdateTime(Calendar.getInstance().getTime());
		suffererInfoDao.saveAndFlush(oldPo);

        return getSuffererInfoById(oldPo.getId());
	}

	@Override
	@Transactional
	public SuffererDetailDTO updateSuffererStatus(SuffererStatusUpdateDTO dto) {
		vaildSuffererStatusProperty(dto);
		SuffererDetailDTO resultDto = new SuffererDetailDTO();
		Optional<SuffererInfoPO> optional = suffererInfoDao.findById(dto.getId());
		if(!optional.isPresent()){
			return resultDto;
		}
		//修改管理状态
		SuffererInfoPO oldPo = optional.get();
		if(oldPo.getSuffererStatusTime() != null){
			if(dto.getSuffererStatus() == oldPo.getSuffererStatus()
					&& dto.getSuffererStatusTime().compareTo(oldPo.getSuffererStatusTime()) == 0
					&& dto.getCurSiteId().equals(oldPo.getCurSiteId())){
				throw new RuntimeException("管理状态、时间和地点没有任何改变!");
			}
		}
		WechatUserinfo userInfo = getWechatUserinfoBySession(dto.getSessionKey());
		if(userInfo == null){
			throw new RuntimeException("登陆失效，请重新登陆!");
		}
		SiteStaffPO staffPo = siteStaffService.getSiteStaffByOpenId(userInfo.getOpenid());
		//记录位置改变
		SuffererInfoPO sitePo = null;
		if(!dto.getCurSiteId().equals(oldPo.getCurSiteId())){
			SuffererInfoPO curSitePo = new SuffererInfoPO();
			curSitePo.setCurSiteId(dto.getCurSiteId());
			curSitePo.setCurSiteName(dto.getCurSiteName());
			curSitePo.setMoveIntoTime(dto.getSuffererStatusTime());
			curSitePo.setUpdateBy(staffPo.getRealname());
			saveChangeSiteTrack(curSitePo,oldPo);
			sitePo = new SuffererInfoPO();
			BeanUtils.copyProperties(oldPo, sitePo);
			sitePo.setCurSiteId(dto.getCurSiteId());
			sitePo.setCurSiteName(dto.getCurSiteName());
			sitePo = suffererInfoDao.saveAndFlush(sitePo);
		}

		//记录管理状态改变
		if(sitePo != null && dto.getSuffererStatus() != oldPo.getSuffererStatus()){
			SuffererInfoPO curStatusPo = new SuffererInfoPO();
			curStatusPo.setSuffererStatus(dto.getSuffererStatus());
			curStatusPo.setSuffererStatusTime(dto.getSuffererStatusTime());
			curStatusPo.setUpdateBy(staffPo.getRealname());
			saveChangeStatusTrack(curStatusPo,sitePo);
		}else if(dto.getSuffererStatus() != oldPo.getSuffererStatus()){
			SuffererInfoPO curStatusPo = new SuffererInfoPO();
			curStatusPo.setSuffererStatus(dto.getSuffererStatus());
			curStatusPo.setSuffererStatusTime(dto.getSuffererStatusTime());
			curStatusPo.setUpdateBy(staffPo.getRealname());
			saveChangeStatusTrack(curStatusPo,oldPo);
		}

		oldPo.setUpdateBy(staffPo.getRealname());
		oldPo.setUpdateTime(Calendar.getInstance().getTime());
		oldPo.setSuffererStatus(dto.getSuffererStatus());
		oldPo.setSuffererStatusTime(dto.getSuffererStatusTime());
		oldPo.setCurSiteId(dto.getCurSiteId());
		oldPo.setCurSiteName(dto.getCurSiteName());
		suffererInfoDao.saveAndFlush(oldPo);

		return getSuffererInfoById(oldPo.getId());
	}

	@Override
	@Transactional
	public SuffererDetailDTO updateillnessState(IllnessStateUpdateDTO dto) {
		vaildIllnessStateProperty(dto);
		SuffererDetailDTO resultDto = new SuffererDetailDTO();
		Optional<SuffererInfoPO> optional = suffererInfoDao.findById(dto.getId());
		if(!optional.isPresent()){
			return resultDto;
		}
		//修改管理状态
		//修改病人类型
		SuffererInfoPO po = optional.get();
		if(po.getIllnessStateTime() != null){
			if(dto.getIllnessState() == po.getIllnessState() && dto.getIllnessStateTime().compareTo(po.getIllnessStateTime()) == 0){
				throw new RuntimeException("病情情况和时间没有任何改变!");
			}
		}
		po.setIllnessState(dto.getIllnessState());
		po.setIllnessStateTime(dto.getIllnessStateTime());
		suffererInfoDao.saveAndFlush(po);

		return getSuffererInfoById(po.getId());
	}

	private void vaildSuffererTypeProperty(SuffererTypeUpdateDTO dto){
		if(dto == null){
			throw new RuntimeException("更改病人类型对象为空!");
		}
		if(StringUtils.isEmpty(dto.getId())){
			throw new RuntimeException("更改病人类型Id为空!");
		}
		if(dto.getSuffererType() == null){
			throw new RuntimeException("更改病人类型为空!");
		}
		if(dto.getSuffererTypeTime() == null){
			throw new RuntimeException("更改病人类型时间为空!");
		}
		if(StringUtils.isEmpty(dto.getSessionKey())){
			throw new RuntimeException("用户sessionKey为空!");
		}
	}

	private void vaildSuffererStatusProperty(SuffererStatusUpdateDTO dto){
		if(dto == null){
			throw new RuntimeException("更改管理状态对象为空!");
		}
		if(StringUtils.isEmpty(dto.getId())){
			throw new RuntimeException("更改管理状态Id为空!");
		}
		if(dto.getSuffererStatus() == null){
			throw new RuntimeException("更改管理状态为空!");
		}
		if(dto.getSuffererStatusTime() == null){
			throw new RuntimeException("更改管理状态时间为空!");
		}
		if(StringUtils.isEmpty(dto.getSessionKey())){
			throw new RuntimeException("用户sessionKey为空!");
		}
	}

	private void vaildIllnessStateProperty(IllnessStateUpdateDTO dto){
		if(dto == null){
			throw new RuntimeException("更改病情情况对象为空!");
		}
		if(StringUtils.isEmpty(dto.getId())){
			throw new RuntimeException("更改病情情况Id为空!");
		}
		if(dto.getIllnessState() == null){
			throw new RuntimeException("更改病情情况为空!");
		}
		if(dto.getIllnessStateTime() == null){
			throw new RuntimeException("更改病情情况时间为空!");
		}
		if(StringUtils.isEmpty(dto.getSessionKey())){
			throw new RuntimeException("用户sessionKey为空!");
		}
	}


	@Override
	public List<CodeBasDistrictDTO> getCodeBaseDistrictByParentCode() {
		List<CodeBasDistrictPO> codeBasDistrictPO = codeBasDistrictDao.findByParentCodeIn(Arrays.asList(parentCodes));
		List<CodeBasDistrictDTO> dtos = CodeBasDistrictWrapper.listPoToDto(codeBasDistrictPO);
		return dtos;
	}

	@Override
	@Transactional
	public SuffererDetailDTO saveSuffererInfo(SuffererInfoPO po) {
		if(po == null){
			throw new RuntimeException("患者信息为空!");
		}
		SuffererInfoPO suffererInfoPo = suffererInfoDao.findBySuffererCard(po.getSuffererCard());
		if(suffererInfoPo != null){
			throw new RuntimeException("患者信息已经存在!");
		}
		//保存患者信息
		SuffererInfoPO data = suffererInfoDao.saveAndFlush(po);
		saveOriginalTrack(data);//保存位置记录


		return getSuffererInfoById(data.getId());
	}

	@Override
	@Transactional
	public SuffererWebDetailDTO updateSuffererInfo(SuffererInfoPO po) {
		if(po == null){
			throw new RuntimeException("更新患者对象为空!");
		}
		if(StringUtils.isEmpty(po.getId())){
			throw new RuntimeException("患者主键为空!");
		}
		Optional<SuffererInfoPO> optional = suffererInfoDao.findById(po.getId());
		if(!optional.isPresent()){
			throw new RuntimeException("患者信息为空!");
		}
		SuffererInfoPO oldPo = optional.get();
		//如果位置信息有变动，则记录位置信息
		SuffererInfoPO sitePo = null;
		if(!oldPo.getCurSiteId().equals(po.getCurSiteId())){
			saveChangeSiteTrack(po,oldPo);
			sitePo = new SuffererInfoPO();
			BeanUtils.copyProperties(oldPo, sitePo);
			sitePo.setCurSiteId(po.getCurSiteId());
			sitePo.setCurSiteName(po.getCurSiteName());
			sitePo = suffererInfoDao.saveAndFlush(sitePo);
		}
		//如果管理状态有变动，则记录管理状态
		if(sitePo != null && oldPo.getSuffererStatus() != po.getSuffererStatus()){
			saveChangeStatusTrack(po,sitePo);
		}else if(oldPo.getSuffererStatus() != po.getSuffererStatus()){
			saveChangeStatusTrack(po,oldPo);
		}
		//如果患者类型有变动，则记录患者类型
		if(sitePo != null && oldPo.getSuffererType() != po.getSuffererType()){
			saveChangeTypeTrack(po,sitePo);
		}else if(oldPo.getSuffererType() != po.getSuffererType()){
			saveChangeStatusTrack(po,oldPo);
		}
		suffererInfoDao.saveAndFlush(po);
		return getSuffererInfoWebById(po.getId());
	}

	@Override
	public String checkSuffererInfoExists(String suffererCard) {
		if(StringUtils.isEmpty(suffererCard)){
			throw new RuntimeException("身份证信息为空!");
		}
		SuffererInfoPO suffererInfoPo = suffererInfoDao.findBySuffererCard(suffererCard);
		if(suffererInfoPo != null){
			return suffererInfoPo.getId();
		}
		return null;
	}

	private void saveChangeTypeTrack(SuffererInfoPO curPo,SuffererInfoPO oldPo){
		SuffererTracePO traceSitePO = new SuffererTracePO();
		traceSitePO.setId(UUIDUtils.getUUID());
		traceSitePO.setSiteId(oldPo.getCurSiteId());
		traceSitePO.setSuffererId(oldPo.getId());
		traceSitePO.setSuffererName(oldPo.getSuffererName());
		traceSitePO.setCreateBy(curPo.getUpdateBy());
		traceSitePO.setCreateTime(Calendar.getInstance().getTime());
		traceSitePO.setUpdateBy(curPo.getUpdateBy());
		traceSitePO.setUpdateTime(Calendar.getInstance().getTime());
		suffererTraceDao.saveAndFlush(traceSitePO);
	}

	private void saveChangeStatusTrack(SuffererInfoPO curPo,SuffererInfoPO oldPo){
		SuffererTracePO traceSitePO = new SuffererTracePO();
		traceSitePO.setId(UUIDUtils.getUUID());
		traceSitePO.setSiteId(oldPo.getCurSiteId());
		traceSitePO.setSuffererId(oldPo.getId());
		traceSitePO.setSuffererName(oldPo.getSuffererName());
		traceSitePO.setCreateBy(curPo.getUpdateBy());
		traceSitePO.setCreateTime(Calendar.getInstance().getTime());
		traceSitePO.setUpdateBy(curPo.getUpdateBy());
		traceSitePO.setUpdateTime(Calendar.getInstance().getTime());
		suffererTraceDao.saveAndFlush(traceSitePO);
	}

	private void saveChangeSiteTrack(SuffererInfoPO curPo,SuffererInfoPO oldPo){
		List<SuffererTracePO> results = new ArrayList<>();
		SuffererTracePO traceSitePO = new SuffererTracePO();
		traceSitePO.setId(UUIDUtils.getUUID());
		traceSitePO.setSiteId(oldPo.getCurSiteId());
		traceSitePO.setSuffererId(oldPo.getId());
		traceSitePO.setSuffererName(oldPo.getSuffererName());
		traceSitePO.setCreateBy(curPo.getUpdateBy());
		traceSitePO.setCreateTime(Calendar.getInstance().getTime());
		traceSitePO.setUpdateBy(curPo.getUpdateBy());
		traceSitePO.setUpdateTime(Calendar.getInstance().getTime());
		results.add(traceSitePO);

		traceSitePO = new SuffererTracePO();
		traceSitePO.setId(UUIDUtils.getUUID());
		traceSitePO.setSiteId(curPo.getCurSiteId());
		traceSitePO.setSuffererId(oldPo.getId());
		traceSitePO.setSuffererName(oldPo.getSuffererName());
		traceSitePO.setCreateBy(curPo.getUpdateBy());
		traceSitePO.setCreateTime(Calendar.getInstance().getTime());
		traceSitePO.setUpdateBy(curPo.getUpdateBy());
		traceSitePO.setUpdateTime(Calendar.getInstance().getTime());
		results.add(traceSitePO);

		suffererTraceDao.saveAll(results);
	}


	private void saveOriginalTrack(SuffererInfoPO data){
		SuffererTracePO traceSitePO = new SuffererTracePO();
		traceSitePO.setId(UUIDUtils.getUUID());
		traceSitePO.setSiteId(data.getCurSiteId());
		traceSitePO.setSuffererId(data.getId());
		traceSitePO.setSuffererName(data.getSuffererName());
		traceSitePO.setCreateBy(data.getCreateBy());
		traceSitePO.setCreateTime(Calendar.getInstance().getTime());
		traceSitePO.setUpdateBy(data.getCreateBy());
		traceSitePO.setUpdateTime(Calendar.getInstance().getTime());
		suffererTraceDao.saveAndFlush(traceSitePO);
	}



	private WechatUserinfo getWechatUserinfoBySession(String sessionKey){
		WechatUserManager wechatUserManager = WechatUserManagerFactory.getInstance();
		WechatUserinfo userInfo =wechatUserManager.getUser(sessionKey);
		return userInfo;
	}



	@Override
	public JSONObject analyseCDCSuffererData(String date) {
		List<SuffererInfoPO> suffererInfos = suffererInfoDao.findBySuffererStatusNotAndAreaCode(SuffererStatusEnum.Isolate.getCode(),"420115");
		JSONObject jsonObject = new JSONObject();
		if(CollectionUtils.isEmpty(suffererInfos)){
			return jsonObject;
		}
		//累计确诊
		Integer confirm = suffererInfos.stream().filter(a->SuffererTypeEnum.Confirmed.getCode().equals(a.getSuffererType())).collect(Collectors.toList()).size();
		//累计疑似
		Integer suspected = suffererInfos.stream().filter(a->SuffererTypeEnum.Suspected.getCode().equals(a.getSuffererType())).collect(Collectors.toList()).size();
		//累计死亡
		Integer dying = suffererInfos.stream().filter(a->SuffererStatusEnum.Death.getCode().equals(a.getSuffererType())).collect(Collectors.toList()).size();
		//当前重症
		Integer icu = suffererInfos.stream().filter(a->IllnessStateEnum.ICU.getCode().equals(a.getIllnessState())).collect(Collectors.toList()).size();
		//当前危重
		Integer grave = suffererInfos.stream().filter(a->IllnessStateEnum.GRAVE.getCode().equals(a.getIllnessState())).collect(Collectors.toList()).size();
		//在院治疗
		Integer inHospital = suffererInfos.stream().filter(a->SuffererStatusEnum.InHospital.getCode().equals(a.getSuffererStatus())).collect(Collectors.toList()).size();
		//累计治愈
		Integer curing = suffererInfos.stream().filter(a->IllnessStateEnum.CURING.getCode().equals(a.getIllnessState())).collect(Collectors.toList()).size();

		jsonObject.put("confirm", confirm);
		jsonObject.put("suspected", suspected);
		jsonObject.put("dying", dying);
		jsonObject.put("icu", icu);
		jsonObject.put("grave", grave);
		jsonObject.put("inHospital", inHospital);
		jsonObject.put("curing", curing);
		return jsonObject;
	}



	@Override
	public JSONObject analyseCDCSuffererToday() {
		String yesterday = DateUtils.getYesterdayString();
		List<SuffererInfoPO> suffererInfos = suffererInfoDao.findBySuffererStatusNotAndAreaCodeAndCreateTimeBetween(SuffererStatusEnum.Isolate.getCode(),"420115", DateUtils.getYesterdayBeginTime(),DateUtils.getYesterdayEndTime());
		JSONObject jsonObject = new JSONObject();
		if(CollectionUtils.isEmpty(suffererInfos)){
			return jsonObject;
		}
		Integer confirm = suffererInfos.stream().filter(a->SuffererTypeEnum.Confirmed.getCode().equals(a.getSuffererType())).collect(Collectors.toList()).size();
		Integer suspected = suffererInfos.stream().filter(a->SuffererTypeEnum.Suspected.getCode().equals(a.getSuffererType())).collect(Collectors.toList()).size();
		Integer dying = suffererInfos.stream().filter(a->SuffererStatusEnum.Death.getCode().equals(a.getSuffererStatus())).collect(Collectors.toList()).size();
		jsonObject.put("confirm", confirm);
		jsonObject.put("suspected", suspected);
		jsonObject.put("dying", dying);
		jsonObject.put("yesterday", yesterday);
		return jsonObject;
	}

	@Override
	public SuffererCensusJx analyseJxSuffererToday(String totalDate) {
		SuffererCensusJx census = new SuffererCensusJx();
		//String yesterday = DateUtils.getYesterdayString();
		census.setExpiryDate(totalDate);
		//获取密接累计值
		int contactsPOS = suffererTraceDao.countByTraceTypeAndChangeTimeBetween(SuffererTypeEnum.Close_contacts.getCode(),
				DateUtils.dateToStringBeginOrEnd(totalDate,true),DateUtils.dateToStringBeginOrEnd(totalDate,false));
		census.setContactAdd(contactsPOS);
		//获取发热
		int hotPOS = suffererTraceDao.countByTraceTypeAndChangeTimeBetween(SuffererTypeEnum.Close_contacts.getCode(),
				DateUtils.dateToStringBeginOrEnd(totalDate,true),DateUtils.dateToStringBeginOrEnd(totalDate,false));
		census.setHotAdd(hotPOS);
//		List<SuffererInfoPO> suffererInfos = suffererInfoDao.findBySuffererStatusAndAreaCodeAndCreateTimeBetween(SuffererStatusEnum.Isolate.getCode(),
//				"420115", DateUtils.dateToStringBeginOrEnd(totalDate,true),DateUtils.dateToStringBeginOrEnd(totalDate,false));
//		//List<SuffererInfoPO> suffererInfos = suffererInfoDao.findBySuffererStatusAndAreaCodeAndCreateTimeBetween(SuffererStatusEnum.Isolate.getCode(),"420115", DateUtils.getYesterdayBeginTime(),DateUtils.getYesterdayEndTime());
//		if(!CollectionUtils.isEmpty(suffererInfos)){
//			Integer contactAdd = suffererInfos.stream().filter(a->SuffererTypeEnum.Close_contacts.getCode().equals(a.getSuffererType())).collect(Collectors.toList()).size();
//			census.setContactAdd(contactAdd);
//			Integer hotAdd = suffererInfos.stream().filter(a->SuffererTypeEnum.Heat.getCode().equals(a.getSuffererType())).collect(Collectors.toList()).size();
//			census.setHotAdd(hotAdd);
//
//		}

		return census;
	}

	@Override
	public Page<SuffererInfoDTO> getSuffererInfoTodayBySuffererType(Integer suffererType,PageRequest pageRequest) {
		if(suffererType == null){
			throw new RuntimeException("状态为空!");
		}
		Page<SuffererInfoPO> pagePo = suffererInfoDao.findBySuffererStatusNotAndAreaCodeAndSuffererTypeAndCreateTimeBetween(SuffererStatusEnum.Isolate.getCode(),"420115", suffererType,DateUtils.getYesterdayBeginTime(),DateUtils.getYesterdayEndTime(), pageRequest);
		return toPageDTO(pagePo);
	}

	@Override
	public Page<SuffererInfoDTO> getJxSuffererInfoTodayBySuffererType(Integer suffererType,Pageable pageable, String totalDate) {
		if(suffererType == null){
			throw new RuntimeException("状态为空!");
		}
		List<SuffererTracePO>  suffererTracePOS  =  suffererTraceDao.findByTraceTypeAndChangeTimeBetween(suffererType,
				DateUtils.dateToStringBeginOrEnd(totalDate,true),DateUtils.dateToStringBeginOrEnd(totalDate,false));
		List<String>  suffererList = suffererTracePOS.stream().map(SuffererTracePO -> SuffererTracePO.getSuffererId()).collect(Collectors.toList());
		// 根据userid生成分页集合
		Specification<SuffererInfoPO> spec =getSpecification(suffererList);
		Page<SuffererInfoPO> pagePo = suffererInfoDao.findAll(spec, pageable);
		return toPageDTO(pagePo);
	}

	private Specification<SuffererInfoPO>  getSpecification(List<String>  suffererList ){
		Specification<SuffererInfoPO> suffererInfoPOSpecification = new Specification<SuffererInfoPO>() {
			@Override
			public Predicate toPredicate(Root<SuffererInfoPO> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder cb) {
				Predicate  predicate = cb.in(root.get("id")).value(suffererList);
				List<Predicate> list = new ArrayList<>();
				list.add(predicate);
				Predicate[] p = new Predicate[list.size()];
				return cb.and(list.toArray(p));
			}
		};
		return  suffererInfoPOSpecification;
	}

	@Override
	public Page<SuffererInfoDTO> getSuffererInfoTodayByIllnessState(Integer illnessState, PageRequest pageRequest) {
		if(illnessState == null){
			throw new RuntimeException("状态为空!");
		}
		Page<SuffererInfoPO> pagePo = suffererInfoDao.findBySuffererStatusNotAndAreaCodeAndIllnessStateAndCreateTimeBetween(SuffererStatusEnum.Isolate.getCode(),"420115", illnessState,DateUtils.getYesterdayBeginTime(),DateUtils.getYesterdayEndTime(), pageRequest);
		return toPageDTO(pagePo);
	}

	@Override
	public Page<SuffererInfoDTO> getSuffererInfoTodayBySuffererStatus(Integer suffererStatus,PageRequest pageRequest) {
		Page<SuffererInfoPO> pagePo = suffererInfoDao.findBySuffererStatusAndAreaCodeAndCreateTimeBetween(suffererStatus,"420115",DateUtils.getYesterdayBeginTime(),DateUtils.getYesterdayEndTime(), pageRequest);
		return toPageDTO(pagePo);
	}


	@Override
	public SuffererCensusJx analyseJxSuffererData() {
		SuffererCensusJx census = new SuffererCensusJx();
		String yesterday = DateUtils.getYesterdayString();
		census.setExpiryDate(yesterday);

		List<SuffererInfoPO> suffererInfos =
				suffererInfoDao.findBySuffererStatusAndAreaCode(SuffererStatusEnum.Isolate.getCode(),"420115");

		if(!CollectionUtils.isEmpty(suffererInfos)){
			census.setTotal(suffererInfos.size());

			Integer confirmTotal = suffererInfos.stream().filter(a->SuffererTypeEnum.Confirmed.getCode().equals(a.getSuffererType())).collect(Collectors.toList()).size();
			census.setConfirm(confirmTotal);

			Integer debtTotal = suffererInfos.stream().filter(a->SuffererTypeEnum.Suspected.getCode().equals(a.getSuffererType())).collect(Collectors.toList()).size();
			census.setSuspected(debtTotal);

			Integer contactTotal = suffererInfos.stream().filter(a->SuffererTypeEnum.Close_contacts.getCode().equals(a.getSuffererType())).collect(Collectors.toList()).size();
			census.setContactTotal(contactTotal);

			Integer hotTotal = suffererInfos.stream().filter(a->SuffererTypeEnum.Heat.getCode().equals(a.getSuffererType())).collect(Collectors.toList()).size();
			census.setHotTotal(hotTotal);

			Integer detainedObserTotal = suffererInfos.stream().filter(a->SuffererTypeEnum.Discharged.getCode().equals(a.getSuffererType())).collect(Collectors.toList()).size();
			census.setDetainedObserTotal(detainedObserTotal);
		}

		return census;
	}

	@Override
	public void export(SuffererInfoWebPageDTO dto,HttpServletResponse response, HttpServletRequest request) throws Exception {
		Specification<SuffererInfoPO>  specification = getSpecification(dto);
		List<SuffererInfoDTO> listDto= toDTOList(suffererInfoDao.findAll(specification));
		ExcelExecutor<SuffererInfoDTO> excel = new ExcelExecutor<>(SuffererInfoDTO.class);
		String exprotName = "患者信息表";
		ExcelUtils.setExportExcel(response, request, exprotName, true);
		excel.getListToExcel(listDto, exprotName, response.getOutputStream());

	}

	@Override
	public Result<String> importExcel(MultipartFile file) {
		if (null == file) {
			return Result.error("导入文件为空");
		}
        Workbook workbook = getSheetSize(file);
        // 上传的Excel的页签数量
        int sheetSize = workbook.getNumberOfSheets();
        //循环每页excel
        List<SuffererInfoPO> poList = new ArrayList<>();
        Map<String,Object> map = new HashMap<>();
        try {
            for (int index = 0; index < sheetSize; index++) {
                //第二个sheet页为数据页
                if (1 == index) {
                    // 每个页签创建一个Sheet对象
                    Sheet sheet = workbook.getSheetAt(index);
                    // sheet.getRows()返回该页的总行数
                    int rowCount = sheet.getPhysicalNumberOfRows();
                    log.info("疫情上报信息excel表的总行数=" + rowCount);
                    //遍历每一行
                    for (int r = 2; r < rowCount; r++) { //从第三行开始遍历获取值，从0开始
                        SuffererInfoPO po = new SuffererInfoPO();
                        Row row = sheet.getRow(r);  //获取每一行对象
                        boolean flag = isRowEmpty(row);   //判断该行每个单元格都为空，若为空则跳出当前循环
                        if (flag) {
                            continue;
                        }
                        Date nowDate = new Date();
                        //这里有规范数据总列数为10行，因此写死列数
                        int coloumnNums = 9;
                        for (int c = 0; c < coloumnNums; c++) {  //循环获取每一列的值
                            Cell cell = row.getCell(c);   //获取每一列的值
                            Date DateCellValue = null;
                            String cellValue = "";
                            switch (cell.getCellType()) {
                                case Cell.CELL_TYPE_NUMERIC:
                                    if (HSSFDateUtil.isCellDateFormatted(cell)) {
                                        DateCellValue = cell.getDateCellValue();
                                    } else {
                                        cell.setCellType(HSSFCell.CELL_TYPE_STRING);//强制将每一列的值转换为String
                                        cellValue = cell.getStringCellValue();
                                    }
                                    break;
                                case Cell.CELL_TYPE_STRING: // 字符串
                                    cell.setCellType(HSSFCell.CELL_TYPE_STRING);//强制将每一列的值转换为String
                                    cellValue = cell.getStringCellValue();
                                    break;
                            }
                            if (c == 0) {//姓名
                                po.setSuffererName(cellValue);
                                continue;
                            }
                            if (c == 1) {//身份证号码
                                po.setSuffererCard(cellValue);
                                continue;
                            }
                            if (c == 2) {//办理手机号
                                po.setSuffererTel(cellValue);
                                continue;
                            }
                            if (c == 3) {//由何区转入
                                po.setFromAddress(cellValue);
                                continue;
                            }
                            if (c == 4) {//病人类型
                                if (StringUtils.isNotBlank(cellValue)) {
                                    po.setSuffererType(Integer.parseInt(cellValue));
                                    continue;
                                } else {
                                    po.setSuffererType(null);
                                    continue;
                                }
                            }
                            if (c == 5) {//隔离点所在区
                                po.setAreaName(cellValue);
                                continue;
                            }
                            if (c == 6) {//所在隔离点名称
                                po.setCurSiteName(cellValue);
                                continue;
                            }
                            if (c == 7) {//隔离点床位总数
                                if (StringUtils.isNotBlank(cellValue)) {
                                    po.setSuffererSiteBedNum(Integer.parseInt(cellValue));
                                    continue;
                                } else {
                                    po.setSuffererSiteBedNum(null);
                                    continue;
                                }
                            }
                            if (c == 8) {//进入隔离点日期
                                po.setMoveIntoTime(DateCellValue);
                                continue;
                            }
                        }
                        map.put(po.getSuffererCard(),po);
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            return Result.error("导入失败");
        }
        for (Map.Entry<String,Object> entry : map.entrySet()) {
            SuffererInfoPO newPo = (SuffererInfoPO)entry.getValue();
            poList.add(newPo);
        }
        suffererExcelService.importIsolateExcel(poList);
        return Result.success("导入成功");
	}

	@Override
	public void exportStat(HttpServletResponse response, HttpServletRequest request,String totalDate) throws Exception {
		//查询数据
		if (StringUtils.isEmpty(totalDate)) {
			totalDate = DateUtils.getCurrentDay();
		}
		SuffererCensusJx censusJx = analyseJxSuffererToday(totalDate);
		Date begin = DateUtils.dateToStringBeginOrEnd(totalDate, true);
		Date end = DateUtils.dateToStringBeginOrEnd(totalDate, false);
		List<SuffererAnalysisExportDTO> contactList = SuffererWrapper.toAnalysisExportDTOList(suffererInfoDao.findByAreaCodeAndSuffererStatusAndSuffererTypeAndCreateTimeBetween("420115", SuffererStatusEnum.Isolate.getCode(), 1, begin,end));
		List<SuffererAnalysisExportDTO> hotList = SuffererWrapper.toAnalysisExportDTOList(suffererInfoDao.findByAreaCodeAndSuffererStatusAndSuffererTypeAndCreateTimeBetween("420115", SuffererStatusEnum.Isolate.getCode(), 2, begin,end));
		//读取模板
		org.springframework.core.io.Resource resource = new ClassPathResource("template/sufferer_jx_analysis_template.xls");
		InputStream is = resource.getInputStream();
		Workbook workbook = ExcelUtils.getWorkbook(is);
		SheetExecutor<SuffererAnalysisExportDTO> sheetExecutor = new SheetExecutor<>(SuffererAnalysisExportDTO.class);
		//密接数据
		Sheet contactSheet = workbook.getSheetAt(0);
		workbook.setSheetName(0,contactSheet.getSheetName()+String.valueOf(censusJx.getContactAdd()));
		sheetExecutor.setSheet(contactSheet,contactList);
		//发热数据
		Sheet hotSheet = workbook.getSheetAt(1);
		workbook.setSheetName(1,hotSheet.getSheetName()+String.valueOf(censusJx.getHotAdd()));
		sheetExecutor.setSheet(hotSheet,hotList);
		//写出excel
		ExcelUtils.setExportExcel(response, request, "隔离点数据统计"+totalDate, false);
		ServletOutputStream outputStream = response.getOutputStream();
		outputStream.flush();
		workbook.write(outputStream);
		outputStream.close();
	}

    @Override
    public List<SuffererInfoPO> findAll() {
        return suffererInfoDao.findAll();
    }

	@Override
	@Modifying
	@Transactional(rollbackOn = Exception.class)
	public void saveAll(List<SuffererInfoPO> all) {
		String sql = "update sufferer_info t set t.sufferer_age=?, t.sufferer_gender=? where t.sufferer_card=?";
		jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				SuffererInfoPO po = all.get(i);
				ps.setFloat(1, po.getSuffererAge());
				ps.setString(2, po.getSuffererGender());
				ps.setString(3, po.getSuffererCard());
			}

			@Override
			public int getBatchSize() {
				return all.size();
			}
		});

	}

	@Override
	public List<SuffererIsolationStatisticsDTO> isolatetionStatistics(Date time) {
		String timeStr=DateUtils.format(time,"yyyy-MM-dd");
		List<SuffererIsolationStatisticsDTO> list=new ArrayList<>();
		int touchNum=0;
		int feverNum=0;
		int leaveNum=0;
		if(CollectionUtils.isEmpty(suffererInfoDao.findBySuffererTypeAndDataDate(1, timeStr))){
			if(!CollectionUtils.isEmpty(suffererInfoAllDao.findBySuffererTypeAndDataDate(1, timeStr))){
				touchNum=suffererInfoAllDao.findBySuffererTypeAndDataDate(1, timeStr).size();
			}
		}else{
			touchNum=suffererInfoDao.findBySuffererTypeAndDataDate(1, timeStr).size();
		}
		SuffererIsolationStatisticsDTO dto1=new SuffererIsolationStatisticsDTO();
		dto1.setName("密切接触者");
		dto1.setNum(touchNum);


		if(CollectionUtils.isEmpty(suffererInfoDao.findBySuffererTypeAndDataDate(2, timeStr))){
			if(!CollectionUtils.isEmpty(suffererInfoAllDao.findBySuffererTypeAndDataDate(1, timeStr))){
                feverNum=suffererInfoAllDao.findBySuffererTypeAndDataDate(2, timeStr).size();
			}
		}else{
            feverNum=suffererInfoDao.findBySuffererTypeAndDataDate(2, timeStr).size();
		}
		SuffererIsolationStatisticsDTO dto2=new SuffererIsolationStatisticsDTO();
		dto2.setName("发热人员");
		dto2.setNum(feverNum);

		if(CollectionUtils.isEmpty(suffererInfoDao.findBySuffererTypeAndDataDate(5, timeStr))){
            if(!CollectionUtils.isEmpty(suffererInfoAllDao.findBySuffererTypeAndDataDate(5, timeStr))){
				leaveNum= suffererInfoAllDao.findBySuffererTypeAndDataDate(5, timeStr).size();
			}
        }else{
			leaveNum= suffererInfoDao.findBySuffererTypeAndDataDate(5, timeStr).size();
		}
        SuffererIsolationStatisticsDTO dto3=new SuffererIsolationStatisticsDTO();
        dto3.setName("出院留观");
        dto3.setNum(leaveNum);

		SuffererIsolationStatisticsDTO dto4=new SuffererIsolationStatisticsDTO();
		dto4.setName("当前隔离");
		dto4.setNum(feverNum+touchNum+leaveNum);
		list.add(dto4);
		list.add(dto1);
		list.add(dto2);
		list.add(dto3);
		return list;
	}

	private Workbook getSheetSize(MultipartFile file) {

		InputStream stream = null;
		Workbook workbook = null;
		//获取附件
		try {
			stream = file.getInputStream();
			//利用POI解析Excel
			workbook = WorkbookFactory.create(stream);
		} catch (Exception e) {
			log.error("解析Excel发生异常", e);
			e.printStackTrace();
		}finally{
			try {
				stream.close();
			} catch (IOException e) {
				log.error("解析Excel过程关闭流异常", e);
				e.printStackTrace();
			}
		}
		return workbook;
	}

        /**
         *  判断excel表格中每一行中的每个单元格是否都为空,若都为空，则返回true,否则返回false
         * @param row
         * @return
         */
        private boolean isRowEmpty(Row row){
            for (int i = row.getFirstCellNum(); i < row.getLastCellNum(); i++) {
                Cell cell = row.getCell(i);
                if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK){
                    return false;
                }
            }
            return true;
        }

}
