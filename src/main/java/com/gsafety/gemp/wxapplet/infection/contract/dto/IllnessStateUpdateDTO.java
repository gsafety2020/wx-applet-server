package com.gsafety.gemp.wxapplet.infection.contract.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
* @ClassName: illnessStateUpdateDTO 
* @Description: 更新病情情况入参
* @author luoxiao
* @date 2020年2月28日 下午10:15:05 
*
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("更新病情情况入参")
public class IllnessStateUpdateDTO implements Serializable{

	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;
	
    @ApiModelProperty(value = "患者ID")
	private String id;
	
    @ApiModelProperty(value = "病情. 1.治愈；2死亡；3.危重 4.重症 5.其他")
    private Integer illnessState;
	
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd hh:mm")
	@ApiModelProperty(value = "病情情况改变时间(格式yyyy-MM-dd hh:mm)")
	private Date illnessStateTime;
	
    @ApiModelProperty(value = "用户sessionkey")
    private String sessionKey;

}
