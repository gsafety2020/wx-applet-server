package com.gsafety.gemp.wxapplet.healthcode.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gsafety.gemp.wxapplet.healthcode.dao.po.PestUserAuthorizePO;


/**
 * 
* @Description: 用户权限DAO层
* @author xuelq
* @date 2020年2月27日 下午4:02:37 
*
 */
public interface PestUserAuthorizeDao extends JpaRepository<PestUserAuthorizePO, String>,
JpaSpecificationExecutor<PestUserAuthorizePO> {

	List<PestUserAuthorizePO> findByMobileAndAuthorizeTypeInAndIsDelete(String mobile, String[] auths, String isDelete);
	
	int countByMobileAndAuthorizeTypeInAndIsDelete(String mobile, String[] auths, String isDelete);
	
}
