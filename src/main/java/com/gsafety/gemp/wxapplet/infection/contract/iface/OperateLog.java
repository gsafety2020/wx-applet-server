package com.gsafety.gemp.wxapplet.infection.contract.iface;

import java.lang.annotation.*;

/**
 * @author lzx
 * @version 1.0
 * @date 2020/3/1 18:58
 */
@Target(ElementType.METHOD) //注解放置的目标位置,METHOD是可注解在方法级别上
@Retention(RetentionPolicy.RUNTIME) //注解在哪个阶段执行
@Documented //生成文档
public @interface OperateLog {
    String title() default "";
    String methodType() default "";
}
