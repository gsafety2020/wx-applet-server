package com.gsafety.gemp.wxapplet.utils;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

/**
 * 日期操作工具
 *
 * @author wistronITS
 */
public class DateUtils {
    /**
     * 日期格式：yyyy-MM-dd
     */
    public static String DATE_PATTERN = "yyyy-MM-dd";

    /**
     * 日期格式：yyyyMMdd
     */
    public static String DATE_PATTERN_A = "yyyyMMdd";

    /**
     * 日期时间格式：yyyy-MM-dd HH:mm:ss
     */
    public static String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

    /**
     * 时间格式(24小时制)：HHmmss
     */
    public static String TIME_PATTERN = "HHmmss";

    /**
     * 时间格式(24小时制)：HH:mm:ss
     */
    public static String TIME_PATTERN_24 = "HH:mm:ss";

    /**
     * 时间格式(12小时制)：hh:mm:ss
     */
    public static String TIME_PATTERN_12 = "hh:mm:ss";

    /**
     * 获取年份
     */
    public static void getYear() {
        LocalDateTime localTime = LocalDateTime.now();
        int year = localTime.get(ChronoField.YEAR);
        System.out.println(year);
    }

    /**
     * 获取月份
     */
    public static void getMonth() {
        LocalDateTime localTime = LocalDateTime.now();
        int month = localTime.get(ChronoField.MONTH_OF_YEAR);
        System.out.println(month);
    }

    /**
     * 获取某月的第几天
     */
    public static void getMonthOfDay() {
        LocalDateTime localTime = LocalDateTime.now();
        int day = localTime.get(ChronoField.DAY_OF_MONTH);
        System.out.println(day);
    }

    /**
     * 格式化日期为字符串
     *
     * @param date    需要格式化的日期
     * @param pattern 格式，如：yyyy-MM-dd
     * @return 日期字符串
     */
    public static String format(Date date, String pattern) {
        Instant instant = date.toInstant();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
        return localDateTime.format(DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * 格式化日期为字符串
     *
     * @param date    需要格式化的日期
     * @param pattern 格式，如：yyyy-MM-dd
     * @return 日期字符串
     */
    public static String format(Date date, String pattern, int day) {
        Instant instant = date.toInstant();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).plusDays(day);
        return localDateTime.format(DateTimeFormatter.ofPattern(pattern));
    }

    public static LocalDateTime format(Date date) {
        Instant instant = date.toInstant();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
        return localDateTime;
    }



    /**
     * 解析字符串日期为Date
     *
     * @param dateStr 日期字符串
     * @param pattern 格式，如：yyyy-MM-dd
     * @return Date
     */
    public static Date parse(String dateStr, String pattern) {
        LocalDateTime localDateTime = LocalDateTime.parse(dateStr, DateTimeFormatter.ofPattern(pattern));
        Instant instant = localDateTime.atZone(ZoneId.systemDefault()).toInstant();
        return Date.from(instant);
    }

    /**
     * 解析字符串日期为Date
     *
     * @param dateStr 日期字符串
     * @param pattern 格式，如：yyyy-MM-dd
     * @return Date
     */
    public static Date parseEndOfDay(String dateStr, String pattern) {
        LocalDateTime localDateTime = LocalDateTime.of(LocalDate.parse(dateStr, DateTimeFormatter.ofPattern(pattern)), LocalTime.MAX.withNano(0));
//        LocalDateTime localDateTime = LocalDateTime.parse(dateStr, DateTimeFormatter.ofPattern(pattern));
        Instant instant = localDateTime.atZone(ZoneId.systemDefault()).toInstant();

        return Date.from(instant);
    }

    /**
     * 为Date增减分钟(减传负数)
     *
     * @param date    日期
     * @param minutes 要增减的分钟数
     * @return 新的日期
     */
    public static Date addReduceMinutes(Date date, Long minutes) {
        LocalDateTime dateTime = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
        LocalDateTime newDateTime = dateTime.plusMinutes(minutes);
        return Date.from(newDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * 增加时间
     *
     * @param date date
     * @param hour 要增加的小时数
     * @return 新的日期
     */
    public static Date addHour(Date date, Long hour) {
        LocalDateTime dateTime = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
        LocalDateTime localDateTime = dateTime.plusHours(hour);
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * 增加天数
     * @param date
     * @param days
     * @return
     */
    public static Date addDays(Date date , BigDecimal days){
        if(days == null){
            return null;
        }
        Long seconds = days.multiply(new BigDecimal(86400)).setScale(0,BigDecimal.ROUND_HALF_UP).longValue();
        LocalDateTime dateTime = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
        LocalDateTime localDateTime = dateTime.plusSeconds(seconds);
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * 当天的起始时间
     *
     * @return 如：Tue Jun 11 00:00:00 CST 2019
     */
    public static Date getStartTime() {
        LocalDateTime now = LocalDateTime.now().withHour(0).withMinute(0).withSecond(0);
        return localDateTime2Date(now);
    }

    /**
     * 当天的结束时间
     *
     * @return 如：Tue Jun 11 23:59:59 CST 2019
     */
    public static Date getEndTime() {
        LocalDateTime now = LocalDateTime.now().withHour(23).withMinute(59).withSecond(59).withNano(999);
        return localDateTime2Date(now);
    }

    /**
     * 减月份
     *
     * @param monthsToSubtract 月份
     * @return Date
     */
    public static Date minusMonths(long monthsToSubtract) {
        LocalDate localDate = LocalDate.now().minusMonths(monthsToSubtract);
        return localDate2Date(localDate);
    }

    /**
     * LocalDate类型转为Date
     *
     * @param localDate
     * @return
     */
    public static Date localDate2Date(LocalDate localDate) {
        ZonedDateTime zonedDateTime = localDate.atStartOfDay(ZoneId.systemDefault());
        return Date.from(zonedDateTime.toInstant());
    }

    /**
     * LocalDateTime类型转为Date
     *
     * @param localDateTime
     * @return Date
     */
    public static Date localDateTime2Date(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * 查询当前年的第一天
     *
     * @param pattern 格式，如：yyyyMMdd
     * @return 20190101
     */
    public static String getFirstDayOfCurrentYear(String pattern) {
        LocalDateTime localDateTime = LocalDateTime.now().withMonth(1).withDayOfMonth(1);
        return format(localDateTime2Date(localDateTime), StringUtils.isEmpty(pattern) ? DATE_PATTERN_A : pattern);
    }

    public static String getFirstDayOfCurrentYear() {
        LocalDateTime localDateTime = LocalDateTime.now().withMonth(1).withDayOfMonth(1);
        return format(localDateTime2Date(localDateTime), DATE_PATTERN_A);
    }

    /**
     * 查询前一年最后一个月第一天
     *
     * @param pattern 格式，如：yyyyMMdd
     * @return 20190101
     */
    public static String getLastMonthFirstDayOfPreviousYear(String pattern) {
        LocalDateTime localDateTime = LocalDateTime.now().minusYears(1L).withMonth(12).withDayOfMonth(1);
        return format(localDateTime2Date(localDateTime), StringUtils.isEmpty(pattern) ? DATE_PATTERN_A : pattern);
    }

    public static String getLastMonthFirstDayOfPreviousYear() {
        LocalDateTime localDateTime = LocalDateTime.now().minusYears(1L).withMonth(12).withDayOfMonth(1);
        return format(localDateTime2Date(localDateTime), DATE_PATTERN_A);
    }

    /**
     * 查询前一年最后一个月的最后一天
     *
     * @param pattern 格式，如：yyyyMMdd
     * @return 20190101
     */
    public static String getLastMonthLastDayOfPreviousYear(String pattern) {
        LocalDateTime localDateTime = LocalDateTime.now().minusYears(1L).with(TemporalAdjusters.lastDayOfYear());
        return format(localDateTime2Date(localDateTime), StringUtils.isEmpty(pattern) ? DATE_PATTERN_A : pattern);
    }

    public static String getLastMonthLastDayOfPreviousYear() {
        LocalDateTime localDateTime = LocalDateTime.now().minusYears(1L).with(TemporalAdjusters.lastDayOfYear());
        return format(localDateTime2Date(localDateTime), DATE_PATTERN_A);
    }

    /**
     * 获取当前日期
     *
     * @param pattern 格式，如：yyyy-MM-dd
     * @return 2019-01-01
     */
    public static String getCurrentDay(String pattern) {
        LocalDateTime localDateTime = LocalDateTime.now();
        return format(localDateTime2Date(localDateTime), StringUtils.isEmpty(pattern) ? DATE_PATTERN : pattern);
    }

    public static String getCurrentDay() {
        LocalDateTime localDateTime = LocalDateTime.now();
        return format(localDateTime2Date(localDateTime), DATE_PATTERN);
    }

    public static String getCurrentDateTime() {
        LocalDateTime localDateTime = LocalDateTime.now();
        return format(localDateTime2Date(localDateTime), DATE_TIME_PATTERN);
    }

    public static String getCurrentDateTime(String pattern) {
        LocalDateTime localDateTime = LocalDateTime.now();
        return format(localDateTime2Date(localDateTime), pattern);
    }

    /**
     * Date 与 Localdatetime 的转换
     */
    @SuppressWarnings("unused")
	public static void transformWithDate() {
        Date date = new Date();
        LocalDateTime localDateTime = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        Date date1 = Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * 格式化时间-默认yyyy-MM-dd HH:mm:ss格式
     *
     * @param dateTime LocalDateTime对象
     * @param pattern  要格式化的字符串
     * @return
     */
    public static String formatDateTime(LocalDateTime dateTime, String pattern) {
        if (dateTime == null) {
            return null;
        }
        return dateTime.format(DateTimeFormatter.ofPattern(StringUtils.isEmpty(pattern) ? DATE_TIME_PATTERN : pattern));
    }

    public static String formatDateTime(LocalDateTime dateTime) {
        return formatDateTime(dateTime, DATE_TIME_PATTERN);
    }

    /**
     * 获取某天的00:00:00
     *
     * @param dateTime
     * @return
     */
    public static String getDayStart(LocalDateTime dateTime) {
        return formatDateTime(dateTime.with(LocalTime.MIN));
    }

    public static String getDayStart() {
        return getDayStart(LocalDateTime.now());
    }

    /**
     * 获取某天的23:59:59
     *
     * @param dateTime
     * @return
     */
    public static String getDayEnd(LocalDateTime dateTime) {
        return formatDateTime(dateTime.with(LocalTime.MAX));
    }

    public static String getDayEnd() {
        return getDayEnd(LocalDateTime.now());
    }

    /**
     * 获取某月第一天的00:00:00
     *
     * @param dateTime LocalDateTime对象
     * @return
     */
    public static String getFirstDayOfMonth(LocalDateTime dateTime) {
        return formatDateTime(dateTime.with(TemporalAdjusters.firstDayOfMonth()).with(LocalTime.MIN));
    }

    public static String getFirstDayOfMonth() {
        return getFirstDayOfMonth(LocalDateTime.now());
    }

    /**
     * 获取某月最后一天的23:59:59
     *
     * @param dateTime LocalDateTime对象
     * @return
     */
    public static String getLastDayOfMonth(LocalDateTime dateTime) {
        return formatDateTime(dateTime.with(TemporalAdjusters.lastDayOfMonth()).with(LocalTime.MAX));
    }

    public static String getLastDayOfMonth() {
        return getLastDayOfMonth(LocalDateTime.now());
    }

    /**
     * 获得x周的开始时间 0本周 1，上周 2上上周
     *
     * @param week
     * @return
     */
    public static Date getBeginDayOfLastWeek(int week) {
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int dayofweek = cal.get(Calendar.DAY_OF_WEEK);
        if (dayofweek == 1) {
            dayofweek += 7;
        }
        cal.add(Calendar.DATE, 2 - dayofweek - week * 7);
        return getDayStartTime(cal.getTime());
    }

    /**
     * 获得x周的结束时间 0本周 1，上周 2上上周
     *
     * @param week
     * @return
     */
    public static Date getEndDayOfLastWeek(int week) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getBeginDayOfLastWeek(week));
        cal.add(Calendar.DAY_OF_WEEK, 6);
        Date weekEndSta = cal.getTime();
        return getDayEndTime(weekEndSta);
    }

    // 获取某个日期的开始时间
    public static Timestamp getDayStartTime(Date d) {
        Calendar calendar = Calendar.getInstance();
        if (null != d) {
            calendar.setTime(d);
        }
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return new Timestamp(calendar.getTimeInMillis());
    }


    // 获取某个日期的结束时间
    public static Timestamp getDayEndTime(Date d) {
        Calendar calendar = Calendar.getInstance();
        if (null != d) {
            calendar.setTime(d);
        }
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH), 23, 59, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return new Timestamp(calendar.getTimeInMillis());
    }

    public static Date toDateFormatDate(String date) {
        SimpleDateFormat format = new SimpleDateFormat(DATE_PATTERN);
        Date baseDate = null;
        try {
            baseDate = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return baseDate;
    }

    /**
     * 根据开始时间-结束时间计算中间间隔日期
     *
     * @param startDate 2019-05-25
     * @param endDate   2019-06-01
     * @return
     */
    public static List<String> findDates(LocalDate startDate, LocalDate endDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        List<String> timeList = Lists.newArrayList();

        long distance = ChronoUnit.DAYS.between(startDate, endDate);
        if (distance < 1) {
            return timeList;
        }
        Stream.iterate(startDate, d -> {
            return d.plusDays(1);
        }).limit(distance + 1).forEach(f -> {
            timeList.add(formatter.format(f));
        });
        return timeList;
    }

    /**
     * localDate转String
     *
     * @param localDate
     * @return
     */
    public static String format(LocalDate localDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_PATTERN_A);
        return formatter.format(localDate);
    }

    public static LocalDate parseToLocalDate(String strDate, String pattern) {
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern(pattern);
        return LocalDate.parse(strDate, fmt);
    }

    public static ArrayList<String> getListPastDateString(int intervals,String partten) {
        ArrayList<String> pastDaysList = new ArrayList<>();
        for (int i = 0; i <intervals; i++) {
            pastDaysList.add(getPastDate(i,partten));
        }
        return pastDaysList;
    }

    /**
     * 获取过去第几天的日期
     *
     * @param past
     * @return
     */
    public static String getPastDate(int past,String partten) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - past);
        Date today = calendar.getTime();
        SimpleDateFormat format = new SimpleDateFormat(partten);
        String result = format.format(today);
        return result;
    }

    public static Date getYesterdayDate(){
    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    	Calendar calendar = Calendar.getInstance();
    	calendar.add(Calendar.DATE, -1);
    	String dateFormat = df.format(calendar.getTime());
    	Date date =  null;
    	try {
			 date = df.parse(dateFormat);
		} catch (ParseException e) {
			e.printStackTrace();
		}
    	return date;
    }

    public static String getYesterdayString(){
    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    	Calendar calendar = Calendar.getInstance();
    	calendar.add(Calendar.DATE, -1);
    	String dateFormat = df.format(calendar.getTime());
		return dateFormat;
    }

    public static String getBeforeYesterdayString(){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -2);
        String dateFormat = df.format(calendar.getTime());
        return dateFormat;
    }

    /*
    获取某一日期0点0分0秒 或者 23点59分59秒 时间
     */
    public static Date dateToStringBeginOrEnd(String date,Boolean flag) {
        Date time ;
       try {
           SimpleDateFormat dateformat1 = new SimpleDateFormat("yyyy-MM-dd");
           Date date1 = dateformat1.parse(date);
           Calendar calendar1 = Calendar.getInstance();
           //获取某一天的0点0分0秒 或者 23点59分59秒
           if (flag == true) {
               calendar1.setTime(date1);
               calendar1.set(calendar1.get(Calendar.YEAR), calendar1.get(Calendar.MONTH), calendar1.get(Calendar.DAY_OF_MONTH),
                       0, 0, 0);
               time = calendar1.getTime();
               //time = dateformat1.format(beginOfDate);
           } else {
               Calendar calendar2 = Calendar.getInstance();
               calendar2.setTime(date1);
               calendar1.set(calendar2.get(Calendar.YEAR), calendar2.get(Calendar.MONTH), calendar2.get(Calendar.DAY_OF_MONTH),
                       23, 59, 59);
               time = calendar1.getTime();
               //time = dateformat1.format(endOfDate);
           }
       }catch (ParseException ex){
           time=null;
       }
        return time;
    }


    public static Date getYesterdayBeginTime(){
    	Calendar calendar = Calendar.getInstance();
    	calendar.add(Calendar.DATE, -1);
    	calendar.set(Calendar.HOUR_OF_DAY,0);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,0);
		return calendar.getTime();
    }

    public static Date getYesterdayEndTime(){
    	Calendar calendar = Calendar.getInstance();
    	calendar.set(Calendar.HOUR_OF_DAY,23);
        calendar.set(Calendar.MINUTE,59);
        calendar.set(Calendar.SECOND,59);
		return calendar.getTime();
    }


    public static Date getTodayBeginTime(){
    	Calendar calendar = Calendar.getInstance();
    	calendar.add(Calendar.DATE, 0);
    	calendar.set(Calendar.HOUR_OF_DAY,0);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,0);
		return calendar.getTime();
    }

    public static Date getTodayEndTime(){
    	Calendar calendar = Calendar.getInstance();
    	calendar.add(Calendar.DATE, 0);
    	calendar.set(Calendar.HOUR_OF_DAY,23);
        calendar.set(Calendar.MINUTE,59);
        calendar.set(Calendar.SECOND,59);
		return calendar.getTime();
    }

    public static Date parseDate(Date date,String partten){
    	SimpleDateFormat simple = new SimpleDateFormat(partten);
    	String formatDate = simple.format(date);
    	Date target = null;
    	try {
    		target = simple.parse(formatDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return target;
    }

}
