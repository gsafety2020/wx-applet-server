package com.gsafety.gemp.wxapplet.infection.service;

import com.gsafety.gemp.common.excel.ExcelUtils;
import com.gsafety.gemp.wxapplet.infection.contract.dto.*;
import com.gsafety.gemp.wxapplet.infection.contract.enums.CdcTraceTypeEnum;
import com.gsafety.gemp.wxapplet.infection.contract.iface.CdcSuffererTraceService;
import com.gsafety.gemp.wxapplet.infection.contract.iface.TraceService;
import com.gsafety.gemp.wxapplet.infection.contract.params.CdcSuffererNewlySearchParam;
import com.gsafety.gemp.wxapplet.infection.contract.params.TraceSearchParam;
import com.gsafety.gemp.wxapplet.infection.dao.CdcSuffererInfoAllDao;
import com.gsafety.gemp.wxapplet.infection.dao.CdcSuffererInfoDao;
import com.gsafety.gemp.wxapplet.infection.dao.CdcSuffererTraceDao;
import com.gsafety.gemp.wxapplet.infection.dao.LatestAdressInfoDao;
import com.gsafety.gemp.wxapplet.infection.dao.po.CdcSuffererInfo;
import com.gsafety.gemp.wxapplet.infection.dao.po.CdcSuffererInfoAll;
import com.gsafety.gemp.wxapplet.infection.dao.po.CdcSuffererTrace;
import com.gsafety.gemp.wxapplet.infection.dao.po.LatestAdressInfoPO;
import com.gsafety.gemp.wxapplet.infection.wrapper.CdcSuffererInfoWrapper;
import com.gsafety.gemp.wxapplet.utils.DateUtils;
import com.gsafety.gemp.wxapplet.utils.SheetExecutor;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

@Service
public class CdcSuffererTraceServiceImpl implements CdcSuffererTraceService {

    @Autowired
    private CdcSuffererInfoDao cdcSuffererInfoDao;
    @Autowired
    private CdcSuffererInfoAllDao cdcSuffererInfoAllDao;
    @Autowired
    private CdcSuffererTraceDao dao;
    @Autowired
    private LatestAdressInfoDao latestAdressInfoDao;
    @Autowired
    private TraceService traceService;


    @Override
    public CdcSuffererTraceDTO getTraceList(String suffererId) {
        CdcSuffererTraceDTO traceDTO = new CdcSuffererTraceDTO();
        List<CdcSuffererTracesDTO> list = new ArrayList<>();

        Map<String,String > hospitalNameMap = new HashMap<>();
        List<CdcSuffererTrace> traces = dao.findAllBySuffererIdNoOrderByChangeTimeAscChangeTypeDesc(suffererId);
        for (CdcSuffererTrace trace : traces) {
            if(hospitalNameMap.containsKey(trace.getHospitalName())){
                list.add(buildTracesDTO(trace,false));
            }else{
                hospitalNameMap.put(trace.getHospitalName(),"1");
                list.add(buildTracesDTO(trace,true));
            }
            traceDTO.setSuffererName(trace.getSuffererName());
        }
        traceDTO.setList(list);
        return traceDTO;
    }

    @Override
    public CdcSuffererTraceDTO getTraceList(String suffererId,String suffererName) {
        CdcSuffererTraceDTO traceDTO = new CdcSuffererTraceDTO();
        traceDTO.setSuffererName(suffererName);
        List<CdcSuffererTracesDTO> list = new ArrayList<>();
        if (StringUtils.isNotEmpty(suffererId)) {
            Map<String,String > hospitalNameMap = new HashMap<>();
            List<CdcSuffererTrace> traces = dao.findAllBySuffererIdNoOrderByChangeTimeAscChangeTypeDesc(suffererId);
            for (CdcSuffererTrace trace : traces) {
                if(hospitalNameMap.containsKey(trace.getHospitalName())){
                    list.add(buildTracesDTO(trace,false));
                }else{
                    hospitalNameMap.put(trace.getHospitalName(),"1");
                    list.add(buildTracesDTO(trace,true));
                }
            }
        }
        traceDTO.setList(list);
        return traceDTO;
    }


    @Override
    public Page<CdcSuffererTracesDTO> getTracePage(TraceSearchParam dto){
        Sort sort = Sort.by(Sort.Order.asc("changeTime"), Sort.Order.desc("changeType"));
        PageRequest pageRequest = PageRequest.of(dto.getNowPage(), dto.getPageSize(),sort);

        List<CdcSuffererTracesDTO> list = new ArrayList<>();

        Page<CdcSuffererTrace> page = this.dao.findAll(concatSpecification(dto), pageRequest);
        for (CdcSuffererTrace trace : page.getContent()) {
            list.add(buildTracesDTO(trace,false));
        }
        return new PageImpl<>(list, pageRequest, page.getTotalElements());
    }




    private CdcSuffererTracesDTO buildTracesDTO(CdcSuffererTrace po,boolean flag){
        if(po.getChangeType().equals(CdcTraceTypeEnum.TYEP_ZHUANCHU.getCode()) ){
            return CdcSuffererTracesDTO.builder()
                    .hospitalName(flag ? po.getHospitalName() : null )
                    .changeType("状态").changeInfo(CdcTraceTypeEnum.getNameByCode(po.getChangeType()) + po.getHospitalName())
                    .changeTime(po.getChangeTime()).build();
        }
        if (po.getChangeType().equals(CdcTraceTypeEnum.TYEP_CHUYUAN.getCode())){
            return CdcSuffererTracesDTO.builder()
                    .hospitalName(flag ? po.getHospitalName() : null )
                    .changeType("状态").changeInfo("从" + po.getHospitalName() +CdcTraceTypeEnum.getNameByCode(po.getChangeType()) )
                    .changeTime(po.getChangeTime()).build();
        }
        if( po.getChangeType().equals(CdcTraceTypeEnum.TYPE_ZHUANRU.getCode())){
            return CdcSuffererTracesDTO.builder()
                    .hospitalName(flag ? po.getHospitalName() : null )
                    .changeType("状态").changeInfo(CdcTraceTypeEnum.getNameByCode(po.getChangeType()) + po.getHospitalName())
                    .changeTime(po.getChangeTime()).build();
        }
        if (po.getChangeType().equals(CdcTraceTypeEnum.TYPE_JIUZHEN.getCode())){
            return CdcSuffererTracesDTO.builder()
                    .hospitalName(flag ? po.getHospitalName() : null )
                    .changeType("状态").changeInfo(CdcTraceTypeEnum.getNameByCode(po.getChangeType()) +"-"+ po.getHospitalName() )
                    .changeTime(po.getChangeTime()).build();
        }
        if(po.getChangeType().equals(CdcTraceTypeEnum.TYPE_SIWANG.getCode()) && "2".equals(po.getSourceType())){
            return CdcSuffererTracesDTO.builder()
                    .hospitalName(flag ? po.getHospitalName() : null )
                    .changeType("状态").changeInfo(CdcTraceTypeEnum.getNameByCode(po.getChangeType()) +"-"+ po.getHospitalName() )
                    .changeTime(po.getChangeTime()).build();
        }

        return CdcSuffererTracesDTO.builder()
                    .hospitalName(flag ? po.getHospitalName() : null )
                    .changeType("病例").changeInfo(CdcTraceTypeEnum.getNameByCode(po.getChangeType()))
                    .changeTime(po.getChangeTime()).build();
    }



    /**
     *
     * @param dataDate  yyyy-MM-dd
     * @param type  1，临床诊断 2，确诊 3，阳性检测 4，疑似 5.死亡 6.转入,7.转出
     * @return
     */
    @Override
    public Integer statisticsNewly(String dataDate, String type) {
        return dao.queryCountByChangeTimeAndChangeType(dataDate,type);
    }

    @Override
    public Page<CdcSuffererInfoDTO> getNewlyList(CdcSuffererNewlySearchParam dto) {
        dto.setSourceType("3");
        PageRequest pageRequest = PageRequest.of(dto.getNowPage(), dto.getPageSize());
        Page<CdcSuffererTrace> page = this.dao.findAll(concatSpecification(dto), pageRequest);
        boolean exists = cdcSuffererInfoDao.existsByDataDate(dto.getDataDate());
        List<CdcSuffererInfoDTO> list = new ArrayList<>();
        if(exists) {
            for (CdcSuffererTrace trace : page.getContent()) {
                CdcSuffererInfo info = this.cdcSuffererInfoDao.findCdcSuffererInfoByIdNoAndDataDate(trace.getSuffererIdNo(),dto.getDataDate());
                if(info != null ){
                    CdcSuffererInfoDTO infoDto = CdcSuffererInfoWrapper.toDTO(info);
                    //获取当前位置

                    infoDto.setCurLocation(traceService.getLocation(infoDto.getIdNo()));
                    list.add(CdcSuffererInfoWrapper.toDTO(info));
                }
            }
        } else {
            for (CdcSuffererTrace trace : page.getContent()) {
                CdcSuffererInfoAll info = cdcSuffererInfoAllDao.findCdcSuffererInfoByIdNoAndDataDate(trace.getSuffererIdNo(),dto.getDataDate());
                if(info != null ){
                    CdcSuffererInfoDTO infoDto = CdcSuffererInfoWrapper.toDTO(info);
                    //获取当前位置

                    infoDto.setCurLocation(traceService.getLocation(infoDto.getIdNo()));
                    list.add(CdcSuffererInfoWrapper.toDTO(info));
                }
            }
        }

        return new PageImpl<>(list, pageRequest, page.getTotalElements());
    }

    @Override
    public void statExport(HttpServletResponse response, HttpServletRequest request, String date) throws Exception {
        if (StringUtils.isEmpty(date)) {
            date = DateUtils.getCurrentDay();
        }
        CdcSuffererNewlySearchParam searchParam = CdcSuffererNewlySearchParam.builder().dataDate(date).build();
        //读取模板
        org.springframework.core.io.Resource resource = new ClassPathResource("template/cdc_sufferer_stat_template.xls");
        InputStream is = resource.getInputStream();
        Workbook workbook = ExcelUtils.getWorkbook(is);
        SheetExecutor<CdcSuffererInfoStatExportDTO> sheetExecutor = new SheetExecutor<>(CdcSuffererInfoStatExportDTO.class);
        for (int i = 0; i < 7; i++) {
            String type = String.valueOf(i + 1);
            Sheet sheet = workbook.getSheetAt(i);
            Integer count = statisticsNewly(date, type);
            workbook.setSheetName(i,sheet.getSheetName()+String.valueOf(count));
            searchParam.setType(type);
            List<CdcSuffererTrace> traceList = this.dao.findAll(concatSpecification(searchParam));
            List<CdcSuffererInfoStatExportDTO> list = new ArrayList();
            for (CdcSuffererTrace trace : traceList) {
                CdcSuffererInfo info = this.cdcSuffererInfoDao.findCdcSuffererInfoByIdNoAndDataDate(trace.getSuffererIdNo(), trace.getChangeTime());
                if(info != null ){
                    list.add(CdcSuffererInfoWrapper.toStatExportDTO(info));
                }
            }
            sheetExecutor.setSheet(sheet,list);
        }
        //写出excel
        ExcelUtils.setExportExcel(response, request, "疾控中心数据统计"+date, false);
        ServletOutputStream outputStream = response.getOutputStream();
        outputStream.flush();
        workbook.write(outputStream);
        outputStream.close();
    }

    @Override
    public void traceExport(String suffererIdNo, String suffererName, HttpServletResponse response, HttpServletRequest request) throws Exception {
        //获取数据
        CdcSuffererTraceDTO cdcSuffererTraceDTO = getTraceList(suffererIdNo, suffererName);
        //获取模板
        Resource resource = new ClassPathResource("template/sufferer_trace_template.xls");
        InputStream is = resource.getInputStream();
        Workbook workbook = ExcelUtils.getWorkbook(is);
        Sheet sheet = workbook.getSheetAt(0);
        //姓名数据
        sheet.getRow(0).getCell(1).setCellValue(cdcSuffererTraceDTO.getSuffererName());
        //列表数据
        SheetExecutor<CdcSuffererTracesDTO> sheetExecutor = new SheetExecutor<>(CdcSuffererTracesDTO.class);
        sheetExecutor.setSheet(sheet,cdcSuffererTraceDTO.getList(),1,1);
        //写出excel
        ExcelUtils.setExportExcel(response, request, "患者溯源"+cdcSuffererTraceDTO.getSuffererName() , true);
        ServletOutputStream outputStream = response.getOutputStream();
        outputStream.flush();
        workbook.write(outputStream);
        outputStream.close();
    }


    private Specification<CdcSuffererTrace> concatSpecification(CdcSuffererNewlySearchParam dto){
        return (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(cb.like(root.get("changeTime"), dto.getDataDate()+"%"));
            predicates.add(cb.equal(root.get("changeType"),dto.getType()));
            predicates.add(cb.equal(root.get("sourceType"),dto.getSourceType()));
            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

    private Specification<CdcSuffererTrace> concatSpecification(TraceSearchParam dto){
        return (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            if(StringUtils.isNotEmpty(dto.getIdNo())){
                predicates.add(cb.equal(root.get("suffererIdNo"),dto.getIdNo()));
            }
            if(StringUtils.isNotEmpty(dto.getSuffererName())){
                predicates.add(cb.equal(root.get("suffererName"),dto.getSuffererName()));
            }
            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }




}
