package com.gsafety.gemp.wxapplet.web;

import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.gsafety.gemp.wxapplet.annotation.PassToken;
import com.gsafety.gemp.wxapplet.infection.dao.UserDao;
import com.gsafety.gemp.wxapplet.infection.dao.po.UserPO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.Objects;

/**
 * 创建时间：2019-05-11 14:17 <br>
 * @author Administrator
 */
@Component
@Slf4j
public class AuthenticationInterceptor implements HandlerInterceptor {


    @Resource
    private UserDao userDao;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        String token = request.getHeader("token");
        if (!(handler instanceof HandlerMethod) || StringUtils.isBlank(request.getHeader("client"))) {
            return true;
        }

        Method method = ((HandlerMethod) handler).getMethod();
        if (method.isAnnotationPresent(PassToken.class)) {
            return true;
        }

        if (StringUtils.isBlank(token)) {
            throw new RuntimeException("无效token，请重新登录!");
        }

        try {
            String userId = JWT.decode(token).getAudience().get(0);
            UserPO user = userDao.findById(userId).get();
            if (Objects.isNull(user)) {
                throw new RuntimeException("用户不存在，请重新登录");
            }

            if(StringUtils.isEmpty(user.getToken())) {
                throw new RuntimeException("用户已注销，请重新登录");
            }

            UserContext.setUser(user);

        } catch (JWTDecodeException j) {
            throw new RuntimeException("401非法token");
        }

        return true;
    }

}
