package com.gsafety.gemp.wxapplet.infection.dao;

import com.gsafety.gemp.wxapplet.infection.dao.po.OperateLogPO;
import com.gsafety.gemp.wxapplet.infection.dao.po.PointHospitalInfoPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;


public  interface PointHospitalInfoDao  extends JpaRepository<PointHospitalInfoPO, String>,
        JpaSpecificationExecutor<PointHospitalInfoPO> {


    PointHospitalInfoPO findByIdNo(String idNo);

    List<PointHospitalInfoPO> findAllByIdNoOrderByMoveIntoTimeDesc(String idNo);

    List<PointHospitalInfoPO> findAllByIdNoOrderByDataDateDesc(String idNo);

    List<PointHospitalInfoPO> findBySuffererNameOrderByCreateTimeDesc(String suffererName);

    List<PointHospitalInfoPO> findByDataDate(String dataDate);

    List<PointHospitalInfoPO> findBySuffererName(String suffererName);

}
