package com.gsafety.gemp.wxapplet.infection.contract.iface;

import com.gsafety.gemp.wxapplet.healthcode.contract.dto.Result;

public interface TraceService {
    Result<Object> saveTraceSuffererInfoData(String dataDate);

    void generateFangCangTrace(String dataDate);

    void generatePointTrace(String dataDate);

    void generateSiteTrace(String dataDate);

    void genrateFeverTrace(String dataDate);
    String getLocation(String idNo);

}
