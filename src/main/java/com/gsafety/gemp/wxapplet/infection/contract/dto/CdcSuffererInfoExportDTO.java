package com.gsafety.gemp.wxapplet.infection.contract.dto;

import com.gsafety.gemp.common.excel.ExcelAttribute;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CdcSuffererInfoExportDTO {

    private String id;

    @ExcelAttribute(name="卡片ID")
    private String cardId;

    @ExcelAttribute(name="卡片编号")
    private String cardCode;

    @ExcelAttribute(name="卡片状态")
    private String cardStatus;

    @ExcelAttribute(name="患者姓名")
    private String suffererName;

    @ExcelAttribute(name="患儿家长姓名")
    private String suffererParent;

    @ExcelAttribute(name="有效证件号")
    private String idNo;

    @ExcelAttribute(name="性别")
    private String sex;

    @ExcelAttribute(name="出生日期")
    private String birthday;

    @ExcelAttribute(name="年龄")
    private String age;

    @ExcelAttribute(name="患者工作单位")
    private String workUnit;

    @ExcelAttribute(name="联系电话")
    private String telphone;

    @ExcelAttribute(name="病人属于")
    private String location;

    @ExcelAttribute(name="现住地址国标")
    private String addressCode;

    @ExcelAttribute(name="现住详细地址")
    private String detailedAddress;

    @ExcelAttribute(name="人群分类")
    private String occupation;

    private String caseType;

    @ExcelAttribute(name="病例分类")
    private String caseTypeName;

    @ExcelAttribute(name="病例分类2")
    private String caseType2;

    @ExcelAttribute(name="发病日期")
    private String onsetDate;

    @ExcelAttribute(name="诊断时间")
    private String caseDate;

    @ExcelAttribute(name="死亡日期")
    private String deadthDate;

    @ExcelAttribute(name="疾病名称")
    private String diseaseName;

    @ExcelAttribute(name="订正前病种")
    private String beforeRevisionName;

    @ExcelAttribute(name="订正前诊断时间")
    private String beforeRevisionCaseDate;

    @ExcelAttribute(name="订正前终审时间")
    private String fjbrDate;

    @ExcelAttribute(name="填卡医生")
    private String fillInDoctor;

    @ExcelAttribute(name="医生填卡日期")
    private String fillInDate;

    @ExcelAttribute(name="报告单位地区编码")
    private String reportUnitCode;

    @ExcelAttribute(name="报告单位")
    private String reportUnit;

    @ExcelAttribute(name="单位类型")
    private String unitType;

    @ExcelAttribute(name="报告卡录入时间")
    private String reportFillInDate;

    @ExcelAttribute(name="录卡用户")
    private String reportFillInUser;

    @ExcelAttribute(name="录卡用户所属单位")
    private String reportFillInUnit;

    @ExcelAttribute(name="县区审核时间")
    private String districtReviewDate;

    @ExcelAttribute(name="地市审核时间")
    private String cityReviewDate;

    @ExcelAttribute(name="省市审核时间")
    private String provinceReviewDate;

    @ExcelAttribute(name="审核状态")
    private String reviewStatus;

    @ExcelAttribute(name="订正报告时间")
    private String reportRevisionDate;

    @ExcelAttribute(name="订正终审时间")
    private String revisionFinalJudgmentDate;

    @ExcelAttribute(name="终审死亡时间")
    private String finalJudgmentDeathDate;

    @ExcelAttribute(name="订正用户")
    private String revisionUser;

    @ExcelAttribute(name="订正用户所属单位")
    private String revisionUserUnit;

    @ExcelAttribute(name="删除时间")
    private String deleteDate;

    @ExcelAttribute(name="删除用户")
    private String deleteUser;

    @ExcelAttribute(name="删除用户所属单位")
    private String deleteUserUnit;

    @ExcelAttribute(name="删除原因")
    private String deleteReason;

    @ExcelAttribute(name="备注")
    private String rmark;

    @ExcelAttribute(name="临床严重程度")
    private String clinicalSeverity;

    @ExcelAttribute(name="转诊状态")
    private String referralType;

    @ExcelAttribute(name="收治机构")
    private String receiveOrg;

    private String importDate;

    private Date creatTime;

    private Date updateTime;


}
