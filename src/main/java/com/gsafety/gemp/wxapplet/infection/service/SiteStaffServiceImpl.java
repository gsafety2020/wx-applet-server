package com.gsafety.gemp.wxapplet.infection.service;

import java.util.Calendar;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.druid.util.StringUtils;
import com.gsafety.gemp.wxapplet.infection.contract.enums.SiteStaffRelationStatus;
import com.gsafety.gemp.wxapplet.infection.contract.iface.SiteStaffService;
import com.gsafety.gemp.wxapplet.infection.dao.SiteStaffDao;
import com.gsafety.gemp.wxapplet.infection.dao.po.SiteStaffPO;
import com.gsafety.gemp.wxapplet.utils.UUIDUtils;

/**
 * 
* @ClassName: SiteStaffServiceImpl 
* @Description: 操作人员服务实现类 
* @author luoxiao
* @date 2020年3月1日 下午7:18:25 
*
 */
@Service("siteStaffService")
public class SiteStaffServiceImpl implements SiteStaffService{

	@Autowired
	private SiteStaffDao siteStaffDao;	
	
	@Override
	public SiteStaffPO getSiteStaffByOpenId(String openId) {
		if(StringUtils.isEmpty(openId)){
			throw new RuntimeException("openId为空!");
		}
		return siteStaffDao.findByOpenid(openId);
	}

	@Override
	public SiteStaffPO getSiteStaffById(String id) {
		if(StringUtils.isEmpty(id)){
			throw new RuntimeException("openId为空!");
		}
		SiteStaffPO siteStaffPO = null;
		Optional<SiteStaffPO> optional = siteStaffDao.findById(id);
		if(optional.isPresent()){
			siteStaffPO = optional.get(); 
		}
		return siteStaffPO;
	}

	@Override
	public void saveSiteStaff(SiteStaffPO po) {
		if(po == null){
			throw new RuntimeException("保存对象为空!");
		}
		if(StringUtils.isEmpty(po.getPhone())){
			throw new RuntimeException("电话号码为空!");
		}
		if(StringUtils.isEmpty(po.getOpenid())){
			throw new RuntimeException("小程序openId为空!");
		}
		SiteStaffPO existStaffPOByPhone = siteStaffDao.findByPhone(po.getPhone());
		SiteStaffPO existStaffPOByOpenId = siteStaffDao.findByPhone(po.getPhone());
		if(existStaffPOByOpenId != null){
			return;
		}
		if(existStaffPOByPhone == null){
			po.setId(UUIDUtils.getUUID());
			po.setUsername(po.getPhone());
			po.setPwd("123456");
			po.setOpenid(po.getOpenid());
			po.setSiteIdGroup(SiteStaffRelationStatus.GroupSiteEnum.ALL.getCode());
			po.setCreateBy("system");
			po.setCreateTime(Calendar.getInstance().getTime());
			siteStaffDao.saveAndFlush(po);
		}else{
			boolean flag = false;
			if(StringUtils.isEmpty(existStaffPOByPhone.getOpenid()) || !po.getOpenid().equals(existStaffPOByPhone.getOpenid())){
				flag = true;
				existStaffPOByPhone.setOpenid(po.getOpenid());
			}
			if(StringUtils.isEmpty(existStaffPOByPhone.getSiteIdGroup())){
				flag = true;
				existStaffPOByPhone.setSiteIdGroup(SiteStaffRelationStatus.GroupSiteEnum.ALL.getCode());
			}
			if(flag){
				existStaffPOByPhone.setUpdateBy("system");
				existStaffPOByPhone.setUpdateTime(Calendar.getInstance().getTime());
				siteStaffDao.saveAndFlush(existStaffPOByPhone);
			}
		}
	}

}
