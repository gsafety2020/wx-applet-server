package com.gsafety.gemp.wxapplet.infection.contract.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zxiao
 * @version 1.0
 * @date 2020/2/28 2:33
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DestinationTypeDTO {

    @ApiModelProperty(value = "code")
    private String code;

    @ApiModelProperty(value = "目的地类型")
    private String name;
}
