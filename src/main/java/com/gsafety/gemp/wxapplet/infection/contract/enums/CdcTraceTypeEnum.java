package com.gsafety.gemp.wxapplet.infection.contract.enums;

/**
 * @author fanlx
 */

public enum CdcTraceTypeEnum {

    TYPE_LINCHUANG("1","临床诊断病例"),
    TYPE_QUEZHEN("2","确诊病例"),
    TYPE_YANGXING("3","阳性检测"),
    TYPE_YISI("4","疑似病例"),
    TYPE_SIWANG("5","死亡"),
    TYPE_ZHUANRU("6","进入"),
    TYEP_ZHUANCHU("7","离开"),
    TYEP_FABING("8","发病"),
    TYEP_CHUYUAN("9","出院"),
    TYPE_JIUZHEN("10","发热门诊")
    ;

    private String code;
    private String name;
    CdcTraceTypeEnum(String code, String name){
        this.code = code;
        this.name = name;
    }


    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static String getNameByCode(String code) {
        for (CdcTraceTypeEnum traceTypeEnum : values()) {
            if (traceTypeEnum.code.equals(code)){
                return traceTypeEnum.name;
            }
        }
        return null;
    }
}
