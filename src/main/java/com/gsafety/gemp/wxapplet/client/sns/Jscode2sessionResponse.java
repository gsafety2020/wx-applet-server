/**
 * 
 */
package com.gsafety.gemp.wxapplet.client.sns;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gsafety.gemp.wxapplet.client.Response;


/**
 * @author yangbo
 *
 */
public class Jscode2sessionResponse implements Response {

	/**
	 * 
	 */
	String openid;

	/**
	 * 
	 */
	@JsonProperty(value = "session_key")
	@JSONField(name = "session_key")
	String sessionKey;

	/**
	 * 
	 */
	String unionid;

	/**
	 * 
	 */
	int errcode;

	/**
	 * 
	 */
	String errmsg;

	/**
	 * @return the openid
	 */
	public String getOpenid() {
		return openid;
	}

	/**
	 * @param openid
	 *            the openid to set
	 */
	public void setOpenid(String openid) {
		this.openid = openid;
	}

	/**
	 * @return the sessionKey
	 */
	public String getSessionKey() {
		return sessionKey;
	}

	/**
	 * @param sessionKey
	 *            the sessionKey to set
	 */
	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	/**
	 * @return the unionid
	 */
	public String getUnionid() {
		return unionid;
	}

	/**
	 * @param unionid
	 *            the unionid to set
	 */
	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}

	/**
	 * @return the errcode
	 */
	public int getErrcode() {
		return errcode;
	}

	/**
	 * @param errcode
	 *            the errcode to set
	 */
	public void setErrcode(int errcode) {
		this.errcode = errcode;
	}

	/**
	 * @return the errmsg
	 */
	public String getErrmsg() {
		return errmsg;
	}

	/**
	 * @param errmsg
	 *            the errmsg to set
	 */
	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}

	@Override
	public String toString() {
		return "Jscode2sessionResponse [openid=" + openid + ", sessionKey=" + sessionKey + ", unionid=" + unionid
				+ ", errcode=" + errcode + ", errmsg=" + errmsg + "]";
	}

}