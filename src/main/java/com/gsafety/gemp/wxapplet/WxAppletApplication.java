package com.gsafety.gemp.wxapplet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(scanBasePackages = { "com.gsafety.gemp" })
@EnableJpaRepositories(basePackages = { "com.gsafety.gemp.**.dao" })
@EntityScan(basePackages = { "com.gsafety.gemp.**.dao.po" })
@EnableTransactionManagement
@EnableAsync
@EnableScheduling
public class WxAppletApplication {

    public static void main(String[] args) {
        try{
            SpringApplication.run(WxAppletApplication.class, args);
        }catch (RuntimeException e){
            e.printStackTrace();
        }
    }

}
