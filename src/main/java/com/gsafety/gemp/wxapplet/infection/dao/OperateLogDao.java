package com.gsafety.gemp.wxapplet.infection.dao;

import com.gsafety.gemp.wxapplet.infection.dao.po.OperateLogPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author lzx
 * @version 1.0
 * @date 2020/3/1 19:25
 */
public interface OperateLogDao  extends JpaRepository<OperateLogPO, String>,
        JpaSpecificationExecutor<OperateLogPO> {

}
