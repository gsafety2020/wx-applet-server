package com.gsafety.gemp.wxapplet.healthcode.wrapper;

import com.gsafety.gemp.wxapplet.healthcode.contract.dto.PestWechatUserDTO;
import com.gsafety.gemp.wxapplet.healthcode.dao.po.PestWechatUserPO;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import com.gsafety.gemp.wxapplet.healthcode.constant.PestWechatUserStatus.GenderEnum;

import java.util.ArrayList;
import java.util.List;

import static com.gsafety.gemp.wxapplet.healthcode.constant.PestWechatUserStatus.GenderEnum.FEMALE;
import static com.gsafety.gemp.wxapplet.healthcode.constant.PestWechatUserStatus.GenderEnum.MALE;
import static com.gsafety.gemp.wxapplet.healthcode.constant.PestWechatUserStatus.GenderEnum.UNKONW;


/**
 * 
* @ClassName: PestWechatUserWrapper
* @Description: 微信用户信息 po 与 dto互转
* @author wangwenhai
* @date 2020年2月27日 下午3:45:08
*
 */
public class PestWechatUserWrapper {

	/**
	 *
	 * 该方法用于实现单个PO转DTO
	 *
	 * @author wangwenhai
	 *
	 * @since 2020年2月27日 下午3:45:08
	 *
	 * @param po
	 * @return
	 */
	public static PestWechatUserDTO toDTO(PestWechatUserPO po) {
		PestWechatUserDTO dto = PestWechatUserDTO.builder().build();
		BeanUtils.copyProperties(po, dto);
		String gender=po.getGender();
		if(UNKONW.getCode().equals(gender)){
			dto.setGender(UNKONW.getValue());
		}else if(MALE.getCode().equals(gender)){
			dto.setGender(MALE.getValue());
		}else if(FEMALE.getCode().equals(gender)){
			dto.setGender(FEMALE.getValue());
		}
		return dto;
	}

	/**
	 *
	 * 该方法用于实现list po转DTO
	 *
	 * @author wangwenhai
	 *
	 * @since 2020年2月27日 下午3:45:08
	 *
	 * @param records
	 * @return
	 */
	public static List<PestWechatUserDTO> toDTOList(List<PestWechatUserPO> records) {
		List<PestWechatUserDTO> list = new ArrayList<>();
		records.stream().forEach(record -> list.add(toDTO(record)));
		return list;
	}

	/**
	 *
	 * 该方法用于实现page po转DTO
	 *
	 * @author wangwenhai
	 *
	 * @since 2020年2月27日 下午3:45:08
	 *
	 * @param page
	 * @return
	 */
	public static Page<PestWechatUserDTO> toPageDTO(Page<PestWechatUserPO> page){
		List<PestWechatUserDTO> content = toDTOList(page.getContent());
		return new PageImpl<>(content, page.getPageable(), page.getTotalElements());
	}

	public static PestWechatUserPO dtoToPo(PestWechatUserDTO dto) {
		PestWechatUserPO po = new PestWechatUserPO();
		BeanUtils.copyProperties(dto, po);
		
		return po;
	}
}
