package com.gsafety.gemp.wxapplet.utils;

import java.util.UUID;

/**
 * 
* @ClassName: UUIDUtils 
* @Description:UUID工具类
* @author luoxiao
* @date 2020年2月3日 上午4:21:03 
*
 */
public class UUIDUtils {

	public static String getUUID(){
        return UUID.randomUUID().toString().replace("-", "");
   }
}
