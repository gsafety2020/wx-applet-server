package com.gsafety.gemp.wxapplet.infection.dao.po;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
* @ClassName: SuffererSiteTracePO 
* @Description:  统计局状态变更记录PO
* @author luoxiao
* @date 2020年3月1日 下午3:07:04 
*
 */
@Entity
@Table(name = "sufferer_trace")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SuffererTracePO implements Serializable{

	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name = "id")
	private String id;
	
	/**隔离患者id*/
	@Column(name = "sufferer_id")
	private String suffererId;
		
	/**隔离者，患者姓名*/
	@Column(name = "sufferer_name")
	private String suffererName;
	
	/**医院Id*/
	@Column(name = "site_id")
	private String siteId;
	
	/**当前所在隔离点或医院ID*/
	@Column(name = "site_name")
	private String siteName;
	
	/**1.密接人员；2.发热患者；3.疑似患者；4.确诊;5.出院留观者6. 20日前的临床诊断病例；7、转入；8、转出*/
	@Column(name = "trace_type")
	private Integer traceType;
	
	/**创建时间*/
	@Column(name = "change_time")
	private Date changeTime;
	
	/**所在隔离点序列号 系统生成*/
	@Column(name = "sequence_no")
	private Integer sequenceNo;

	/**创建人*/
	@Column(name = "CREATE_BY")
	private String createBy;
	
	/**更新人*/
	@Column(name = "UPDATE_BY")
	private String updateBy;
	
	/**创建时间*/
	@Column(name = "CREATE_TIME")
	private Date createTime;
	
	/**更新时间*/
	@Column(name = "UPDATE_TIME")
	private Date updateTime;
	
	/**备注*/
	@Column(name = "REMARK")
	private String remark;


	
}
