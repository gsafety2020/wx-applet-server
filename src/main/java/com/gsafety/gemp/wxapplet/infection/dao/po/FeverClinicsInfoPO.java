package com.gsafety.gemp.wxapplet.infection.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "fever_clinics_info", schema = "wx_applet", catalog = "")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FeverClinicsInfoPO {

    @Id
    @Column(name = "ID")
    private String id;

    /**发热门诊医院*/
    @Column(name = "HOSPITAL")
    private String hospital;

    /**患者姓名*/
    @Column(name = "SUFFERER_NAME")
    private String suffererName;

    /**性别(1-男,2-女)*/
    @Column(name = "GENDER")
    private String gender;

    /**年龄*/
    @Column(name = "AGE")
    private String age;

    /**身份证号码*/
    @Column(name = "ID_NO")
    private String idNo;

    /**联系电话*/
    @Column(name = "TELPHONE")
    private String telphone;

    /**居住地址*/
    @Column(name = "ADDRESS")
    private String address;

    /**主要症状*/
    @Column(name = "SYPTOM")
    private String syptom;

    /** 就诊时间 **/
    @Column(name = "CONSULTATION_TIME")
    private Date consultationTime;

    /**患者来源*/
    @Column(name = "SOURCE")
    private String source;

    /**患者去向*/
    @Column(name = "DIRECTION")
    private String direction;

    /** 创建时间**/
    @Column(name="CREATE_TIME")
    private Date createTime;

    /** 更新时间**/
    @Column(name="UPDATE_TIME")
    private Date updateTime;

    /** 备注**/
    @Column(name="NOTE")
    private String note;

    /**
     * 数据日期
     */
    @Column(name="DATA_DATE")
    private String dataDate;
}
