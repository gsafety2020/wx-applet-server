package com.gsafety.gemp.wxapplet.infection.controller;


import com.gsafety.gemp.wxapplet.healthcode.contract.dto.Result;
import com.gsafety.gemp.wxapplet.infection.contract.iface.CloseContactInfoService;
import com.gsafety.gemp.wxapplet.infection.contract.params.CloseContactInfoSearchParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(value = "密接人群接口", tags = {"密接人群接口"})
@RestController
@RequestMapping("/infection/closeContact")
public class CloseContactInfoController {


    @Autowired
    private CloseContactInfoService closeContactInfoService;

    @ApiOperation(value="密接人员分页查询")
    @PostMapping("/page")
    public Result findPage(@RequestBody  CloseContactInfoSearchParam param){
        return new Result<>().success("成功",closeContactInfoService.findPage(param));
    }

    @ApiOperation(value="密接人员网状拓扑图")
    @PostMapping("/list")
    public Result findList(@ApiParam(value = "患者身份证id") @RequestParam String idCard){
        return new Result<>().success("成功",closeContactInfoService.findNetList(idCard));
    }

}
