package com.gsafety.gemp.wxapplet.infection.dao.po;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * User
 * <p>
 * 创建人：zxiao
 * 创建时间：2020-03-01 14:03 <br>
 * <p>
 * 修改人： <br>
 * 修改时间： <br>
 * 修改备注： <br>
 * </p>
 */
@Entity
@Table(name = "site_staff")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserPO implements Serializable {

    private static final long serialVersionUID = 1896193017964792372L;

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "username")
    private String username;

    @Column(name = "pwd")
    @JsonIgnore
    private String pwd;

    @Column(name = "openid")
    private String openid;

    @Column(name = "realname")
    private String realname;

    @Column(name = "phone")
    private String phone;

    @Column(name = "id_card")
    private String idCard;

    @Column(name = "site_id_group")
    private String siteIdGroup;

    @Column(name = "create_by")
    private String createBy;

    @Column(name = "update_by")
    private String updateBy;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    @Column(name = "token")
    private String token;

    @Column(name = "source_type")
    private Integer sourceType;

    public boolean checkPwd(String pwd) {
        return Objects.equals(pwd, this.pwd);
    }
}
