package com.gsafety.gemp.wxapplet.infection.controller;

import com.gsafety.gemp.wxapplet.healthcode.contract.dto.Result;
import com.gsafety.gemp.wxapplet.infection.contract.iface.TraceService;
import com.gsafety.gemp.wxapplet.utils.ResubmitLock;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author fanlx
 */
@Api(value = "流水生成", tags = {"流水生成"})
@RestController
@RequestMapping("/infection/trace")
@Slf4j
public class TraceGenrateController {

    @Autowired
    private TraceService traceService;

    private static String SITE_KEY = "generate:site";

    private static String FEVER_KEY = "generate:fever";

    private static String FC_KEY = "generate:fangcang";

    private static String POINT_KEY = "generate:point";


    @ApiOperation(value = "生成隔离点流水")
    @PostMapping("generate/site/{dataDate}")
    public Result generateSiteTrace(@PathVariable("dataDate") @ApiParam("数据日期") String dataDate){
        ResubmitLock lock = ResubmitLock.getInstance();
        if(!lock.lock(SITE_KEY+dataDate,"")){
            return new Result().fail("流水生成正在执行中，请稍后执行");
        }
        try {
            traceService.generateSiteTrace(dataDate);
        }catch (Exception e){
            e.printStackTrace();

            return new Result().fail("生成隔离点流水执行失败请检查日志查找原因");
        }
        lock.unLock(true,SITE_KEY+dataDate,10);
        return new Result().success("成功");
    }

    @ApiOperation(value = "生成发热门诊流水")
    @PostMapping("generate/fever/{dataDate}")
    public Result generateFeverTrace(@PathVariable("dataDate") @ApiParam("数据日期") String dataDate){
        ResubmitLock lock = ResubmitLock.getInstance();
        if(!lock.lock(FEVER_KEY+dataDate,"")){
            return new Result().fail("流水生成正在执行中，请稍后执行");
        }
        try {
            traceService.genrateFeverTrace(dataDate);
        }catch (Exception e){
            e.printStackTrace();
            return new Result().fail("生成发热门诊流水执行失败请检查日志查找原因");
        }
        lock.unLock(true,FEVER_KEY+dataDate,10);
        return new Result().success("成功");
    }

    @ApiOperation(value = "生成定点医院流水")
    @PostMapping("generate/point/{dataDate}")
    public Result generatePointTrace(@PathVariable("dataDate") @ApiParam("数据日期") String dataDate){
        ResubmitLock lock = ResubmitLock.getInstance();
        if(!lock.lock(POINT_KEY+dataDate,"")){
            return new Result().fail("当日流水生成正在执行中，请稍后执行");
        }
        try {
            traceService.generatePointTrace(dataDate);
        }catch (Exception e){
            e.printStackTrace();
            return new Result().fail("生成定点医院流水执行失败请检查日志查找原因");
        }
        lock.unLock(true,POINT_KEY+dataDate,10);
        return new Result().success("成功");
    }

    @ApiOperation(value = "生成方舱医院流水")
    @PostMapping("generate/fangcang/{dataDate}")
    public Result generateFangcangTrace(@PathVariable("dataDate") @ApiParam("数据日期") String dataDate){
        ResubmitLock lock = ResubmitLock.getInstance();
        if(!lock.lock(FC_KEY+dataDate,"")){
            return new Result().fail("当日流水生成正在执行中，请稍后执行");
        }
        try {
            traceService.generateFangCangTrace(dataDate);
        }catch (Exception e){
            e.printStackTrace();
            return new Result().fail("生成方舱医院流水执行失败请检查日志查找原因");
        }
        lock.unLock(true,FC_KEY+dataDate,10);
        return new Result().success("成功");
    }
}
