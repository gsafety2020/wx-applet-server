package com.gsafety.gemp.wxapplet.infection.contract.iface;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import com.gsafety.gemp.common.result.Result;
import com.gsafety.gemp.wxapplet.infection.contract.common.SuffererCensusJx;
import com.gsafety.gemp.wxapplet.infection.contract.dto.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import com.alibaba.fastjson.JSONObject;
import com.gsafety.gemp.wxapplet.healthcode.contract.dto.CodeBasDistrictDTO;
import com.gsafety.gemp.wxapplet.infection.dao.po.SiteStaffPO;
import com.gsafety.gemp.wxapplet.infection.dao.po.SuffererInfoPO;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 病患追溯信息服务
 *
 * @author liyanhong
 * @since 2020/2/27  10:44
 */
public interface SuffererInfoService {

    /**
     * 通过关键字查询患者姓名/手机号/身份证，返回匹配的记录数据
     *
     * @param key
     * @return
     */
    Page<SuffererBaseDTO> searchSuffererInfo(String staffId,String key, PageRequest pageRequest);

    /**
     *
    * @Title: searchSuffererInfo
    * @Description: Web端分页查询
    * @param @param dto
    * @param @param pageRequest
    * @param @return    设定文件
    * @return Page<SuffererBaseDTO>    返回类型
    * @throws
     */
    Page<SuffererInfoDTO> searchWebSuffererInfo(String staffId, SuffererInfoWebPageDTO dto, PageRequest pageRequest);

    /**
     * 获取病患详情
     *
     * @param id
     * @return
     */
    SuffererDetailDTO getSuffererInfoById(String id);

    /**
     * 获取病患详情
     *
     * @param id
     * @return
     */
    SuffererInfoDTO getSuffererWebById(String id);

    /**
     * 更新患者的状态
     * @param dto
     * @return
     */
    SuffererDetailDTO updateSuffererStatus(SuffererEditDTO dto);

    /**
     *
    * @Title: getSiteInfoSelectByAreaCode
    * @Description: 根据操作人员staffId查询转出位置
    * @param @param staffPo
    * @param @return    设定文件
    * @return List<SiteInfoSelectDTO>    返回类型
    * @throws
     */
    List<SiteInfoSelectDTO> getSiteInfoSelectByStaff(SiteStaffPO staffPo);

    /**
     *
    * @Title: updateSuffererType
    * @Description: 更新患者类型
    * @param @return    设定文件
    * @return SuffererDetailDTO    返回类型
    * @throws
     */
    SuffererDetailDTO updateSuffererType(SuffererTypeUpdateDTO dto);

    /**
     *
    * @Title: updateSuffererStatus
    * @Description: 更新管理状态
    * @param @return    设定文件
    * @return SuffererDetailDTO    返回类型
    * @throws
     */
    SuffererDetailDTO updateSuffererStatus(SuffererStatusUpdateDTO dto);

    /**
     *
    * @Title: updateillnessStateTime
    * @Description: 更新 病情情况
    * @param @return    设定文件
    * @return SuffererDetailDTO    返回类型
    * @throws
     */
    SuffererDetailDTO updateillnessState(IllnessStateUpdateDTO dto);

    /**
     *
    * @Title: getCodeBaseDistrictByParentCode
    * @Description: 根据武汉市下面
    * @param @param parentCode
    * @param @return    设定文件
    * @return List<CodeBasDistrictDTO>    返回类型
    * @throws
     */
    List<CodeBasDistrictDTO> getCodeBaseDistrictByParentCode();

    /**
     *
    * @Title: saveSuffererInfo
    * @Description: 保存用户信息
    * @param @param dto
    * @param @return    设定文件
    * @return SuffererDetailDTO    返回类型
    * @throws
     */
    SuffererDetailDTO saveSuffererInfo(SuffererInfoPO po);

    /**
     *
    * @Title: updateSuffererInfo
    * @Description: 更新用户信息
    * @param @param po
    * @param @return    设定文件
    * @return SuffererDetailDTO    返回类型
    * @throws
     */
    SuffererWebDetailDTO updateSuffererInfo(SuffererInfoPO po);

    /**
     *
    * @Title: checkSuffererInfoExists
    * @Description: 通过身份证校验患者是否存在
    * @param @param suffererCard
    * @param @return    设定文件
    * @return boolean    返回类型
    * @throws
     */
    String checkSuffererInfoExists(String suffererCard);

    /**
     *
    * @Title: analyseCDCSuffererData
    * @Description: CDC病患数据累计统计
    * @param @return    设定文件
    * @return JSONObject    返回类型
    * @throws
     */
    JSONObject analyseCDCSuffererData(String date);

    /**
     *
    * @Title: analyseCDCSuffererToday
    * @Description: CDC病患昨日数据统计
    * @param @return    设定文件
    * @return JSONObject    返回类型
    * @throws
     */
    JSONObject analyseCDCSuffererToday();

    /**
     *
     * @Title: analyseJxSuffererToday
     * @Description: jx病患昨日数据统计
     * @param @return    设定文件
     * @return JSONObject    返回类型
     * @throws
     */
    SuffererCensusJx analyseJxSuffererToday(String totalDate);

    Page<SuffererInfoDTO> getSuffererInfoTodayBySuffererType(Integer suffererType,PageRequest pageRequest);

    Page<SuffererInfoDTO> getJxSuffererInfoTodayBySuffererType(Integer suffererType, Pageable pageable, String totalDate);

    Page<SuffererInfoDTO> getSuffererInfoTodayByIllnessState(Integer illnessState, PageRequest pageRequest);

    Page<SuffererInfoDTO> getSuffererInfoTodayBySuffererStatus(Integer suffererStatus,PageRequest pageRequest);

    /**
     *
     * @Title: analyseJxSuffererData
     * @Description: 江夏病患数据累计统计
     * @param @return    设定文件
     * @return JSONObject    返回类型
     * @throws
     */
    SuffererCensusJx analyseJxSuffererData();

    void export(SuffererInfoWebPageDTO dto, HttpServletResponse response, HttpServletRequest request) throws Exception;

    Result<String> importExcel(MultipartFile file);

    /**
     * 导出
     * @param response
     * @param request
     * @param totalDate
     * @throws Exception
     */
    void exportStat(HttpServletResponse response, HttpServletRequest request,String totalDate) throws Exception;

    /**
     * 查询所有隔离数据数据
     * @return
     */
    List<SuffererInfoPO> findAll();

    /**
     * 保存所有数据
     * @param all
     */
    void saveAll(List<SuffererInfoPO> all);

    List<SuffererIsolationStatisticsDTO> isolatetionStatistics(Date time);
}
