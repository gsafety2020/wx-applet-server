package com.gsafety.gemp.wxapplet.infection.contract.dto;


import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CdcStatisticsNewlyDTO {

    @ApiParam("新增确诊")
    private Integer newCase;
    @ApiParam("新增临床检测")
    private Integer newClinical;
    @ApiParam("新增疑似")
    private Integer newSuspected;
    @ApiParam("新增阳性检测")
    private Integer newPositive;
    @ApiParam("新增死亡")
    private Integer newDeath;
    @ApiParam("新增转入")
    private Integer newIn;
    @ApiParam("新增转出")
    private Integer newOut;
}
