package com.gsafety.gemp.wxapplet.infection.controller;

import com.gsafety.gemp.wxapplet.healthcode.contract.dto.Result;
import com.gsafety.gemp.wxapplet.infection.contract.dto.FangcangSuffererInfoDTO;
import com.gsafety.gemp.wxapplet.infection.contract.iface.FangcangSuffererInfoService;
import com.gsafety.gemp.wxapplet.infection.contract.params.FangcangSuffererInfoSearchParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

/**
 * @author dusiwei
 */
@Api(value = "方舱病患信息接口", tags = {"方舱病患信息接口"})
@RestController
@RequestMapping("/infection/fangcang")
@Slf4j
public class FangcangSuffererInfoController {

    @Autowired
    private FangcangSuffererInfoService fangcangSuffererInfoService;

    @PostMapping(value = "/sufferer/page")
    @ApiOperation(value = "病患信息分页列表")
    public Result<Page<FangcangSuffererInfoDTO>> findPage(@RequestBody FangcangSuffererInfoSearchParam dto){
        Page<FangcangSuffererInfoDTO> page = fangcangSuffererInfoService.findPage(dto);
        return new Result<Page<FangcangSuffererInfoDTO>>().success("查询成功!",page);
    }

    @GetMapping(value = "/sufferer/one")
    @ApiOperation(value = "病患信息单条详情")
    public Result<FangcangSuffererInfoDTO> findPage(@ApiParam(value = "id", required = true) @RequestParam("id") String id){
        if (StringUtils.isEmpty(id) || !fangcangSuffererInfoService.existsById(id)) {
            return new Result<FangcangSuffererInfoDTO>().fail("id为空或记录不存在！");
        }
        FangcangSuffererInfoDTO dto = fangcangSuffererInfoService.findOne(id);
        return new Result<FangcangSuffererInfoDTO>().success("查询成功!",dto);
    }

}
