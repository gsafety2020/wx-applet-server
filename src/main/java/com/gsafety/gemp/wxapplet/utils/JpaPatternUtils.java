/**
 * 
 */
package com.gsafety.gemp.wxapplet.utils;

import org.apache.commons.lang.StringUtils;

/**
 * 
 * jpa中转义相关工具类
 * 
 * @author yangbo
 *
 */
public class JpaPatternUtils {

	/**
	 * 
	 */
	public static final char ESCAPE = '/';

	/**
	 * 
	 */
	public static final String SP_CHAR_PERCENT = "%";

	/**
	 * 
	 */
	private static final String SP_CHAR_BOTTOM_LINE = "_";

	/**
	 * 
	 */
	private JpaPatternUtils() {
		super();
	}

	/**
	 * 通配符全匹配查询条件组装,示例: "我要%干什么"转为"我要\%干什么"
	 * 
	 * @param string
	 * @return
	 */
	public static String pattern(String string) {
		StringBuilder sb = new StringBuilder();
		sb.append(SP_CHAR_PERCENT);
		sb.append(StringUtils.replace(StringUtils.replace(string, SP_CHAR_PERCENT, ESCAPE + SP_CHAR_PERCENT),
				SP_CHAR_BOTTOM_LINE, ESCAPE + SP_CHAR_BOTTOM_LINE));
		sb.append(SP_CHAR_PERCENT);
		return sb.toString();

	}
	
}
