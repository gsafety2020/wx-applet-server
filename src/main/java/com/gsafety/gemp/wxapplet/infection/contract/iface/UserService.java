package com.gsafety.gemp.wxapplet.infection.contract.iface;

import com.gsafety.gemp.wxapplet.infection.dao.po.UserPO;

/**
 * 病患追溯信息服务
 *
 * @author zxiao
 * @since 2020/2/27  10:44
 */
public interface UserService {
    /**
     * 根据用户名查询用户
     * @param username
     * @return
     */
    UserPO findUser(String username);

    /**
     * 更新Token
     * @param user
     */
    void updateToken(UserPO user);

    /**
     * 根据ID查询用户
     * @param id
     * @return
     */
    UserPO findById(String id);
}
