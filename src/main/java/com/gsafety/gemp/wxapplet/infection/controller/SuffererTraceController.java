package com.gsafety.gemp.wxapplet.infection.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gsafety.gemp.wxapplet.healthcode.contract.dto.Result;
import com.gsafety.gemp.wxapplet.infection.contract.iface.SuffererTraceService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;


@Api(value = "病患追溯信息接口", tags = {"病患追溯信息接口"})
@RestController
@RequestMapping("/infection")
@Slf4j
public class SuffererTraceController {


    @Autowired
    private SuffererTraceService suffererTraceService;


    @ApiOperation(value = "病患溯源查询")
    @GetMapping("sufferer/trace/{id}")
    public Result<Map<String,Object>> getSuffererTraceList(@PathVariable("id") @ApiParam(value = "患者id") String suffererId){
        Result<Map<String,Object>> result = new Result<>();
        try{
        	Map<String,Object> map = suffererTraceService.trackTJJSufferer(suffererId);
        	return result.success("病患溯源查询成功!",map);
        }catch(Exception e){
            log.error("病患溯源查询失败", e);
            return result.fail("病患溯源查询失败");
        }
    }
}
