package com.gsafety.gemp.wxapplet.infection.contract.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 患者详情信息
 *
 * @author liyanhong
 * @since 2020/2/27  13:56
 */
@Data
@ApiModel("患者详情")
public class SuffererDetailDTO extends SuffererBaseDTO{

    private static final long serialVersionUID = -5083311777286913325L;
  
    @JsonProperty("sufferer_type")
    @ApiModelProperty(value = "患者类型")
    private String suffererType;
    
    @JsonProperty("sufferer_type_time")
    @ApiModelProperty(value = "患者类型时间记录")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
    private Date suffererTypeTime;

    @JsonProperty("sufferer_status")
    @ApiModelProperty(value = "患者状态")
    private String suffererStatus;
    
    @JsonProperty("sufferer_status_time")
    @ApiModelProperty(value = "管理状态时间记录")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd hh:mm")
    private Date suffererStatusTime;

    @JsonProperty("sufferer_card")
    @ApiModelProperty(value = "身份证号")
    private String suffererCard;
    
    @JsonProperty("illness_state")
    @ApiModelProperty(value = "病情. 1.治愈；2死亡；3.危重 4.其他")
    private Integer illnessState;
    
    @JsonProperty("illness_state_time")
    @ApiModelProperty(value = "病情情况时间记录")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
    private Date illnessStateTime;
    
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd hh:mm")
	@ApiModelProperty(value = "病情时间")
	private Date illnessBeginTime;
    
    @JsonProperty("illness_name")
    @ApiModelProperty(value = "病情名称")
    private String  illnessName;

    @JsonProperty("cur_site_name")
    @ApiModelProperty(value = "当前位置")
    private String curSiteName;
    
    @JsonProperty("cur_site_id")
    @ApiModelProperty(value = "当前所在隔离点或医院ID")
    private String curSiteId;

    @JsonProperty("update_time")
    @ApiModelProperty(value = "更新时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
    private Date updateTime;
}
