package com.gsafety.gemp.wxapplet.utils;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

/**
 * @author lzx
 * @version 1.0
 * @date 2020/3/1 23:21
 */
public class EmptyUtils implements Serializable {
    /**
     * 获取String不为空
     *
     * @param cs
     * @return
     */
    public static boolean isNotEmpty(CharSequence cs) {
        return !isEmpty(cs);
    }

    /**
     * 获取String为空
     *
     * @param cs
     * @return
     */
    public static boolean isEmpty(CharSequence cs) {
        int strLen;
        if (cs != null && (strLen = cs.length()) != 0) {
            for (int i = 0; i < strLen; ++i) {
                if (!Character.isWhitespace(cs.charAt(i))) {
                    return false;
                }
            }
            return true;
        } else {
            return true;
        }
    }

    /**
     * 获取interger不为空
     *
     * @param integer
     * @return
     */
    public static boolean isNotEmpty(Integer integer) {
        if (integer != null && isNotEmpty(integer.toString())) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 获取interger为空
     *
     * @param integer
     * @return
     */
    public static boolean isEmpty(Integer integer) {
        boolean b = isEmpty(integer.toString());
        if (integer == null || (integer != null && b)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 获取集合类是否不为空
     *
     * @param collection
     * @return
     */
    public static boolean isNotEmpty(Collection<?> collection) {
        return !isEmpty(collection);


    }

    /**
     * 获取集合类是否为空
     *
     * @param collection
     * @return
     */
    public static boolean isEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();

    }


    public static String trimAll(String str) {
        return str.trim()
                .replaceAll("[　*| *| *|//s*]*", "")
                .replaceAll("^[　*| *| *|//s*]*", "")
                .replaceAll("[　*| *| *|//s*]*$", "");
    }

    public static String addOrderStr2Select(String s) {
        s = s.toLowerCase().replaceAll(" +", " ");
        String[] s2 = s.split(" ");
        String orderStr = "";
        for (int i = 0; i < s2.length; i++) {
            if (Objects.equals("by", s2[i])) {
                orderStr = s2[i + 1];
                if (Objects.equals(",", s2[i + 2])) {

                }
                break;
            }

        }
        return s.replace("select", "select " + orderStr + ",");
    }
}

