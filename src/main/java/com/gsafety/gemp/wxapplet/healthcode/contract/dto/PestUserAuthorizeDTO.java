package com.gsafety.gemp.wxapplet.healthcode.contract.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * 
* @Description: 用户权限返回参数
* @author xuelq
* @date 2020年2月27日 下午3:47:39 
*
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("用户权限返回参数")
public class PestUserAuthorizeDTO implements Serializable{

	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value="手机号码")
	private String mobile;
	
	@ApiModelProperty(value="权限类型")
	private String authorizeType;

}
