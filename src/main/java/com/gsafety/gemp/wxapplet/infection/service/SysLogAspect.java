package com.gsafety.gemp.wxapplet.infection.service;

import com.gsafety.gemp.wxapplet.healthcode.controller.WechatAuthController;
import com.gsafety.gemp.wxapplet.healthcode.factory.WechatUserManager;
import com.gsafety.gemp.wxapplet.healthcode.factory.WechatUserManagerFactory;
import com.gsafety.gemp.wxapplet.infection.contract.iface.OperateLog;
import com.gsafety.gemp.wxapplet.infection.contract.iface.SysLogAspectService;
import com.gsafety.gemp.wxapplet.infection.dao.OperateLogDao;
import com.gsafety.gemp.wxapplet.infection.dao.po.OperateLogPO;
import com.gsafety.gemp.wxapplet.utils.EmptyUtils;
import com.gsafety.gemp.wxapplet.utils.UUIDUtils;
import com.gsafety.gemp.wxapplet.web.UserContext;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.UUID;

/**
 * @author lzx
 * @version 1.0
 * @date 2020/3/1 19:05
 */

@Aspect
@Component
public class SysLogAspect  implements SysLogAspectService {

    @Autowired
    private OperateLogDao operateLogDao;

    //定义切点 @Pointcut
    //在注解的位置切入代码
    @Pointcut("@annotation(com.gsafety.gemp.wxapplet.infection.contract.iface.OperateLog)")
    public void logPoinCut() {
    }

    //切面 配置通知
    @AfterReturning("logPoinCut()")
    public void saveSysLog(JoinPoint joinPoint) {
        //保存日志
        OperateLogPO sysLog = new OperateLogPO();
        //从切面织入点处通过反射机制获取织入点处的方法
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        //获取切入点所在的方法
        Method method = signature.getMethod();
        //1、获取操作接口配置注解信息===================================
        OperateLog myLog = method.getAnnotation(OperateLog.class);
        if (myLog != null) {
            // 获取请求token或sessionKey
            String title = myLog.title();  //获取操作主题
            String methodType = myLog.methodType(); //获取操作方法
            sysLog.setTitle(title);
            sysLog.setOperateType(methodType);
            sysLog.setCreateTime(new Date());
            sysLog.setUpdateTime(new Date());
            //设置随机32位码
            sysLog.setId(UUIDUtils.getUUID());
        }
        //2、通过反射获取请求的类名===================================
        String className = joinPoint.getTarget().getClass().getName();
        //获取请求的方法名
        String methodName = method.getName();
        sysLog.setMethod(className + "." + methodName);
        //3、通过请求获取用户id======================================
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        //查看是否PC登录
        String token= request.getHeader("token");
        if(!EmptyUtils.isEmpty(token)){
            //根据token拿用户id
            String usrName = UserContext.getUser().getUsername();
            sysLog.setCreateBy(usrName);
            sysLog.setUpdateBy(usrName);
        }
        //如果是小程序存用户的openid，openid与username一一对应
        String sessionKey= request.getHeader("sessionKey");
        if(!EmptyUtils.isEmpty(token)){
            // 根据
            WechatUserManager wechatUserManager = WechatUserManagerFactory.getInstance();
            WechatAuthController.WechatUserinfo userInfo =wechatUserManager.getUser(sessionKey);
            sysLog.setCreateBy(userInfo.getOpenid());
            sysLog.setUpdateBy(userInfo.getOpenid());
        }

        //调用接口保存SysLog实体类到数据库
        save(sysLog);
        //System.out.println("将日志记录到数据库");
    }

    @Override
    public void save(OperateLogPO dto) {
        operateLogDao.save(dto);
    }
}
