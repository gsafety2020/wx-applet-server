package com.gsafety.gemp.wxapplet.infection.contract.enums;

import com.gsafety.gemp.wxapplet.utils.StringCodeEnum;
import lombok.Getter;

/**
 * @author lzx
 * @version 1.0
 * @date 2020/2/28 0:30
 */
@Getter
public enum DestinationTypeEnum implements StringCodeEnum {
    ISOLATION("1","隔离点"),
    HOSPITAL("2","医院"),
    HOME("3","自家"),
    OTHER("4","其他");

    private String code;
    private String name;
    DestinationTypeEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static String getNameByCode(String code) {
        for (DestinationTypeEnum dataTypeEnum : values()) {
            if (dataTypeEnum.code.equals(code)){
                return dataTypeEnum.name;
            }
        }
        return null;
    }
}
