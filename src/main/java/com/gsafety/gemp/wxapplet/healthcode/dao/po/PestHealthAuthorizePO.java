package com.gsafety.gemp.wxapplet.healthcode.dao.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
* @ClassName: PestHealthAuthorizePO 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author luoxiao
* @date 2020年2月26日 下午4:05:55 
*
 */
@Entity
@Table(name = "pest_health_authorize")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PestHealthAuthorizePO implements Serializable{
	
	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "authorize_id")
	/**主键*/
	private String authorizeId;
	
	@Column(name = "device_id")
	/**设备Id*/
	private String deviceId;
	
	@Column(name = "device_name")
	/**设备名称*/
	private String deviceName;
	
	@Column(name = "device_type")
	/**设备类型*/
	private String deviceType;
	
	@Column(name = "mobile")
	/**手机号*/
	private String mobile;
	
	@Column(name = "province_code")
	/**授权的省份code*/
	private String provinceCode;
	
	@Column(name = "province_name")
	/**授权的省份名称*/
	private String provinceName;
	
	@Column(name = "area_code")
	/**授权的地区code*/
	private String areaCode;
	
	@Column(name = "area_name")
	/**授权的地区名称*/
	private String areaName;
	
	@Column(name = "parent_code")
	/**父节点code*/
	private String parentCode;
	
	@Column(name = "parent_name")
	/**父节点名称*/
	private String parentName;
	
	@Column(name = "address")
	/**授权位置*/
	private String address;
	
	@Column(name = "latitude")
	/**纬度*/
	private BigDecimal latitude;
	
	@Column(name = "longitude")
	/**经度*/
	private BigDecimal longitude;
	
	@Column(name = "create_time")
	/**创建时间*/
	private Date createTime;
	
	@Column(name = "update_time")
	/**更新时间*/
	private Date updateTime;
	
	@Column(name = "is_delete")
	/**是否删除0：未删除  1.已删除*/
	private String isDelete;
}
