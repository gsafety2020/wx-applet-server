package com.gsafety.gemp.wxapplet.healthcode.contract.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhoushuyan
 * @date 2020-01-29 11:27
 * @Description
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Result<T> {

    /**
     * 成功返回默认信息
     */
    public static final String DEFAULT_SUCCESS_MSG = "ok";

    /**
     * 区域编码
     */
    @ApiModelProperty(value = "返回code，正常为200，其他code失败")
    private String code;

    /**
     * 舆情热点值
     */
    @ApiModelProperty(value = "返回信息")
    private String msg;

    /**
     * 返回数据对象
     */
    @ApiModelProperty(value = "返回数据")
    private T data;

    public Result<T> success(){
        return success(DEFAULT_SUCCESS_MSG, null);
    }

    public Result<T> fail(){
        return fail("fail");
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
	public Result<T> success(String msg, T data){
        return new Result("200", msg, data);
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public Result<T> prompt(String msg, T data){
        return new Result("201", msg, data);
    }

    // FIXME 这应该是个静态方法，如果自己返回自己
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public Result<T> success(String msg){
        return new Result("200", msg, null);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public Result<T> success(Object data){
        return new Result("200", DEFAULT_SUCCESS_MSG, data);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public Result<T> fail(String msg){
        return new Result("-100", msg, null);
    }
}
