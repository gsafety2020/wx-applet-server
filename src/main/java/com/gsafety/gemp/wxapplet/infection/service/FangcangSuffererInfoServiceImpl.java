package com.gsafety.gemp.wxapplet.infection.service;

import cn.hutool.core.collection.CollectionUtil;
import com.gsafety.gemp.wxapplet.infection.contract.dto.FangcangSuffererInfoDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.NucleicDetectionInfoDTO;
import com.gsafety.gemp.wxapplet.infection.contract.iface.FangcangSuffererInfoService;
import com.gsafety.gemp.wxapplet.infection.contract.params.FangcangSuffererInfoSearchParam;
import com.gsafety.gemp.wxapplet.infection.dao.FangcangSuffererInfoAllDao;
import com.gsafety.gemp.wxapplet.infection.dao.FangcangSuffererInfoDao;
import com.gsafety.gemp.wxapplet.infection.dao.po.FangcangSuffererInfoAllPO;
import com.gsafety.gemp.wxapplet.infection.dao.po.FangcangSuffererInfoPO;
import com.gsafety.gemp.wxapplet.infection.dao.po.NucleicDetectionInfoPO;
import com.gsafety.gemp.wxapplet.infection.service.spec.FangcangSuffererInfoAllSpecifications;
import com.gsafety.gemp.wxapplet.infection.service.spec.FangcangSuffererInfoSpecifications;
import com.gsafety.gemp.wxapplet.infection.wrapper.FangcangSuffererInfoWrapper;
import com.gsafety.gemp.wxapplet.infection.wrapper.SuffererWrapper;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.weaver.ast.And;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author dusiwei
 */
@Service
public class FangcangSuffererInfoServiceImpl implements FangcangSuffererInfoService {

    @Autowired
    private FangcangSuffererInfoDao fangcangSuffererInfoDao;

    @Autowired
    private FangcangSuffererInfoAllDao fangcangSuffererInfoAllDao;

    @Override
    public Page<FangcangSuffererInfoDTO> findPage(FangcangSuffererInfoSearchParam dto) {
        int nowPage = dto.getNowPage()-1<0?0:dto.getNowPage()-1;
        Sort sort = Sort.by(Sort.Direction.DESC, "admittedTime")
                .and(Sort.by(Sort.Direction.DESC, "dataTime"));
        PageRequest pageRequest = PageRequest.of(nowPage, dto.getPageSize(),sort);
        Specification<FangcangSuffererInfoAllPO> spec = Specification.where(FangcangSuffererInfoAllSpecifications.suffererNameLike(dto.getSuffererName()))
                .and(FangcangSuffererInfoAllSpecifications.idNoLike(dto.getIdNo()))
                .and(FangcangSuffererInfoAllSpecifications.telephoneLike(dto.getTelephone()))
                .and(FangcangSuffererInfoAllSpecifications.suffererStatusEqual(dto.getSuffererStatus()))
                .and(FangcangSuffererInfoAllSpecifications.admittedTimeEqual(dto.getAdmittedTime()))
                .and(FangcangSuffererInfoAllSpecifications.groupByIdNO());
        List<FangcangSuffererInfoAllPO> content = fangcangSuffererInfoAllDao.findAll(spec, pageRequest.getSort());
        List<FangcangSuffererInfoAllPO> filterList=new ArrayList<>();
        for(FangcangSuffererInfoAllPO po:content){
            List<FangcangSuffererInfoAllPO> poList = fangcangSuffererInfoAllDao.findByIdNoOrderByDataTimeDesc(po.getIdNo());
            filterList.add(poList.get(0));
        }
        List<FangcangSuffererInfoAllPO> subList = subPageList(filterList,pageRequest);
        if(CollectionUtil.isEmpty(subList)){
            Page<FangcangSuffererInfoAllPO> emptyPageList=new PageImpl<FangcangSuffererInfoAllPO>(new ArrayList<>(),pageRequest,filterList.size());
            return FangcangSuffererInfoWrapper.allToPageDTO(emptyPageList);
        }
        Page<FangcangSuffererInfoAllPO> pageList=new PageImpl<FangcangSuffererInfoAllPO>(subList,pageRequest,filterList.size());
        return FangcangSuffererInfoWrapper.allToPageDTO(pageList);
    }
    private List<FangcangSuffererInfoAllPO> subPageList(List<FangcangSuffererInfoAllPO> list,PageRequest pageRequest) {
        if (CollectionUtil.isEmpty(list)) {
            return null;
        }
        if (list.size() == 0) {
            return null;
        }

        Integer count = list.size();
        Integer pageCount = 0;
        if (count % pageRequest.getPageSize() == 0) {
            pageCount = count / pageRequest.getPageSize();
        } else {
            pageCount = count / pageRequest.getPageSize() + 1;
        }

        int fromIndex = 0;
        int toIndex = 0;

        if (pageRequest.getPageNumber() + 1 != pageCount) {
            fromIndex = (pageRequest.getPageNumber()) * pageRequest.getPageSize();
            toIndex = fromIndex + pageRequest.getPageSize();
        } else {
            fromIndex = (pageRequest.getPageNumber()) * pageRequest.getPageSize();
            toIndex = count;
        }

        List<FangcangSuffererInfoAllPO> pageList = list.subList(fromIndex, toIndex);

        return pageList;
    }

    @Override
    public FangcangSuffererInfoDTO findOne(String id) {
        Optional<FangcangSuffererInfoAllPO> optional = fangcangSuffererInfoAllDao.findById(id);
        if (optional.isPresent()) {
            return FangcangSuffererInfoWrapper.allToDTO(optional.get());
        } else {
            return FangcangSuffererInfoDTO.builder().id(id).build();
        }
    }

    @Override
    public boolean existsById(String id) {
        return fangcangSuffererInfoAllDao.existsById(id);
    }

}
