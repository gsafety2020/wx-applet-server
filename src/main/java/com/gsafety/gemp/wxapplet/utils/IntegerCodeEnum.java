package com.gsafety.gemp.wxapplet.utils;

/**
 * @author Administrator
 * @Title: StringCodeEnum
 * @ProjectName gemp
 * @Description: TODO
 * @date 2020/2/2813:33
 */
public interface IntegerCodeEnum {

    /**
     * 返回枚举字符串型编码code
     * @return
     */
    Integer getCode();
}
