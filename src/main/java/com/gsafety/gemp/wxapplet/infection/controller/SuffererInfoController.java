package com.gsafety.gemp.wxapplet.infection.controller;

import java.util.Calendar;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gsafety.gemp.wxapplet.healthcode.contract.dto.CodeBasDistrictDTO;
import com.gsafety.gemp.wxapplet.healthcode.contract.dto.Result;
import com.gsafety.gemp.wxapplet.healthcode.controller.WechatAuthController;
import com.gsafety.gemp.wxapplet.healthcode.controller.WechatAuthController.WechatUserinfo;
import com.gsafety.gemp.wxapplet.healthcode.factory.WechatUserManager;
import com.gsafety.gemp.wxapplet.healthcode.factory.WechatUserManagerFactory;
import com.gsafety.gemp.wxapplet.infection.contract.dto.IllnessStateUpdateDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.OperAuthorizeInfDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.SiteInfoSelectDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.SuffererBaseDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.SuffererDetailDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.SuffererEditDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.SuffererInfoEditDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.SuffererInfoSaveDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.SuffererStatusUpdateDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.SuffererTypeUpdateDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.SuffererWebDetailDTO;
import com.gsafety.gemp.wxapplet.infection.contract.iface.SiteStaffService;
import com.gsafety.gemp.wxapplet.infection.contract.iface.SuffererInfoService;
import com.gsafety.gemp.wxapplet.infection.dao.po.SiteStaffPO;
import com.gsafety.gemp.wxapplet.infection.dao.po.SuffererInfoPO;
import com.gsafety.gemp.wxapplet.utils.UUIDUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * 病患追溯信息接口
 *
 * @author liyanhong
 * @since 2020/2/27  10:41
 */
@Api(value = "病患追溯信息接口", tags = {"病患追溯信息接口"})
@RestController
@RequestMapping("/infection")
@Slf4j
public class SuffererInfoController {
	
	@Resource
    private SuffererInfoService suffererInfoService;
    
    @Resource
    private SiteStaffService siteStaffService;

	@ApiOperation(value = "查询病患信息")
    @GetMapping("/sufferer/search")
    public Page<SuffererBaseDTO> searchSuffererInfo(@ApiParam(value = "搜索关键字") @RequestParam(required = false)  String key,
                                                    @ApiParam(value = "页码,从0开始") @RequestParam(required = false) Integer page,
                                                    @ApiParam(value = "每页行数") @RequestParam(required = false) Integer size,
                                                    @ApiParam(value = "用户身份") @RequestParam(required = false) String sessionKey) {
        if(page == null || page < 0){
            page = 0;
        }

        if(size == null || size < 1){
            size = 10;
        }
        WechatUserinfo userInfo = getWechatUserinfoBySession(sessionKey);
		if(userInfo == null){
			throw new RuntimeException("登陆失效,请重新登陆!");
		}
		//获取用户
		SiteStaffPO staffPo = siteStaffService.getSiteStaffByOpenId(userInfo.getOpenid()); 
		if(staffPo == null){
			throw new RuntimeException("微信用户与系统用户没有绑定!");
		}
        PageRequest pageRequest = PageRequest.of(page, size);
        return suffererInfoService.searchSuffererInfo(staffPo.getId(),key, pageRequest);
    }

    @ApiOperation(value = "获取病患详情")
    @GetMapping("/sufferer/detail")
    public SuffererDetailDTO getSuffererInfoById(@ApiParam(value = "患者ID") @RequestParam String id) {

        return suffererInfoService.getSuffererInfoById(id);
    }

    @ApiOperation(value = "更新病患状态")
    @Deprecated
    @PostMapping("/sufferer/detail/update")
    public SuffererDetailDTO updateSuffererStatus(@RequestBody SuffererEditDTO dto) {

        WechatAuthController.WechatUserinfo userInfo = WechatUserManagerFactory.getInstance().getUser(dto.getSessionKey());
        if(userInfo != null){
            dto.setUpdateBy(userInfo.getOpenid());
        }
        return suffererInfoService.updateSuffererStatus(dto);
    }
    
    @ApiOperation(value = "更新病人类型")
    @PostMapping("/sufferer/sufferertype/update")
    public Result<SuffererDetailDTO> suffererTypeUpdate(@RequestBody SuffererTypeUpdateDTO dto){
    	Result<SuffererDetailDTO> result = new Result<>();
    	try{
    		SuffererDetailDTO suffererDetailDTO = suffererInfoService.updateSuffererType(dto);
    		return result.success("更改成功!", suffererDetailDTO);
    	}catch(Exception e){
    		log.error("病人类型更新失败[{}]失败", dto, e);
    		return result.fail(e.getMessage());
    	}
    }
    
    @ApiOperation(value = "更新管理状态")
    @PostMapping("/sufferer/suffererstatus/update")
    public Result<SuffererDetailDTO> suffererStatusUpdate(@RequestBody SuffererStatusUpdateDTO dto){
    	Result<SuffererDetailDTO> result = new Result<>();
    	try{
    		SuffererDetailDTO suffererDetailDTO = suffererInfoService.updateSuffererStatus(dto);
    		return result.success("更改成功!", suffererDetailDTO);
    	}catch(Exception e){
    		log.error("病人类型更新失败[{}]失败", dto, e);
    		return result.fail(e.getMessage());
    	}
    }
    
    @ApiOperation(value = "更新 病情情况")
    @PostMapping("/sufferer/illnessstate/update")
    public Result<SuffererDetailDTO> illnessStateUpdate(@RequestBody IllnessStateUpdateDTO dto){
    	Result<SuffererDetailDTO> result = new Result<>();
    	try{
    		SuffererDetailDTO suffererDetailDTO = suffererInfoService.updateillnessState(dto);
    		return result.success("更改成功!", suffererDetailDTO);
    	}catch(Exception e){
    		log.error("病人类型更新失败[{}]失败", dto, e);
    		return result.fail(e.getMessage());
    	}
    }

    @ApiOperation(value = "查询转出位置下拉框")
    @PostMapping("/site/select/query")
    public Result<List<SiteInfoSelectDTO>> querySiteInfoSelect(@RequestHeader("sessionKey") String sessionKey){
    	Result<List<SiteInfoSelectDTO>> result = new Result<List<SiteInfoSelectDTO>>();
    	try{
    		WechatUserinfo userInfo = getWechatUserinfoBySession(sessionKey);
    		if(userInfo == null){
    			throw new RuntimeException("登陆失效,请重新登陆!");
    		}
    		//获取用户
    		SiteStaffPO staffPo = siteStaffService.getSiteStaffByOpenId(userInfo.getOpenid()); 
    		if(staffPo == null){
    			throw new RuntimeException("微信用户与系统用户没有绑定!");
    		}
    		List<SiteInfoSelectDTO> selectDto = suffererInfoService.getSiteInfoSelectByStaff(staffPo);
    		return result.success("查询成功!", selectDto);
    	}catch(Exception e){
    		log.error("查询转出位置下拉框[{}]失败", sessionKey, e);
    		return result.fail(e.getMessage());
    	}
    }
    
    @ApiOperation(value = "保存病患信息")
    @PostMapping("/sufferer/save")
    public Result<SuffererDetailDTO> save(@RequestBody SuffererInfoSaveDTO dto){
    	Result<SuffererDetailDTO> result = new Result<>();
    	try{
    		SuffererInfoPO po = new SuffererInfoPO();
    		BeanUtils.copyProperties(dto, po);
    		WechatUserinfo userInfo = getWechatUserinfoBySession(dto.getSessionKey());
    		if(userInfo == null){
    			throw new Exception("登陆失效,请重新登陆!");
    		}
    		//获取用户
    		SiteStaffPO staffPo = siteStaffService.getSiteStaffByOpenId(userInfo.getOpenid()); 
    		if(staffPo == null){
    			throw new Exception("微信用户与系统用户没有绑定!");
    		}
    		po.setId(UUIDUtils.getUUID());
    		po.setCreateBy(staffPo.getRealname());
    		po.setCreateTime(Calendar.getInstance().getTime());
    		po.setUpdateBy(staffPo.getRealname());
    		po.setUpdateTime(Calendar.getInstance().getTime());
    		if(StringUtils.isEmpty(dto.getAreaCode())){
    			po.setAreaCode("420115");
    			po.setAreaName("江夏区");
    		}
    		SuffererDetailDTO resultDto = suffererInfoService.saveSuffererInfo(po);
    		return result.success("保存病患信息成功!",resultDto);
    	}catch(Exception e){
    		log.error("保存病患信息[{}]失败", dto, e);
    		return result.fail(e.getMessage());
    	}
    }
    
    @ApiOperation(value = "更新病患信息")
    @PostMapping("/sufferer/update")
    public Result<SuffererWebDetailDTO> update(@RequestBody SuffererInfoEditDTO dto){
    	Result<SuffererWebDetailDTO> result = new Result<>();
    	try{
    		SuffererInfoPO po = new SuffererInfoPO();
    		BeanUtils.copyProperties(dto, po);
    		WechatUserinfo userInfo = getWechatUserinfoBySession(dto.getSessionKey());
    		if(userInfo == null){
    			throw new Exception("登陆失效,请重新登陆!");
    		}
    		//获取用户
    		SiteStaffPO staffPo = siteStaffService.getSiteStaffByOpenId(userInfo.getOpenid()); 
    		if(staffPo == null){
    			throw new Exception("微信用户与系统用户没有绑定!");
    		}
    		po.setUpdateBy(staffPo.getRealname());
    		po.setUpdateTime(Calendar.getInstance().getTime());
    		SuffererWebDetailDTO resultDto = suffererInfoService.updateSuffererInfo(po);
    		return result.success("保存病患信息成功!",resultDto);
    	}catch(Exception e){
    		log.error("保存病患信息[{}]失败", dto, e);
    		return result.fail(e.getMessage());
    	}
    }
    
    @ApiOperation(value = "获取武汉市下面所有的区")
    @PostMapping("/sufferer/area/selected")
    public Result<List<CodeBasDistrictDTO>> areaSelected(){
    	Result<List<CodeBasDistrictDTO>> result = new Result<>();
    	try{
    		List<CodeBasDistrictDTO> resultDto = suffererInfoService.getCodeBaseDistrictByParentCode();
    		return result.success("保存病患信息成功!",resultDto);
    	}catch(Exception e){
    		log.error("获取武汉市下面所有的区信息失败", e);
    		return result.fail(e.getMessage());
    	}
    }
    
    @ApiOperation(value = "通过身份证校验患者是否存在")
    @PostMapping("/sufferer/check/suffererinfo")
    public Result<String> checkSuffererinfo(@ApiParam(value = "身份证") @RequestParam String suffererCard){
    	Result<String> result = new Result<>();
    	try{
    		String id = suffererInfoService.checkSuffererInfoExists(suffererCard);
    		if(StringUtils.isEmpty(id)){
    			return result.success("用户信息不存在!", id);
    		}
    		return result.prompt("用户信息已经存在!", id);
    	}catch(Exception e){
    		log.error("身份证校验患者是否存在[{}]失败", suffererCard, e);
    		return result.fail(e.getMessage());
    	}
    }
    
    @ApiOperation(value = "查询用户是否授权")
	@PostMapping("/whetherUserAuthorize")
    public Result<?> whetherUserAuthorize(@RequestHeader("sessionKey") String sessionKey){
    	Result<Boolean> result = new Result<Boolean>();
    	try{
    		if(StringUtils.isEmpty(sessionKey)){
    			throw new RuntimeException("小程序sessionKey为空!");
    		}
    		WechatUserinfo userInfo = getWechatUserinfoBySession(sessionKey); 
    		SiteStaffPO  staffPo = siteStaffService.getSiteStaffByOpenId(userInfo.getOpenid());
    		if(staffPo == null){
    			return result.success("用户未授权!", false);
    		}
    		return result.success("用户已授权!", true);
    	}catch(Exception e){
    		log.error("查询用户是否授权[{}]失败",sessionKey);
    		return result.fail(e.getMessage());
    	}
    }
    
    @ApiOperation(value = "用户授权")
	@PostMapping("/haveOperAuthorize")
	public Result<?> haveOperAuthorize(@RequestBody OperAuthorizeInfDTO operAuthorizeInf){
    	Result<?>  result = new Result<String>();
    	try{
    		if(operAuthorizeInf == null){
    			throw new RuntimeException("授权参数为空!");
    		}
    		if(StringUtils.isEmpty(operAuthorizeInf.getSessionKey())){
    			throw new RuntimeException("小程序sessionKey为空!");
    		}
    		if(StringUtils.isEmpty(operAuthorizeInf.getMobile())){
    			throw new RuntimeException("手机号为空!");
    		}
    		WechatUserinfo userInfo = getWechatUserinfoBySession(operAuthorizeInf.getSessionKey()); 
    		SiteStaffPO po = new SiteStaffPO();
    		po.setPhone(operAuthorizeInf.getMobile());
    		po.setOpenid(userInfo.getOpenid());
    		siteStaffService.saveSiteStaff(po);
    		return result.success("授权成功!");
    	}catch(Exception e){
    		log.error("用户授权失败",e);
    		return result.fail(e.getMessage());
    	}
    }
    
    private WechatUserinfo getWechatUserinfoBySession(String sessionKey){
		WechatUserManager wechatUserManager = WechatUserManagerFactory.getInstance();
		WechatUserinfo userInfo =wechatUserManager.getUser(sessionKey);
		return userInfo;
	}
}
