package com.gsafety.gemp.wxapplet.infection.contract.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zxiao
 * @version 1.0
 * @date 2020/2/28 2:33
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SuffererStatusDTO {

    @ApiModelProperty(value = "code")
    private Integer code;

    @ApiModelProperty(value = "患者状态")
    private String name;
}
