package com.gsafety.gemp.wxapplet.infection.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gsafety.gemp.wxapplet.infection.dao.po.SiteStaffRelationPO;

/**
 *
* @ClassName: SiteStaffRelationDao
* @Description: 隔离群DAO
* @author luoxiao
* @date 2020年3月1日 下午4:41:22
*
 */
public interface SiteStaffRelationDao extends JpaRepository<SiteStaffRelationPO, String>,
JpaSpecificationExecutor<SiteStaffRelationPO>{

	List<SiteStaffRelationPO> findByStaffId(String staffId);

	List<SiteStaffRelationPO> findBySiteIdGroup(String siteIdGroup);
}
