package com.gsafety.gemp.wxapplet.infection.dao;

import com.gsafety.gemp.wxapplet.infection.dao.po.SiteInfoPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author lzx
 * @version 1.0
 * @date 2020/2/28 1:18
 */
public interface KeyAssignmentDao extends JpaRepository<SiteInfoPO, String>, JpaSpecificationExecutor<SiteInfoPO> {

    //List<SiteInfo> findSiteName();
}
