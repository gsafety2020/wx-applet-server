package com.gsafety.gemp.wxapplet.infection.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gsafety.gemp.wxapplet.infection.dao.po.SiteInfoPO;

/**
 * 隔离点/医院站点信息DAO
 *
 * @author liyanhong
 * @since 2020/2/27  10:25
 */
public interface SiteInfoDao extends JpaRepository<SiteInfoPO, String>,
        JpaSpecificationExecutor<SiteInfoPO> {
	
	List<SiteInfoPO> findByIdIn(List<String> id);
}
