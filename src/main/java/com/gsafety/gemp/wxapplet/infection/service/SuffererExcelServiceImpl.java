package com.gsafety.gemp.wxapplet.infection.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.DigestUtils;

import com.gsafety.gemp.wxapplet.healthcode.dao.CodeBasDistrictDao;
import com.gsafety.gemp.wxapplet.healthcode.dao.po.CodeBasDistrictPO;
import com.gsafety.gemp.wxapplet.infection.cache.SiteInfoCache;
import com.gsafety.gemp.wxapplet.infection.contract.enums.SourceTypeEnum;
import com.gsafety.gemp.wxapplet.infection.contract.enums.SuffererTraceTypeEnum;
import com.gsafety.gemp.wxapplet.infection.contract.iface.SuffererExcelService;
import com.gsafety.gemp.wxapplet.infection.contract.iface.SuffererTraceService;
import com.gsafety.gemp.wxapplet.infection.dao.SuffererInfoAllDao;
import com.gsafety.gemp.wxapplet.infection.dao.SuffererInfoDao;
import com.gsafety.gemp.wxapplet.infection.dao.SuffererTraceDao;
import com.gsafety.gemp.wxapplet.infection.dao.po.SuffererInfoAllPO;
import com.gsafety.gemp.wxapplet.infection.dao.po.SuffererInfoPO;
import com.gsafety.gemp.wxapplet.infection.dao.po.SuffererTracePO;
import com.gsafety.gemp.wxapplet.infection.dao.po.UserPO;
import com.gsafety.gemp.wxapplet.utils.DateUtils;
import com.gsafety.gemp.wxapplet.utils.UUIDUtils;
import com.gsafety.gemp.wxapplet.web.UserContext;

import lombok.extern.slf4j.Slf4j;

@Service("suffererExcelService")
@Transactional
@Slf4j
public class SuffererExcelServiceImpl implements SuffererExcelService{

	@Autowired
	private SuffererInfoDao suffererInfoDao;
	
	@Autowired
	private SuffererInfoAllDao suffererInfoAllDao;
	
	@Autowired
	private SiteInfoCache siteInfoCache;
	
	@Autowired
	private SuffererTraceDao suffererTraceDao;
	
	@Autowired
	private SuffererTraceService suffererTraceService;
	
	@Autowired
	private CodeBasDistrictDao codeBasDistrictDao;
	
	private final static String[] parentCodes = {"420100"};
	
	@Override
	public void compareBakSuffererInfoTrace() {
		//获取昨天备份数据
		Map<String,SuffererInfoAllPO> map = getYesterdaySuffererInfoAllData();
		//判断是否存在当日的流水记录
		if(whetherTodayTrackData()){
			suffererTraceDao.deleteByCreateTimeBetween(DateUtils.getTodayBeginTime(), DateUtils.getTodayEndTime());
			suffererTraceDao.flush();
		}
		//获取当前sufferer_info数据库数据
		List<SuffererInfoPO> suffererInfos = suffererInfoDao.findAll();
		//通过统计局患者和昨天的备份数据比较得到记录流水数据
		List<SuffererTracePO> tracks = getSuffererTrack(suffererInfos,map);
		//保存流水记录
		suffererTraceDao.saveAll(tracks);
	}
	
	@Override
	public void importIsolateExcel(List<SuffererInfoPO> suffererInfos) {
		log.info("统计局患者数据导入开始[total:{}条]",suffererInfos.size());
		//数据处理
		handlerImportData(suffererInfos);
		log.info("统计局患者数据处理完成!");
		//获取昨天备份数据
		Map<String,SuffererInfoAllPO> map = getYesterdaySuffererInfoAllData();
		//判断是否存在当日的流水记录
		if(whetherTodayTrackData()){
			suffererTraceDao.deleteByCreateTimeBetween(DateUtils.getTodayBeginTime(), DateUtils.getTodayEndTime());
			suffererTraceDao.flush();
		}
		//通过统计局患者和昨天的备份数据比较得到记录流水数据
		List<SuffererTracePO> tracks = getSuffererTrack(suffererInfos,map);
		//保存数据
		suffererInfoDao.saveAll(suffererInfos);
		//保存流水记录
		suffererTraceDao.saveAll(tracks);
		
	}
	
	private boolean whetherTodayTrackData(){
		int count = suffererTraceDao.countByCreateTimeBetween(DateUtils.getTodayBeginTime(), DateUtils.getTodayEndTime());
		if(count == 0){
			return false;
		}
		return true;
	}
	
	private void handlerImportData(List<SuffererInfoPO> suffererInfos){
		if(CollectionUtils.isEmpty(suffererInfos)){
			throw new RuntimeException("导入数据为空!");
		}
		Map<String,List<SuffererInfoPO>> map = suffererInfos.stream().collect(Collectors.groupingBy(b -> b.getSuffererCard()));
		for(Entry<String, List<SuffererInfoPO>> entry : map.entrySet()){
			List<SuffererInfoPO> list = entry.getValue();
			if(list.size()>1){
				throw new RuntimeException("请检查数据,身份证有重复的数据!");
			}
		}
		//隔离点名字  name:id Map
		Map<String,String> siteMap = siteInfoCache.getMapKeyIdValueNameCache();
		//隔离点名字  name:id codeBaseDistrict
		Map<String,String> codeBasMap = getCodeBasMapKeyNameValueId();
		for(SuffererInfoPO po : suffererInfos){
			if(StringUtils.isNotEmpty(po.getCurSiteName())){
				String curSiteId = siteMap.get(po.getCurSiteName());
				po.setCurSiteId(curSiteId); //通过隔离点名称名称拿到SiteId
			}
			if(StringUtils.isNotEmpty(po.getAreaName())){
				String areaCode = codeBasMap.get(po.getAreaName());
				po.setAreaCode(areaCode);//通过区名称拿到区编码
			}
			if(StringUtils.isNotEmpty(po.getFromAddress())){
				String fromAreaCode = codeBasMap.get(po.getFromAddress());
				po.setFromAreaCode(fromAreaCode);  //通过转入区名称拿到转入区编码
			}
			Date todayDate = Calendar.getInstance().getTime();
			po.setId(DigestUtils.md5DigestAsHex(po.getSuffererCard().getBytes()));
			po.setCreateTime(todayDate);
			po.setUpdateTime(todayDate);
			po.setSourcetype(SourceTypeEnum.TJJ.getCode());
		}
	}
	
	private Map<String,String> getCodeBasMapKeyNameValueId(){
		List<CodeBasDistrictPO> codeBasDistricts = codeBasDistrictDao.findByParentCodeIn(Arrays.asList(parentCodes));
		Map<String,String> map = codeBasDistricts.stream().collect(Collectors.toMap(CodeBasDistrictPO::getDistrictName, CodeBasDistrictPO::getDistrictCode));
		return map;
	}
	
	
	private Map<String,SuffererInfoAllPO> getYesterdaySuffererInfoAllData(){
		Map<String,SuffererInfoAllPO> map = null;
		//获取最近一次数据的时间
		SuffererInfoAllPO po = suffererInfoAllDao.findFirstByOrderByDataDateDesc();
		if(po == null || po.getDataDate() == null){
			return map;
		}
		List<SuffererInfoAllPO> suffererInfoAlls = suffererInfoAllDao.findByDataDate(po.getDataDate());
		if(!CollectionUtils.isEmpty(suffererInfoAlls)){
			map =  suffererInfoAlls.stream().collect(Collectors.toMap(SuffererInfoAllPO::getSuffererId, a->a));
		}
		return map;
	}
	
	private List<SuffererTracePO> getSuffererTrack(List<SuffererInfoPO> suffererInfos,Map<String,SuffererInfoAllPO> map){
		List<SuffererTracePO> suffererTraces = new ArrayList<SuffererTracePO>();
		Long count = suffererTraceDao.count();
		if(count == 0 && map == null){  //当流水表数据不存在时,并且备份表不存在时,以info表为准初始化记录表
			suffererTraces.addAll(getRecordsBySuffererInfoIsNull(suffererInfos));
			suffererTraceDao.saveAll(suffererTraces);
			suffererTraceDao.flush();
			suffererTraces.clear(); 
		}else if(count == 0 && map != null){ //当流水表数据不存在时,并且备份表存在时,以info_all表为准初始化记录表
			List<SuffererInfoAllPO> infoAlls = map.values().stream().collect(Collectors.toList());
			suffererTraces.addAll(getRecordsBySuffererInfoAllIsNull(infoAlls));
			suffererTraceDao.saveAll(suffererTraces);
			suffererTraceDao.flush();
			suffererTraces.clear();
		}
		if(map == null){
			return suffererTraces;
		}
		//当流水数据存在时
		suffererTraces.addAll(getRecordsBySuffererInfoIsNotNull(suffererInfos,map));
		return suffererTraces;
	}
	
	//昨天的患者信息不为空
	private List<SuffererTracePO> getRecordsBySuffererInfoIsNotNull(List<SuffererInfoPO> suffererInfos,Map<String,SuffererInfoAllPO> map){
		List<SuffererTracePO> results = new ArrayList<>();
		Map<String,SuffererTracePO> trackMaxSequenceNoMap = suffererTraceService.getSuffererIdSequenceNo();
		for(SuffererInfoPO po : suffererInfos){
			if(trackMaxSequenceNoMap.get(po.getId()) == null){
				results.addAll(firstRecordSuffererTrace(po,po.getId()));
			}else{
				List<SuffererTracePO> oneRecords = secondRecordSuffererTrace(po,map,trackMaxSequenceNoMap);
				if(!CollectionUtils.isEmpty(oneRecords)){
					results.addAll(oneRecords);
				}
			}
		}
		return results;
	}
	
	private List<SuffererTracePO> secondRecordSuffererTrace(SuffererInfoPO infoPo,Map<String,SuffererInfoAllPO> map,Map<String,SuffererTracePO> trackMaxSequenceNoMap){
		SuffererInfoAllPO infoAllPo = map.get(infoPo.getId());
		//表示本地导入的数据，上一次没有   等于新增
		if(infoAllPo == null){
			return firstRecordSuffererTrace(infoPo,infoPo.getId());
		}
		String isSiteChangeFlag = siteIsChange(infoPo.getCurSiteName(),infoAllPo.getCurSiteName());
		String isSuffererTypeChangeFlag = suffererTypeIsChange(infoPo.getSuffererType(),infoAllPo.getSuffererType());
		return compareSiteAndSuffererName(isSiteChangeFlag+isSuffererTypeChangeFlag,infoPo,infoAllPo,trackMaxSequenceNoMap);
	}
	
	private String siteIsChange(String suffererInfoCurSiteName,String suffererInfoAllCurSiteName){
		if(suffererInfoCurSiteName.equals(suffererInfoAllCurSiteName)){
			return "0";   //位置无变化
		}
		return "1";  //位置有变化
	}
	
	private String suffererTypeIsChange(Integer suffererInfoSuffererType,Integer suffererInfoAllSuffererType){
		if(suffererInfoSuffererType.equals(suffererInfoAllSuffererType)){
			return "0";   //类型无变化
		}
		return "1";   //位置有变化
	}
	
	private List<SuffererTracePO> compareSiteAndSuffererName(String flag,SuffererInfoPO info,SuffererInfoAllPO infoAll,Map<String,SuffererTracePO> trackMaxSequenceNoMap){
		switch(flag){
			//位置无变化,位置有变化
			case "01":
				return siteNotChangeAndTypeChange(info,trackMaxSequenceNoMap);
			//位置有变化,类型无变化
			case "10":
				return siteChangeAndTypeNotChange(info,infoAll,trackMaxSequenceNoMap);
			//位置有变化,位置有变化
			case "11":
				return siteChangeAndTypeChange(info,infoAll,trackMaxSequenceNoMap);
			default:
				return null;
		}		
	}
	
	//位置无变化,位置有变化
	private List<SuffererTracePO> siteNotChangeAndTypeChange(SuffererInfoPO info,Map<String,SuffererTracePO> trackMaxSequenceNoMap){
		List<SuffererTracePO> results = new ArrayList<>();
		UserPO  userPO = UserContext.getUser();
		
		SuffererTracePO trace = new SuffererTracePO();
		trace.setId(UUIDUtils.getUUID());
		trace.setSuffererId(info.getId());
		trace.setSuffererName(info.getSuffererName());
		trace.setSiteId(info.getCurSiteId());
		trace.setSiteName(info.getCurSiteName());
		trace.setTraceType(info.getSuffererType());
		trace.setSequenceNo(trackMaxSequenceNoMap.get(info.getId()).getSequenceNo());
		trace.setCreateTime(Calendar.getInstance().getTime());
		if(userPO != null){
			trace.setCreateBy(userPO.getRealname());
			trace.setUpdateBy(userPO.getRealname());
		}
		trace.setUpdateTime(Calendar.getInstance().getTime());
		results.add(trace);
		
		return results;
	}
	
	//位置有变化,类型无变化
	private List<SuffererTracePO> siteChangeAndTypeNotChange(SuffererInfoPO info,SuffererInfoAllPO infoAll,Map<String,SuffererTracePO> trackMaxSequenceNoMap){
		List<SuffererTracePO> results = new ArrayList<>();
		UserPO  userPO = UserContext.getUser();
		
		SuffererTracePO trace = new SuffererTracePO();
		Integer sequenceNo = trackMaxSequenceNoMap.get(info.getId()).getSequenceNo();
		trace.setId(UUIDUtils.getUUID());
		trace.setSuffererId(info.getId());
		trace.setSuffererName(info.getSuffererName());
		trace.setSiteId(infoAll.getCurSiteId());
		trace.setSiteName(infoAll.getCurSiteName());
		trace.setTraceType(SuffererTraceTypeEnum.ROLL_OUT.getCode());
		trace.setChangeTime(info.getMoveIntoTime());
		trace.setSequenceNo(sequenceNo);
		trace.setCreateTime(Calendar.getInstance().getTime());
		if(userPO != null){
			trace.setCreateBy(userPO.getRealname());
			trace.setUpdateBy(userPO.getRealname());
		}
		trace.setUpdateTime(Calendar.getInstance().getTime());
		results.add(trace);
		
		trace = new SuffererTracePO();
		trace.setId(UUIDUtils.getUUID());
		trace.setSuffererId(info.getId());
		trace.setSuffererName(info.getSuffererName());
		trace.setSiteId(info.getCurSiteId());
		trace.setSiteName(info.getCurSiteName());
		trace.setTraceType(SuffererTraceTypeEnum.SHIFT_IN.getCode());
		trace.setChangeTime(info.getMoveIntoTime());
		trace.setSequenceNo(sequenceNo+1);
		trace.setCreateTime(Calendar.getInstance().getTime());
		if(userPO != null){
			trace.setCreateBy(userPO.getRealname());
			trace.setUpdateBy(userPO.getRealname());
		}
		trace.setUpdateTime(Calendar.getInstance().getTime());
		results.add(trace);
		
		return results;
	}

	//位置有变化,位置有变化
	private List<SuffererTracePO> siteChangeAndTypeChange(SuffererInfoPO info,SuffererInfoAllPO infoAll,Map<String,SuffererTracePO> trackMaxSequenceNoMap){
		List<SuffererTracePO> results = new ArrayList<>();
		UserPO  userPO = UserContext.getUser();
		
		SuffererTracePO trace = new SuffererTracePO();
		Integer sequenceNo = trackMaxSequenceNoMap.get(info.getId()).getSequenceNo();
		trace.setId(UUIDUtils.getUUID());
		trace.setSuffererId(info.getId());
		trace.setSuffererName(info.getSuffererName());
		trace.setSiteId(infoAll.getCurSiteId());
		trace.setSiteName(infoAll.getCurSiteName());
		trace.setTraceType(SuffererTraceTypeEnum.ROLL_OUT.getCode());
		trace.setChangeTime(info.getMoveIntoTime());
		trace.setSequenceNo(sequenceNo);
		trace.setCreateTime(Calendar.getInstance().getTime());
		if(userPO != null){
			trace.setCreateBy(userPO.getRealname());
			trace.setUpdateBy(userPO.getRealname());
		}
		trace.setUpdateTime(Calendar.getInstance().getTime());
		results.add(trace);
		
		trace = new SuffererTracePO();
		trace.setId(UUIDUtils.getUUID());
		trace.setSuffererId(info.getId());
		trace.setSuffererName(info.getSuffererName());
		trace.setSiteId(info.getCurSiteId());
		trace.setSiteName(info.getCurSiteName());
		trace.setTraceType(SuffererTraceTypeEnum.SHIFT_IN.getCode());
		trace.setChangeTime(info.getMoveIntoTime());
		trace.setSequenceNo(sequenceNo+1);
		trace.setCreateTime(Calendar.getInstance().getTime());
		if(userPO != null){
			trace.setCreateBy(userPO.getRealname());
			trace.setUpdateBy(userPO.getRealname());
		}
		trace.setUpdateTime(Calendar.getInstance().getTime());
		results.add(trace);
		
		trace = new SuffererTracePO();
		trace.setId(UUIDUtils.getUUID());
		trace.setSuffererId(info.getId());
		trace.setSuffererName(info.getSuffererName());
		trace.setSiteId(info.getCurSiteId());
		trace.setSiteName(info.getCurSiteName());
		trace.setTraceType(info.getSuffererType());
		trace.setSequenceNo(sequenceNo+1);
		trace.setCreateTime(Calendar.getInstance().getTime());
		if(userPO != null){
			trace.setCreateBy(userPO.getRealname());
			trace.setUpdateBy(userPO.getRealname());
		}
		trace.setUpdateTime(Calendar.getInstance().getTime());
		results.add(trace);
		return results;
	}
	
	//昨天的患者信息为空
	private List<SuffererTracePO> getRecordsBySuffererInfoIsNull(List<SuffererInfoPO> suffererInfos){
		List<SuffererTracePO> results = new ArrayList<>();
		for(SuffererInfoPO po : suffererInfos){
			List<SuffererTracePO> oneRecords = firstRecordSuffererTrace(po,po.getId());
			results.addAll(oneRecords);
		}
		return results;
	}
	
	//昨天的患者信息为空
	private List<SuffererTracePO> getRecordsBySuffererInfoAllIsNull(List<SuffererInfoAllPO> suffererInfos){
		List<SuffererTracePO> results = new ArrayList<>();
		for(SuffererInfoAllPO allpo : suffererInfos){
			SuffererInfoPO po = new SuffererInfoPO();
			BeanUtils.copyProperties(allpo, po);
			List<SuffererTracePO> oneRecords = firstRecordSuffererTrace(po,allpo.getSuffererId());
			results.addAll(oneRecords);
		}
		return results;
	}
	
	
	private List<SuffererTracePO> firstRecordSuffererTrace(SuffererInfoPO po,String sufffererId){
		
		List<SuffererTracePO> records = new ArrayList<>();
		UserPO  userPO = UserContext.getUser();
		
		SuffererTracePO trace = new SuffererTracePO();
		trace.setId(UUIDUtils.getUUID());
		trace.setSuffererId(sufffererId);
		trace.setSuffererName(po.getSuffererName());
		trace.setSiteId(po.getCurSiteId());
		trace.setSiteName(po.getCurSiteName());
		trace.setTraceType(SuffererTraceTypeEnum.SHIFT_IN.getCode());
		trace.setChangeTime(po.getMoveIntoTime());
		trace.setSequenceNo(0);
		trace.setCreateTime(Calendar.getInstance().getTime());
		if(userPO != null){
			trace.setCreateBy(userPO.getRealname());
			trace.setUpdateBy(userPO.getRealname());
		}
		trace.setUpdateTime(Calendar.getInstance().getTime());
		records.add(trace);
		
		trace = new SuffererTracePO();
		trace.setId(UUIDUtils.getUUID());
		trace.setSuffererId(po.getId());
		trace.setSuffererName(po.getSuffererName());
		trace.setSiteId(po.getCurSiteId());
		trace.setSiteName(po.getCurSiteName());
		trace.setTraceType(po.getSuffererType());
		trace.setSequenceNo(0);
		trace.setCreateTime(Calendar.getInstance().getTime());
		if(userPO != null){
			trace.setCreateBy(userPO.getRealname());
			trace.setUpdateBy(userPO.getRealname());
		}
		trace.setUpdateTime(Calendar.getInstance().getTime());
		records.add(trace);
		
		return records;
	}
}
