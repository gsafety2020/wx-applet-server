package com.gsafety.gemp.wxapplet.infection.dao.po;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.gsafety.gemp.wxapplet.infection.dao.po.SuffererInfoPO.SuffererInfoPOBuilder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
* @ClassName: SuffererInfoAllPO
* @Description:  统计局患者备份表
* @author luoxiao
* @date 2020年3月5日 下午6:52:16
*
 */
@Entity
@Table(name = "sufferer_info_all")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SuffererInfoAllPO implements Serializable{

	private static final long serialVersionUID = -1548629281175109475L;
    @Id
    @Column(name = "id")
    private String id;

    /**患者Id*/
    @Column(name = "sufferer_id")
    private String suffererId;

    /**来自那个区编码*/
    @Column(name = "from_area_code")
    private String fromAreaCode;

    /**转入区地址*/
    @Column(name = "from_address")
    private String fromAddress;

    /**区编码*/
    @Column(name = "area_code")
    private String areaCode;

    /**区名称*/
    @Column(name = "area_name")
    private String areaName;

    /**隔离者，患者姓名*/
    @Column(name = "sufferer_name")
    private String suffererName;

    /**病人类型：1.密接人员；2.发热患者；3.疑似患者；4.确诊; 99.其他;*/
    @Column(name = "sufferer_type")
    private Integer suffererType;

    /**病人类型改变时间*/
    @Column(name = "sufferer_type_time")
    private Date suffererTypeTime;

    /**病患状态：1.隔离点在管（进入）；2.在院治疗（进入）；3.治愈出院（离开）；4.病亡（离开）；5.转出（离开）；99.其他；*/
    @Column(name = "sufferer_status")
    private Integer suffererStatus;

    /**病患状态改变时间*/
    @Column(name = "sufferer_status_time")
    private Date suffererStatusTime;

    /**病情. 1.危重；2重症；3.其他*/
    @Column(name = "illness_state")
    private Integer illnessState;

    /**病情改变时间*/
    @Column(name = "illness_state_time")
    private Date illnessStateTime;

    /**病情时间*/
    @Column(name = "illness_begin_time")
    private Date illnessBeginTime;

    /**年龄*/
    @Column(name = "sufferer_age")
    private Float suffererAge;

    /**年龄*/
    @Column(name = "sufferer_gender")
    private String suffererGender;

    /**身份证*/
    @Column(name = "sufferer_card")
    private String suffererCard;

    /**隔离者联系电话*/
    @Column(name = "sufferer_tel")
    private String suffererTel;

    /**隔离者住址*/
    @Column(name = "sufferer_address")
    private String suffererAddress;

    /**当前所在隔离点或医院ID*/
    @Column(name = "cur_site_id")
    private String curSiteId;

    /**当前所在隔离点或医院名称*/
    @Column(name = "cur_site_name")
    private String curSiteName;

    /**房号*/
    @Column(name = "room_number")
    private String roomNumber;

    /**备注*/
    @Column(name = "remark")
    private String remark;

    /**序号*/
    @Column(name = "seq_number")
    private Integer seqNumber;

    /**入住时间*/
    @Column(name = "move_into_time")
    private Date moveIntoTime;

    /**转出时间*/
    @Column(name = "turn_out_time")
    private Date turnOutTime;

    /**转出目标地类型。1：隔离点；2.医院；3.自家；4.其他；*/
    @Column(name = "turn_out_to_place_type")
    private Integer turnOutToPlaceType;

    /**转出目标地名称*/
    @Column(name = "turn_out_to_place")
    private String turnOutToPlace;

    /**预计解除时间*/
    @Column(name = "turn_out_plan_time")
    private Date turnOutPlanTime;

    /**死亡时间*/
    @Column(name = "death_date")
    private Date deathDate;

    @Column(name = "sourcetype")
    private Integer sourcetype;

    @Column(name = "data_date")
    private String dataDate;

    /**创建者名称*/
    @Column(name = "create_by")
    private String createBy;

    /**更新者名称*/
    @Column(name = "update_by")
    private String updateBy;

    /**创建时间*/
    @Column(name = "create_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createTime;

    /**更新时间*/
    @Column(name = "update_time")
    private Date updateTime;

    /** 隔离点床位数 **/
    @Column(name = "sufferer_site_bed_num")
    private Integer suffererSiteBedNum;
}
