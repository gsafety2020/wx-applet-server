package com.gsafety.gemp.wxapplet.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.gsafety.gemp.wxapplet.infection.dao.po.UserPO;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * JWTUtils
 */
@Component
public class JWTUtils {

    /**
     * 密钥
     */
    private static final String secret = "ncov";

    /**
     * 发行者
     */
    private static final String Issuer = "gsafety";

    /**
     * 签名的主题
     */
    private static final String Subject = "admin";

    public static String create(UserPO user) {
        // TODO 改成使用数据库测试数据，结合Redis缓存用户信息？？？？
        return JWT.create()
                /*设置头部信息 Header，可以不设置，使用默认值*/
//                .withHeader()
                .withClaim("loginName", "gsafety")
                .withIssuer(Issuer)
                //签名是有谁生成 例如 服务器
                .withSubject(Subject)
                .withAudience(user.getId())
                .withIssuedAt(new Date())
                .withExpiresAt(new Date(System.currentTimeMillis() + 7200 * 1000))
                /*签名 Signature */
                .sign(Algorithm.HMAC256(secret));

    }

    /**
     *
     * @param token
     * @return
     */
    public static DecodedJWT verify(String token) {
        try {
            JWTVerifier verifier = JWT.require(Algorithm.HMAC256(secret))
                    .withIssuer(Issuer)
                    .build();
            return verifier.verify(token);
        } catch (JWTVerificationException exception) {
            exception.printStackTrace();
        }
        return null;
    }

}
