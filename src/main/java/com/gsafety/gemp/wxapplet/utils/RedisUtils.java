package com.gsafety.gemp.wxapplet.utils;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class RedisUtils {

	@Autowired
	private RedisTemplate<String, Object> redisTemplate;
	
	/**
	 * 
	* @Title: expire 
	* @Description: 指定缓存失效时间
	* @param @param key 键
	* @param @param time 时间(秒)
	* @param @return    设定文件 
	* @return boolean    返回类型 
	* @throws
	 */
	public boolean expire(String key,long time){
        try {
            if(time>0){
                redisTemplate.expire(key, time, TimeUnit.SECONDS);
            }
            return true;
        } catch (Exception e) {
            log.error("RedisUtils expire(String key,long time) failure."+e.getMessage());
            return false;
        }
    }
	
	/**
	 * 
	* @Title: getExpire 
	* @Description: 获取过期时间
	* @param @param key key 键 不能为null
	* @param @return    时间(秒) 返回0代表为永久有效
	* @return long    返回类型 
	* @throws
	 */
	public long getExpire(String key){
        return redisTemplate.getExpire(key,TimeUnit.SECONDS);
    }
	
	/**
	 * 
	* @Title: hasKey 
	* @Description: 判断key是否存在
	* @param @param key 键
	* @param @return   true 存在 false不存在
	* @return boolean    返回类型 
	* @throws
	 */
	public boolean hasKey(String key){
        try {
            return redisTemplate.hasKey(key);
        } catch (Exception e) {
            log.error("RedisUtils hasKey(String key) failure."+e.getMessage());
            return false;
        }
    }
	
	/**
	 * 
	* @Title: del 
	* @Description:删除缓存
	* @param @param key   可以传一个值 或多个
	* @return void    返回类型 
	* @throws
	 */
	@SuppressWarnings("unchecked")
	public  void del(String ... key){
        if(key!=null&&key.length>0){
            if(key.length==1){
                redisTemplate.delete(key[0]);
            }else{
                redisTemplate.delete(CollectionUtils.arrayToList(key));
            }
        }
    }
	
	/**
	 * 
	* @Title: get 
	* @Description: 普通缓存获取
	* @param @param key 键
	* @param @return    设定文件 
	* @return Object    返回类型 
	* @throws
	 */
	public  Object get(String key){
        return key==null?null:redisTemplate.opsForValue().get(key);
    }
	
	/**
	 * 
	* @Title: set 
	* @Description: 普通缓存放入
	* @param @param key
	* @param @param value
	* @param @return    true成功 false失败
	* @return boolean    返回类型 
	* @throws
	 */
	public  boolean set(String key,Object value) {
        try {
            redisTemplate.opsForValue().set(key, value);
            return true;
        } catch (Exception e) {
        	e.printStackTrace();
            log.error("RedisUtils set(String key,Object value) failure."+e.getMessage());
            return false;
        }
    }
	
	/**
	 * 
	* @Title: set 
	* @Description: 普通缓存放入并设置时间
	* @param @param key 键 
	* @param @param value 值
	* @param @param time 时间(秒) time要大于0 如果time小于等于0 将设置无限期
	* @param @return    设定文件 
	* @return boolean    返回类型 
	* @throws
	 */
	public  boolean set(String key,Object value,long time){
        try {
            if(time>0){
                redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
            }else{
                set(key, value);
            }
            return true;
        } catch (Exception e) {
            log.error("RedisUtils set(String key,Object value,long time) failure."+e.getMessage());
            return false;
        }
    }
	
	/**
	 * 
	* @Title: hget 
	* @Description: 值
	* @param @param key
	* @param @param item
	* @return Object    返回类型 
	* @throws
	 */
	public  Object hget(String key,String item){
        return redisTemplate.opsForHash().get(key, item);
    }
	
	/**
	 * 
	* @Title: hmget 
	* @Description: 获取hashKey对应的所有键值
	* @param @param key
	* @param @return    设定文件 
	* @return Map<Object,Object>    返回类型 
	* @throws
	 */
	public  Map<Object,Object> hmget(String key){
	    return redisTemplate.opsForHash().entries(key);
	}
	
	/**
	 * 
	* @Title: hmset 
	* @Description: HashSet
	* @param @param key 键
	* @param @param map 对应多个键值
	* @param @return    设定文件 
	* @return boolean    返回类型 
	* @throws
	 */
	public  boolean hmset(String key, Map<String,Object> map){
        try {
            redisTemplate.opsForHash().putAll(key, map);
            return true;
        } catch (Exception e) {
            log.error("RedisUtils hmset(String key, Map<String,Object> map) failure."+e.getMessage());
            return false;
        }
    }
	
	/**
	 * 
	* @Title: HashSet  并设置时间
	* @Description:  HashSet  并设置时间
	* @param @param key 键
	* @param @param map  对应多个键值
	* @param @param time 时间(秒)
	* @param @return    true成功 false失败
	* @return boolean    返回类型 
	* @throws
	 */
	public  boolean hmset(String key, Map<String,Object> map, long time){
        try {
            redisTemplate.opsForHash().putAll(key, map);
            if(time>0){
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            log.error("RedisUtils hmset(String key, Map<String,Object> map, long time) failure."+e.getMessage());
            return false;
        }
    }
	
	/**
	 * 
	* @Title: hset 
	* @Description:  向一张hash表中放入数据,如果不存在将创建
	* @param @param key  键
	* @param @param item 项
	* @param @param value 值
 	* @return true 成功 false失败
	* @return boolean    返回类型 
	* @throws
	 */
	public  boolean hset(String key,String item,Object value) {
        try {
            redisTemplate.opsForHash().put(key, item, value);
            return true;
        } catch (Exception e) {
            log.error("RedisUtils hset(String key,String item,Object value) failure."+e.getMessage());
            return false;
        }
    }
	
	/**
	 * 
	* @Title: hset 
	* @Description: 向一张hash表中放入数据,如果不存在将创建
	* @param @param key
	* @param @param item
	* @param @param value
	* @param @param time 时间(秒)  注意:如果已存在的hash表有时间,这里将会替换原有的时间
	* @param @return    设定文件 
	* @return boolean    true 成功 false失败
	* @throws
	 */
	public  boolean hset(String key,String item,Object value,long time) {
        try {
            redisTemplate.opsForHash().put(key, item, value);
            if(time>0){
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            log.error("RedisUtils hset(String key,String item,Object value,long time) failure."+e.getMessage());
            return false;
        }
    }
	
	
	/**
	 * 
	* @Title: hdel 
	* @Description:  删除hash表中的值
	* @param @param key 键 不能为null
	* @param @param item 项 可以使多个 不能为null
	* @return void    返回类型 
	* @throws
	 */
    public  void hdel(String key, Object... item){
        redisTemplate.opsForHash().delete(key,item);
    }
    
    /**
     * 
    * @Title: hHasKey 
    * @Description: 判断hash表中是否有该项的值
    * @param @param key key 键 不能为null
    * @param @param item 项 不能为null
    * @param @return true 存在 false不存在
    * @return boolean    返回类型 
    * @throws
     */
    public  boolean hHasKey(String key, String item){
        return redisTemplate.opsForHash().hasKey(key, item);
    }
    
    /**
     * 
    * @Title: sGet 
    * @Description: TODO(这里用一句话描述这个方法的作用) 
    * @param @param key
    * @param @return    设定文件 
    * @return Set<Object>    返回类型 
    * @throws
     */
    public  Set<Object> sGet(String key){
        try {
            return redisTemplate.opsForSet().members(key);
        } catch (Exception e) {
            log.error("RedisUtils sGet(String key) failure."+e.getMessage());
            return null;
        }
    }
    
    /**
     * 
    * @Title: sHasKey 
    * @Description: 根据value从一个set中查询,是否存在
    * @param @param key
    * @param @param value
    * @param @return    true 存在 false不存在
    * @return boolean    返回类型 
    * @throws
     */
    public  boolean sHasKey(String key,Object value){
        try {
            return redisTemplate.opsForSet().isMember(key, value);
        } catch (Exception e) {
            log.error("RedisUtils sHasKey(String key,Object value) failure."+e.getMessage());
            return false;
        }
    }
    
    /**
     * 
    * @Title: sSet 
    * @Description: 将数据放入set缓存
    * @param @param key
    * @param @param values
    * @param @return   成功个数
    * @return long    返回类型 
    * @throws
     */
    public  long sSet(String key, Object...values) {
        try {
            return redisTemplate.opsForSet().add(key, values);
        } catch (Exception e) {
            log.error("RedisUtils sSet(String key, Object...values) failure."+e.getMessage());
            return 0;
        }
    }
    
    /**
     * 
    * @Title: sSetAndTime 
    * @Description:  将set数据放入缓存
    * @param @param key
    * @param @param time 时间(秒)
    * @param @param values
    * @param @return    设定文件 
    * @return long    返回类型 
    * @throws
     */
    public   long sSetAndTime(String key,long time,Object...values) {
        try {
            Long count = redisTemplate.opsForSet().add(key, values);
            if(time>0) {
                expire(key, time);
            }
            return count;
        } catch (Exception e) {
            log.error("RedisUtils sSetAndTime(String key,long time,Object...values) failure."+e.getMessage());
            return 0;
        }
    }
    
    /**
     * 
    * @Title: sGetSetSize 
    * @Description: 获取set缓存的长度
    * @param @param key 键
    * @param @return    设定文件 
    * @return long    返回类型 
    * @throws
     */
    public  long sGetSetSize(String key){
        try {
            return redisTemplate.opsForSet().size(key);
        } catch (Exception e) {
            log.error("RedisUtils sGetSetSize(String key) failure."+e.getMessage());
            return 0;
        }
    }

    /**
     * 
    * @Title: setRemove 
    * @Description: 移除值为value的 
    * @param @param key
    * @param @param values
    * @param @return  移除的个数
    * @return long    返回类型 
    * @throws
     */
    public   long setRemove(String key, Object ...values) {
        try {
            Long count = redisTemplate.opsForSet().remove(key, values);
            return count;
        } catch (Exception e) {
            log.error("RedisUtils setRemove(String key, Object ...values) failure."+e.getMessage());
            return 0;
        }
    }
    
    public  List<Object> lGet(String key, long start, long end){
        try {
            return redisTemplate.opsForList().range(key, start, end);
        } catch (Exception e) {
            log.error("RedisUtils lGet(String key, long start, long end) failure."+e.getMessage());
            return null;
        }
    }
    
    /**
     * 
    * @Title: lGetListSize 
    * @Description:获取list缓存的长度
    * @param @param key
    * @param @return    设定文件 
    * @return long    返回类型 
    * @throws
     */
    public  long lGetListSize(String key){
        try {
            return redisTemplate.opsForList().size(key);
        } catch (Exception e) {
            log.error("RedisUtils lGetListSize(String key) failure."+e.getMessage());
            return 0;
        }
    }
    
    /**
     * 
    * @Title: lGetIndex 
    * @Description: 通过索引 获取list中的值
    * @param @param key
    * @param @param index
    * @param @return  index 索引  index>=0时， 0 表头，1 第二个元素，依次类推；index<0时，-1，表尾，-2倒数第二个元素，依次类推
    * @return Object    返回类型 
    * @throws
     */
    public   Object lGetIndex(String key,long index){
        try {
            return redisTemplate.opsForList().index(key, index);
        } catch (Exception e) {
            log.error("RedisUtils lGetIndex(String key,long index) failure."+e.getMessage());
            return null;
        }
    }

    /**
     * 
    * @Title: lSet 
    * @Description: 将list放入缓存
    * @param @param key
    * @param @param value
    * @param @return    设定文件 
    * @return boolean    返回类型 
    * @throws
     */
    public  boolean lSet(String key, Object value) {
        try {
            redisTemplate.opsForList().rightPush(key, value);
            return true;
        } catch (Exception e) {
            log.error("RedisUtils lSet(String key, Object value) failure."+e.getMessage());
            return false;
        }
    }

    /**
     * 
    * @Title: lSet 
    * @Description: 将list放入缓存
    * @param @param key
    * @param @param value
    * @param @param time 时间(秒)
    * @param @return    设定文件 
    * @return boolean    返回类型 
    * @throws
     */
    public  boolean lSet(String key, Object value, long time) {
        try {
            redisTemplate.opsForList().rightPush(key, value);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            log.error("RedisUtils lSet(String key, Object value, long time) failure."+e.getMessage());
            return false;
        }
    }
    
    /**
     * 
    * @Title: lUpdateIndex 
    * @Description: 根据索引修改list中的某条数据 
    * @param @param key 键
    * @param @param index 索引
    * @param @param value 值
    * @param @return    设定文件 
    * @return boolean    返回类型 
    * @throws
     */
    public   boolean lUpdateIndex(String key, long index,Object value) {
        try {
            redisTemplate.opsForList().set(key, index, value);
            return true;
        } catch (Exception e) {
            log.error("RedisUtils lUpdateIndex(String key, long index,Object value) failure."+e.getMessage());
            return false;
        }
    }
    
    /**
     * 
    * @Title: lRemove 
    * @Description: 移除多个值为value
    * @param @param key
    * @param @param count 移除多少个
    * @param @param value
    * @param @return    设定文件 
    * @return long    返回类型 
    * @throws
     */
    public  long lRemove(String key,long count,Object value) {
        try {
            Long remove = redisTemplate.opsForList().remove(key, count, value);
            return remove;
        } catch (Exception e) {
            log.error("RedisUtils lRemove(String key,long count,Object value) failure."+e.getMessage());
            return 0;
        }
    }

}
