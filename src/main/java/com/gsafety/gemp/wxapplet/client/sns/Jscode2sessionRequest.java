/**
 * 
 */
package com.gsafety.gemp.wxapplet.client.sns;

import org.springframework.http.HttpMethod;

import com.gsafety.gemp.wxapplet.client.Request;



/**
 * @author yangbo
 *
 */
public class Jscode2sessionRequest implements Request<Jscode2sessionResponse> {

	/**
	 * 
	 */
	private static final String DEFAUTL_GRANTTYPE = "authorization_code";

	/**
	 * 
	 */
	private final String appid;

	/**
	 * 
	 */
	private final String secret;

	/**
	 * 
	 */
	private final String jsCode;

	/**
	 * 
	 */
	private final String grantType;

	/**
	 * @return the appid
	 */
	public String getAppid() {
		return appid;
	}

	/**
	 * @return the secret
	 */
	public String getSecret() {
		return secret;
	}

	/**
	 * @return the jsCode
	 */
	public String getJsCode() {
		return jsCode;
	}

	/**
	 * @return the grantType
	 */
	public String getGrantType() {
		return grantType;
	}

	/**
	 * @param appid
	 * @param secret
	 * @param jsCode
	 */
	public Jscode2sessionRequest(String appid, String secret, String jsCode) {
		this(appid, secret, jsCode, DEFAUTL_GRANTTYPE);
	}

	/**
	 * @param appid
	 * @param secret
	 * @param jsCode
	 * @param grantType
	 */
	public Jscode2sessionRequest(String appid, String secret, String jsCode, String grantType) {
		super();
		this.appid = appid;
		this.secret = secret;
		this.jsCode = jsCode;
		this.grantType = grantType;
	}

	@Override
	public HttpMethod getHttpMethod() {
		return HttpMethod.GET;
	}

	@Override
	public String getMethodScope() {
		return "jscode2session";
	}

	@Override
	public Class<Jscode2sessionResponse> getResponseClass() {
		return Jscode2sessionResponse.class;
	}

	@Override
	public Object getRequestMessage() {
		StringBuilder sb = new StringBuilder();
		sb.append("appid=" + this.appid);
		sb.append("&secret=" + this.secret);
		sb.append("&js_code=" + this.jsCode);
		sb.append("&grant_type=" + this.grantType);
		return sb.toString();
	}

}