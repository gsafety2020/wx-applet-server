package com.gsafety.gemp.wxapplet.healthcode.contract.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
* @ClassName: PestHealthCodeDTO 
* @Description: 健康码入参
* @author luoxiao
* @date 2020年2月21日 下午6:48:11 
*
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("城市编码返回参数")
public class PestHealthCodeDTO implements Serializable{
	
	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value="主键OPENID+AREACODE")
	private String healthCodeId;
	
	@ApiModelProperty(value="小程序openId")
	private String openId;
	
	@ApiModelProperty(value="简称")
	private String nickName;
	
	@ApiModelProperty(value="用户姓名")
	private String userName;
	
	@ApiModelProperty(value="手机号")
	private String mobile;
	
	@ApiModelProperty(value="地址")
	private String address;
	
	@ApiModelProperty(value="身份证")
	private String identityCard;
	
	@ApiModelProperty(value="省编码")
	private String provinceCode;
	
	@ApiModelProperty(value="地区编码")
	private String areaCode;
	
	@ApiModelProperty(value="地区父节点编码")
	private String parentCode;
	
	@ApiModelProperty(value="地区名称")
	private String areaName;
	
	@ApiModelProperty(value="省名称")
	private String provinceName;
	
	@ApiModelProperty(value="用户头像")
	private String imageUrl;
	
	@ApiModelProperty(value="当前是否在[城市](1:是，0:否)")
	private String currentCity;
	
	@ApiModelProperty(value="您最近14天是否离开过[城市](1:是，0:否)")
	private String fornightLeaveCity;
	
	@ApiModelProperty(value="您所住楼道是否有确诊病人或者密切接触者(1:是，0:否)")
	private String nearHavePatient;
	
	@ApiModelProperty(value="你接触过发热或呼吸道症状病人(1:是，0:否)")
	private String touchPatient;
	
	@ApiModelProperty(value="当前的健康状态是否正常(1:是，0:否)")
	private String normalHealth;
	
	@ApiModelProperty(value="健康码颜色(后台自动生成,red：红色，green：绿色，yellow：黄色)")
	private String healthCodeColor;
	
	@ApiModelProperty(value="二维码路径")
	private String healthCodeUrl;
	
	@ApiModelProperty(value="创建时间")
	@JsonFormat(pattern = "yyyy-MM-dd",locale = "zh",timezone = "GMT+8")
	private Date createTime;
	
	@ApiModelProperty(value="更新时间")
	@JsonFormat(pattern = "yyyy-MM-dd",locale = "zh",timezone = "GMT+8")
	private Date updateTime;

	@ApiModelProperty(value="总分")
	private Integer score;
}
