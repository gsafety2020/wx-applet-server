package com.gsafety.gemp.wxapplet.infection.contract.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 患者基本信息
 *
 * @author zxiao
 * @since 2020/2/27  14:17
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("用户基本信息")
public class UserDTO implements Serializable {

    private static final long serialVersionUID = -7033143140164839207L;

    @ApiModelProperty(value = "用户ID")
    private String id;

    @ApiModelProperty("用户登录名")
    private String username;

    @ApiModelProperty(value = "小程序id")
    private String openid;

    @ApiModelProperty(value = "工作人员真实姓名")
    private String realname;

    @ApiModelProperty(value = "工作人员联系方式")
    private String phone;

    @ApiModelProperty(value = "身份证")
    private String idCard;

    @ApiModelProperty(value = "隔离点群id,关联site_inffo表主键")
    private String siteIdGroup;

    @ApiModelProperty(value = "隔离点群id,关联site_inffo表主键")
    private String pwd;

    @ApiModelProperty(value = "用户Token")
    private String token;

}
