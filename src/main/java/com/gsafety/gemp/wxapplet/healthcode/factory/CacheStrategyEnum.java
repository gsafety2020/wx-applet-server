package com.gsafety.gemp.wxapplet.healthcode.factory;

/**
 * 
* @ClassName: CacheStrategyEnum 
* @Description: 缓存策略 枚举类
* @author luoxiao
* @date 2020年2月29日 下午1:51:25 
*
 */
public enum CacheStrategyEnum {

	LOCAL("local","本地缓存"),REDIS("redis","redis缓存");
	
	private String code;
	
	private String value;

	private CacheStrategyEnum(String code, String value) {
		this.code = code;
		this.value = value;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
