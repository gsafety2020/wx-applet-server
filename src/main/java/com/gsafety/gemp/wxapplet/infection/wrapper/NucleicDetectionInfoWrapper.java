package com.gsafety.gemp.wxapplet.infection.wrapper;

import com.gsafety.gemp.wxapplet.infection.contract.dto.NucleicDetectionInfoDTO;
import com.gsafety.gemp.wxapplet.infection.contract.enums.*;
import com.gsafety.gemp.wxapplet.infection.dao.po.NucleicDetectionInfoPO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.ArrayList;
import java.util.List;

public class NucleicDetectionInfoWrapper {

    public static NucleicDetectionInfoDTO toDTO(NucleicDetectionInfoPO po) {
        NucleicDetectionInfoDTO dto = new NucleicDetectionInfoDTO();
        BeanUtils.copyProperties(po, dto);
        if(StringUtils.isNotBlank(po.getDetectionResult())){
            dto.setDetectionResult(NucleicDetectionResultEnum.getNameByCode(po.getDetectionResult()));
        }
        if(StringUtils.isNotBlank(po.getIsolationType())){
            dto.setIsolationType(NucleicIsolationTypeEnum.getNameByCode(po.getIsolationType()));
        }
        if(StringUtils.isNotBlank(po.getGender())){
            dto.setGender(PointGenderEnum.getNameByCode(po.getGender()));
        }
        if(StringUtils.isNotBlank(po.getOverseasReturnFlag())){
            dto.setOverseasReturnFlag(NucleicOverseasFlagEnum.getNameByCode(po.getOverseasReturnFlag()));
        }
        return dto;
    }

    public static List<NucleicDetectionInfoDTO> toDTOList(List<NucleicDetectionInfoPO> records) {
        List<NucleicDetectionInfoDTO> list = new ArrayList<>();
        records.stream().forEach(record -> list.add(toDTO(record)));
        return list;
    }

    public static Page<NucleicDetectionInfoDTO> toPageDTO(Page<NucleicDetectionInfoPO> page){
        List<NucleicDetectionInfoDTO> content = toDTOList(page.getContent());
        return new PageImpl<>(content, page.getPageable(), page.getTotalElements());
    }
}
