package com.gsafety.gemp.wxapplet.infection.contract.enums;

import com.gsafety.gemp.wxapplet.infection.contract.dto.CaseTypeDTO;

import java.util.ArrayList;
import java.util.List;

public enum CaseTypeStyleEnum {

    TYPE_CC("0","#35AA47"),
    TYPE_LINCHUANG("1","#FFA944"),
    TYPE_QUEZHEN("2","#EB0000"),
    TYPE_YANGXING("3","#FF5ABF"),
    TYPE_YISHI("4","#CEDB00");

    private String code;
    private String name;
    CaseTypeStyleEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static String getNameByCode(String code) {
        for (CaseTypeStyleEnum caseTypeEnum : values()) {
            if (caseTypeEnum.code.equals(code)){
                return caseTypeEnum.name;
            }
        }
        return null;
    }

    public static String getCodeByName(String name) {
        for (CaseTypeStyleEnum caseTypeEnum : values()) {
            if (caseTypeEnum.name.equals(name)){
                return caseTypeEnum.code;
            }
        }
        return null;
    }

    public static List<CaseTypeDTO> getList(){
        List<CaseTypeDTO> list=new ArrayList<>();
        CaseTypeDTO caseTypeDTO=null;
        for (CaseTypeStyleEnum caseTypeEnum : values()) {
            caseTypeDTO=new CaseTypeDTO();
            caseTypeDTO.setCode(Integer.parseInt(caseTypeEnum.code));
            caseTypeDTO.setName(caseTypeEnum.name);
            list.add(caseTypeDTO);
        }
        return list;
    }

}
