package com.gsafety.gemp.wxapplet.infection.contract.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
* @ClassName: SuffererTypeUpdateDTO 
* @Description: 更新患者类型入参
* @author luoxiao
* @date 2020年2月28日 下午10:15:33 
*
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("更新患者类型入参")
public class SuffererTypeUpdateDTO implements Serializable{

	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;
	
    @ApiModelProperty(value = "患者ID")
	private String id;
	
    @ApiModelProperty(value = "病人类型：1.密接人员；2.发热患者；3.疑似患者；4.确诊; 5.其他;")
    private Integer suffererType;
	
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd hh:mm")
	@ApiModelProperty(value = "患者类型改变时间(格式yyyy-MM-dd hh:mm)")
	private Date suffererTypeTime;
	
    @ApiModelProperty(value = "用户sessionkey")
    private String sessionKey;

}
