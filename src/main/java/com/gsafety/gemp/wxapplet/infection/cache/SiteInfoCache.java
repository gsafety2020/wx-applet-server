package com.gsafety.gemp.wxapplet.infection.cache;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gsafety.gemp.wxapplet.infection.dao.SiteInfoDao;
import com.gsafety.gemp.wxapplet.infection.dao.po.SiteInfoPO;

@Component
public class SiteInfoCache{

	@Autowired
	private SiteInfoDao siteInfoDao;
	
	private static Map<String,String> map = new HashMap<String,String>();
	
	@PostConstruct
	public void init(){
		List<SiteInfoPO> po = siteInfoDao.findAll();
		//转成Map格式  key: site_name   value:site_id
		map = po.stream().collect(Collectors.toMap(SiteInfoPO::getSiteName, SiteInfoPO::getId));
	}

	public Map<String,String> getMapKeyIdValueNameCache(){
		return map;
	}
	
	public void refreashMapKeyIdValueNameCache(){
		map.clear();
		List<SiteInfoPO> po = siteInfoDao.findAll();
		map = po.stream().collect(Collectors.toMap(SiteInfoPO::getSiteName, SiteInfoPO::getId));
	}
	
}
