package com.gsafety.gemp.wxapplet.infection.contract.enums;

import com.gsafety.gemp.wxapplet.utils.IntegerCodeEnum;
import lombok.Getter;
import org.apache.commons.lang.StringUtils;

/**
 * 病情
 *
 * @author liyanhong
 * @since 2020/2/27  11:13
 */
@Getter
public enum IllnessStateEnum implements IntegerCodeEnum{

    CURING(1, "治愈"),
   // DYING(2, "死亡"),
    GRAVE(3, "危重"),
    ICU(4,"重症"),
    OTHER(5, "其他");

    private Integer code;
    private String value;

    IllnessStateEnum(Integer code, String value) {
        this.code = code;
        this.value = value;
    }

    public static String getValueByCode(Integer code) {
        for (IllnessStateEnum en : IllnessStateEnum.values()) {
            if (en.getCode() == code) {
                return en.getValue();
            }
        }
        return StringUtils.EMPTY;
    }
}
