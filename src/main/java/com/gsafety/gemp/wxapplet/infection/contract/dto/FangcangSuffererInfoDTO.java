package com.gsafety.gemp.wxapplet.infection.contract.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author dusiwei
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FangcangSuffererInfoDTO {

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "姓名")
    private String suffererName;

    @ApiModelProperty(value = "性别")
    private String suffererSex;

    @ApiModelProperty(value = "年龄")
    private String suffererAge;

    @ApiModelProperty(value = "入院时间,yyyy-MM-dd")
    private String admittedTime;

    @ApiModelProperty(value = "病区")
    private String ward;

    @ApiModelProperty(value = "床号")
    private String bedNo;

    @ApiModelProperty(value = "身份证号")
    private String idNo;

    @ApiModelProperty(value = "地址")
    private String suffererAddress;

    @ApiModelProperty(value = "联系电话")
    private String telephone;

    @ApiModelProperty(value = "状态：1在舱,2转院,3出院")
    private String suffererStatus;

    @ApiModelProperty(value = "状态：1在舱,2转院,3出院")
    private String suffererStatusName;

    @ApiModelProperty(value = "转院日期")
    private String transferDate;

    @ApiModelProperty(value = "转往医院")
    private String transferHospital;

    @ApiModelProperty(value = "出院日期")
    private String leaveDate;

    @ApiModelProperty(value = "所属区划")
    private String district;

    @ApiModelProperty(value = "导入日期")
    private String importDate;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "数据日期yyyyMMdd")
    private String dataTime;

    @ApiModelProperty(value = "医院名称，默认江夏方舱医院")
    private String hospitalName;

}
