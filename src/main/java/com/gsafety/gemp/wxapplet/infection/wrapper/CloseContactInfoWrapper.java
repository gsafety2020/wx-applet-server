package com.gsafety.gemp.wxapplet.infection.wrapper;

import com.gsafety.gemp.wxapplet.infection.contract.dto.CloseContactInfoDTO;
import com.gsafety.gemp.wxapplet.infection.dao.po.CloseContactInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

public class CloseContactInfoWrapper {

    public static CloseContactInfoDTO toDTO(CloseContactInfo po) {
        CloseContactInfoDTO dto = new CloseContactInfoDTO();
        BeanUtils.copyProperties(po, dto);
        if(StringUtils.isNotEmpty(dto.getCcIdno())){
            dto.setInfectedType("1");
        }
        return dto;
    }

    public static List<CloseContactInfoDTO> toDTOList(List<CloseContactInfo> list){
        List<CloseContactInfoDTO> dtoList = new ArrayList<>();
        list.stream().forEach(po -> dtoList.add(toDTO(po)));
        return dtoList;
    }
}
