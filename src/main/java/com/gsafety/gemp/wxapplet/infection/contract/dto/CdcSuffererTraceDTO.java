package com.gsafety.gemp.wxapplet.infection.contract.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CdcSuffererTraceDTO {

    private String suffererName ;
    private List<CdcSuffererTracesDTO> list;
}
