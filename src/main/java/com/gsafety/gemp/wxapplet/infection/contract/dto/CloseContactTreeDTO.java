package com.gsafety.gemp.wxapplet.infection.contract.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author lzx
 * @version 1.0
 * @date 2020/3/25 15:23
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CloseContactTreeDTO {

    private String id;
    @ApiModelProperty(value = "是否根节点")
    private boolean root = false;
    @ApiModelProperty(value = "患者姓名")
    private String suffererName;
    @ApiModelProperty(value = "患者身份证号")
    private String suffererIdno;
    @ApiModelProperty(value = "患者")
    private String suffererType;
    @ApiModelProperty(value = "父节点")
    private String parentId;
    @ApiModelProperty(value = "样式-背景色")
    private String backGroundColor;
//    private List<CloseContactTreeDTO> childTreeDto;

}
