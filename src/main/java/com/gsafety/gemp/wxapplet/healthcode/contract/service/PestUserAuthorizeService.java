package com.gsafety.gemp.wxapplet.healthcode.contract.service;

import java.util.List;

import com.gsafety.gemp.wxapplet.healthcode.contract.dto.PestUserAuthorizeDTO;

/**
 * 
* @Description: 用户权限服务类
* @author xuelq
* @date 2020年2月27日 下午3:55:04 
*
 */
public interface PestUserAuthorizeService {

	/**
	 * 根据参数获取权限列表
	 * @param mobile 手机号码
	 * @param auths 权限数组
	 * @return List<PestUserAuthorizeDTO>
	 */
	List<PestUserAuthorizeDTO> getUserAuthorizes(String mobile, String[] auths);
	
	/**
	 * 根据参数获取权限数
	 * @param mobile 手机号码
	 * @param auths 权限数组
	 * @return int
	 */
	int getUserAuthorizeCount(String mobile, String[] auths);
	
}
