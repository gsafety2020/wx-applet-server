package com.gsafety.gemp.wxapplet.infection.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.gsafety.gemp.wxapplet.infection.contract.iface.SuffererInfoAllService;
import com.gsafety.gemp.wxapplet.infection.dao.SuffererInfoAllDao;
import com.gsafety.gemp.wxapplet.infection.dao.SuffererInfoDao;
import com.gsafety.gemp.wxapplet.infection.dao.po.SuffererInfoAllPO;
import com.gsafety.gemp.wxapplet.infection.dao.po.SuffererInfoPO;
import com.gsafety.gemp.wxapplet.utils.UUIDUtils;

/**
 * 
* @ClassName: SuffererInfoAllServiceImpl 
* @Description: 统计局患者服务实现类
* @author luoxiao
* @date 2020年3月5日 下午6:59:15 
*
 */
@Service("suffererInfoAllService")
public class SuffererInfoAllServiceImpl implements SuffererInfoAllService{

	@Autowired
	private SuffererInfoDao suffererInfoDao;
	
	@Autowired
	private SuffererInfoAllDao suffererInfoAllDao;

	@Override
	@Transactional
	public void bakYesterdaySuffererInfoData() {
		//拿到suffererInfo表中昨天的数据
		List<SuffererInfoPO> suffererInfos = suffererInfoDao.findAll();
		//数据处理
		if(!CollectionUtils.isEmpty(suffererInfos)){ //suffererInfo表中存在需要备份的数据
			List<SuffererInfoAllPO> suffererInfoAlls = convertSuffererInfoToSuffererInfoAll(suffererInfos);
			suffererInfoAllDao.saveAll(suffererInfoAlls);
		}
		//备份完成后，删除主表数据
		suffererInfoDao.deleteAll();;
	}
		
	private List<SuffererInfoAllPO> convertSuffererInfoToSuffererInfoAll(List<SuffererInfoPO> list){
		List<SuffererInfoAllPO> lists = new ArrayList<SuffererInfoAllPO>();
		SuffererInfoAllPO suffererInfoAll = null;
		for(SuffererInfoPO po : list){
			suffererInfoAll = new SuffererInfoAllPO();
			BeanUtils.copyProperties(po, suffererInfoAll);
			suffererInfoAll.setSuffererId(po.getId());
			suffererInfoAll.setId(UUIDUtils.getUUID());
			lists.add(suffererInfoAll);
		}
		return lists;
	}
	
}
