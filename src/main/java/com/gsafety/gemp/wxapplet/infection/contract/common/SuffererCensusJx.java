package com.gsafety.gemp.wxapplet.infection.contract.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Administrator
 * @Title: SuffererCensusJx
 * @ProjectName wx-applet
 * @Description: TODO
 * @date 2020/3/312:11
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("江夏病患统计数据")
public class SuffererCensusJx {

    @ApiModelProperty("截止日期")
    private String expiryDate;

    @ApiModelProperty(value = "隔离人员总数")
    private Integer total = 0;

    @ApiModelProperty(value = "密接人员数")
    private Integer contactTotal = 0;

    @ApiModelProperty(value = "发热患者")
    private Integer hotTotal = 0;

    @ApiModelProperty(value = "疑似患者")
    private Integer suspected = 0;

    @ApiModelProperty(value = "确诊患者")
    private Integer confirm = 0;

    @ApiModelProperty(value = "出院观察")
    private Integer detainedObserTotal = 0;

    @ApiModelProperty(value = "新增密接人员")
    private Integer contactAdd = 0;

    @ApiModelProperty(value = "新增发热人员")
    private Integer hotAdd = 0;
}
