package com.gsafety.gemp.wxapplet.infection.contract.enums;

import org.apache.commons.lang.StringUtils;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SuffererTraceTypeEnum {

	TOUCH(1,"密接"),FEVER(2,"发热"),SUSPECT(3,"疑似"),CONFIRM(4,"确诊"),
	DISCHARGED(5,"出院留观者"),DIAGNOSED_20_DAYS_AGO(6,"20日前的临床诊断病例"),
	SHIFT_IN(7,"转入"),ROLL_OUT(8,"转出");
	
	private Integer code;
	
	private String value;
	
	public static String getValueByCode(Integer code) {
        for (SuffererTraceTypeEnum en : SuffererTraceTypeEnum.values()) {
            if (en.getCode().equals(code)) {
                return en.getValue();
            }
        }
        return StringUtils.EMPTY;
    }
}
