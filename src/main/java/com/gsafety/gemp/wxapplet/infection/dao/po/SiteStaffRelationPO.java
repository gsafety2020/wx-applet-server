package com.gsafety.gemp.wxapplet.infection.dao.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
* @ClassName: SiteStaffRelationPO 
* @Description: 隔离群点与操作人员关系PO
* @author luoxiao
* @date 2020年3月1日 下午2:54:43 
*
 */
@Entity
@Table(name = "site_staff_relation")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SiteStaffRelationPO implements Serializable{

	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name = "id")
	private String id;
	
	/**工作人员id*/
	@Column(name = "staffId")
	private String staffId;
	
	/**隔离点id*/
	@Column(name = "siteId")
	private String siteId;
	
	/**隔离点群id*/
	@Column(name = "siteIdGroup")
	private String siteIdGroup;

}
