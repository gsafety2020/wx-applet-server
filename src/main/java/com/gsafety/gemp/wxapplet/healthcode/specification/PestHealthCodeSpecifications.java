/**
 * 
 */
package com.gsafety.gemp.wxapplet.healthcode.specification;

import com.gsafety.gemp.wxapplet.healthcode.dao.po.PestHealthCodePO;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.jpa.domain.Specification;

/**
 * 该类用于健康码列表搜索
 *
 * @author wangwenhai
 *
 * @since 2020年2月25日 上午19:50:24
 *
 */
public class PestHealthCodeSpecifications {

	/**
	 * 昵称字段
	 */
	public static final String FIELD_NICK_NAME = "nickName";

	/**
	 * 姓名
	 */
	public static final String FIELD_USER_NAME = "userName";

	/**
     * 手机号
     */
    public static final String FIELD_MOBILE = "mobile";

    /**
     * 健康码颜色
     */
    public static final String FIELD_HEALTH_CODE_COLOR = "healthCodeColor";

    /**
     * 地区名称
     */
    public static final String FIELD_AREA_NAME = "areaName";



	private PestHealthCodeSpecifications() {
		super();
	}

	/**
	 * 
	 * nickNameLike 昵称搜索条件
	 *
	 * @author wangwenhai
	 * 
	 * @since 2020年2月25日 下午19:58:16
	 *
	 * @param nickName
	 * @return
	 */
	public static Specification<PestHealthCodePO> nickNameLike(String nickName) {

		return (root, query, builer) ->{
			if(StringUtils.isEmpty(nickName)){
				return null;
			}
			return builer.like(root.get(FIELD_NICK_NAME), "%" + nickName + "%");
		};
	}


	/**
	 *
	 * userNameLike,姓名搜索条件
	 *
	 * @author wangwenhai
	 *
	 * @since 2020年2月25日 下午20:00:16
	 *
	 * @param userName
	 * @return
	 */
	public static Specification<PestHealthCodePO> userNameLike(String userName) {

		return (root, query, builer) ->{
			if(StringUtils.isEmpty(userName)){
				return null;
			}
			return builer.like(root.get(FIELD_USER_NAME),"%" + userName + "%");
		};
	}


	/**
	 * mobileLike 手机号码搜索条件
	 *
	 *
	 * @author wangwenhai
	 *
	 * @since 2020年2月25日 下午20:03:16
	 *
	 * @param mobile
	 * @return
	 */
	public static Specification<PestHealthCodePO> mobilelike(String mobile) {

		return (root, query, builer) ->{
			if(StringUtils.isEmpty(mobile)){
				return null;
			}
			return builer.like(root.get(FIELD_MOBILE),"%" + mobile +"%");
		};
	}

	/**
     * healthCodeColorEqual 健康码颜色查询
     *
     *
     * @author wangwenhai
     *
     * @since 2020年2月25日 下午20:25:16
     *
     * @param healthCodeColor
     * @return
     */
    public static Specification<PestHealthCodePO> healthCodeColorEqual(String healthCodeColor) {

        return (root, query, builer) ->{
            if(StringUtils.isEmpty(healthCodeColor)){
                return null;
            }
            return builer.equal(root.get(FIELD_HEALTH_CODE_COLOR),healthCodeColor);
        };
    }


    /**
     * areaNameLike 健康码地区名称查询
     *
     *
     * @author wangwenhai
     *
     * @since 2020年2月25日 下午22:15:16
     *
     * @param areaName
     * @return
     */
    public static Specification<PestHealthCodePO> areaNameLike(String areaName) {

        return (root, query, builer) ->{
            if(StringUtils.isEmpty(areaName)){
                return null;
            }
            return builer.like(root.get(FIELD_AREA_NAME),"%" +areaName +"%");
        };
    }


}
