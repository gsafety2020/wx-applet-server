package com.gsafety.gemp.wxapplet.healthcode.service;

import java.awt.Color;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import com.gsafety.gemp.wxapplet.healthcode.contract.params.DimQueryParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.alibaba.druid.util.StringUtils;
import com.gsafety.gemp.wxapplet.healthcode.constant.PestHealthCodeStatus;
import com.gsafety.gemp.wxapplet.healthcode.contract.dto.CodeBasDistrictDTO;
import com.gsafety.gemp.wxapplet.healthcode.contract.dto.PestHealthCodeDTO;
import com.gsafety.gemp.wxapplet.healthcode.contract.dto.PestHealthCodeSaveDTO;
import com.gsafety.gemp.wxapplet.healthcode.contract.dto.in.PestHealthCodeReportDTO;
import com.gsafety.gemp.wxapplet.healthcode.contract.service.PestHealthCodeService;
import com.gsafety.gemp.wxapplet.healthcode.controller.WechatAuthController.WechatUserinfo;
import com.gsafety.gemp.wxapplet.healthcode.dao.CodeBasDistrictDao;
import com.gsafety.gemp.wxapplet.healthcode.dao.PestHealthCodeDao;
import com.gsafety.gemp.wxapplet.healthcode.dao.po.CodeBasDistrictPO;
import com.gsafety.gemp.wxapplet.healthcode.dao.po.PestHealthCodePO;
import com.gsafety.gemp.wxapplet.healthcode.factory.WechatUserManager;
import com.gsafety.gemp.wxapplet.healthcode.factory.WechatUserManagerFactory;
import com.gsafety.gemp.wxapplet.healthcode.wrapper.CodeBasDistrictWrapper;
import com.gsafety.gemp.wxapplet.healthcode.wrapper.PestHealthCodeWrapper;
import com.gsafety.gemp.wxapplet.utils.QRCodeUtils;
import com.gsafety.gemp.wxapplet.utils.UUIDUtils;

import static com.gsafety.gemp.wxapplet.healthcode.specification.PestHealthCodeSpecifications.*;
import static com.gsafety.gemp.wxapplet.healthcode.wrapper.HealthCodeWrapper.*;


import lombok.extern.slf4j.Slf4j;

/**
 * 
* @ClassName: PestHealthCodeServiceImpl 
* @Description: 健康码服务实现类
* @author luoxiao
* @date 2020年2月21日 下午4:47:02 
*
 */
@Service("pestHealthCodeService")
@Slf4j
public class PestHealthCodeServiceImpl implements PestHealthCodeService{

	@Autowired
	private PestHealthCodeDao pestHealthCodeDao;
	
	@Autowired
	private CodeBasDistrictDao codeBasDistrictDao;
	
	@Value("${static.wechart.zxing.url.text}")
	private String qrcodeText;
	
	@Value("${static.wechart.zxing.generator.path}")
	private String qrcodePath;
	
	@Value("${static.wechart.zxing.url.prefix}")
	private String qrcodeUrl;
	
	@Value("${static.wechart.zxing.width}")
	private Integer width;
	
	@Value("${static.wechart.zxing.height}")
	private Integer height;
	
	private static String[] specialCityCode = {"110000"};
	
	private static String[] provinceCode = {"420000"};
	
	private static Map<String,Color> colorMap = new HashMap<>();
	
	static{
		colorMap.put(PestHealthCodeStatus.CodeColorEnum.RED.getCode(), new Color(241,119,98));
		colorMap.put(PestHealthCodeStatus.CodeColorEnum.GREEN.getCode(), new Color(88,201,118));
		colorMap.put(PestHealthCodeStatus.CodeColorEnum.YELLOW.getCode(), new Color(232,197,75));
	}
	
	@Override
	public List<CodeBasDistrictDTO> getObjectByDistrictCodes(List<String> districtCodes) {
		if(CollectionUtils.isEmpty(districtCodes)){
			throw new RuntimeException("地区编码不能为空!");
		}
		List<CodeBasDistrictPO> pos = codeBasDistrictDao.findByDistrictCodeIn(districtCodes);
		List<CodeBasDistrictDTO> dtos = CodeBasDistrictWrapper.listPoToDto(pos);
		return dtos;
	}

	@Override
	public List<CodeBasDistrictDTO> getObjectByParentCode() {
		List<CodeBasDistrictPO> specialPo = codeBasDistrictDao.findByDistrictCodeIn(Arrays.asList(specialCityCode));
		List<CodeBasDistrictPO> provincePo = codeBasDistrictDao.findByParentCodeIn(Arrays.asList(provinceCode));
		specialPo.addAll(provincePo);
		specialPo = specialPo.stream().filter(a->!"429000".equals(a.getDistrictCode())).collect(Collectors.toList());
		List<CodeBasDistrictDTO> dtos = CodeBasDistrictWrapper.listPoToDto(specialPo);
		return dtos;
	}

	@Override
	public PestHealthCodeSaveDTO saveUpdate(PestHealthCodeReportDTO dto) {
		//验证字段
		vaildParameter(dto);
		//获取微信用户信息
		WechatUserManager wechatUserManager = WechatUserManagerFactory.getInstance();
		WechatUserinfo userInfo =wechatUserManager.getUser(dto.getSessionKey());
		PestHealthCodePO po = PestHealthCodeWrapper.dtoToPo(dto);
		//设置健康码字段属性值
		setPestHealthCodePOCoumln(po,userInfo);
		//判断是否已经存在
		Optional<PestHealthCodePO> optional = pestHealthCodeDao.findById(po.getHealthCodeId());
		if(optional.isPresent()){
			po.setUpdateTime(Calendar.getInstance().getTime());
		}else{
			po.setUpdateTime(Calendar.getInstance().getTime());
			po.setCreateTime(Calendar.getInstance().getTime());
		}
		//保存更新
		PestHealthCodePO resultPo = pestHealthCodeDao.saveAndFlush(po);
		PestHealthCodeSaveDTO resultDto = PestHealthCodeWrapper.poToSaveDto(resultPo);
		return resultDto;
	}
	
	@Override
	public PestHealthCodeDTO getHealthCodeById(String healthCodeId) {
		if(StringUtils.isEmpty(healthCodeId)){
			throw new RuntimeException("healthCodeId不能为空!");
		}
		PestHealthCodePO po = pestHealthCodeDao.findByHealthCodeId(healthCodeId);
		if(po == null){
			throw new RuntimeException("查询对象不存在!");
		}
		PestHealthCodeDTO dto = PestHealthCodeWrapper.poToDto(po);
		return dto;
	}
	
	private void setPestHealthCodePOCoumln(PestHealthCodePO po,WechatUserinfo userInfo){
		if(userInfo == null){
			throw new RuntimeException("登陆失效,请重新登陆!");
		}
		log.info("微信用户信息[{}]",userInfo);
		PestHealthCodePO isExistsPo = pestHealthCodeDao.findByOpenIdAndAreaCodeAndIsDelete(userInfo.getOpenid(), po.getAreaCode(), PestHealthCodeStatus.DeleteEnum.NOT_DELETE.getCode());
		if(isExistsPo != null){
			throw new RuntimeException("该用户健康码已经存在!");
		}
		String openId = userInfo.getOpenid();  //小程序openId
		String nickName = userInfo.getNickName(); //小程序简称
		po.setHealthCodeId(UUIDUtils.getUUID()); //设置主键
		po.setIsDelete(PestHealthCodeStatus.DeleteEnum.NOT_DELETE.getCode());
		po.setOpenId(openId);
		po.setImageUrl(userInfo.getAvatarUrl());
		po.setNickName(nickName);
		//获取总分
		po.setScore(calucateScore(po));
		//获取健康码颜色
		po.setHealthCodeColor(judgeHealthCodeColor(po));
		//获取省编码 父节点编码
		setDistrictInfoToPo(po);
		//生成二维码图片
		String pattern = "PNG";//二维码图片格式
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		String dateStr = format.format(Calendar.getInstance().getTime());
		String qrcodePathDate = qrcodePath+File.separator+dateStr;
		String text = qrcodeText + po.getHealthCodeId(); //二维码内容
		int color = colorMap.get(po.getHealthCodeColor()).getRGB();
		if(StringUtils.isEmpty(po.getImageUrl())){
			QRCodeUtils.generateQRCodeColorImage(text, width, height, qrcodePathDate, po.getHealthCodeId(), pattern, color, Color.WHITE.getRGB());
		}else{
			QRCodeUtils.generateQRCodeLogoImage(text, po.getImageUrl(), width, height, qrcodePathDate, po.getHealthCodeId(), pattern, color, Color.WHITE.getRGB());
		}
		String fullName = po.getHealthCodeId()+"."+ pattern; //二维码全名
		String requestUrl = qrcodeUrl + dateStr + File.separator + fullName;  //二维码web访问路径
		String imageFilePath = qrcodePathDate+File.separator + fullName;
		po.setImageFilePath(imageFilePath);
		po.setHealthCodeUrl(requestUrl);
		po.setQrCodeText(text);
	}
	
	private void setDistrictInfoToPo(PestHealthCodePO po){
		CodeBasDistrictPO codeBasePo = codeBasDistrictDao.findById(po.getAreaCode()).get();
		String areaName = codeBasePo.getDistrictName();
		String parentCode = codeBasePo.getParentCode();
		String provinceCode = null;
		String provinceName = null;
		if(Arrays.asList(specialCityCode).contains(po.getAreaCode())){
			provinceCode = po.getAreaCode();
			provinceName = areaName;
		}else{
			CodeBasDistrictPO parentPo = codeBasDistrictDao.findById(parentCode).get();
			provinceName = parentPo.getDistrictName();
			provinceCode = parentPo.getDistrictCode();
		}
		po.setParentCode(parentCode);
		po.setProvinceCode(provinceCode);
		po.setProvinceName(provinceName);
		po.setAreaName(areaName);
	}
	
	private Integer calucateScore(PestHealthCodePO po){
		Integer score = 0;
		String prefix = po.getIdentityCard().substring(0, 2);
		if(prefix.equals("42")){
			po.setIsPest(PestHealthCodeStatus.QuestionEnum.YES.getCode());
			return -99;
		}
		po.setIsPest(PestHealthCodeStatus.QuestionEnum.NO.getCode());
		if(po.getCurrentCity().equals(PestHealthCodeStatus.QuestionEnum.YES.getCode())){
			score = score + 1;
		}
		if(po.getFornightLeaveCity().equals(PestHealthCodeStatus.QuestionEnum.NO.getCode())){
			score = score + 1;
		}
		if(po.getNearHavePatient().equals(PestHealthCodeStatus.QuestionEnum.NO.getCode())){
			score = score + 1;
		}
		if(po.getTouchPatient().equals(PestHealthCodeStatus.QuestionEnum.NO.getCode())){
			score = score + 1;
		}
		if(po.getNormalHealth().equals(PestHealthCodeStatus.QuestionEnum.YES.getCode())){
			score = score + 1;
		}
		return score;
	}
	
	private String judgeHealthCodeColor(PestHealthCodePO po){
		if(po.getScore() == 5){
			return PestHealthCodeStatus.CodeColorEnum.GREEN.getCode();
		}else if(po.getScore() == -99){
			return PestHealthCodeStatus.CodeColorEnum.RED.getCode();
		}else{
			return PestHealthCodeStatus.CodeColorEnum.YELLOW.getCode();
		}
	}
	
	private void vaildParameter(PestHealthCodeReportDTO dto){
		if(dto == null){
			throw new RuntimeException("健康码上报信息对象不能为空!");
		}
		if(StringUtils.isEmpty(dto.getSessionKey())){
			throw new RuntimeException("小程序sessionKey不能为空!");
		}
		if(StringUtils.isEmpty(dto.getAreaCode())){
			throw new RuntimeException("地区编码不能为空!");
		}
		if(StringUtils.isEmpty(dto.getUserName())){
			throw new RuntimeException("用户姓名不能为空!");
		}
		if(StringUtils.isEmpty(dto.getMobile())){
			throw new RuntimeException("手机号不能为空!");
		}
		if(StringUtils.isEmpty(dto.getIdentityCard())){
			throw new RuntimeException("身份证不能为空!");
		}
		if(StringUtils.isEmpty(dto.getCurrentCity()) ||
				StringUtils.isEmpty(dto.getFornightLeaveCity()) ||
				StringUtils.isEmpty(dto.getNearHavePatient()) ||
				StringUtils.isEmpty(dto.getNormalHealth()) ||
				StringUtils.isEmpty(dto.getTouchPatient())){
			throw new RuntimeException("问题回答不能为空!");
		}
	}

	@Override
	public PestHealthCodeDTO getEffectiveHealthCodeByOpenIdAndAreaCode(String openId, String areaCode) {
		if(StringUtils.isEmpty(openId)){
			throw new RuntimeException("openId为空!");
		}
		if(StringUtils.isEmpty(areaCode)){
			throw new RuntimeException("areaCode为空!");
		}
		PestHealthCodePO po = pestHealthCodeDao.findByOpenIdAndAreaCodeAndIsDelete(openId, areaCode, PestHealthCodeStatus.DeleteEnum.NOT_DELETE.getCode());
		if(po == null){
			throw new RuntimeException("查询对象不存在!");
		}
		PestHealthCodeDTO dto = PestHealthCodeWrapper.poToDto(po);
		return dto;
	}

	@Override
	public void removeHealthCode(String healthCodeId) {
		if(StringUtils.isEmpty(healthCodeId)){
			throw new RuntimeException("参数为空!");
		}
		PestHealthCodePO po = pestHealthCodeDao.findByHealthCodeId(healthCodeId);
		if(po == null){
			throw new RuntimeException("移除对象不存在!");
		}
		po.setIsDelete(PestHealthCodeStatus.DeleteEnum.DELETED.getCode());
		pestHealthCodeDao.saveAndFlush(po);
	}

	@Override
	public Page<PestHealthCodeDTO> findHealthCodePage(DimQueryParam queryParam, Pageable pageable) {
		Page<PestHealthCodePO> page = pestHealthCodeDao.findAll(userNameLike(queryParam.getKeywords())
				.or(nickNameLike(queryParam.getKeywords()))
				.or(mobilelike(queryParam.getKeywords()))
				.or(areaNameLike(queryParam.getKeywords()))
				.and(healthCodeColorEqual(queryParam.getHealthCodeColor())), pageable);
		return toPageDTO(page);
	}

	@Override
	public void save(PestHealthCodeDTO dto) {
		pestHealthCodeDao.save(dtoToPo(dto));
	}




}
