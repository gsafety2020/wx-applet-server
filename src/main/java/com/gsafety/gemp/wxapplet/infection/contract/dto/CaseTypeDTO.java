package com.gsafety.gemp.wxapplet.infection.contract.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CaseTypeDTO
{
	@ApiModelProperty(value = "code")
	private Integer code;

	@ApiModelProperty(value = "名称")
	private String name;
}
