package com.gsafety.gemp.wxapplet.infection.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "point_hospital_info", schema = "wx_applet", catalog = "")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PointHospitalInfoPO implements Serializable {

    @Id
    @Column(name = "id")
    private String id;

    /**患者姓名*/
    @Column(name = "SUFFERER_NAME")
    private String suffererName;

    /**性别(1-男,2-女)*/
    @Column(name = "GENDER")
    private String gender;

    /**年龄*/
    @Column(name = "age")
    private String age;

    /**身份证号码*/
    @Column(name = "idNo")
    private String idNo;

    /**地址*/
    @Column(name = "DETAILED_ADDRESS")
    private String detailed_address;

    /**联系电话*/
    @Column(name = "TELPHONE")
    private String telphone;

    /**入住时间*/
    @Column(name = "MOVE_INTO_TIME")
    private Date moveIntoTime;

    /**类型(1-确诊，2-疑似)*/
    @Column(name = "SUFFERER_TYPE")
    private String suffererType;

    /**病情(1-重症，2-危重，99-其他)*/
    @Column(name = "ILLNESS_STATE")
    private String illnessState;

    /**状态(1-当前在院，2-治愈出院，3-转出，4-死亡)*/
    @Column(name = "SUFFERER_STATUS")
    private String suffererStatus;

    /** 治愈出院时间**/
    @Column(name="DISCHARGE_TIME")
    private Date dischargeTime;

    /** 死亡时间**/
    @Column(name="DEATH_DATE")
    private Date deathDate;

    /** 医院名称**/
    @Column(name="HOSPITAL_NAME")
    private String hospitalName;

    /** 创建时间**/
    @Column(name="CREATE_TIME")
    private Date createTime;

    /** 更新时间**/
    @Column(name="UPDATE_TIME")
    private Date updateTime;

    /** 数据日期**/
    @Column(name="DATA_DATE")
    private String dataDate;

}
