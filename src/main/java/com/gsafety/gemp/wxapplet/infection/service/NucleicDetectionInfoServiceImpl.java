package com.gsafety.gemp.wxapplet.infection.service;

import cn.hutool.core.collection.CollectionUtil;
import com.google.common.collect.Lists;
import com.gsafety.gemp.wxapplet.infection.contract.dto.*;
import com.gsafety.gemp.wxapplet.infection.contract.iface.NucleicDetectionInfoService;
import com.gsafety.gemp.wxapplet.infection.dao.NucleicDetectionInfoDao;
import com.gsafety.gemp.wxapplet.infection.dao.po.NucleicDetectionInfoPO;
import com.gsafety.gemp.wxapplet.infection.dao.po.PointHospitalInfoPO;
import com.gsafety.gemp.wxapplet.infection.wrapper.NucleicDetectionInfoWrapper;
import com.gsafety.gemp.wxapplet.infection.wrapper.PointHospitalInfoWrapper;
import com.gsafety.gemp.wxapplet.utils.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class NucleicDetectionInfoServiceImpl implements NucleicDetectionInfoService {

    @Resource
    private NucleicDetectionInfoDao detectionInfoDao;

    @Override
    public Page<NucleicDetectionInfoDTO> search(NucleicDetectionInfoPageDTO dto, PageRequest pageRequest) {
        Specification<NucleicDetectionInfoPO> spec = concatSpecification(dto);
        Page<NucleicDetectionInfoPO> poPage = detectionInfoDao.findAll(spec, pageRequest);
        Page<NucleicDetectionInfoDTO> PageDto= NucleicDetectionInfoWrapper.toPageDTO(poPage);
        if(StringUtils.isNotBlank(dto.getCompareFlag())&&!"all".equals(dto.getDetectionNum())) {
            Sort sort = Sort.by(Sort.Direction.DESC, "sendSampleTime");
            PageRequest pageRequest2= PageRequest.of(dto.getPage(), dto.getSize(),sort);
            List<NucleicDetectionInfoDTO> content= NucleicDetectionInfoWrapper.toDTOList(detectionInfoDao.findAll(spec,sort));
            List<NucleicDetectionInfoDTO> list=content.stream().filter(t->StringUtils.isNotBlank(t.getIdNo())).collect(Collectors.collectingAndThen(
                        Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(c->c.getIdNo()))), ArrayList::new));

            List<NucleicDetectionInfoDTO> list2=content.stream().filter(t->StringUtils.isBlank(t.getIdNo())).collect(Collectors.collectingAndThen(
                    Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(c->c.getSuffererName()))), ArrayList::new));
            list.addAll(list2);

            List<NucleicDetectionInfoDTO> subList = subPageList(list,pageRequest2);
            if(CollectionUtil.isEmpty(subList)){
                Page<NucleicDetectionInfoDTO> emptyPageList=new PageImpl<NucleicDetectionInfoDTO>(new ArrayList<>(),pageRequest2,list.size());
                return emptyPageList;
            }
            Page<NucleicDetectionInfoDTO> pageList=new PageImpl<NucleicDetectionInfoDTO>(subList,pageRequest2,list.size());
            return pageList;
        }
        return PageDto;
    }


    private List<NucleicDetectionInfoDTO> subPageList(List<NucleicDetectionInfoDTO> list,PageRequest pageRequest) {
        if (CollectionUtil.isEmpty(list)) {
            return null;
        }
        if (list.size() == 0) {
            return null;
        }

        Integer count = list.size();
        Integer pageCount = 0;
        if (count % pageRequest.getPageSize() == 0) {
            pageCount = count / pageRequest.getPageSize();
        } else {
            pageCount = count / pageRequest.getPageSize() + 1;
        }

        int fromIndex = 0;
        int toIndex = 0;

        if(pageRequest.getPageNumber()+1!= pageCount) {
            fromIndex = (pageRequest.getPageNumber()) * pageRequest.getPageSize();
            toIndex = fromIndex + pageRequest.getPageSize();
        } else {
            fromIndex = (pageRequest.getPageNumber()) * pageRequest.getPageSize();
            toIndex = count;
        }

        List<NucleicDetectionInfoDTO> pageList = list.subList(fromIndex, toIndex);

        return pageList;
    }

    @Override
    public NucleicDetectionInfoPO findOne(String id) {
        Optional<NucleicDetectionInfoPO> optional = detectionInfoDao.findById(id);
        return optional.isPresent() ? optional.get() : null;
    }

    @Override
    public void deteCtionNumUpdate() {
        /** 根据身份证号更新检测次数**/
        List<NucleicDetectionInfoPO> all = detectionInfoDao.findByIdNoIsNotNullOrderBySendSampleTime();
        Map<String,List<NucleicDetectionInfoPO>> map=all.stream().collect(Collectors.groupingBy(NucleicDetectionInfoPO::getIdNo));
        for(Map.Entry<String,List<NucleicDetectionInfoPO>> m1:map.entrySet()){
            List<NucleicDetectionInfoPO> list = m1.getValue();
            int size = list.size();
            list.stream().forEach(t->{
                t.setDetectionNum(String.valueOf(size));
            });
            detectionInfoDao.saveAll(list);
        }
        /** 根据身份证号为空的信息根据姓名更新检测次数**/
        List<NucleicDetectionInfoPO> all2 = detectionInfoDao.findByIdNoIsNullOrderBySendSampleTime();
        Map<String,List<NucleicDetectionInfoPO>> map2=all2.stream().collect(Collectors.groupingBy(NucleicDetectionInfoPO::getSuffererName));
        for(Map.Entry<String,List<NucleicDetectionInfoPO>> m1:map2.entrySet()){
            List<NucleicDetectionInfoPO> list = m1.getValue();
            int size = list.size();
            list.stream().forEach(t->{
                    t.setDetectionNum(String.valueOf(size));
            });
            detectionInfoDao.saveAll(list);
        }

    }

    @Override
    public List<PointCommonDTO> detectionNumList() {
        List<PointCommonDTO> commonDTOList=new ArrayList<>();
        PointCommonDTO commonDTO=null;
        List<NucleicDetectionInfoPO> all = detectionInfoDao.findByIdNoIsNotNullAndDetectionNumIsNotNullOrderByDetectionNum();
        Map<String,List<NucleicDetectionInfoPO>> map=all.stream().collect(Collectors.groupingBy(NucleicDetectionInfoPO::getDetectionNum));
        for(Map.Entry<String,List<NucleicDetectionInfoPO>> m1:map.entrySet()){
            commonDTO=new PointCommonDTO();
            commonDTO.setName(m1.getKey());
            commonDTO.setCode(m1.getKey());
            commonDTOList.add(commonDTO);
        }
        commonDTOList.sort(new Comparator<PointCommonDTO>() {
            @Override
            public int compare(PointCommonDTO o1, PointCommonDTO o2) {
                return Integer.parseInt(o1.getCode())-Integer.parseInt(o2.getCode());
            }
        });
        commonDTOList.add(0,new PointCommonDTO().builder().name("全部").code("all").build());
        return commonDTOList;
    }

    private Specification<NucleicDetectionInfoPO> concatSpecification(NucleicDetectionInfoPageDTO dto){
        Specification<NucleicDetectionInfoPO> spec = new Specification<NucleicDetectionInfoPO>(){

            @Override
            public Predicate toPredicate(Root<NucleicDetectionInfoPO> root, CriteriaQuery<?> query,
                                         CriteriaBuilder cb) {
                List<Predicate> predicates = new ArrayList<>(); //所有的断言
                /*if(!CollectionUtils.isEmpty(sites)){
                    Predicate curSiteIdIn = cb.in(root.get("curSiteId")).value(sites);
                    predicates.add(curSiteIdIn);
                }*/

                if(dto != null){
                    if(org.apache.commons.lang.StringUtils.isNotEmpty(dto.getSuffererName())){
                        Predicate suffererNameLike = cb.like(root.get("suffererName"), "%"+dto.getSuffererName().trim()+"%");
                        predicates.add(suffererNameLike);
                    }
                    if(org.apache.commons.lang.StringUtils.isNotEmpty(dto.getIdNo())){
                        Predicate suffererCardLike = cb.like(root.get("idNo"), "%"+dto.getIdNo().trim()+"%");
                        predicates.add(suffererCardLike);
                    }
                    if(null!=dto.getSendSampleTime()){
                        List<String> idInList=getIdListBySendSampleTime(dto.getSendSampleTime());
                        Predicate idInListPre= cb.and(root.get("id").in(idInList));
                        predicates.add(idInListPre);
                    }
                    if(StringUtils.isNotBlank(dto.getCompareFlag())&&!"all".equals(dto.getDetectionNum())) {
                        if("0".equals(dto.getCompareFlag())) {
                            if (StringUtils.isNotBlank(dto.getDetectionNum())) {
                                Predicate detectionNumEquals = cb.equal(root.get("detectionNum"), dto.getDetectionNum());
                                predicates.add(detectionNumEquals);
                            }
                        }else if("1".equals(dto.getCompareFlag())){
                            if (StringUtils.isNotBlank(dto.getDetectionNum())) {
                                Predicate detectionNumEquals = cb.lessThan(root.get("detectionNum"), Integer.parseInt(dto.getDetectionNum()));
                                predicates.add(detectionNumEquals);
                            }
                        }else if("2".equals(dto.getCompareFlag())){
                            if (StringUtils.isNotBlank(dto.getDetectionNum())) {
                                Predicate detectionNumEquals = cb.greaterThan(root.get("detectionNum"), Integer.parseInt(dto.getDetectionNum()));
                                predicates.add(detectionNumEquals);
                            }
                        }
                    }
                    if(StringUtils.isNotEmpty(dto.getDetectionResult())&&!"0".equals(dto.getDetectionResult())){
                        Predicate detectionResultEquals = cb.equal(root.get("detectionResult"), dto.getDetectionResult());
                        predicates.add(detectionResultEquals);
                    }
                    if(StringUtils.isNotEmpty(dto.getIsolationType())&&!"0".equals(dto.getIsolationType())){
                        Predicate isolationTypeEquals = cb.equal(root.get("isolationType"), dto.getIsolationType());
                        predicates.add(isolationTypeEquals);
                    }
                    /*if(StringUtils.isNotEmpty(dto.getOrgType())&&!"0".equals(dto.getOrgType())){
                        Predicate orgTypeLike=null;
                        if("1".equals(dto.getOrgType())){
                            orgTypeLike=cb.or(cb.like(root.get("isolationPoint"),"%醉江月%"),cb.like(root.get("isolationPoint"),"%怡佳%")
                            ,cb.like(root.get("isolationPoint"),"%百捷%"));
                        }
                        if("2".equals(dto.getOrgType())){
                            orgTypeLike=cb.or(cb.like(root.get("isolationPoint"),"%长峰%"),cb.like(root.get("entrustOrg"),"%侨亚%"));
                        }
                        if("3".equals(dto.getOrgType())){
                            String[] isolationTypeNotIn={"1","2"};
                            List<NucleicDetectionInfoPO> list = detectionInfoDao.findByIsolationTypeIn(isolationTypeNotIn);
                            List<String> idNotIn=new ArrayList<>();
                            for(NucleicDetectionInfoPO po:list){
                                idNotIn.add(po.getId());
                            }
                            orgTypeLike=cb.and(cb.notLike(root.get("entrustOrg"),"%人民医%"),cb.notLike(root.get("entrustOrg"),"%中医%")
                            ,cb.not(root.get("id").in(idNotIn)));

                        }
                        if("4".equals(dto.getOrgType())){
                            orgTypeLike=cb.or(cb.like(root.get("entrustOrg"),"%人民医%"));
                        }
                        if("5".equals(dto.getOrgType())){
                            orgTypeLike=cb.or(cb.like(root.get("entrustOrg"),"%中医%"));
                        }
                        predicates.add(orgTypeLike);
                    }*/


                }
                return cb.and(predicates.toArray(new Predicate[predicates.size()]));
            }

        };
        return spec;
    }

    private List<String> getIdListBySendSampleTime(Date sendSampleTime){
        String searchTime=DateUtils.format(sendSampleTime,"yyyy-MM-dd");
        List<String> idList=new ArrayList<>();
        List<NucleicDetectionInfoPO> all = detectionInfoDao.findAll();
        for(NucleicDetectionInfoPO po:all){
            if(null!=po.getSendSampleTime()){
                String time = DateUtils.format(po.getSendSampleTime(), "yyyy-MM-dd");
                if(StringUtils.equals(searchTime,time)) {
                    idList.add(po.getId());
                }
            }
        }
        return idList;
    }
}
