package com.gsafety.gemp.wxapplet.healthcode.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gsafety.gemp.wxapplet.healthcode.dao.po.CodeBasDistrictPO;

/**
 * @author dusiwei
 */
public interface CodeBasDistrictDao extends JpaRepository<CodeBasDistrictPO, String>,
        JpaSpecificationExecutor<CodeBasDistrictPO> {
	
	List<CodeBasDistrictPO> findByDistrictCodeIn(List<String> districtCodes);

	List<CodeBasDistrictPO> findByParentCodeIn(List<String> parentCodes);

}
