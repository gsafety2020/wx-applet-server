package com.gsafety.gemp.wxapplet.infection.controller;

import com.gsafety.gemp.wxapplet.healthcode.contract.dto.Result;
import com.gsafety.gemp.wxapplet.infection.contract.dto.*;
import com.gsafety.gemp.wxapplet.infection.contract.enums.DestinationTypeEnum;
import com.gsafety.gemp.wxapplet.infection.contract.enums.IllnessStateEnum;
import com.gsafety.gemp.wxapplet.infection.contract.enums.PatientTypeEnum;
import com.gsafety.gemp.wxapplet.infection.contract.enums.SuffererStatusEnum;
import com.gsafety.gemp.wxapplet.infection.contract.iface.KeyAssignmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lzx
 * @version 1.0
 * @date 2020/2/28 1:01
 */

@Api(value = "新增页面键值对接口", tags = {"下拉列表接口"})
@RestController
@RequestMapping("/key/v1/patient")
public class KeyAssignmentController {

    @Autowired
    private KeyAssignmentService keyAssignmentService;

    /**
     * 获取所有隔离点数据
     * @param
     * @return
     */
    @ApiOperation(value = "隔离点列表")
    @GetMapping(value = "/findSiteList")
    public Result findSiteName(){
        List<SiteNameDTO> list = keyAssignmentService.findSiteName();
        return new Result().success(list);
    }

    /**
     * 转出目的地类型
     * @param
     * @return
     */
    @ApiOperation(value = "转出目的地类型")
    @GetMapping(value = "/findDestinationType")
    public Result findDestinationType(){
        List<DestinationTypeDTO> list = new ArrayList<>();
        for(DestinationTypeEnum item:DestinationTypeEnum.values()){
            DestinationTypeDTO EnumModel = new DestinationTypeDTO();
            EnumModel.setCode(item.getCode());
            EnumModel.setName(item.getName());
            list.add(EnumModel);
        }

        return new Result().success(list);
    }

    /**
     * 病情状态
     * @param
     * @return
     */
    @ApiOperation(value = "病情状态")
    @GetMapping(value = "/findIllnessState")
    public Result findIllnessState(){
        List<IllnessStateDTO> list = new ArrayList<>();
        for(IllnessStateEnum item: IllnessStateEnum.values()){
            IllnessStateDTO EnumModel = new IllnessStateDTO();
            EnumModel.setCode(item.getCode());
            EnumModel.setName(item.getValue());
            list.add(EnumModel);
        }

        return new Result().success(list);
    }

    /**
     * 病情状态
     * @param
     * @return
     */
    @ApiOperation(value = "患者状态列表")
    @GetMapping(value = "/findSuffererStatus")
    public Result findSuffererStatus(){
        List<SuffererStatusDTO> list = new ArrayList();
        for(SuffererStatusEnum item: SuffererStatusEnum.values()){
            SuffererStatusDTO EnumModel = new SuffererStatusDTO();
            EnumModel.setCode(item.getCode());
            EnumModel.setName(item.getValue());
            list.add(EnumModel);
        }

        return new Result().success(list);
    }

    @ApiOperation(value = "患者类型列表")
    @GetMapping(value = "/findPatientTypeList")
    public Result getPatientType(){
        List<PatientTypeDTO> list = new ArrayList<>();
        for(PatientTypeEnum item:PatientTypeEnum.values()){
            //通过遍历枚举拿到遍历的枚举值，然后根据枚举值拿到对应的枚举名称 在添加到list集合里 就Ok 很实在的方法 而且还很简单
            PatientTypeDTO EnumModel = new PatientTypeDTO();
            EnumModel.setCode(item.getCode());
            EnumModel.setName(item.getName());
            list.add(EnumModel);
        }

        return new Result().success(list);
    }

}
