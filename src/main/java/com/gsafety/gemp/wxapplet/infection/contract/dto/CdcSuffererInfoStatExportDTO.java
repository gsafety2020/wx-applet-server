package com.gsafety.gemp.wxapplet.infection.contract.dto;

import com.gsafety.gemp.common.excel.ExcelAttribute;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author dusiwei
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CdcSuffererInfoStatExportDTO {

    @ExcelAttribute(name ="姓名")
    private String suffererName;

    @ExcelAttribute(name ="性别")
    private String sex;

    @ExcelAttribute(name ="年龄")
    private String age;

    @ExcelAttribute(name ="身份证号")
    private String idNo;

    @ExcelAttribute(name ="电话")
    private String telphone;

    @ExcelAttribute(name ="诊断时间")
    private String caseDate;

    @ExcelAttribute(name = "当前状态")
    private String status;

    @ExcelAttribute(name ="病情情况")
    private String clinicalSeverity;

}
