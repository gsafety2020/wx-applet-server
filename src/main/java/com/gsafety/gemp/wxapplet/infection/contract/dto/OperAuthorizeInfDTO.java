package com.gsafety.gemp.wxapplet.infection.contract.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * 
* @ClassName: OperAuthorizeInfDTO 
* @Description: 用户授权入参
* @author luoxiao
* @date 2020年3月2日 下午8:47:16 
*
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("患者溯源用户授权入参")
public class OperAuthorizeInfDTO implements Serializable{
	
	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "sessionKey", required = true, example = "sessionKey", dataType = "String")
	private String sessionKey;

	@ApiModelProperty(value = "手机号码", required = true, example = "13512345678", dataType = "String")
	private String mobile;

}
