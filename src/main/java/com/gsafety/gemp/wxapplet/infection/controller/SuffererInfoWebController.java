package com.gsafety.gemp.wxapplet.infection.controller;

import com.gsafety.gemp.wxapplet.healthcode.contract.dto.CodeBasDistrictDTO;
import com.gsafety.gemp.wxapplet.healthcode.contract.dto.Result;
import com.gsafety.gemp.wxapplet.infection.contract.dto.*;
import com.gsafety.gemp.wxapplet.infection.contract.enums.SuffererTypeEnum;
import com.gsafety.gemp.wxapplet.infection.contract.iface.SiteStaffService;
import com.gsafety.gemp.wxapplet.infection.contract.iface.SuffererInfoService;
import com.gsafety.gemp.wxapplet.infection.dao.po.SiteStaffPO;
import com.gsafety.gemp.wxapplet.infection.dao.po.SuffererInfoPO;
import com.gsafety.gemp.wxapplet.infection.dao.po.UserPO;
import com.gsafety.gemp.wxapplet.utils.UUIDUtils;
import com.gsafety.gemp.wxapplet.web.UserContext;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Api(value = "病患追溯信息WEB接口", tags = {"病患追溯信息WEB接口"})
@RestController
@RequestMapping("/infectionweb")
@Slf4j
public class SuffererInfoWebController {

	@Resource
    private SuffererInfoService suffererInfoService;

    @Resource
    private SiteStaffService siteStaffService;

    @ApiOperation(value = "病患信息分页查询")
    @PostMapping("/sufferer/search")
	@ApiImplicitParams({ @ApiImplicitParam(paramType = "header", dataType = "String", name = "token", value = "token标记", required = true) })
    public Result searchSuffererInfo(@RequestBody SuffererInfoWebPageDTO dto) {
        if(dto.getPage() == null || dto.getPage() <= 0){
        	dto.setPage(0);
        }else{
        	dto.setPage(dto.getPage() - 1);
        }

        if(dto.getSize() == null || dto.getSize() < 1){
        	dto.setSize(10);
        }
        //按照隔离点名称升序，进入时间倒序
		List<Sort.Order> list = new ArrayList<>();
		Sort.Order order1 = new Sort.Order(Sort.Direction.ASC, "curSiteName");
		Sort.Order order2 = new Sort.Order(Sort.Direction.DESC, "moveIntoTime");
		list.add(order1);
		list.add(order2);
        PageRequest pageRequest = PageRequest.of(dto.getPage(), dto.getSize(),Sort.by(list));
		UserPO user = UserContext.getUser();
        return new Result().success(suffererInfoService.searchWebSuffererInfo(user.getId(), dto, pageRequest));
    }

    @ApiOperation(value = "获取病患详情")
    @GetMapping("/sufferer/detail")
    public Result getSuffererInfoById(@ApiParam(value = "患者ID") @RequestParam String id) {
		return new Result().success(suffererInfoService.getSuffererWebById(id));
    }

	@ApiOperation(value = "病患类型下拉框")
	@GetMapping("/suffererType/List")
	public Result getSuffererTypeList() {
		return new Result().success(SuffererTypeEnum.getList());
	}



    //todo 无效的接口是否删除
    @ApiOperation(value = "查询转出位置下拉框")
    @PostMapping("/site/select/query")
    public Result<List<SiteInfoSelectDTO>> querySiteInfoSelect(){
    	Result<List<SiteInfoSelectDTO>> result = new Result<List<SiteInfoSelectDTO>>();
    	try{
    		//获取用户  SiteStaffPO staffPo = new SiteStaffPO(); 需要改成后端获取登陆用户
    		SiteStaffPO staffPo = new SiteStaffPO();
    		UserPO  userPO = UserContext.getUser();
    		staffPo.setSiteIdGroup(userPO.getSiteIdGroup());

    		List<SiteInfoSelectDTO> selectDto = suffererInfoService.getSiteInfoSelectByStaff(staffPo);
    		return result.success("查询成功!", selectDto);
    	}catch(Exception e){
    		return result.fail(e.getMessage());
    	}
    }

    @ApiOperation(value = "保存病患信息")
    @PostMapping("/sufferer/save")
    public Result<SuffererDetailDTO> save(@RequestBody SuffererInfoWebSaveDTO dto){
    	Result<SuffererDetailDTO> result = new Result<>();
    	try{
    		SuffererInfoPO po = new SuffererInfoPO();
    		BeanUtils.copyProperties(dto, po);
    		po.setId(UUIDUtils.getUUID());

    		//获取用户  SiteStaffPO staffPo = new SiteStaffPO(); 需要改成后端获取登陆用户
//    		SiteStaffPO staffPo = new SiteStaffPO();
    		if(StringUtils.isEmpty(dto.getAreaCode())){
    			po.setAreaCode("420115");
    			po.setAreaName("江夏区");
    		}
    		po.setCreateBy(UserContext.getUser().getRealname());
    		po.setCreateTime(Calendar.getInstance().getTime());
    		po.setUpdateBy(UserContext.getUser().getRealname());
    		po.setUpdateTime(Calendar.getInstance().getTime());
    		SuffererDetailDTO resultDto = suffererInfoService.saveSuffererInfo(po);
    		return result.success("保存病患信息成功!",resultDto);
    	}catch(Exception e){
    		log.error("保存病患信息[{}]失败", dto, e);
    		return result.fail(e.getMessage());
    	}
    }

    @ApiOperation(value = "更新病患信息")
    @PostMapping("/sufferer/update")
    public Result<SuffererWebDetailDTO> update(@RequestBody SuffererInfoWebEditDTO dto){
    	Result<SuffererWebDetailDTO> result = new Result<>();
    	try{
    		SuffererInfoPO po = new SuffererInfoPO();
    		BeanUtils.copyProperties(dto, po);
    		//获取用户  SiteStaffPO staffPo = new SiteStaffPO(); 需要改成后端获取登陆用户
    		SiteStaffPO staffPo = new SiteStaffPO();

    		po.setUpdateBy(staffPo.getRealname());
    		po.setUpdateTime(Calendar.getInstance().getTime());
    		SuffererWebDetailDTO resultDto = suffererInfoService.updateSuffererInfo(po);
    		return result.success("保存病患信息成功!",resultDto);
    	}catch(Exception e){
    		log.error("保存病患信息[{}]失败", dto, e);
    		return result.fail(e.getMessage());
    	}
    }

    @ApiOperation(value = "获取武汉市下面所有的区")
    @PostMapping("/sufferer/area/selected")
    public Result<List<CodeBasDistrictDTO>> areaSelected(){
    	Result<List<CodeBasDistrictDTO>> result = new Result<>();
    	try{
    		List<CodeBasDistrictDTO> resultDto = suffererInfoService.getCodeBaseDistrictByParentCode();
    		return result.success("保存病患信息成功!",resultDto);
    	}catch(Exception e){
    		log.error("获取武汉市下面所有的区信息失败", e);
    		return result.fail(e.getMessage());
    	}
    }

    @ApiOperation(value = "通过身份证校验患者是否存在")
    @PostMapping("/sufferer/check/suffererinfo")
    public Result<String> checkSuffererinfo(@ApiParam(value = "身份证") @RequestParam String suffererCard){
    	Result<String> result = new Result<>();
    	try{
    		String id = suffererInfoService.checkSuffererInfoExists(suffererCard);
    		if(StringUtils.isEmpty(id)){
    			return result.success("用户信息不存在!", id);
    		}
    		return result.prompt("用户信息已经存在!", id);
    	}catch(Exception e){
    		log.error("身份证校验患者是否存在[{}]失败", suffererCard, e);
    		return result.fail(e.getMessage());
    	}
    }

	@ApiOperation(value = "隔离人员统计")
	@PostMapping("/isolation/statistics")
	public Result<List<SuffererIsolationStatisticsDTO>> isolationStatistics(@RequestBody SuffererisonlationSearchDTO dto){
		return new Result().success(suffererInfoService.isolatetionStatistics(dto.getTime()));
	}
}
