package com.gsafety.gemp.wxapplet.infection.contract.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("病患溯源信息")
public class SuffererTraceDTO {

    @ApiModelProperty(value = "患者姓名")
    private String suffererName ;

    private List<SuffererTraceDetailDTO> list ;

}
