package com.gsafety.gemp.wxapplet.utils;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

/**
 * 
* @ClassName: QRCodeUtils 
* @Description: 二维码生成工具类
* @author luoxiao
* @date 2020年2月21日 下午9:02:32 
*
 */
public class QRCodeUtils {

	/**
	 * 
	* @Title: generateQRCodeImage 
	* @Description: 生成二维码图片
	* @param @param url   二维码链接
	* @param @param width 宽度
	* @param @param height  高度
	* @param @param filePath   路径
	* @param @param fileName   文件名
	* @param @param pattern   图片格式 PNG  JPG等
	* @return void    返回类型 
	* @throws
	 */
	public static void generateQRCodeImage(String url, int width, int height, String filePath,String fileName,String pattern){
		QRCodeWriter qrCodeWriter = new QRCodeWriter();
		BitMatrix bitMatrix = null;
		try {
			bitMatrix = qrCodeWriter.encode(url, BarcodeFormat.QR_CODE, width, height);
			
		} catch (WriterException e) {
			e.printStackTrace();
		}
		File file = new File(filePath);
		if (!file.exists()) {
			file.mkdirs();
        }
		Path path = FileSystems.getDefault().getPath(filePath+File.separator+fileName+"."+pattern);
		try {
			MatrixToImageWriter.writeToPath(bitMatrix, pattern, path);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	* @Title: generateQRCodeBufferImage 
	* @Description: 获取图片BufferImage对象 
	* @param @param url    二维码内容
	* @param @param width  宽度
	* @param @param height 高度
	* @param @param color  二维码颜色
	* @param @param backgroundColor 背景颜色
	* @param @return    设定文件 
	* @return BufferedImage    返回类型 
	* @throws
	 */
	public static BufferedImage generateQRCodeBufferImage(String url,int width, int height,int color,int backgroundColor){
		Map<EncodeHintType, Object> hints = new HashMap<>();
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
        hints.put(EncodeHintType.MARGIN, 2);
        BufferedImage image = null;
        try {
            BitMatrix matrix = (new MultiFormatWriter()).encode(url, BarcodeFormat.QR_CODE, width, height, hints);
            image = MatrixToImageWriter.toBufferedImage(matrix, new MatrixToImageConfig(color,backgroundColor));
        } catch (Exception e) {
            e.printStackTrace();
        }
		return image;
	} 
	
	/**
	 * 
	* @Title: generateQRCodeLogoImage 
	* @Description: 生成带有logo图片的二维码
	* @param @param url    二维码内容
	* @param @param imageURL  logo网络图片地址
	* @param @param width   宽度
	* @param @param height  高度
	* @param @param filePath  存储本地文件路径
	* @param @param fileName  文件名
	* @param @param pattern   文件格式
	* @param @param color     二维码颜色
	* @param @param backgroundColor    二维码背景色
	* @return void    返回类型 
	* @throws
	 */
	public static void generateQRCodeLogoImage(String url,String imageURL, int width, int height, String filePath,String fileName,String pattern,int color,int backgroundColor){
		try {
			File file = new File(filePath);
			if (!file.exists()) {
				file.mkdirs();
	        }
			BufferedImage image = generateQRCodeBufferImage(url,width,height,color,backgroundColor);
			Graphics2D g = image.createGraphics();  //读取二维码图片，并构建绘图对象
			BufferedImage logoImage = getRemoteImage(imageURL);//读取网路图片
			int widthLogo = image.getWidth()/5; 
			int heightLogo = image.getWidth()/5; 
			//计算图片放置位置
			int x = (image.getWidth() - widthLogo) / 2;
			int y = (image.getHeight() - heightLogo) / 2 ;
			
			g.drawImage(logoImage, x, y, widthLogo, heightLogo, null);
			g.drawRoundRect(x, y, widthLogo, heightLogo, 10, 10);
			g.setStroke(new BasicStroke(2));
			g.setColor(Color.WHITE);
			g.drawRect(x, y, widthLogo, heightLogo);
			g.dispose();
			ImageIO.write(image, pattern, new FileOutputStream(filePath+File.separator+fileName+"."+pattern));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	* @Title: generateQRCodeColorImage 
	* @Description: 生成单一的带有颜色的二维码
	* @param @param url   二维码内容
	* @param @param width 宽度
	* @param @param height 高度
	* @param @param filePath  本地文件存储路径
	* @param @param fileName   文件名
	* @param @param pattern    文件格式
	* @param @param color     二维码颜色
	* @param @param backgroundColor    背景色
	* @return void    返回类型 
	* @throws
	 */
	public static void generateQRCodeColorImage(String url, int width, int height, String filePath,String fileName,String pattern,int color,int backgroundColor){
		try {
			File file = new File(filePath);
			if (!file.exists()) {
				file.mkdirs();
	        }
			BufferedImage image = generateQRCodeBufferImage(url,width,height,color,backgroundColor);
			ImageIO.write(image, pattern, new FileOutputStream(filePath+File.separator+fileName+"."+pattern));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	* @Title: getRemoteImage 
	* @Description: 获取远程图片BufferImage地址
	* @param @param imageURL   图片网络地址
	* @param @return    设定文件 
	* @return BufferedImage    返回类型 
	* @throws
	 */
	public static BufferedImage getRemoteImage(String imageURL){
		URL url = null;
		InputStream is = null;
		BufferedImage bufferedImage = null;
		try {
			url = new URL(imageURL);
			is = url.openStream();
			bufferedImage = ImageIO.read(is);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			throw new RuntimeException("imageURL: " + imageURL + ",读取失败!");
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("imageURL: " + imageURL + ",读取失败!");
		} finally {
			try {
				if(is!=null) {
					is.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException("imageURL: " + imageURL + ",文件流关闭异常!");
			}
		}
		return bufferedImage;
	}

}
