package com.gsafety.gemp.wxapplet.infection.controller;


import cn.hutool.core.util.ObjectUtil;
import com.gsafety.gemp.wxapplet.healthcode.contract.dto.Result;
import com.gsafety.gemp.wxapplet.infection.contract.dto.FeverClinicsInfoDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.FeverClinicsInfoPageDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.PointHospitalInfoPageDTO;
import com.gsafety.gemp.wxapplet.infection.contract.iface.FeverClinicsInfoService;
import com.gsafety.gemp.wxapplet.infection.dao.po.PointHospitalInfoPO;
import com.gsafety.gemp.wxapplet.infection.wrapper.PointHospitalInfoWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

@Api(value = "发热门诊患者信息接口", tags = {"发热门诊患者信息接口"})
@RestController
@RequestMapping("/infection/clinics")
@Slf4j
public class FeverClinicsInfoController {

    @Autowired
    private FeverClinicsInfoService feverClinicsInfoService;

    @ApiOperation(value="发热门诊患者信息分页查询接口")
    @PostMapping("/list")
    public Result list(@RequestBody FeverClinicsInfoPageDTO dto){
        if(dto.getPage() == null || dto.getPage() <= 0){
            dto.setPage(0);
        }else{
            dto.setPage(dto.getPage() - 1);
        }

        if(dto.getSize() == null || dto.getSize() < 1){
            dto.setSize(10);
        }
        Sort sort = Sort.by(Sort.Direction.DESC, "updateTime");
        PageRequest pageRequest = PageRequest.of(dto.getPage(), dto.getSize(),sort);
        return new Result().success(feverClinicsInfoService.search(dto,pageRequest));
    }

    @ApiOperation(value="发热门诊患者详情接口")
    @GetMapping("/detail")
    public Result sufferDetail(@ApiParam(value = "患者ID") @RequestParam String id){
        FeverClinicsInfoDTO dto = feverClinicsInfoService.findOne(id);
        if (ObjectUtil.isNull(dto)) {
            return new Result().fail("数据查找失败");
        }

        return new Result().success(dto);
    }
}
