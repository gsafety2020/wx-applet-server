package com.gsafety.gemp.wxapplet.healthcode.contract.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * 
* @ClassName: PestHealthAuthorizeDTO 
* @Description: 授权设备输出参数
* @author luoxiao
* @date 2020年2月26日 下午5:47:39 
*
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("授权返回参数")
public class PestHealthAuthorizeDTO implements Serializable{

	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "授权主键")
	private String authorizeId;
	
	@ApiModelProperty(value = "设备id")
	private String deviceId;
	
	@ApiModelProperty(value = "设备名称")
	private String deviceName;
	
	@ApiModelProperty(value = "设备类型（0：PDA,1：其他）")
	private String deviceType;

}
