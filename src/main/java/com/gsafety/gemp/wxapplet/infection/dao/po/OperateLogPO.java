package com.gsafety.gemp.wxapplet.infection.dao.po;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lzx
 * @version 1.0
 * @date 2020/3/1 18:47
 */
@Entity
@Table(name = "operate_log")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OperateLogPO implements Serializable {
    private static final long serialVersionUID = 1000L;

    @Id
    @Column(name = "id")
    private String id;

    /**操作具体事项*/
    @Column(name = "title")
    private String title;

    /**操作类型*/
    @Column(name = "operate_type")
    private String operateType;

    /**访问方法*/
    @Column(name = "method")
    private String method;

    /**创建人*/
    @Column(name = "create_by")
    private String createBy;

    /**更新人*/
    @Column(name = "update_by")
    private String updateBy;

    /**创建时间*/
    @Column(name = "create_time")
    private Date createTime;

    /**更新时间*/
    @Column(name = "update_time")
    private Date updateTime;

    /**备注信息*/
    @Column(name = "remark")
    private Date remark;

}
