package com.gsafety.gemp.wxapplet.infection.contract.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
* @ClassName: SffererStatusUpdateDTO 
* @Description: 更新管理状态入参
* @author luoxiao
* @date 2020年2月28日 下午10:15:23 
*
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("更新管理状态入参")
public class SuffererStatusUpdateDTO implements Serializable{

	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;
	
    @ApiModelProperty(value = "患者ID")
	private String id;
	
    @ApiModelProperty(value = "管理状态状态：1.隔离点在管（进入）；2.在院治疗（进入）；3.治愈出院（离开）；4.病亡（离开）；5.转出（离开）；6.其他；")
    private Integer suffererStatus;

    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd hh:mm")
	@ApiModelProperty(value = "管理状态改变时间(格式yyyy-MM-dd hh:mm)")
	private Date suffererStatusTime;
	
    @ApiModelProperty(value = "用户sessionkey")
    private String sessionKey;
	
    @ApiModelProperty(value = "当前位置")
    private String curSiteName;

    @ApiModelProperty(value = "当前位置ID")
    private String curSiteId;

}
