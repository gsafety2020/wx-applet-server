package com.gsafety.gemp.wxapplet.infection.wrapper;

import com.gsafety.gemp.wxapplet.infection.contract.dto.FeverClinicsInfoDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.PointHospitalInfoDTO;
import com.gsafety.gemp.wxapplet.infection.contract.enums.PointGenderEnum;
import com.gsafety.gemp.wxapplet.infection.contract.enums.PointIllnessStateEnum;
import com.gsafety.gemp.wxapplet.infection.contract.enums.PointSuffererStatusEnum;
import com.gsafety.gemp.wxapplet.infection.contract.enums.PointSuffererTypeEnum;
import com.gsafety.gemp.wxapplet.infection.dao.po.FeverClinicsInfoPO;
import com.gsafety.gemp.wxapplet.infection.dao.po.PointHospitalInfoPO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.ArrayList;
import java.util.List;

public class FeverClinicsInfoWrapper {

    public static FeverClinicsInfoDTO toDTO(FeverClinicsInfoPO po) {
        FeverClinicsInfoDTO dto = new FeverClinicsInfoDTO();
        BeanUtils.copyProperties(po, dto);
        if(StringUtils.isNotBlank(po.getGender())){
            dto.setGender(PointGenderEnum.getNameByCode(po.getGender()));
        }
        return dto;
    }

    public static List<FeverClinicsInfoDTO> toDTOList(List<FeverClinicsInfoPO> records) {
        List<FeverClinicsInfoDTO> list = new ArrayList<>();
        records.stream().forEach(record -> list.add(toDTO(record)));
        return list;
    }

    public static Page<FeverClinicsInfoDTO> toPageDTO(Page<FeverClinicsInfoPO> page){
        List<FeverClinicsInfoDTO> content = toDTOList(page.getContent());
        return new PageImpl<>(content, page.getPageable(), page.getTotalElements());
    }
}
