package com.gsafety.gemp.wxapplet.healthcode.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import com.gsafety.gemp.wxapplet.healthcode.constant.PestUserAuthorizeStatus;
import com.gsafety.gemp.wxapplet.healthcode.contract.dto.PestUserAuthorizeDTO;
import com.gsafety.gemp.wxapplet.healthcode.contract.service.PestUserAuthorizeService;
import com.gsafety.gemp.wxapplet.healthcode.dao.PestUserAuthorizeDao;
import com.gsafety.gemp.wxapplet.healthcode.dao.po.PestUserAuthorizePO;
import com.gsafety.gemp.wxapplet.healthcode.wrapper.UserAuthorizeWrapper;


/**
 * 
* @Description: 用户权限服务层
* @author xuelq
* @date 2020年2月26日 上午12:04:22 
*
 */
@EnableAsync
@Service("pestUserAuthorizeService")
public class PestUserAuthorizeServiceImpl implements PestUserAuthorizeService{

	@Autowired
	private PestUserAuthorizeDao pestUserAuthorizeDao;

	@Override
	public List<PestUserAuthorizeDTO> getUserAuthorizes(String mobile, String[] auths) {
		
		List<PestUserAuthorizePO> pos = pestUserAuthorizeDao.findByMobileAndAuthorizeTypeInAndIsDelete(mobile, auths, 
				PestUserAuthorizeStatus.DeleteEnum.NOT_DELETE.getCode());
		
		List<PestUserAuthorizeDTO> dtos = UserAuthorizeWrapper.listPoToDto(pos);
		
		return dtos;
	}

	@Override
	public int getUserAuthorizeCount(String mobile, String[] auths) {
		int count = pestUserAuthorizeDao.countByMobileAndAuthorizeTypeInAndIsDelete(mobile, auths, 
				PestUserAuthorizeStatus.DeleteEnum.NOT_DELETE.getCode());
		return count;
	}
	

}
