package com.gsafety.gemp.wxapplet.infection.contract.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CdcSuffererInfoPageDTO {

    @ApiModelProperty(value = "患者姓名")
    private String suffererName;

    @ApiModelProperty(value = "身份证号")
    private String suffererCard;

    @ApiModelProperty(value = "患者电话")
    private String suffererTel;

    @ApiModelProperty(value = "病情程度、临床诊断病例；2、确诊病例；3、阳性检测；4、疑似病例")
    private Integer caseType;

    @ApiModelProperty(value = "病例分类、普通型2、轻型3、危重型4、无症状感染者5、重型6、其它")
    private Integer clinicalSeverity;

    @ApiModelProperty(value = "当前状态")
    private Integer curStatus;

    @ApiModelProperty(value = "页码,从0开始")
    private Integer page;

    @ApiModelProperty(value = "每页行数")
    private Integer size;
}
