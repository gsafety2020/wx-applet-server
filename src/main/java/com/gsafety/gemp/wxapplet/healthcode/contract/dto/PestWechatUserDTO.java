package com.gsafety.gemp.wxapplet.healthcode.contract.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.io.Serializable;
import java.util.Date;

/**
 * 
* @ClassName: PestWechatUserDTO
* @Description: 微信用户表
* @author wangwenhai
* @date 2020年2月27日 上午11:12:09
*
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("微信用户信息")
public class PestWechatUserDTO implements Serializable{

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value="小程序openId")
	/**用户微信openId*/
	private String openId;

	@ApiModelProperty(value="昵称")
	/**昵称*/
	private String nickName;

	@ApiModelProperty(value="性别")
	/**性别，0-未知，1-男性，2-女性*/
	private String gender;

	@ApiModelProperty(value="城市")
	/**城市*/
	private String city;

	@ApiModelProperty(value="省份")
	/**省*/
	private String province;

	@ApiModelProperty(value="国家")
	/**国家*/
	private String country;

	@ApiModelProperty(value="头像")
	/**头像*/
	private String avatarUrl;

	@ApiModelProperty(value="唯一标识")
	/**唯一标识*/
	private String unionId;

	@ApiModelProperty(value="是否授权，0-未授权，1-已授权")
	/**是否授权，0-未授权，1-已授权*/
	private String withCredentials;

	@ApiModelProperty(value="首次访问时间")
	@JsonFormat(pattern = "yyyy-MM-dd",locale = "zh",timezone = "GMT+8")
	/**首次访问时间*/
	private Date firstAccessTime;

	@ApiModelProperty(value="授权时间")
	@JsonFormat(pattern = "yyyy-MM-dd",locale = "zh",timezone = "GMT+8")
	/**授权时间*/
	private Date credentialsTime;

	@ApiModelProperty(value="更新时间")
	@JsonFormat(pattern = "yyyy-MM-dd",locale = "zh",timezone = "GMT+8")
	/**最后更新时间*/
	private Date updateTime;

	@ApiModelProperty(value="appid")
	/**appid*/
	private String appid;

}
