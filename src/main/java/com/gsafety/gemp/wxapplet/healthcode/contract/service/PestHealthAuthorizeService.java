package com.gsafety.gemp.wxapplet.healthcode.contract.service;

import com.gsafety.gemp.wxapplet.healthcode.contract.dto.PestHealthAuthorizeDTO;

/**
 * 
* @ClassName: PestHealthAuthorizeService 
* @Description: 扫码授权服务类
* @author luoxiao
* @date 2020年2月26日 下午5:25:04 
*
 */
public interface PestHealthAuthorizeService {

	/**
	 * 
	* @Title: getAuthorizeInfo 
	* @Description: 根据设备获取授权信息 
	* @param @param deviceId
	* @param @return    设定文件 
	* @return PestHealthAuthorizeDTO    返回类型 
	* @throws
	 */
	PestHealthAuthorizeDTO getAuthorizeInfo(String deviceId);
}
