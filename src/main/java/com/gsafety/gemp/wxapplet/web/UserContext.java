package com.gsafety.gemp.wxapplet.web;

import com.gsafety.gemp.wxapplet.infection.dao.po.UserPO;

/**
 * @author Administrator
 * @Title: UserContext
 * @ProjectName wx-applet
 * @Description: TODO
 * @date 2020/3/120:58
 */
public class UserContext {

    private static ThreadLocal<UserPO> userHolder = new ThreadLocal<UserPO>();

    public static void setUser(UserPO loginUser) {
        userHolder.set(loginUser);
    }

    public static UserPO getUser() {
        return userHolder.get();
    }
}
