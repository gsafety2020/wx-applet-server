package com.gsafety.gemp.wxapplet.infection.contract.iface;

import java.util.List;

import com.gsafety.gemp.wxapplet.infection.dao.po.SuffererInfoPO;

/**
 * 
* @ClassName: SuffererExcelService 
* @Description: 病患信息Excel导入
* @author luoxiao
* @date 2020年3月5日 下午3:51:49 
*
 */
public interface SuffererExcelService {

	/**
	 * 
	* @Title: importIsolateExcel 
	* @Description: 导入统计局患者数据
	* @param @param suffererInfos
	* @param @return    设定文件 
	* @return boolean   true：成功  false：失败
	* @throws
	 */
	public void importIsolateExcel(List<SuffererInfoPO> suffererInfos);
	
	
	public void compareBakSuffererInfoTrace();
}
