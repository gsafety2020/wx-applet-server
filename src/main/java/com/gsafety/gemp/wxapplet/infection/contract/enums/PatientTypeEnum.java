package com.gsafety.gemp.wxapplet.infection.contract.enums;

import com.gsafety.gemp.wxapplet.utils.StringCodeEnum;
import lombok.Getter;

/**
 * @author lzx
 * @version 1.0
 * @date 2020/2/28 0:30
 */
@Getter
public enum PatientTypeEnum implements StringCodeEnum {

    TOUCH("1","密接人员"),
    HOT("2","发热患者"),
    SUSPECT("3","疑似患者"),
    DIAGNOSE("4","确诊"),
    OTHER("7","其他");

    private String code;
    private String name;
    PatientTypeEnum(String code,String name){
        this.code = code;
        this.name = name;
    }

    public static String getNameByCode(String code) {
        for (PatientTypeEnum dataTypeEnum : values()) {
            if (dataTypeEnum.code.equals(code)){
                return dataTypeEnum.name;
            }
        }
        return null;
    }

}
