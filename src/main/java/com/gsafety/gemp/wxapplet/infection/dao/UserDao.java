package com.gsafety.gemp.wxapplet.infection.dao;

import com.gsafety.gemp.wxapplet.infection.dao.po.UserPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * 患者信息DAO
 *
 * @author liyanhong
 * @since 2020/2/27  10:25
 */
public interface UserDao extends JpaRepository<UserPO, String>, JpaSpecificationExecutor<UserPO> {

    /**
     * 根据用户名查询用户
     * @param username
     * @return
     */
    UserPO findByUsername(String username);
}
