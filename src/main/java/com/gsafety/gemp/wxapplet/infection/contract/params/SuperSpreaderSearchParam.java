package com.gsafety.gemp.wxapplet.infection.contract.params;

import com.gsafety.gemp.wxapplet.infection.contract.common.PageReq;
import io.swagger.annotations.ApiModel;


import java.io.Serializable;

@ApiModel(description="超级传播者查询条件")
public class SuperSpreaderSearchParam extends PageReq implements Serializable {
}
