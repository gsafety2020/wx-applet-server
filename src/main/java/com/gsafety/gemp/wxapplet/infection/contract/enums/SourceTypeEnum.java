package com.gsafety.gemp.wxapplet.infection.contract.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 
* @ClassName: SourceTypeEnum 
* @Description: 数据来源枚举类 
* @author luoxiao
* @date 2020年3月5日 下午4:48:00 
*
 */
@Getter
@AllArgsConstructor
public enum SourceTypeEnum {

	TJJ(2,"统计局"),WJW(1,"卫健委");
	
	private Integer code;
	
	private String value;
}
