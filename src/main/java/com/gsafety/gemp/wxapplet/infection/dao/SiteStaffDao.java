package com.gsafety.gemp.wxapplet.infection.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gsafety.gemp.wxapplet.infection.dao.po.SiteStaffPO;

/**
 * 
* @ClassName: SiteStaffDao 
* @Description: 操作人员信息DAO
* @author luoxiao
* @date 2020年3月1日 下午4:40:24 
*
 */
public interface SiteStaffDao extends JpaRepository<SiteStaffPO, String>,
JpaSpecificationExecutor<SiteStaffPO>{

	SiteStaffPO findByOpenid(String openId);
	
	SiteStaffPO findByPhone(String phone);
}
