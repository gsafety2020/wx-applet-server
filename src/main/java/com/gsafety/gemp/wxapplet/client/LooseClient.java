/**
 * 
 */
package com.gsafety.gemp.wxapplet.client;

import com.gsafety.gemp.wxapplet.client.exception.DefaultClientException;

/**
 * @author lenovo
 *
 */
public interface LooseClient {

	/**
	 * 
	 * @param request
	 * @return
	 */
	public <T extends Response> T execute(Request<T> request) throws DefaultClientException;
}