package com.gsafety.gemp.wxapplet.healthcode.dao.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
* @ClassName: PestHealthRecordPO 
* @Description: 微信健康码打开记录表
* @author luoxiao
* @date 2020年2月26日 下午4:17:36 
*
 */
@Entity
@Table(name = "pest_health_record")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PestHealthRecordPO  implements Serializable{

	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "record_id")
	/**主键UUID*/
	private String recordId;
	
	@Column(name = "health_code_id")
	/**用户健康码ID 关联pest_health_code*/
	private String healthCodeId;
	
	@Column(name = "authorize_id")
	/**用户扫码授权ID 关联pest_health_authorize*/
	private String authorizeId;
	
	@Column(name = "device_id")
	/**设备id*/
	private String deviceId;
	
	@Column(name = "device_name")
	/***/
	private String deviceName;
	
	@Column(name = "device_type")
	/***/
	private String deviceType;
	
	@Column(name = "area_code")
	/***/
	private String areaCode;
	
	@Column(name = "area_name")
	/***/
	private String areaName;
	
	@Column(name = "address")
	/***/
	private String address;
	
	@Column(name = "longitude")
	/***/
	private BigDecimal longitude;
	
	@Column(name = "latitude")
	/***/
	private BigDecimal latitude;
	
	@Column(name = "is_delete")
	/***/
	private String isDelete;
	
	@Column(name = "post_date")
	/***/
	private Date postDate;
	
	@Column(name = "create_time")
	/***/
	private Date createTime;
	
	@Column(name = "update_time")
	/***/
	private Date updateTime;
	
	

}
