package com.gsafety.gemp.wxapplet.infection.contract.iface;

import com.gsafety.gemp.wxapplet.infection.contract.dto.FeverClinicsInfoDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.FeverClinicsInfoPageDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.PointHospitalInfoDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.PointHospitalInfoPageDTO;
import com.gsafety.gemp.wxapplet.infection.dao.po.FeverClinicsInfoPO;
import com.gsafety.gemp.wxapplet.infection.dao.po.PointHospitalInfoPO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface FeverClinicsInfoService {

    Page<FeverClinicsInfoDTO> search(FeverClinicsInfoPageDTO dto, PageRequest pageRequest);

    /**
     * 发热门诊患者信息详情
     * @param id
     * @return
     */
    FeverClinicsInfoDTO findOne(String id);
}
