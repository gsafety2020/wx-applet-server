package com.gsafety.gemp.wxapplet.healthcode.dao.po;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
* @Description: 用户表
* @author xuelq
* @date 2020年2月27日 下午3:53:09 
*
 */
@Entity
@Table(name = "pest_user_authorize")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PestUserAuthorizePO implements Serializable{

	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "authorize_id")
	/**主键*/
	private String authorizeId;
	
	@Column(name = "mobile")
	/**手机号*/
	private String mobile;
	
	@Column(name = "authorize_type")
	/**授权类型*/
	private String authorizeType;
	
	@Column(name = "remark")
	/**备注*/
	private String remark;
	
	@Column(name = "create_by")
	/**创建人*/
	private Date createBy;
	
	@Column(name = "create_time")
	/**创建时间*/
	private Date createTime;
	
	@Column(name = "upate_by")
	/**更新人*/
	private Date updateBy;
	
	@Column(name = "update_time")
	/**更新时间*/
	private Date updateTime;
	
	@Column(name = "is_delete")
	/**是否删除0：未删除  1.已删除*/
	private String isDelete;

}
