package com.gsafety.gemp.wxapplet.infection.dao;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import com.gsafety.gemp.wxapplet.infection.dao.po.SuffererInfoAllPO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gsafety.gemp.wxapplet.infection.dao.po.SuffererInfoPO;

/**
 * 患者信息DAO
 *
 * @author liyanhong
 * @since 2020/2/27  10:25
 */
public interface SuffererInfoDao extends JpaRepository<SuffererInfoPO, String>,
        JpaSpecificationExecutor<SuffererInfoPO> {

	Page<SuffererInfoPO> findByCurSiteIdInOrderByUpdateTimeDesc(List<String> curSiteId,Pageable pageable);
	
	Long countByCurSiteIdIn(List<String> curSiteId);
	
	Page<SuffererInfoPO> findByCurSiteIdInAndSuffererNameLikeOrSuffererCardLikeOrSuffererTelLikeOrderByUpdateTimeDesc(List<String> curSiteId,String name, String card, String tel,Pageable pageable);
    
    Long countByCurSiteIdInAndSuffererNameLikeOrSuffererCardLikeOrSuffererTelLike(List<String> curSiteId,String name, String card, String tel);
    
    Page<SuffererInfoPO> findBySuffererNameLikeOrSuffererCardLikeOrSuffererTelLikeOrderByUpdateTimeDesc(String name, String card, String tel,Pageable pageable);
    
    Long countBySuffererNameLikeOrSuffererCardLikeOrSuffererTelLike(String name, String card, String tel);

    SuffererInfoPO findBySuffererCard(String suffererCard);

    List<SuffererInfoPO> findAllBySuffererCardOrderByMoveIntoTimeDesc(String suffererCard);

    List<SuffererInfoPO> findAllBySuffererCardOrderByDataDateDesc(String suffererCard);

    List<SuffererInfoPO> findBySuffererStatusNotAndAreaCode(Integer suffererStatus,String areaCode);
    
    List<SuffererInfoPO> findBySuffererStatusNotAndAreaCodeAndCreateTimeBetween(Integer suffererStatus,String areaCode,Date startTime,Date endTime);
    
    Page<SuffererInfoPO> findBySuffererStatusNotAndAreaCodeAndSuffererTypeAndCreateTimeBetween(Integer suffererStatus,String areaCode,Integer suffererType,Date startTime,Date endTime,Pageable pageable);
    
    Page<SuffererInfoPO> findBySuffererStatusNotAndAreaCodeAndIllnessStateAndCreateTimeBetween(Integer suffererStatus,String areaCode,Integer illnessState,Date startTime,Date endTime,Pageable pageable);

    Page<SuffererInfoPO> findBySuffererStatusAndAreaCodeAndCreateTimeBetween(Integer suffererStatus,String areaCode,Date startTime,Date endTime,Pageable pageable);
    
    List<SuffererInfoPO> findBySuffererStatusAndAreaCodeAndCreateTimeBetween(Integer code, String s, Date yesterdayBeginTime, Date yesterdayEndTime);

    Page<SuffererInfoPO> findBySuffererStatusAndAreaCodeAndSuffererTypeAndCreateTimeBetween(Integer code, String s, Integer suffererType, Date yesterdayBeginTime, Date yesterdayEndTime, Pageable pageable);

    List<SuffererInfoPO> findBySuffererStatusAndAreaCode(Integer code, String s);
    
    List<SuffererInfoPO> findByDataDate(String dataDate);
    
    @Transactional
    void deleteByDataDate(String dataDate);

    List<SuffererInfoPO> findByAreaCodeAndSuffererStatusAndSuffererTypeAndCreateTimeBetween(String s, Integer code, Integer suffererType, Date yesterdayBeginTime, Date yesterdayEndTime);
    
    List<SuffererInfoPO> findBySuffererType(Integer suffererType);

    List<SuffererInfoPO> findBySuffererNameOrderByCreateTimeDesc(String suffererName);

    List<SuffererInfoPO> findBySuffererTypeAndDataDate(Integer suffererType, String date);

    List<SuffererInfoPO> findByTurnOutTime( Date date);
}
