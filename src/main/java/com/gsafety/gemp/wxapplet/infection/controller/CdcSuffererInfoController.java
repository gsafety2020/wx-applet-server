package com.gsafety.gemp.wxapplet.infection.controller;


import com.gsafety.gemp.wxapplet.healthcode.contract.dto.Result;
import com.gsafety.gemp.wxapplet.infection.contract.dto.*;
import com.gsafety.gemp.wxapplet.infection.contract.enums.CaseTypeEnum;
import com.gsafety.gemp.wxapplet.infection.contract.enums.ClinicalSeverityEnum;
import com.gsafety.gemp.wxapplet.infection.contract.iface.CdcSuffererInfoService;
import com.gsafety.gemp.wxapplet.infection.contract.iface.CdcSuffererTraceService;
import com.gsafety.gemp.wxapplet.infection.contract.iface.CloseContactInfoService;
import com.gsafety.gemp.wxapplet.infection.contract.iface.TraceService;
import com.gsafety.gemp.wxapplet.infection.contract.params.CdcSuffererNewlySearchParam;
import com.gsafety.gemp.wxapplet.infection.contract.params.SuperSpreaderSearchParam;
import com.gsafety.gemp.wxapplet.infection.contract.params.TraceSearchParam;
import com.gsafety.gemp.wxapplet.utils.DateUtils;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Api(value = "cdc病患信息接口", tags = {"cdc病患信息接口"})
@RestController
@RequestMapping("/infection/cdc")
@Slf4j
public class CdcSuffererInfoController {

    @Autowired
    private CdcSuffererInfoService cdcSuffererInfoService;

    @Autowired
    private CdcSuffererTraceService cdcSuffererTraceService;
    @Autowired
    private TraceService traceService;
    @Autowired
    private CloseContactInfoService closeContactInfoService;


    @ApiOperation(value="疾控中心导入接口")
    @PostMapping("import")
    public Result importExcel(MultipartFile file, @RequestParam @ApiParam("数据日期 yyyy-MM-dd") String dataDate){
        if(StringUtils.isEmpty(dataDate)){
            dataDate = DateUtils.getYesterdayString();
        }
        return cdcSuffererInfoService.importExcel(file,dataDate);
    }

    @ApiOperation(value="疾控中心数据对比生成流水")
    @PostMapping("compareData")
    public Result compareCdcSuffererInfo(){
        return traceService.saveTraceSuffererInfoData(DateUtils.getYesterdayString());
    }

    @ApiOperation(value="疾控中心数据对比生成流水")
    @PostMapping("compareData/{dataDate}")
    public Result compareCdcSuffererInfo(@PathVariable("dataDate") @ApiParam("数据日期 yyyy-MM-dd") String dataDate){
        return traceService.saveTraceSuffererInfoData(dataDate);
    }



    @ApiOperation(value="疾控中心分页查询接口")
    @PostMapping("/list")
    public Result list(@RequestBody CdcSuffererInfoPageDTO dto){
        if(dto.getPage() == null || dto.getPage() <= 0){
            dto.setPage(0);
        }else{
            dto.setPage(dto.getPage() - 1);
        }

        if(dto.getSize() == null || dto.getSize() < 1){
            dto.setSize(10);
        }
       //根据订正终审时间倒序
        Sort sort = Sort.by(Sort.Direction.DESC, "revisionFinalJudgmentDate");
        PageRequest pageRequest = PageRequest.of(dto.getPage(), dto.getSize(),sort);
        /* UserPO user = UserContext.getUser();*/
        return new Result().success(cdcSuffererInfoService.searchWebSuffererInfo(dto,pageRequest));
    }

    /**
     * 病例分类
     * @param
     * @return
     */
    @ApiOperation(value = "病例分类")
    @GetMapping(value = "/findCaseType")
    public Result findCaseType(){
        List<CaseTypeDTO> dto=CaseTypeEnum.getList();
        return new Result().success(dto);
    }

    /**
     * 病情程度
     * @param
     * @return
     */
    @ApiOperation(value = "病情程度")
    @GetMapping(value = "/findClinicalSeverity")
    public Result findClinicalSeverity(){
        List<CaseTypeDTO> dto= ClinicalSeverityEnum.getList();
        return new Result().success(dto);
    }

    /**
     * 病情状态
     * @param
     * @return
     */
    @ApiOperation(value = "当前状态")
    @GetMapping(value = "/findCurStatus")
    public Result findCurStatus(){
        List<CaseTypeDTO> list=new ArrayList<>();
        CaseTypeDTO dto3=new CaseTypeDTO();
        dto3.setCode(0);
        dto3.setName("全部");
        list.add(dto3);
        CaseTypeDTO dto1=new CaseTypeDTO();
        dto1.setCode(1);
        dto1.setName("死亡");
        list.add(dto1);
        CaseTypeDTO dto=new CaseTypeDTO();
        dto.setCode(2);
        dto.setName("在院治疗");
        list.add(dto);
        CaseTypeDTO dto2=new CaseTypeDTO();
        dto2.setCode(3);
        dto2.setName("转诊");
        list.add(dto2);
        return new Result().success(list);
    }

    @ApiOperation(value = "累计导出")
    @PostMapping(value = "/suffererinfo/export/v1")
    public void infoExport(HttpServletResponse response, HttpServletRequest request) throws Exception {
        cdcSuffererInfoService.export(response,request);
    }


    @GetMapping(value = "/suffererinfo/statisticsNewly")
    @ApiOperation(value = "新增统计")
    public Result statisticsNewly(@ApiParam("数据日期 yyyy-MM-dd") @RequestParam("dataDate") String dataDate){
        Integer count1 = cdcSuffererTraceService.statisticsNewly(dataDate, "1");
        Integer count2 = cdcSuffererTraceService.statisticsNewly(dataDate, "2");
        Integer count3 = cdcSuffererTraceService.statisticsNewly(dataDate, "3");
        Integer count4 = cdcSuffererTraceService.statisticsNewly(dataDate, "4");
        Integer count5 = cdcSuffererTraceService.statisticsNewly(dataDate, "5");
        Integer count6 = cdcSuffererTraceService.statisticsNewly(dataDate, "6");
        Integer count7 = cdcSuffererTraceService.statisticsNewly(dataDate, "7");
        CdcStatisticsNewlyDTO dto = CdcStatisticsNewlyDTO.builder().newCase(count2).newSuspected(count4).newPositive(count3).newDeath(count5)
                .newIn(count6).newOut(count7).newClinical(count1).build();
        return new Result().success("成功",dto);
    }



    @PostMapping(value = "/suffererinfo/newlyPage")
    @ApiOperation(value = "新增病患信息列表")
    public Result statisticsNewly(@RequestBody CdcSuffererNewlySearchParam dto){
        Page<CdcSuffererInfoDTO> page = cdcSuffererTraceService.getNewlyList(dto);
        return new Result().success("成功",page);
    }

    @GetMapping(value = "/suffererinfo/trace")
    @ApiOperation(value = "cdc溯源详情")
    public Result gettraceList(@RequestParam(value = "suffererIdNo",required = false) String suffererIdNo
            ,@RequestParam(value = "suffererName",required = false) String suffererName){
        CdcSuffererTraceDTO traceDTO = cdcSuffererTraceService.getTraceList(suffererIdNo,suffererName);
        return new Result().success(traceDTO);
    }


    @PostMapping(value = "/suffererinfo/tracePage")
    @ApiOperation(value = "cdc溯源详情")
    public Result gettraceList(@RequestBody TraceSearchParam dto) {
        Page<CdcSuffererTracesDTO> tracePage = cdcSuffererTraceService.getTracePage(dto);
        return new Result().success(tracePage);
    }


    @ApiOperation(value = "统计导出")
    @PostMapping(value = "/suffererinfo/newlyExport")
    @ApiImplicitParams({
            @ApiImplicitParam(name="date",value="统计日期",dataType="String", paramType = "query")
    })
    public void statExport(HttpServletResponse response, HttpServletRequest request, String date) throws Exception {
        cdcSuffererTraceService.statExport(response,request,date);
    }
    @GetMapping(value = "/suffererinfo/one")
    @ApiOperation(value = "疫情网患者详情")
    public Result<CdcSuffererInfoDTO> findPage(@ApiParam(value = "id", required = true) @RequestParam("id") String id){
        if (StringUtils.isEmpty(id) || !cdcSuffererInfoService.existsById(id)) {
            return new Result<CdcSuffererInfoDTO>().fail("id为空或记录不存在！");
        }
        CdcSuffererInfoDTO dto = cdcSuffererInfoService.findOne(id);
        return new Result<CdcSuffererInfoDTO>().success("查询成功!",dto);
    }

    @GetMapping(value = "/trace/export")
    @ApiOperation(value = "cdc溯源详情导出")
    public void traceExport(@RequestParam(value = "suffererIdNo",required = false) String suffererIdNo
            ,@RequestParam(value = "suffererName",required = false) String suffererName
            ,HttpServletResponse response, HttpServletRequest request) throws Exception {
        cdcSuffererTraceService.traceExport(suffererIdNo,suffererName,response,request);
    }

    @PostMapping(value = "/superSpreader/page")
    @ApiOperation(value = "超级传播者分页查询")
    public Result findSuperSpreaderPage(@RequestBody SuperSpreaderSearchParam param){
        return new Result().success(closeContactInfoService.findSuperSpreader(param));
    }

}
