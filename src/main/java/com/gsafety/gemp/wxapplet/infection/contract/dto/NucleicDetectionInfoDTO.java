package com.gsafety.gemp.wxapplet.infection.contract.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NucleicDetectionInfoDTO {

    @ApiModelProperty(value = "主键")
    private String id;

    /** 编号**/
    @ApiModelProperty(value = "编号")
    private String number;

    /** 委托机构**/
    @ApiModelProperty(value = "委托机构")
    private String entrustOrg;

    /** 隔离点名称**/
    @ApiModelProperty(value = "隔离点名称")
    private String isolationPoint;

    /** 隔离点类型**/
    @ApiModelProperty(value = "隔离点类型(1-发热门诊隔离点 ，2-发热病人隔离点)")
    private String isolationType;

    /** 患者姓名**/
    @ApiModelProperty(value = "患者姓名")
    private String suffererName;

    /** 有效身份证号**/
    @ApiModelProperty(value = "有效身份证号")
    private String idNo;

    /** 年龄**/
    @ApiModelProperty(value = "年龄")
    private String age;

    /** 性别**/
    @ApiModelProperty(value = "性别(1-男，2-女)")
    private String gender;

    /** 联系电话**/
    @ApiModelProperty(value = "联系电话")
    private String telphone;

    /** 患者ID**/
    @ApiModelProperty(value = "患者ID")
    private String suffererId;

    /** 病例类型及检测次数**/
    @ApiModelProperty(value = "病例类型及检测次数")
    private String caseTypeDetection;

    /** 样本类型**/
    @ApiModelProperty(value = "样本类型")
    private String sampleType;

    /** 送样日期**/
    @ApiModelProperty(value = "送样日期")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    private Date sendSampleTime;

    /** 第几次检测**/
    @ApiModelProperty(value = "第几次检测")
    private String detectionNum;

    /** 检测结果**/
    @ApiModelProperty(value = "检测结果(1-阴型 2-阳型 3-可疑请结合临床 4-无结果（检测失败） 5-无结果（无标本） 6-无结果（采样不规范）)")
    private String detectionResult;

    /** 检测机构**/
    @ApiModelProperty(value = "检测机构")
    private String detectionOrg;

    /** 是否境外返（来）汉人员**/
    @ApiModelProperty(value = "是否境外返（来）汉人员（0-否，1-是）")
    private String overseasReturnFlag;

    /** 备注**/
    @ApiModelProperty(value = "备注")
    private String note;

    /** 创建时间**/
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /** 更新时间**/
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;
}
