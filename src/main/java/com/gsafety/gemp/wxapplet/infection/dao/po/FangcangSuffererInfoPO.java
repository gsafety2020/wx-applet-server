package com.gsafety.gemp.wxapplet.infection.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author dusiwei
 */
@Entity
@Table(name = "fangcang_sufferer_info")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FangcangSuffererInfoPO implements Serializable {

    private static final long serialVersionUID = -1548629281175101846L;

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "SUFFERER_NAME")
    private String suffererName;

    @Column(name = "SUFFERER_SEX")
    private String suffererSex;

    @Column(name = "SUFFERER_AGE")
    private String suffererAge;

    @Column(name = "ADMITTED_TIME")
    private String admittedTime;

    @Column(name = "WARD")
    private String ward;

    @Column(name = "BED_NO")
    private String bedNo;

    @Column(name = "ID_NO")
    private String idNo;

    @Column(name = "SUFFERER_ADDRESS")
    private String suffererAddress;

    @Column(name = "TELEPHONE")
    private String telephone;

    @Column(name = "SUFFERER_STATUS")
    private String suffererStatus;

    @Column(name = "TRANSFER_DATE")
    private String transferDate;

    @Column(name = "TRANSFER_HOSPITAL")
    private String transferHospital;

    @Column(name = "LEAVE_DATE")
    private String leaveDate;

    @Column(name = "DISTRICT")
    private String district;

    @Column(name = "IMPORT_DATE")
    private String importDate;

    @Column(name = "CREATE_TIME")
    private Date createTime;

    @Column(name = "UPDATE_TIME")
    private Date updateTime;

    @Column(name = "DATA_TIME")
    private String dataTime;

    @Column(name = "HOSPITAL_NAME")
    private String hospitalName;

}
