package com.gsafety.gemp.wxapplet.infection.service;

import com.gsafety.gemp.common.excel.ExcelExecutor;
import com.gsafety.gemp.common.excel.ExcelResult;
import com.gsafety.gemp.common.excel.ExcelUtils;
import com.gsafety.gemp.wxapplet.healthcode.contract.dto.Result;
import com.gsafety.gemp.wxapplet.infection.contract.dto.CdcSuffererInfoDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.CdcSuffererInfoExportDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.CdcSuffererInfoPageDTO;
import com.gsafety.gemp.wxapplet.infection.contract.enums.CaseTypeEnum;
import com.gsafety.gemp.wxapplet.infection.contract.iface.CdcSuffererInfoService;
import com.gsafety.gemp.wxapplet.infection.contract.iface.CdcSuffererTraceService;
import com.gsafety.gemp.wxapplet.infection.contract.iface.TraceService;
import com.gsafety.gemp.wxapplet.infection.dao.CdcSuffererInfoAllDao;
import com.gsafety.gemp.wxapplet.infection.dao.CdcSuffererInfoDao;
import com.gsafety.gemp.wxapplet.infection.dao.LatestAdressInfoDao;
import com.gsafety.gemp.wxapplet.infection.dao.po.CdcSuffererInfo;
import com.gsafety.gemp.wxapplet.infection.dao.po.CdcSuffererInfoAll;
import com.gsafety.gemp.wxapplet.infection.dao.po.LatestAdressInfoPO;
import com.gsafety.gemp.wxapplet.infection.service.spec.CdcSuffererSpecifications;
import com.gsafety.gemp.wxapplet.infection.wrapper.CdcSuffererInfoWrapper;
import com.gsafety.gemp.wxapplet.utils.DateUtils;
import com.gsafety.gemp.wxapplet.utils.SheetExecutor;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;


/**
 * cdc病患信息导入服务
 * @author fanlx
 */
@Service
public class CdcSuffererInfoServiceImpl implements CdcSuffererInfoService {

    @Autowired
    private CdcSuffererInfoDao cdcSuffererInfoDao;
    @Autowired
    private CdcSuffererInfoAllDao cdcSuffererInfoAllDao;

    @Autowired
    private CdcSuffererTraceService cdcSuffererTraceService;

    @Autowired
    private LatestAdressInfoDao latestAdressInfoDao;

    @Autowired
    private TraceService traceService;



    /**
     * cdc病患信息导入接口
     * @param file
     * @return
     */
    @Override
    @Transactional
    public Result importExcel(MultipartFile file,String dataDate) {
        if (null == file) {
            return new Result().fail("文件未找到！");
        }
        Date now = DateTime.now().toDate();
        String today = DateUtils.format(now, "yyyyMMdd");
        String torrow = DateUtils.format(DateUtils.addHour(now, -24L), "yyyyMMdd");

        InputStream fileInputStream = null;
        ExcelResult<CdcSuffererInfoDTO> excelToList = null;
        try {
            fileInputStream = file.getInputStream();
            ExcelExecutor<CdcSuffererInfoDTO> excelExecutor = new ExcelExecutor<>(CdcSuffererInfoDTO.class);
            excelToList = excelExecutor.getExcelToList(null, fileInputStream);
            if (null == excelToList) {
                return new Result().fail("文件解析失败！");
            }

            if(StringUtils.isBlank(excelToList.getSuccessMsg())){
                return new Result().fail(excelToList.getErrorMsg());
            }
            if (StringUtils.isNotBlank(excelToList.getSuccessMsg())) {
                List<CdcSuffererInfoDTO> dtos = excelToList.getList();
                List<CdcSuffererInfo> pos = new ArrayList<>();
                for (CdcSuffererInfoDTO dto : dtos){
                    CdcSuffererInfo po = CdcSuffererInfoWrapper.toPO(dto);
                    po.setId(po.getIdNo() + torrow);
                    po.setCreatTime(now);
                    po.setUpdateTime(now);
                    po.setImportDate(today);
                    po.setDataDate(dataDate);
                    pos.add(po);

                }
                this.cdcSuffererInfoDao.deleteCdcSuffererInfoByImportDate(today);
                this.cdcSuffererInfoDao.saveAll(pos);
                traceService.saveTraceSuffererInfoData(dataDate);
            }
        } catch (Exception e) {

            e.printStackTrace();
            return new Result().fail("上传失败！");
        } finally {
            try {
                if (null != fileInputStream) {
                    fileInputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return new Result().success();
    }


    /**
     * 病患信息表备份到历史表操作
     */
    @Override
    @Transactional
    public void bakYesterdayData() {

        String torrow = DateUtils.format(DateUtils.getYesterdayDate(), "yyyyMMdd");

        this.cdcSuffererInfoAllDao.deleteCdcSuffererInfoAllByImportDate(torrow);
        List<CdcSuffererInfo> infos = this.cdcSuffererInfoDao.findCdcSuffererInfosByImportDate(torrow);
        List<CdcSuffererInfoAll> alls = convertSuffererInfoToSufferInfoAll(infos);
        this.cdcSuffererInfoAllDao.saveAll(alls);

    }

    @Override
    public Page<CdcSuffererInfoDTO> searchWebSuffererInfo(CdcSuffererInfoPageDTO dto, PageRequest pageRequest) {
        /*if(org.apache.commons.lang.StringUtils.isEmpty(staffId)){
            throw new RuntimeException("用户Id为空!");
        }
        SiteStaffPO staffPo = siteStaffService.getSiteStaffById(staffId);
        if(staffPo == null){
            throw new RuntimeException("操作用户不存在!");
        }

        List<String> relationsSite = null;
        if(!SiteStaffRelationStatus.GroupSiteEnum.ALL.getCode().equals(staffPo.getSiteIdGroup())){
            List<SiteStaffRelationPO> relations = siteStaffRelationDao.findByStaffId(staffId);
            if(CollectionUtils.isEmpty(relations)){
                throw new RuntimeException("用户操作站点不存在!");
            }
            relationsSite = relations.stream().map(SiteStaffRelationPO::getSiteId).collect(Collectors.toList());
        }*/

        Specification<CdcSuffererInfo> spec = concatSpecification(dto);
        Page<CdcSuffererInfo> poPage = cdcSuffererInfoDao.findAll(spec, pageRequest);
        Page<CdcSuffererInfoDTO> PageDto=CdcSuffererInfoWrapper.toPageDTO(poPage);
        List<CdcSuffererInfoDTO> content = PageDto.getContent();
        for(CdcSuffererInfoDTO infoDto:content){
            if(StringUtils.isEmpty(infoDto.getDeadthDate())){
                infoDto.setCurStatus("在院治疗");
            }else{
                infoDto.setCurStatus("死亡");
            }

            infoDto.setCurLocation(traceService.getLocation(infoDto.getIdNo()));
        }
//        List<CdcSuffererInfoDTO> list=content.stream().collect(Collectors.collectingAndThen(
//                        Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(c->c.getIdNo()))), ArrayList::new));
//        Page<CdcSuffererInfoDTO> pageList= new PageImpl<CdcSuffererInfoDTO>(list,pageRequest,list.size());
        return PageDto;
    }

    private Specification<CdcSuffererInfo> concatSpecification(CdcSuffererInfoPageDTO dto){
        Specification<CdcSuffererInfo> spec = new Specification<CdcSuffererInfo>(){

            @Override
            public Predicate toPredicate(Root<CdcSuffererInfo> root, CriteriaQuery<?> query,
                                         CriteriaBuilder cb) {
                List<Predicate> predicates = new ArrayList<>(); //所有的断言
                /*if(!CollectionUtils.isEmpty(sites)){
                    Predicate curSiteIdIn = cb.in(root.get("curSiteId")).value(sites);
                    predicates.add(curSiteIdIn);
                }*/

                if(dto != null){
                    if(org.apache.commons.lang.StringUtils.isNotEmpty(dto.getSuffererName())){
                        Predicate suffererNameLike = cb.like(root.get("suffererName"), "%"+dto.getSuffererName().trim()+"%");
                        predicates.add(suffererNameLike);
                    }
                    if(org.apache.commons.lang.StringUtils.isNotEmpty(dto.getSuffererCard())){
                        Predicate suffererCardLike = cb.like(root.get("idNo"), "%"+dto.getSuffererCard().trim()+"%");
                        predicates.add(suffererCardLike);
                    }
                    if(org.apache.commons.lang.StringUtils.isNotEmpty(dto.getSuffererTel())){
                        Predicate suffererTelLike = cb.like(root.get("telphone"), "%"+dto.getSuffererTel().trim()+"%");
                        predicates.add(suffererTelLike);
                    }
                    if(dto.getCaseType()!= null&&dto.getCaseType()!=0){
                        Predicate suffererTypeEquals = cb.equal(root.get("caseType"), dto.getCaseType());
                        predicates.add(suffererTypeEquals);
                    }

                    if(dto.getClinicalSeverity() != null&&dto.getClinicalSeverity()!=0){
                        Predicate suffererStatusEquals = cb.equal(root.get("clinicalSeverity"), dto.getClinicalSeverity());
                        predicates.add(suffererStatusEquals);
                    }
                    if(dto.getCurStatus()!=null&&dto.getCurStatus()!=0) {
                        if (1==dto.getCurStatus()) {
                            Predicate curStatus1 = cb.isNotNull(root.get("deadthDate"));
                            predicates.add(curStatus1);
                        } else if (2==dto.getCurStatus()) {
                            Predicate curStatus2 = cb.isNull(root.get("deadthDate"));
                            predicates.add(curStatus2);
                        }else if(3==dto.getCurStatus()){
                            Predicate curStatus3 = cb.equal(root.get("clinicalSeverity"), "98");
                            predicates.add(curStatus3);
                        }

                    }

                }
                return cb.and(predicates.toArray(new Predicate[predicates.size()]));
            }

        };
        return spec;
    }

    private List<CdcSuffererInfoAll> convertSuffererInfoToSufferInfoAll(List<CdcSuffererInfo> list) {
        List<CdcSuffererInfoAll> alls = new ArrayList<>();
        for (CdcSuffererInfo info : list) {
            CdcSuffererInfoAll all = new CdcSuffererInfoAll();
            BeanUtils.copyProperties(info, all);
            alls.add(all);
        }
        return alls;
    }




    @Override
    public void export(HttpServletResponse response, HttpServletRequest request) throws Exception {
        String currentDay = DateUtils.getCurrentDay(DateUtils.DATE_PATTERN_A);
        //读取模板
        Resource resource = new ClassPathResource("template/sufferer_info_template.xls");
        InputStream is = resource.getInputStream();
        Workbook workbook = ExcelUtils.getWorkbook(is);
        SheetExecutor<CdcSuffererInfoExportDTO> sheetExecutor = new SheetExecutor<>(CdcSuffererInfoExportDTO.class);
        //累计
        Sheet totalSheet = workbook.getSheetAt(0);
        Specification<CdcSuffererInfo> specTotal = Specification.where(CdcSuffererSpecifications.importDateEqual(currentDay));
        long countTotal = cdcSuffererInfoDao.count(specTotal);
        workbook.setSheetName(0,totalSheet.getSheetName()+String.valueOf(countTotal));
        List<CdcSuffererInfo> totalList = cdcSuffererInfoDao.findAll(specTotal);
        sheetExecutor.setSheet(totalSheet,toExportDTOList(totalList));
        //确诊
        Sheet diagnoseSheet = workbook.getSheetAt(1);
        Specification<CdcSuffererInfo> specDiagnose = Specification.where(CdcSuffererSpecifications.importDateEqual(currentDay))
                .and(CdcSuffererSpecifications.caseTypeIn(Arrays.asList(CaseTypeEnum.TYPE_LINCHUANG.getCode(),CaseTypeEnum.TYPE_QUEZHEN.getCode())));
        long countDiagnose = cdcSuffererInfoDao.count(specDiagnose);
        workbook.setSheetName(1,diagnoseSheet.getSheetName()+String.valueOf(countDiagnose));
        List<CdcSuffererInfo> diagnoseList = cdcSuffererInfoDao.findAll(specDiagnose);
        sheetExecutor.setSheet(diagnoseSheet,toExportDTOList(diagnoseList));
        //阳性
        Sheet testSheet = workbook.getSheetAt(2);
        Specification<CdcSuffererInfo> specTest = Specification.where(CdcSuffererSpecifications.importDateEqual(currentDay))
                .and(CdcSuffererSpecifications.caseTypeIn(Arrays.asList(CaseTypeEnum.TYPE_YANGXING.getCode())));
        long countTest = cdcSuffererInfoDao.count(specTest);
        workbook.setSheetName(2,testSheet.getSheetName()+String.valueOf(countTest));
        List<CdcSuffererInfo> testList = cdcSuffererInfoDao.findAll(specTest);
        sheetExecutor.setSheet(testSheet,toExportDTOList(testList));
        //疑似
        Sheet suspectSheet = workbook.getSheetAt(3);
        Specification<CdcSuffererInfo> specSuspect = Specification.where(CdcSuffererSpecifications.importDateEqual(currentDay))
                .and(CdcSuffererSpecifications.caseTypeIn(Arrays.asList(CaseTypeEnum.TYPE_YISHI.getCode())));
        long countSuspect = cdcSuffererInfoDao.count(specSuspect);
        workbook.setSheetName(3,suspectSheet.getSheetName()+String.valueOf(countSuspect));
        List<CdcSuffererInfo> suspectList = cdcSuffererInfoDao.findAll(specSuspect);
        sheetExecutor.setSheet(suspectSheet,toExportDTOList(suspectList));
        //死亡
        Sheet deathSheet = workbook.getSheetAt(4);
        Specification<CdcSuffererInfo> specDeath = Specification.where(CdcSuffererSpecifications.importDateEqual(currentDay))
                .and(CdcSuffererSpecifications.deathDateNotNull())
                .and(CdcSuffererSpecifications.deathDateNotEmpty());
        long countDeath = cdcSuffererInfoDao.count(specDeath);
        workbook.setSheetName(4,deathSheet.getSheetName()+String.valueOf(countDeath));
        List<CdcSuffererInfo> deathList = cdcSuffererInfoDao.findAll(specDeath);
        sheetExecutor.setSheet(deathSheet,toExportDTOList(deathList));
        //写出excel
        ExcelUtils.setExportExcel(response, request, "累计", true);
        ServletOutputStream outputStream = response.getOutputStream();
        outputStream.flush();
        workbook.write(outputStream);
        outputStream.close();
    }

    @Override
    public boolean existsById(String id) {
        return cdcSuffererInfoDao.existsById(id);
    }

    @Override
    public CdcSuffererInfoDTO findOne(String id) {
        Optional<CdcSuffererInfo> optional = cdcSuffererInfoDao.findById(id);
        if (optional.isPresent()) {
            CdcSuffererInfoDTO cdcSuffererInfoDTO = CdcSuffererInfoWrapper.toDTO(optional.get());

            cdcSuffererInfoDTO.setCurLocation(traceService.getLocation(cdcSuffererInfoDTO.getIdNo()));
            return cdcSuffererInfoDTO;
        } else {
            return CdcSuffererInfoDTO.builder().id(id).build();
        }
    }

    private List<CdcSuffererInfoExportDTO> toExportDTOList(List<CdcSuffererInfo> list) {
        List<CdcSuffererInfoExportDTO> exportDTOList = new ArrayList<>();
        for (CdcSuffererInfo cdcSuffererInfo : list) {
            CdcSuffererInfoExportDTO dto = new CdcSuffererInfoExportDTO();
            BeanUtils.copyProperties(cdcSuffererInfo,dto);
            if (StringUtils.isNotEmpty(cdcSuffererInfo.getCaseType())) {
                dto.setCaseTypeName(CaseTypeEnum.getNameByCode(cdcSuffererInfo.getCaseType()));
            }
            exportDTOList.add(dto);
        }
        return exportDTOList;
    }

}
