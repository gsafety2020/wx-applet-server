package com.gsafety.gemp.wxapplet.infection.service;

import com.gsafety.gemp.wxapplet.infection.contract.dto.CdcSuffererInfoDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.CdcSuffererInfoPageDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.PointHospitalInfoDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.PointHospitalInfoPageDTO;
import com.gsafety.gemp.wxapplet.infection.contract.iface.PointHospitalInfoService;
import com.gsafety.gemp.wxapplet.infection.dao.PointHospitalInfoDao;
import com.gsafety.gemp.wxapplet.infection.dao.po.CdcSuffererInfo;
import com.gsafety.gemp.wxapplet.infection.dao.po.PointHospitalInfoPO;
import com.gsafety.gemp.wxapplet.infection.wrapper.CdcSuffererInfoWrapper;
import com.gsafety.gemp.wxapplet.infection.wrapper.PointHospitalInfoWrapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.Transient;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PointHospitalInfoServiceImpl implements PointHospitalInfoService {

    @Resource
    PointHospitalInfoDao pointHospitalInfoDao;


    @Override
    public Page<PointHospitalInfoDTO> search(PointHospitalInfoPageDTO dto,PageRequest pageRequest) {
        Specification<PointHospitalInfoPO> spec = concatSpecification(dto);
        Page<PointHospitalInfoPO> poPage = pointHospitalInfoDao.findAll(spec, pageRequest);
        Page<PointHospitalInfoDTO> PageDto= PointHospitalInfoWrapper.toPageDTO(poPage);
        return PageDto;
    }

    @Override
    public PointHospitalInfoPO findOne(String id) {
        Optional<PointHospitalInfoPO> optional = pointHospitalInfoDao.findById(id);
        return optional.isPresent() ? optional.get() : null;
    }

    private Specification<PointHospitalInfoPO> concatSpecification(PointHospitalInfoPageDTO dto){
        Specification<PointHospitalInfoPO> spec = new Specification<PointHospitalInfoPO>(){

            @Override
            public Predicate toPredicate(Root<PointHospitalInfoPO> root, CriteriaQuery<?> query,
                                         CriteriaBuilder cb) {
                List<Predicate> predicates = new ArrayList<>(); //所有的断言
                /*if(!CollectionUtils.isEmpty(sites)){
                    Predicate curSiteIdIn = cb.in(root.get("curSiteId")).value(sites);
                    predicates.add(curSiteIdIn);
                }*/

                if(dto != null){
                    if(org.apache.commons.lang.StringUtils.isNotEmpty(dto.getSuffererName())){
                        Predicate suffererNameLike = cb.like(root.get("suffererName"), "%"+dto.getSuffererName().trim()+"%");
                        predicates.add(suffererNameLike);
                    }
                    if(org.apache.commons.lang.StringUtils.isNotEmpty(dto.getIdNo())){
                        Predicate suffererCardLike = cb.like(root.get("idNo"), "%"+dto.getIdNo().trim()+"%");
                        predicates.add(suffererCardLike);
                    }
                    if(org.apache.commons.lang.StringUtils.isNotEmpty(dto.getTelphone())){
                        Predicate suffererTelLike = cb.like(root.get("telphone"), "%"+dto.getTelphone().trim()+"%");
                        predicates.add(suffererTelLike);
                    }
                    if(StringUtils.isNotEmpty(dto.getSuffererType())&&!"0".equals(dto.getSuffererType())){
                        Predicate suffererTypeEquals = cb.equal(root.get("suffererType"), dto.getSuffererType());
                        predicates.add(suffererTypeEquals);
                    }

                    if(StringUtils.isNotEmpty(dto.getIllnessState())&&!"0".equals(dto.getIllnessState())){
                        Predicate suffererTypeEquals = cb.equal(root.get("illnessState"), dto.getIllnessState());
                        predicates.add(suffererTypeEquals);
                    }
                    if(StringUtils.isNotEmpty(dto.getSuffererStatus())&&!"0".equals(dto.getSuffererStatus())){
                        Predicate suffererTypeEquals = cb.equal(root.get("suffererStatus"), dto.getSuffererStatus());
                        predicates.add(suffererTypeEquals);
                    }

                }
                return cb.and(predicates.toArray(new Predicate[predicates.size()]));
            }

        };
        return spec;
    }
}
