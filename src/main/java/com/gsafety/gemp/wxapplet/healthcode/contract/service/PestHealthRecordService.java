package com.gsafety.gemp.wxapplet.healthcode.contract.service;

/**
 * 
* @ClassName: PestHealthRecordService 
* @Description: 打卡记录服务类
* @author luoxiao
* @date 2020年2月26日 下午5:25:33 
*
 */
public interface PestHealthRecordService {

	void saveChockInRecordInfo(String authorizeId,String healthCodeId);
}
