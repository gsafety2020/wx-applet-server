package com.gsafety.gemp.wxapplet.infection.contract.iface;

import java.util.Map;

/**
 * 
* @ClassName: SiteSuffererInfoService 
* @Description: 隔离点人员统计
* @author luoxiao
* @date 2020年3月7日 下午9:58:23 
*
 */
public interface SiteSuffererInfoService {

	public Map<String, Object> statisticIsolatedInfo();
}
