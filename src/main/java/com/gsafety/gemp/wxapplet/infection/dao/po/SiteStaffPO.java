package com.gsafety.gemp.wxapplet.infection.dao.po;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
* @ClassName: SiteStaffPO 
* @Description: 操作人员信息表
* @author luoxiao
* @date 2020年3月1日 下午2:48:55 
*
 */
@Entity
@Table(name = "site_staff")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SiteStaffPO implements Serializable{

	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name = "id")
	private String id;
	
	/**用户名*/
	@Column(name = "username")
	private String username;
	
	/**工作人员登录密码*/
	@Column(name = "pwd")
	private String pwd;
	
	/**小程序id*/
	@Column(name = "openid")
	private String openid;
	
	/**工作人员真实姓名*/
	@Column(name = "realname")
	private String realname;
	
	/**工作人员联系方式*/
	@Column(name = "phone")
	private String phone;
	
	/**身份证*/
	@Column(name = "id_card")
	private String idCard;
	
	/**隔离点群id,关联site_inffo表主键*/
	@Column(name = "site_id_group")
	private String siteIdGroup;
	
	/**创建人*/
	@Column(name = "create_by")
	private String createBy;
	
	/**更新人*/
	@Column(name = "update_by")
	private String updateBy;
	
	/**创建时间*/
	@Column(name = "create_time")
	private Date createTime;
	
	/**更新时间*/
	@Column(name = "update_time")
	private Date updateTime;

}
