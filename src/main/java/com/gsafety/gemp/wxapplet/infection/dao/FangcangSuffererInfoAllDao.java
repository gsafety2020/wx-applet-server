package com.gsafety.gemp.wxapplet.infection.dao;

import com.gsafety.gemp.wxapplet.infection.dao.po.FangcangSuffererInfoAllPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface FangcangSuffererInfoAllDao extends JpaRepository<FangcangSuffererInfoAllPO, String>,
        JpaSpecificationExecutor<FangcangSuffererInfoAllPO> {

    List<FangcangSuffererInfoAllPO> findByIdNoOrderByDataTimeDesc(String idNo);
}
