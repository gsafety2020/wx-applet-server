package com.gsafety.gemp.wxapplet.infection.dao;

import com.gsafety.gemp.wxapplet.infection.dao.po.CdcSuffererInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface CdcSuffererInfoDao extends JpaRepository<CdcSuffererInfo, String>, JpaSpecificationExecutor<CdcSuffererInfo> {

    List<CdcSuffererInfo> findCdcSuffererInfosByImportDate(String date);

    void deleteCdcSuffererInfoByImportDate(String date);


    List<CdcSuffererInfo> findAllByDataDate(String dataDate);

    List<CdcSuffererInfo> findCdcSuffererInfoByIdNoOrderByDataDateDesc(String idNo);

    CdcSuffererInfo findCdcSuffererInfoByIdNoAndDataDate(String idNo, String dataDate);

    /**
     * @param dataDate
     * @return 存在返回true
     * @method
     * @description 是否存在该日期的数据
     * @date: 2020/3/10 11:41
     * @author: zxiao
     */
    boolean existsByDataDate(String dataDate);

    List<CdcSuffererInfo> findBySuffererName(String suffererName);

    CdcSuffererInfo findCdcSuffererInfoByIdNo(String idNo);
}
