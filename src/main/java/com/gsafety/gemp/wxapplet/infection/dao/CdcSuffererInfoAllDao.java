package com.gsafety.gemp.wxapplet.infection.dao;

import com.gsafety.gemp.wxapplet.infection.dao.po.CdcSuffererInfo;
import com.gsafety.gemp.wxapplet.infection.dao.po.CdcSuffererInfoAll;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface CdcSuffererInfoAllDao extends JpaRepository<CdcSuffererInfoAll, String>, JpaSpecificationExecutor<CdcSuffererInfoAll> {

    void deleteCdcSuffererInfoAllByImportDate(String date);

    List<CdcSuffererInfoAll> findAllByDataDate(String dataDate);

    CdcSuffererInfoAll findCdcSuffererInfoByIdNoAndDataDate(String suffererIdNo, String dataDate);

    List<CdcSuffererInfoAll> findByIdNoOrderByDataDateDesc(String idNo);
}
