package com.gsafety.gemp.wxapplet.utils;

import java.io.InputStream;
import java.util.Properties;

/**
 *
* @ClassName: PropertiesUtils
* @Description: 读取properties工具类
* @author luoxiao
* @date 2020年2月29日 下午1:37:46
*
 */
public class PropertiesUtils {

	public static Properties loadProperties(String filePath){
		try{
			Properties property = new Properties();
			InputStream in = PropertiesUtils.class.getResourceAsStream(filePath);
            property.load(in);
            return property;
        } catch (Exception e) {
            throw new RuntimeException("配置文件读取失败!");
        }
	}

	public static String get(String filePath,String key) {
		Properties property = loadProperties(filePath);
        return property.getProperty(key);
    }

	public static Long getLong(String filePath,String key){
		String value = get(filePath,key);
		return null == value ? null : Long.valueOf(value);
	}
}
