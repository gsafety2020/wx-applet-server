package com.gsafety.gemp.wxapplet.infection.service.spec;

import com.gsafety.gemp.wxapplet.infection.dao.po.FangcangSuffererInfoPO;
import com.gsafety.gemp.wxapplet.utils.JpaPatternUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.jpa.domain.Specification;

/**
 * @author dusiwei
 */
public class FangcangSuffererInfoSpecifications {

    private FangcangSuffererInfoSpecifications() {
        super();
    }

    public static final String FIELD_SUFFERER_NAME = "suffererName";
    public static final String FIELD_ID_NO = "idNo";
    public static final String FIELD_TELEPHONE = "telephone";
    public static final String FIELD_SUFFERER_STATUS = "suffererStatus";
    public static final String FIELD_ADMITTED_TIME = "admittedTime";

    /**
     * 姓名
     * @param suffererName
     * @return
     */
    public static Specification<FangcangSuffererInfoPO> suffererNameLike(String suffererName) {
        if (StringUtils.isEmpty(suffererName)) {
            return null;
        }
        return (root, query, cb) -> cb.like(root.get(FIELD_SUFFERER_NAME),JpaPatternUtils.pattern(suffererName),JpaPatternUtils.ESCAPE);
    }

    /**
     * 身份证
     * @param idNo
     * @return
     */
    public static Specification<FangcangSuffererInfoPO> idNoLike(String idNo) {
        if (StringUtils.isEmpty(idNo)) {
            return null;
        }
        return (root, query, cb) -> cb.like(root.get(FIELD_ID_NO), JpaPatternUtils.pattern(idNo),JpaPatternUtils.ESCAPE);
    }

    /**
     * 电话
     * @param telephone
     * @return
     */
    public static Specification<FangcangSuffererInfoPO> telephoneLike(String telephone) {
        if (StringUtils.isEmpty(telephone)) {
            return null;
        }
        return (root, query, cb) -> cb.like(root.get(FIELD_TELEPHONE),JpaPatternUtils.pattern(telephone),JpaPatternUtils.ESCAPE);
    }

    /**
     * 状态
     * @param suffererStatus
     * @return
     */
    public static Specification<FangcangSuffererInfoPO> suffererStatusEqual(String suffererStatus) {
        if (StringUtils.isEmpty(suffererStatus)) {
            return null;
        }
        return (root, query, cb) -> cb.equal(root.get(FIELD_SUFFERER_STATUS),suffererStatus);
    }

    /**
     * 入院时间
     * @param admittedTime
     * @return
     */
    public static Specification<FangcangSuffererInfoPO> admittedTimeEqual(String admittedTime) {
        if (StringUtils.isEmpty(admittedTime)) {
            return null;
        }
        return (root, query, cb) -> cb.equal(root.get(FIELD_ADMITTED_TIME),admittedTime);
    }

}
