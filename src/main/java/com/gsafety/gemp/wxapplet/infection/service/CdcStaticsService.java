package com.gsafety.gemp.wxapplet.infection.service;

import com.alibaba.fastjson.JSONObject;
import com.gsafety.gemp.common.result.Result;
import com.gsafety.gemp.wxapplet.healthcode.contract.dto.CodeBasDistrictDTO;
import com.gsafety.gemp.wxapplet.infection.contract.common.SuffererCensusJx;
import com.gsafety.gemp.wxapplet.infection.contract.dto.*;
import com.gsafety.gemp.wxapplet.infection.contract.enums.IllnessStateEnum;
import com.gsafety.gemp.wxapplet.infection.contract.enums.SuffererStatusEnum;
import com.gsafety.gemp.wxapplet.infection.contract.enums.SuffererTypeEnum;
import com.gsafety.gemp.wxapplet.infection.contract.iface.SuffererInfoService;
import com.gsafety.gemp.wxapplet.infection.dao.CdcSuffererInfoAllDao;
import com.gsafety.gemp.wxapplet.infection.dao.CdcSuffererInfoDao;
import com.gsafety.gemp.wxapplet.infection.dao.po.CdcSuffererInfo;
import com.gsafety.gemp.wxapplet.infection.dao.po.CdcSuffererInfoAll;
import com.gsafety.gemp.wxapplet.infection.dao.po.SiteStaffPO;
import com.gsafety.gemp.wxapplet.infection.dao.po.SuffererInfoPO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author lzx
 * @version 1.0
 * @date 2020/3/5 22:09
 */
@Service
public class CdcStaticsService implements  SuffererInfoService {
    @Override
    public JSONObject analyseCDCSuffererData(String date) {
        List<CdcSuffererInfo> suffererInfos = cdcSuffererInfoDao.findAllByDataDate(date);
        List<CdcSuffererInfoAll> suffererInfoAlls = cdcSuffererInfoAllDao.findAllByDataDate(date);
        //以下参数全部初始化为0
        Integer confirm = 0, suspected = 0, dying = 0,
                icu = 0, grave = 0, clinical = 0, positive = 0,total = 0;
        JSONObject jsonObject = new JSONObject();
        if(!CollectionUtils.isEmpty(suffererInfos)){
            //累计确诊
            confirm = suffererInfos.stream().filter(a-> "2".equals(a.getCaseType())).collect(Collectors.toList()).size();
            //累计疑似
            suspected = suffererInfos.stream().filter(a->"4".equals(a.getCaseType())).collect(Collectors.toList()).size();
            //累计死亡
            dying = suffererInfos.stream().filter(a-> !StringUtils.isEmpty(a.getFinalJudgmentDeathDate())).collect(Collectors.toList()).size();
            //当前重症
            icu = suffererInfos.stream().filter(a-> "5".equals(a.getClinicalSeverity())).collect(Collectors.toList()).size();
            //当前危重
            grave = suffererInfos.stream().filter(a->"3".equals(a.getClinicalSeverity())).collect(Collectors.toList()).size();
            //临床检测
            clinical = suffererInfos.stream().filter(a->"1".equals(a.getCaseType())).collect(Collectors.toList()).size();
            //阳性检测
            positive = suffererInfos.stream().filter(a->"3".equals(a.getCaseType())).collect(Collectors.toList()).size();

            total = suffererInfos.size();
        } else if(!CollectionUtils.isEmpty(suffererInfoAlls)) {
            confirm = suffererInfoAlls.stream().filter(a-> "2".equals(a.getCaseType())).collect(Collectors.toList()).size();
            suspected = suffererInfoAlls.stream().filter(a->"4".equals(a.getCaseType())).collect(Collectors.toList()).size();
            dying = suffererInfoAlls.stream().filter(a-> !StringUtils.isEmpty(a.getFinalJudgmentDeathDate())).collect(Collectors.toList()).size();
            icu = suffererInfoAlls.stream().filter(a-> "5".equals(a.getClinicalSeverity())).collect(Collectors.toList()).size();
            grave = suffererInfoAlls.stream().filter(a->"3".equals(a.getClinicalSeverity())).collect(Collectors.toList()).size();
            clinical = suffererInfoAlls.stream().filter(a->"1".equals(a.getCaseType())).collect(Collectors.toList()).size();
            positive = suffererInfoAlls.stream().filter(a->"3".equals(a.getCaseType())).collect(Collectors.toList()).size();
            total = suffererInfoAlls.size();
        }


        jsonObject.put("total",total);
        jsonObject.put("confirm", confirm + clinical);
        jsonObject.put("suspected", suspected);
        jsonObject.put("dying", dying);
        jsonObject.put("icu", icu);
        jsonObject.put("grave", grave);
        jsonObject.put("clinical",clinical);
        jsonObject.put("positive",positive);
        return jsonObject;

    }

    @Override
    public JSONObject analyseCDCSuffererToday() {
        return null;
    }

    @Override
    public SuffererCensusJx analyseJxSuffererToday(String totalDate) {
        return null;
    }

    @Override
    public Page<SuffererInfoDTO> getSuffererInfoTodayBySuffererType(Integer suffererType, PageRequest pageRequest) {
        return null;
    }

    @Override
    public Page<SuffererInfoDTO> getJxSuffererInfoTodayBySuffererType(Integer suffererType, Pageable pageable,String totalDate) {
        return null;
    }

    @Override
    public Page<SuffererInfoDTO> getSuffererInfoTodayByIllnessState(Integer illnessState, PageRequest pageRequest) {
        return null;
    }

    @Override
    public Page<SuffererInfoDTO> getSuffererInfoTodayBySuffererStatus(Integer suffererStatus, PageRequest pageRequest) {
        return null;
    }

    @Override
    public SuffererCensusJx analyseJxSuffererData() {
        return null;
    }

    @Override
    public void export(SuffererInfoWebPageDTO dto, HttpServletResponse response, HttpServletRequest request) throws Exception {

    }

    @Override
    public Result<String> importExcel(MultipartFile file) {
        return null;
    }

    @Override
    public void exportStat(HttpServletResponse response, HttpServletRequest request,String totalDate) {

    }

    @Override
    public List<SuffererInfoPO> findAll() {
        return null;
    }

    @Override
    public void saveAll(List<SuffererInfoPO> all) {

    }

    @Override
    public List<SuffererIsolationStatisticsDTO> isolatetionStatistics(Date time) {
        return null;
    }


    @Autowired
    private CdcSuffererInfoDao cdcSuffererInfoDao;

    @Autowired
    private CdcSuffererInfoAllDao cdcSuffererInfoAllDao;


    @Override
    public Page<SuffererBaseDTO> searchSuffererInfo(String staffId, String key, PageRequest pageRequest) {
        return null;
    }

    @Override
    public Page<SuffererInfoDTO> searchWebSuffererInfo(String staffId, SuffererInfoWebPageDTO dto, PageRequest pageRequest) {
        return null;
    }

    @Override
    public SuffererDetailDTO getSuffererInfoById(String id) {
        return null;
    }

    @Override
    public SuffererInfoDTO getSuffererWebById(String id) {
        return null;
    }

    @Override
    public SuffererDetailDTO updateSuffererStatus(SuffererEditDTO dto) {
        return null;
    }

    @Override
    public List<SiteInfoSelectDTO> getSiteInfoSelectByStaff(SiteStaffPO staffPo) {
        return null;
    }

    @Override
    public SuffererDetailDTO updateSuffererType(SuffererTypeUpdateDTO dto) {
        return null;
    }

    @Override
    public SuffererDetailDTO updateSuffererStatus(SuffererStatusUpdateDTO dto) {
        return null;
    }

    @Override
    public SuffererDetailDTO updateillnessState(IllnessStateUpdateDTO dto) {
        return null;
    }

    @Override
    public List<CodeBasDistrictDTO> getCodeBaseDistrictByParentCode() {
        return null;
    }

    @Override
    public SuffererDetailDTO saveSuffererInfo(SuffererInfoPO po) {
        return null;
    }

    @Override
    public SuffererWebDetailDTO updateSuffererInfo(SuffererInfoPO po) {
        return null;
    }

    @Override
    public String checkSuffererInfoExists(String suffererCard) {
        return null;
    }

}
