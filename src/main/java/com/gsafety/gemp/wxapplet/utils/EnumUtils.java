package com.gsafety.gemp.wxapplet.utils;

/**
 * @author Administrator
 * @Title: EnumUtils
 * @ProjectName gemp
 * @Description: TODO
 * @date 2020/2/2813:32
 */
public class EnumUtils {
    public static <T extends StringCodeEnum> T getEnumByCode(Integer code, Class<T> enumClass) {
        for (T each : enumClass.getEnumConstants()) {
            if(code.equals(Integer.valueOf(each.getCode()))){
                return  each;
            }
        }

        return null;
    }

    public static <T extends IntegerCodeEnum> T getEnumByIntegerCode(Integer code, Class<T> enumClass) {
        for (T each : enumClass.getEnumConstants()) {
            if(code.equals(Integer.valueOf(each.getCode()))){
                return  each;
            }
        }

        return null;
    }
}
