package com.gsafety.gemp.wxapplet.infection.contract.params;


import com.gsafety.gemp.wxapplet.infection.contract.common.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description="病患信息查询对象")
public class SuffererQueryParam extends PageReq implements Serializable {

    @ApiModelProperty(value = "病人类型：1.密接人员；2.发热患者；3.疑似患者；4.确诊; 99.其他;")
    private Integer suffererType;
    @ApiModelProperty(value = "病患状态：1.隔离点在管（进入）；2.在院治疗（进入）；3.治愈出院（离开）；4.病亡（离开）；5.转出（离开）；99.其他；")
    private Integer suffererStatus;
    @ApiModelProperty(value = "病情. 1.危重；2重症；3.其他")
    private Integer illnessState;
    @ApiModelProperty(value = "患者姓名")
    private String name;
    @DateTimeFormat(pattern="yyyy-MM-dd 00:00:00")
    @ApiModelProperty(value = "开始时间")
    private Date startTime;
    @DateTimeFormat(pattern="yyyy-MM-dd 23:59:59")
    @ApiModelProperty(value = "结束时间")
    private Date endTime;

}
