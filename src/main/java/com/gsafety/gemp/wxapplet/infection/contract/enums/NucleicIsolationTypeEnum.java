package com.gsafety.gemp.wxapplet.infection.contract.enums;

import com.gsafety.gemp.wxapplet.infection.contract.dto.PointCommonDTO;

import java.util.ArrayList;
import java.util.List;

public enum  NucleicIsolationTypeEnum {

    ISOLATION_TYPE__0("0","全部"),
    ISOLATION_TYPE__1("1","发热门诊隔离点"),
    ISOLATION_TYPE__2("2","发热病人隔离点"),
    ISOLATION_TYPE__3("3","密切接触者隔离点"),
    ISOLATION_TYPE__4("4","区人民医院"),
    ISOLATION_TYPE__5("5","区中医院");

    private String code;
    private String name;
    NucleicIsolationTypeEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public String getCode(){
        return this.code;
    }

    public static String getNameByCode(String code) {
        for (NucleicIsolationTypeEnum dataTypeEnum : values()) {
            if (dataTypeEnum.code.equals(code)){
                return dataTypeEnum.name;
            }
        }
        return null;
    }

    public static List<PointCommonDTO> getList(){
        List<PointCommonDTO> list=new ArrayList<>();
        PointCommonDTO pointCommonDTO=null;
        for (NucleicIsolationTypeEnum commonEnum : values()) {
            PointCommonDTO commonDTO=new PointCommonDTO();
            commonDTO.setCode(commonEnum.code);
            commonDTO.setName(commonEnum.name);
            list.add(commonDTO);
        }
        return list;
    }
}
