package com.gsafety.gemp.wxapplet.infection.service;

import com.gsafety.gemp.wxapplet.infection.contract.dto.CloseContactInfoDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.CloseContactTreeDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.SuffererInfoDetailDTO;
import com.gsafety.gemp.wxapplet.infection.contract.enums.CaseTypeEnum;
import com.gsafety.gemp.wxapplet.infection.contract.enums.CaseTypeStyleEnum;
import com.gsafety.gemp.wxapplet.infection.contract.iface.CloseContactInfoService;
import com.gsafety.gemp.wxapplet.infection.contract.iface.SuffererInfoDetailService;
import com.gsafety.gemp.wxapplet.infection.contract.iface.TraceService;
import com.gsafety.gemp.wxapplet.infection.contract.params.CloseContactInfoSearchParam;
import com.gsafety.gemp.wxapplet.infection.contract.params.SuperSpreaderSearchParam;
import com.gsafety.gemp.wxapplet.infection.dao.*;
import com.gsafety.gemp.wxapplet.infection.dao.po.*;

import static com.gsafety.gemp.wxapplet.infection.wrapper.CloseContactInfoWrapper.*;

import com.gsafety.gemp.wxapplet.infection.service.spec.CdcSuffererSpecifications;
import io.netty.util.internal.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.Predicate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 密接人员
 * @author fanlx
 */
@Service
public class CloseContactInfoServiceImpl implements CloseContactInfoService {

    @Autowired
    private CloseContactInfoDao closeContactInfoDao;

    @Autowired
    private CdcSuffererInfoDao cdcSuffererInfoDao;


    @Autowired
    private CdcSuffererInfoAllDao cdcSuffererInfoAllDao;

    @Autowired
    private FangcangSuffererInfoDao fangcangSuffererInfoDao;

    @Autowired
    private SuffererInfoDao suffererInfoDao;

    @Autowired
    private PointHospitalInfoDao pointHospitalInfoDao;

    @Autowired
    private FeverClinicsInfoDao feverClinicsInfoDao;

    @Autowired
    private SuffererInfoDetailService suffererInfoDetailService;

    @Autowired
    private TraceService traceService;


    /**
     * 获取当前身份证用户获取网状列表数据
     * @return
     */
    @Override
    public List<CloseContactTreeDTO>  findNetList(String idCard){
        // 最终网状树
        List<CloseContactTreeDTO>  lastList = new ArrayList<>();
        // 原始数据
        List<CloseContactInfoDTO>  orginList = new ArrayList<>();

        /*防止出现死循环*/
        List<String>  currentList = new ArrayList<>();

        //没有身份证密接着排后面
        Sort sort = Sort.by(Sort.Direction.DESC, "ccIdno");
        //去掉身份证为空的数据
        Specification<CloseContactInfo> specTotal = Specification.where(idCardNotNull());

        List<CloseContactInfo>   orginInfo = closeContactInfoDao.findAll(specTotal,sort);
        orginInfo.stream().forEach(record -> orginList.add(toDTO(record)));

        //根据身份证获取患者信息
        CdcSuffererInfo cdcSuffererInfo = cdcSuffererInfoDao.findCdcSuffererInfoByIdNo(idCard);
        //传入idcard加入集合避免死循环
        currentList.add(idCard);
        //根据传入的身份证构建树
        CloseContactTreeDTO  treeDTO = new CloseContactTreeDTO();
        String id = UUID.randomUUID().toString().replace("-", "");
        treeDTO.setId(id);
        treeDTO.setSuffererName(cdcSuffererInfo.getSuffererName());
        treeDTO.setSuffererIdno(cdcSuffererInfo.getIdNo());
        treeDTO.setSuffererType(cdcSuffererInfo.getCaseType());
        treeDTO.setBackGroundColor(CaseTypeStyleEnum.getNameByCode(treeDTO.getSuffererType()));
        treeDTO.setRoot(true);
        lastList.add(treeDTO);
        getChildList(idCard,id,orginList,lastList,currentList);
        //第一个人的数构造完了应该移除全局表避免构造数据丢失
        currentList.clear();

        return lastList;
    }

    /**
     *
     * @param idNo 身份证
     * @param parentId  父节点id
     * @param orgList 全量数据
     * @param finallist 结果列表
     * @param currentList 全局变量，用于避免死循环
     */
    private void getChildList(String idNo ,String parentId,  List<CloseContactInfoDTO> orgList,List<CloseContactTreeDTO>  finallist, List<String> currentList){

        //获取当前患者密接人员
        List<CloseContactInfoDTO> list = orgList.stream().
                filter(item -> !StringUtils.isEmpty(item.getSuffererIdno()) && item.getSuffererIdno().equals(idNo)).collect(Collectors.toList());

        for (CloseContactInfoDTO item : list) {
            String id = UUID.randomUUID().toString().replace("-", "");
            CloseContactTreeDTO treeDTO = new CloseContactTreeDTO();

            if(currentList.contains(item.getCcIdno())){
                continue;
            }
            // 如果密接人员放在递归之后加入集合会死循环
            if(!StringUtils.isEmpty(item.getCcIdno())){
                currentList.add(item.getSuffererIdno());
            }

            //如果当前密接著的身份证为空，不用往下面查找直接加入集合,身份证不为空且不在集合内往下面找
            if (!StringUtils.isEmpty(item.getCcIdno()) && !currentList.contains(item.getCcIdno())) {
                getChildList(item.getCcIdno(), id, orgList, finallist, currentList);
            }
            treeDTO.setId(id);
            treeDTO.setSuffererName(item.getCcName());
            treeDTO.setSuffererIdno(item.getCcIdno());
            //设置密接者病患类型
            if (StringUtil.isNullOrEmpty(item.getCcIdno())) {
                // 没有身份证的为密接人员
                treeDTO.setSuffererType("0");
            } else {
                treeDTO.setSuffererType(getSuffererCastType(item.getCcIdno()));
            }
            treeDTO.setBackGroundColor(CaseTypeStyleEnum.getNameByCode(treeDTO.getSuffererType()));
            treeDTO.setParentId(parentId);
            finallist.add(treeDTO);

        }
    }

    private String getSuffererCastType(String idno){
        CdcSuffererInfo cdcSuffererInfo = cdcSuffererInfoDao.findCdcSuffererInfoByIdNo(idno);
        if(cdcSuffererInfo != null ){
            return cdcSuffererInfo.getCaseType();
        }

        List<CdcSuffererInfoAll> list = cdcSuffererInfoAllDao.findByIdNoOrderByDataDateDesc(idno);
        if(!CollectionUtils.isEmpty(list)){
            return list.get(0).getCaseType();
        }
        return null;
    }

    public  Specification<CloseContactInfo> idCardNotNull() {
        return (root, query, cb) -> root.get("suffererIdno").isNotNull();
    }
    /**
     * 分页查询
     * @param param
     * @return  as
     */
    @Override
    public PageImpl findPage(CloseContactInfoSearchParam param) {

        PageRequest pageRequest = PageRequest.of(param.getNowPage(), param.getPageSize(),Sort.by(Sort.Order.desc("ccIdno")));

        Page<CloseContactInfo> page = closeContactInfoDao.findAll(concatSpecification(param.getSuffererIdNo()), pageRequest);
        List<CloseContactInfoDTO> listDto = new ArrayList<>();
        for (CloseContactInfo contactInfo : page.getContent()) {
            CloseContactInfoDTO infoDTO = toDTO(contactInfo);
            if(!StringUtils.isEmpty(infoDTO.getCcIdno())){
                infoDTO.setCcCurLocation(traceService.getLocation(infoDTO.getCcIdno()));
            }
            listDto.add(infoDTO);
        }

        return new PageImpl<>(listDto, pageRequest, page.getTotalElements());
    }

    @Override
    public void syncCurLocation(String dataDate) {
        List<CloseContactInfo> closeContacts = closeContactInfoDao.findAllByDataDate(dataDate);
        for (CloseContactInfo po : closeContacts) {

            List<SuffererInfoPO> isolations = suffererInfoDao.findBySuffererNameOrderByCreateTimeDesc(po.getCcName());
            if(!CollectionUtils.isEmpty(isolations)){
                po.setCcCurLocation(isolations.get(0).getCurSiteName());
                this.closeContactInfoDao.save(po);
                continue;
            }

            List<PointHospitalInfoPO> pointHospitals = pointHospitalInfoDao.findBySuffererNameOrderByCreateTimeDesc(po.getCcName());
            if(!CollectionUtils.isEmpty(pointHospitals)){
                po.setCcCurLocation("江夏区第一人民医院");
                this.closeContactInfoDao.save(po);
                continue;
            }

            List<FangcangSuffererInfoPO> fangCangs = fangcangSuffererInfoDao.findBySuffererNameOrderByCreateTimeDesc(po.getCcName());
            if(!CollectionUtils.isEmpty(fangCangs)){
                po.setCcCurLocation("江夏区方舱医院");
                this.closeContactInfoDao.save(po);
                continue;
            }


        }

    }

    /**
     * 超级传播者分页查询
     * @param param
     * @return
     */
    @Override
    public Page<SuffererInfoDetailDTO> findSuperSpreader(SuperSpreaderSearchParam param){
        List<SuffererInfoDetailDTO> responseList = new ArrayList<>();
        Pageable pageable = PageRequest.of(param.getNowPage(), param.getPageSize());

        Page<String> page = closeContactInfoDao.findSuperSpreaderList(pageable);

        for (String idno : page.getContent()) {
            responseList.add(suffererInfoDetailService.findSuffererDetail(idno));
        }
        return new PageImpl<>(responseList,PageRequest.of(param.getNowPage(),param.getPageSize()),page.getTotalElements());
    }




    private Specification<CloseContactInfo> concatSpecification(String idNo ){
        return  (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            if(!StringUtils.isEmpty(idNo)) {
                predicates.add(cb.equal(root.get("suffererIdno"), idNo));
            }

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

    /**
     * 生成密接人员身份证信息
     */
    @Override
    public void genrateCloseContactIdNo() {
        List<CloseContactInfo> all = this.closeContactInfoDao.findAll();
        for (CloseContactInfo ccInfo : all) {
            if(StringUtils.isEmpty(ccInfo.getCcIdno())) {

                if(getIdnoFromCdc(ccInfo)){
                    this.closeContactInfoDao.save(ccInfo);
//                    continue;
                }

//                if(getIdnoFromPoint(ccInfo)){
//                    this.closeContactInfoDao.save(ccInfo);
//                    continue;
//                }
//
//                if(getIdnoFromSite(ccInfo)){
//                    this.closeContactInfoDao.save(ccInfo);
//                    continue;
//                }
//                if(getIdnoFromFangCang(ccInfo)){
//                    this.closeContactInfoDao.save(ccInfo);
//                    continue;
//                }
//                if(getIdnoFromFever(ccInfo)){
//                    this.closeContactInfoDao.save(ccInfo);
//                }

            }
        }
    }

    private boolean getIdnoFromCdc(CloseContactInfo ccInfo){
        //疫情网
        List<CdcSuffererInfo> cdcSuffererInfos = cdcSuffererInfoDao.findBySuffererName(ccInfo.getCcName());
        if(cdcSuffererInfos.size() == 1){
            ccInfo.setCcIdno(cdcSuffererInfos.get(0).getIdNo());
            return true;
        }
        if(cdcSuffererInfos.size() > 1){
            for (CdcSuffererInfo suffererInfo : cdcSuffererInfos) {
                if(!StringUtils.isEmpty(suffererInfo.getTelphone()) && suffererInfo.getTelphone().equals(ccInfo.getCcTelphone())){
                    ccInfo.setCcIdno(suffererInfo.getIdNo());
                    return true;
                }
            }
        }
        return false;
    }

    private boolean getIdnoFromPoint(CloseContactInfo ccInfo){
        List<PointHospitalInfoPO> pointInfoPOS = this.pointHospitalInfoDao.findBySuffererName(ccInfo.getCcName());
        if(pointInfoPOS.size() == 1){
            ccInfo.setCcIdno(pointInfoPOS.get(0).getIdNo());
            return true;
        }


        if(pointInfoPOS.size() > 1 ){
            for (PointHospitalInfoPO pointInfoPO : pointInfoPOS) {
                if(!StringUtils.isEmpty(pointInfoPO.getTelphone()) && pointInfoPO.getTelphone().equals(ccInfo.getCcTelphone())){
                    ccInfo.setCcIdno(pointInfoPO.getIdNo());
                    return true;
                }
            }
        }
        return false;
    }

    private boolean getIdnoFromSite(CloseContactInfo ccInfo){
        List<SuffererInfoPO> suffererInfoPOS = suffererInfoDao.findBySuffererNameOrderByCreateTimeDesc(ccInfo.getCcName());
        if(suffererInfoPOS.size() == 1 ){
            ccInfo.setCcIdno(suffererInfoPOS.get(0).getSuffererCard());
            return true;
        }
        if(suffererInfoPOS.size() > 1){
            for (SuffererInfoPO infoPO : suffererInfoPOS) {
                if(!StringUtils.isEmpty(infoPO.getSuffererTel()) && infoPO.getSuffererTel().equals(ccInfo.getCcTelphone())){
                    ccInfo.setCcIdno(infoPO.getSuffererCard());
                    return true;
                }
            }
        }
        return false;
    }

    private boolean getIdnoFromFangCang(CloseContactInfo ccInfo){
        List<FangcangSuffererInfoPO> fangcangInfos = this.fangcangSuffererInfoDao.findBySuffererNameOrderByCreateTimeDesc(ccInfo.getCcName());
        if(fangcangInfos.size() == 1){
            ccInfo.setCcIdno(fangcangInfos.get(0).getIdNo());
            return true;
        }

        if(fangcangInfos.size() > 1){
            for (FangcangSuffererInfoPO info : fangcangInfos) {
                if(!StringUtils.isEmpty(info.getTelephone()) && info.getTelephone().equals(ccInfo.getCcTelphone())){
                    ccInfo.setCcIdno(info.getIdNo());
                    return true;
                }
            }
        }
        return false;
    }

    private boolean getIdnoFromFever(CloseContactInfo ccInfo){
        List<FeverClinicsInfoPO> feverInfos = this.feverClinicsInfoDao.findBySuffererName(ccInfo.getCcName());
        if(feverInfos.size() == 1){
            ccInfo.setCcIdno(feverInfos.get(0).getIdNo());
            return true;
        }
        if(feverInfos.size() > 1){
            for (FeverClinicsInfoPO feverInfo : feverInfos) {
                if(!StringUtils.isEmpty(feverInfo.getTelphone()) && feverInfo.getTelphone().equals(ccInfo.getCcTelphone())){
                    ccInfo.setCcIdno(feverInfo.getIdNo());
                    return true;
                }
            }
        }
        return  false;
    }


}
