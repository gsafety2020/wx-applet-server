package com.gsafety.gemp.wxapplet.infection.service;

import cn.hutool.core.collection.CollectionUtil;
import com.gsafety.gemp.wxapplet.healthcode.contract.dto.Result;
import com.gsafety.gemp.wxapplet.infection.contract.enums.CaseTypeEnum;
import com.gsafety.gemp.wxapplet.infection.contract.enums.CdcTraceTypeEnum;
import com.gsafety.gemp.wxapplet.infection.contract.iface.TraceService;
import com.gsafety.gemp.wxapplet.infection.dao.*;
import com.gsafety.gemp.wxapplet.infection.dao.po.*;
import com.gsafety.gemp.wxapplet.utils.DateUtils;
import com.gsafety.gemp.wxapplet.utils.UUIDUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.gsafety.gemp.wxapplet.infection.contract.enums.PointSuffererStatusEnum.*;
import static com.gsafety.gemp.wxapplet.utils.DateUtils.DATE_PATTERN;


/**
 * 数据流水生成
 *
 * @author fanlx
 */
@Service
public class TraceServiceImpl implements TraceService {

    @Autowired
    private CdcSuffererInfoDao cdcSuffererInfoDao;
    @Autowired
    private CdcSuffererInfoAllDao cdcSuffererInfoAllDao;
    @Autowired
    private CdcSuffererTraceDao dao;
    @Autowired
    private FangcangSuffererInfoDao fangcangSuffererInfoDao;

    @Autowired
    private PointHospitalInfoDao pointHospitalInfoDao;

    @Autowired
    private SuffererInfoDao suffererInfoDao;

    @Autowired
    private FeverClinicsInfoDao feverClinicsInfoDao;

    private static final String FANG_CANG_NAME = "江夏区方舱医院";


    @Override
    @Transactional
    public Result<Object> saveTraceSuffererInfoData(String dataDate) {
        //查询参数时间对应表的数据
        List<CdcSuffererInfo> cdcSuffererInfoList = cdcSuffererInfoDao.findAllByDataDate(dataDate);
        if (CollectionUtil.isEmpty(cdcSuffererInfoList)) {
            return Result.builder().build().success("请导入当日数据");
        }

        String prevDate = getPrevDate(dataDate);
        List<CdcSuffererInfoAll> cdcSuffererInfoAllList = cdcSuffererInfoAllDao.findAllByDataDate(prevDate);
        if (CollectionUtil.isEmpty(cdcSuffererInfoAllList)) {
            return Result.builder().build().success("未找到昨日历史数据");
        }

        this.dao.deleteByRemarkAndSourceType(dataDate, "3");

        List<CdcSuffererTrace> traceList = new ArrayList<>();

        for (CdcSuffererInfo today : cdcSuffererInfoList) {

            String todayIdNo = today.getIdNo();

            boolean isNew = true;

            boolean b = needInit(today);

            if (isSiwang(today)) {
                traceList.add(buildCdcTracePo(today.getIdNo(), today.getSuffererName(),
                        today.getFinalJudgmentDeathDate(), today.getDataDate(), "5"));
                continue;
            }
            if (isLinchuang(today)) {
                traceList.add(buildCdcTracePo(today.getIdNo(), today.getSuffererName(),
                        today.getRevisionFinalJudgmentDate(), today.getDataDate(), today.getCaseType()));
                continue;
            }
            if (isYisi(today)) {
                traceList.add(buildCdcTracePo(today.getIdNo(), today.getSuffererName(),
                        today.getRevisionFinalJudgmentDate(), today.getDataDate(), today.getCaseType()));
                continue;
            }
            if (isQuezhen(today)) {
                traceList.add(buildCdcTracePo(today.getIdNo(), today.getSuffererName(),
                        today.getRevisionFinalJudgmentDate(), today.getDataDate(), today.getCaseType()));
                continue;
            }
            if (isYangxing(today)) {
                traceList.add(buildCdcTracePo(today.getIdNo(), today.getSuffererName(),
                        today.getRevisionFinalJudgmentDate(), today.getDataDate(), today.getCaseType()));
            }

            if(StringUtils.isEmpty(todayIdNo)){
                continue;
            }

            for (CdcSuffererInfoAll tomorrow : cdcSuffererInfoAllList) {
                String tomorrowIdNo = tomorrow.getIdNo();
                if (todayIdNo.equals(tomorrowIdNo)) {
                    isNew = false;
                    if (b) {
                        traceList.addAll(initTracePo(tomorrow));
                    }
                }
            }
            if(isNew){
                traceList.add(buildCdcTracePo(today.getIdNo(), today.getSuffererName(), today.getOnsetDate(), today.getDataDate(), "8"));
            }
        }
        dao.saveAll(traceList);
        return Result.builder().build().success("数据对比成功，数据流水已生成");
    }

    @Override
    public void generateFangCangTrace(String dataDate) {
        List<FangcangSuffererInfoPO> list = fangcangSuffererInfoDao.findByDataTime(dataDate);

        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        List<CdcSuffererTrace> traceList = new ArrayList<>();
        for (FangcangSuffererInfoPO infoPO : list) {

            this.dao.deleteBySourceTypeAndSuffererIdNo("1", infoPO.getIdNo());
            //入院
            traceList.add(buildFCTracePo(infoPO.getIdNo(), infoPO.getSuffererName(), infoPO.getAdmittedTime(),null, "6"));

            //出院
            if (StringUtils.isNotEmpty(infoPO.getLeaveDate())) {
                traceList.add(buildFCTracePo(infoPO.getIdNo(), infoPO.getSuffererName(), infoPO.getLeaveDate(),null, "9"));
            }

            //转院
            if (StringUtils.isNotEmpty(infoPO.getTransferDate())) {
                traceList.add(buildFCTracePo(infoPO.getIdNo(), infoPO.getSuffererName(), infoPO.getTransferDate(),infoPO.getTransferHospital(), "7"));
            }
        }

        this.dao.saveAll(traceList);
    }

    @Override
    public void generatePointTrace(String dataDate) {
        List<PointHospitalInfoPO> list = pointHospitalInfoDao.findByDataDate(dataDate);

        if (CollectionUtils.isEmpty(list)) {
            return;
        }

        List<CdcSuffererTrace> traceList = new ArrayList<>();

        for (PointHospitalInfoPO infoPo : list) {

            this.dao.deleteBySourceTypeAndSuffererIdNo("2", infoPo.getIdNo());

            //入院
            traceList.add(buildPointTracePo(infoPo.getIdNo(),infoPo.getHospitalName(), infoPo.getSuffererName(),
                    DateUtils.format(infoPo.getMoveIntoTime(), DATE_PATTERN), "6"));

            //出院
            if (infoPo.getDischargeTime() != null) {
                if (OUT.getCode().equals(infoPo.getSuffererStatus())) {
                    traceList.add(buildPointTracePo(infoPo.getIdNo(),infoPo.getHospitalName() , infoPo.getSuffererName(),
                            DateUtils.format(infoPo.getDischargeTime(), DATE_PATTERN), "9"));
                    continue;
                }
                if (TURN_OUT.getCode().equals(infoPo.getSuffererStatus())) {
                    traceList.add(buildPointTracePo(infoPo.getIdNo(),infoPo.getHospitalName(), infoPo.getSuffererName(),
                            DateUtils.format(infoPo.getDischargeTime(), DATE_PATTERN), "7"));
                    continue;
                }

            }

            if(infoPo.getDeathDate() != null){
                this.dao.deleteByChangeTypeAndSuffererIdNo("5",infoPo.getIdNo());
                traceList.add(buildPointTracePo(infoPo.getIdNo(),infoPo.getHospitalName() , infoPo.getSuffererName(),
                        DateUtils.format(infoPo.getDischargeTime(), DATE_PATTERN), "5"));
            }
        }

        this.dao.saveAll(traceList);
    }

    @Override
    public void generateSiteTrace(String dataDate) {
        List<SuffererInfoPO> list = this.suffererInfoDao.findByDataDate(dataDate);
        if (CollectionUtils.isEmpty(list)) {
            return;
        }

        List<CdcSuffererTrace> traceList = new ArrayList<>();
        for (SuffererInfoPO infoPO : list) {
            if (StringUtils.isNotEmpty(infoPO.getSuffererCard())) {

                //入院
                if(infoPO.getMoveIntoTime() != null){
                    deleteSiteTrace(infoPO.getSuffererCard(),
                            infoPO.getCurSiteName(),
                            DateUtils.format(infoPO.getMoveIntoTime(), DATE_PATTERN),
                            "6"
                    );
                    traceList.add(buildSiteTracePo(infoPO.getSuffererCard(), infoPO.getCurSiteName(), infoPO.getSuffererName(),
                            DateUtils.format(infoPO.getMoveIntoTime(), DATE_PATTERN), infoPO.getCurSiteId(), "6"));
                }

                //转出
                if(infoPO.getTurnOutTime() != null){
                    deleteSiteTrace(infoPO.getSuffererCard(),
                            infoPO.getCurSiteName(),
                            DateUtils.format(infoPO.getTurnOutTime(), DATE_PATTERN),
                            "7"
                    );
                    traceList.add(buildSiteTracePo(infoPO.getSuffererCard(), infoPO.getCurSiteName(), infoPO.getSuffererName(),
                            DateUtils.format(infoPO.getTurnOutTime(), DATE_PATTERN), infoPO.getCurSiteId(), "7"));
                }


            }

        }
        this.dao.saveAll(traceList);

    }

    private void deleteSiteTrace(String idno,String siteName,String changeTime,String changeType){
        this.dao.deleteAllBySourceTypeAndSuffererIdNoAndHospitalNameAndChangeTimeAndChangeType("4",idno,siteName,changeTime,changeType);
    }

    @Override
    public void genrateFeverTrace(String dataDate) {
        List<FeverClinicsInfoPO> list = feverClinicsInfoDao.findByDataDate(dataDate);
        if (CollectionUtils.isEmpty(list)) {
            return;
        }

        List<CdcSuffererTrace> traceList = new ArrayList<>();
        for (FeverClinicsInfoPO infoPO : list) {
            if(StringUtils.isNotEmpty(infoPO.getIdNo())){
                this.dao.deleteByChangeTimeAndSourceTypeAndSuffererIdNo(
                        DateUtils.format(infoPO.getConsultationTime(),DateUtils.DATE_PATTERN),"5",infoPO.getIdNo());
                if(infoPO.getConsultationTime() != null){
                    traceList.add(buildFeverTracePo(infoPO.getIdNo(),infoPO.getHospital(),infoPO.getSuffererName(),
                            DateUtils.format(infoPO.getConsultationTime(),DateUtils.DATE_PATTERN),"10"));

                }


            }
        }
        this.dao.saveAll(traceList);
    }
    private CdcSuffererTrace buildFeverTracePo(String idNo,String locationName, String suffererName, String changeDate, String type) {
        return buildTracePo(idNo, locationName, suffererName, changeDate, "5", null, type);
    }

    /**
     * 是否需要初始化流水表
     *
     * @param info
     * @return
     */
    private boolean needInit(CdcSuffererInfo info) {
        return this.dao.countBySuffererIdNoAndSourceType(info.getIdNo(), "3") == 0;
    }

    private List<CdcSuffererTrace> initTracePo(CdcSuffererInfoAll info) {

        List<CdcSuffererTrace> list = new ArrayList<>();
        list.add(buildCdcTracePo(info.getIdNo(), info.getSuffererName(), info.getOnsetDate(), info.getDataDate(), "8"));

        list.add(buildCdcTracePo(info.getIdNo(), info.getSuffererName(), info.getRevisionFinalJudgmentDate(), info.getDataDate(), info.getCaseType()));

        if (StringUtils.isNotEmpty(info.getFinalJudgmentDeathDate()) && !".".equals(info.getFinalJudgmentDeathDate())) {
            list.add(buildCdcTracePo(info.getIdNo(), info.getSuffererName(), info.getFinalJudgmentDeathDate(), info.getDataDate(), "5"));
        }
        return list;
    }


    private static String getPrevDate(String dataDate) {
        //获取前一天的日期
        Calendar c = Calendar.getInstance();
        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            date = sdf.parse(dataDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        c.setTime(date);
        int day1 = c.get(Calendar.DATE);
        c.set(Calendar.DATE, day1 - 1);
        String dayPrev = sdf.format(c.getTime());
        return dayPrev;
    }


    private CdcSuffererTrace buildTracePo(String idNo, String locationName, String suffererName, String changeDate, String sourceType, String remark, String type) {
        return buildTracePo(idNo, locationName, suffererName, changeDate, sourceType, remark, null, type);
    }

    private CdcSuffererTrace buildSiteTracePo(String idNo, String locationName, String suffererName, String changeDate, String hospitalId, String type) {
        return buildTracePo(idNo, locationName, suffererName, changeDate, "4", null, hospitalId, type);
    }

    private CdcSuffererTrace buildCdcTracePo(String idNo, String suffererName, String changeDate, String remark, String type) {
        return buildTracePo(idNo, null, suffererName, changeDate, "3", remark, type);
    }

    private CdcSuffererTrace buildFCTracePo(String idNo, String suffererName, String changeDate,String remark, String type) {
        return buildTracePo(idNo, FANG_CANG_NAME, suffererName, changeDate, "1", remark, type);
    }

    private CdcSuffererTrace buildPointTracePo(String idNo,String locationName, String suffererName, String changeDate, String type) {
        return buildTracePo(idNo, locationName, suffererName, changeDate, "2", null, type);
    }




    /**
     * @param idNo         身份证
     * @param locationName 医院id
     * @param suffererName 患者姓名
     * @param changeDate   变更时间
     * @param sourceType   数据源类型 1方舱 2定点医院 3cdc 4隔离点
     * @param remark       备注 一版存日期
     * @param hospitalId   医院id
     * @param type         患者情况变化类型 1，临床诊断 2，确诊 3，阳性检测 4，疑似 5.死亡 6.转入,7.转出,8 发病,9出院 ,10就诊
     * @return
     */
    private CdcSuffererTrace buildTracePo(String idNo, String locationName, String suffererName, String changeDate, String sourceType, String remark, String hospitalId, String type) {
        CdcSuffererTrace po = new CdcSuffererTrace();
        po.setId(UUIDUtils.getUUID());
        po.setSuffererIdNo(idNo);
        po.setHospitalId(hospitalId);
        po.setSuffererName(suffererName);
        po.setHospitalName(locationName);
        po.setChangeTime(StringUtils.substring(changeDate,0,10));
        po.setChangeType(type);
        po.setChangeInfo(CdcTraceTypeEnum.getNameByCode(type));
        po.setCreateTime(new Date());
        po.setRemark(remark);
        po.setSourceType(sourceType);
        return po;
    }




    /**
     * 是否临床诊断
     *
     * @return
     */
    private boolean isLinchuang(CdcSuffererInfo today) {
        if (StringUtils.isEmpty(today.getCaseType())) {
            return false;
        }

        if (today.getCaseType().equals(CaseTypeEnum.TYPE_LINCHUANG.getCode()) && today.getRevisionFinalJudgmentDate().startsWith(today.getDataDate())) {
            return true;
        }
        return false;
    }

    /**
     * 是否确诊
     *
     * @return
     */
    private boolean isQuezhen(CdcSuffererInfo today) {
        if (StringUtils.isEmpty(today.getCaseType())) {
            return false;
        }

        if (today.getCaseType().equals(CaseTypeEnum.TYPE_QUEZHEN.getCode()) && today.getRevisionFinalJudgmentDate().startsWith(today.getDataDate())) {
            return true;
        }
        return false;
    }


    /**
     * 是否阳性检测
     *
     * @return
     */
    private boolean isYangxing(CdcSuffererInfo today) {
        if (StringUtils.isEmpty(today.getCaseType())) {
            return false;
        }

        if (today.getCaseType().equals(CaseTypeEnum.TYPE_YANGXING.getCode()) && today.getRevisionFinalJudgmentDate().startsWith(today.getDataDate())) {
            return true;
        }
        return false;
    }

    /**
     * 是否疑似
     *
     * @param today
     * @return
     */
    private boolean isYisi(CdcSuffererInfo today) {
        if (StringUtils.isEmpty(today.getCaseType())) {
            return false;
        }

        if (today.getCaseType().equals(CaseTypeEnum.TYPE_YISHI.getCode()) && today.getRevisionFinalJudgmentDate().startsWith(today.getDataDate())) {
            return true;
        }
        return false;
    }

    /**
     * 是否死亡
     *
     * @return
     */
    private boolean isSiwang(CdcSuffererInfo today) {
        if (StringUtils.isNotEmpty(today.getFinalJudgmentDeathDate()) && today.getDataDate().equals(today.getFinalJudgmentDeathDate().substring(0, 10))) {
            int count = this.dao.countBySuffererIdNoAndChangeTypeAndSourceType(today.getIdNo(), "5", "2");
            if(count == 0){
                return true;
            }
        }
        return false;
    }

    /**
     * 获取当前位置
     * @param idNo
     * @return
     */
    @Override
    public String getLocation(String idNo){
        List<CdcSuffererTrace> traces = this.dao.findAllBySuffererIdNoOrderByChangeTimeDescChangeTypeAsc(idNo);
        for (CdcSuffererTrace trace : traces) {
            if("3".equals(trace.getSourceType())){
                continue;
            }
            if("1".equals(trace.getSourceType())){
                if(trace.getChangeType().equals(CdcTraceTypeEnum.TYEP_ZHUANCHU.getCode()) ){
                    return "转往定点救治医院";
                }
                if(trace.getChangeType().equals(CdcTraceTypeEnum.TYEP_CHUYUAN.getCode()) ){
                    return "治愈出舱";
                }
            }
            if(trace.getChangeType().equals(CdcTraceTypeEnum.TYEP_ZHUANCHU.getCode()) ){
                return CdcTraceTypeEnum.getNameByCode(trace.getChangeType()) + "-" + trace.getHospitalName();
            }
            if (trace.getChangeType().equals(CdcTraceTypeEnum.TYEP_CHUYUAN.getCode())){
                return "治愈出院-" + trace.getHospitalName()  ;
            }
            if( trace.getChangeType().equals(CdcTraceTypeEnum.TYPE_ZHUANRU.getCode())){
                return trace.getHospitalName();
            }
            if(CdcTraceTypeEnum.TYPE_SIWANG.getCode().equals(trace.getChangeType())){
                return "死亡-" + trace.getHospitalName();
            }

        }
        return "该姓名未能匹配到位置数据";
    }


}
