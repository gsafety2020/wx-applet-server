package com.gsafety.gemp.wxapplet.infection.service;

import com.gsafety.gemp.wxapplet.healthcode.constant.PestWechatUserStatus;
import com.gsafety.gemp.wxapplet.infection.contract.dto.SuffererInfoDetailDTO;
import com.gsafety.gemp.wxapplet.infection.contract.enums.*;
import com.gsafety.gemp.wxapplet.infection.contract.iface.SuffererInfoDetailService;
import com.gsafety.gemp.wxapplet.infection.contract.iface.TraceService;
import com.gsafety.gemp.wxapplet.infection.dao.*;
import com.gsafety.gemp.wxapplet.infection.dao.po.*;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SuffererInfoDetailServiceImpl implements SuffererInfoDetailService {

    @Autowired
    private CdcSuffererInfoDao cdcSuffererInfoDao;

    @Autowired
    private SuffererInfoDao suffererInfoDao;

    @Autowired
    private PointHospitalInfoDao pointHospitalInfoDao;

    @Autowired
    private FangcangSuffererInfoDao fangcangSuffererInfoDao;

    @Autowired
    private LatestAdressInfoDao latestAdressInfoDao;

    @Autowired
    private FeverClinicsInfoDao feverClinicsInfoDao;

    @Autowired
    private CloseContactInfoDao closeContactInfoDao;
    @Autowired
    private TraceService traceService;

    @Override
    public SuffererInfoDetailDTO findSuffererDetail(String idNo) {
        SuffererInfoDetailDTO dto = null;
        //cdc数据
        List<CdcSuffererInfo> cdcList = cdcSuffererInfoDao.findCdcSuffererInfoByIdNoOrderByDataDateDesc(idNo);
        if (CollectionUtils.isNotEmpty(cdcList) && null!=cdcList.get(0)) {
            dto = cdcToDTO(cdcList.get(0));
            dto.setLocation(queryCurLocation(idNo));
            setInfectionRate(dto);
            return dto;
        }

        //隔离点数据
        List<SuffererInfoPO> isolationList = suffererInfoDao.findAllBySuffererCardOrderByDataDateDesc(idNo);
        if (CollectionUtils.isNotEmpty(isolationList) && null!=isolationList.get(0)) {
            dto = isolationToDTO(isolationList.get(0));
            dto.setLocation(queryCurLocation(idNo));
            setInfectionRate(dto);
            return dto;
        }

        //定点医院数据
        List<PointHospitalInfoPO> pointHospitalList = pointHospitalInfoDao.findAllByIdNoOrderByDataDateDesc(idNo);
        if (CollectionUtils.isNotEmpty(pointHospitalList) && null!=pointHospitalList.get(0)) {
            dto = pointHospitalToDTO(pointHospitalList.get(0));
            dto.setLocation(queryCurLocation(idNo));
            setInfectionRate(dto);
            return dto;
        }

        //方舱数据
        List<FangcangSuffererInfoPO> fangcangList = fangcangSuffererInfoDao.findAllByIdNoOrderByDataTimeDesc(idNo);
        if (CollectionUtils.isNotEmpty(fangcangList) && null!=fangcangList.get(0)) {
            dto = fangcangToDTO(fangcangList.get(0));
            dto.setLocation(queryCurLocation(idNo));
            setInfectionRate(dto);
            return dto;
        }
        //发热门诊
        List<FeverClinicsInfoPO> feverList = feverClinicsInfoDao.findByIdNoOrderByDataDateDesc(idNo);
        if(CollectionUtils.isNotEmpty(feverList)){
            dto = feverToDTO(feverList.get(0));
            dto.setLocation(queryCurLocation(idNo));
            setInfectionRate(dto);
            return dto;
        }
        return dto;
    }

    private String queryCurLocation(String idNo){
        return traceService.getLocation(idNo);
    }

    private void setInfectionRate(SuffererInfoDetailDTO dto){
        List<CloseContactInfo> contactInfos = this.closeContactInfoDao.findBySuffererIdno(dto.getIdNo());
        int infectionCount = contactInfos.stream().filter(e -> StringUtils.isNotEmpty(e.getCcIdno())).collect(Collectors.toList()).size();

        if(contactInfos.size() != 0){
            BigDecimal rate = new BigDecimal(infectionCount).divide(new BigDecimal(contactInfos.size()), 4, BigDecimal.ROUND_HALF_UP);
            dto.setInfectionRate(rate);
        }
        dto.setCloseContactNum(infectionCount);
    }


    private SuffererInfoDetailDTO cdcToDTO(CdcSuffererInfo cdcSuffererInfo) {
        SuffererInfoDetailDTO dto = new SuffererInfoDetailDTO();
        dto.setSuffererName(cdcSuffererInfo.getSuffererName());
        dto.setIdNo(cdcSuffererInfo.getIdNo());
        dto.setGender(PestWechatUserStatus.GenderEnum.getNameByCode(cdcSuffererInfo.getSex()));
        dto.setAge(cdcSuffererInfo.getAge());
        dto.setTelephone(cdcSuffererInfo.getTelphone());
        dto.setSuffererType(CaseTypeEnum.getNameByCode(cdcSuffererInfo.getCaseType()));
        return dto;
    }

    private SuffererInfoDetailDTO isolationToDTO(SuffererInfoPO suffererInfoPO) {
        SuffererInfoDetailDTO dto = new SuffererInfoDetailDTO();
        dto.setSuffererName(suffererInfoPO.getSuffererName());
        dto.setIdNo(suffererInfoPO.getSuffererCard());
        dto.setGender(suffererInfoPO.getSuffererGender());
        dto.setAge(String.valueOf(suffererInfoPO.getSuffererAge()));
        dto.setTelephone(suffererInfoPO.getSuffererTel());
        dto.setSuffererType(SuffererTypeEnum.getValueByCode(suffererInfoPO.getSuffererType()));
        return dto;
    }

    private SuffererInfoDetailDTO pointHospitalToDTO(PointHospitalInfoPO pointHospitalInfoPO) {
        SuffererInfoDetailDTO dto = new SuffererInfoDetailDTO();
        dto.setSuffererName(pointHospitalInfoPO.getSuffererName());
        dto.setIdNo(pointHospitalInfoPO.getIdNo());
        dto.setGender(PointGenderEnum.getNameByCode(pointHospitalInfoPO.getGender()));
        dto.setAge(String.valueOf(pointHospitalInfoPO.getAge()));
        dto.setTelephone(pointHospitalInfoPO.getTelphone());
        dto.setSuffererType(PointSuffererTypeEnum.getNameByCode(pointHospitalInfoPO.getSuffererType()));
        return dto;
    }

    private SuffererInfoDetailDTO fangcangToDTO(FangcangSuffererInfoPO fangcangSuffererInfoPO) {
        SuffererInfoDetailDTO dto = new SuffererInfoDetailDTO();
        dto.setSuffererName(fangcangSuffererInfoPO.getSuffererName());
        dto.setIdNo(fangcangSuffererInfoPO.getIdNo());
        dto.setGender(fangcangSuffererInfoPO.getSuffererSex());
        dto.setAge(String.valueOf(fangcangSuffererInfoPO.getSuffererAge()));
        dto.setTelephone(fangcangSuffererInfoPO.getTelephone());
        dto.setSuffererType(FangcangSuffererStatusEnum.getNameByCode(fangcangSuffererInfoPO.getSuffererStatus()));
        return dto;
    }

    private SuffererInfoDetailDTO feverToDTO(FeverClinicsInfoPO feverClinicsInfoPO){
        SuffererInfoDetailDTO dto = new SuffererInfoDetailDTO();
        dto.setSuffererName(feverClinicsInfoPO.getSuffererName());
        dto.setIdNo(feverClinicsInfoPO.getIdNo());
        dto.setGender(PointGenderEnum.getNameByCode(feverClinicsInfoPO.getGender()));
        dto.setAge(String.valueOf(feverClinicsInfoPO.getAge()));
        dto.setTelephone(feverClinicsInfoPO.getTelphone());
        dto.setSuffererType(feverClinicsInfoPO.getSyptom());
        return dto;
    }

}
