package com.gsafety.gemp.wxapplet.infection.quartz;

import java.util.Calendar;

import com.gsafety.gemp.wxapplet.infection.contract.iface.CdcSuffererInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.gsafety.gemp.wxapplet.infection.contract.iface.SuffererInfoAllService;

import lombok.extern.slf4j.Slf4j;

@Configuration
@EnableScheduling
@Slf4j
public class SuffererExcelImportQuartz {

	@Autowired
	private SuffererInfoAllService suffererInfoAllService;

	@Autowired
	private CdcSuffererInfoService cdcSuffererInfoService;

//
//	@Scheduled(cron = "	0 15 1 * * ? ")  //每天上午1点15分执行sufferer_info昨天导入数据的备份
//	private void bakSuffererInfoDataYesterday(){
//		log.info("统计局患者昨天数据备份开始........[{}]",Calendar.getInstance().getTime().toString());
//		Long beginTime = System.currentTimeMillis();
//		suffererInfoAllService.bakYesterdaySuffererInfoData();
//		Long endTime = System.currentTimeMillis();
//		log.info("统计局患者昨天数据备份结束........[{}]",Calendar.getInstance().getTime().toString());
//		log.info("总计花费时间：[time:{}]",endTime-beginTime);
//	}

//	@Scheduled(cron = "	0 30 1 * * ? ")  //每天上午1点30分执行sufferer_info昨天导入数据的备份
//	private void bakCdcSufferInfoDataYesterDay(){
//		log.info("疾患中心患者数据备份到历史表开始 [{}]",Calendar.getInstance().getTime().toString());
//		Long beginTime = System.currentTimeMillis();
//		cdcSuffererInfoService.bakYesterdayData();
//		Long endTime = System.currentTimeMillis();
//		log.info("疾患中心患者数据备份到历史表结束 [{}]",Calendar.getInstance().getTime().toString());
//		log.info("总计花费时间：[time:{}]",endTime-beginTime);
//
//	}

}
