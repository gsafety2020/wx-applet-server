package com.gsafety.gemp.wxapplet.infection.contract.enums;

import com.gsafety.gemp.wxapplet.infection.contract.dto.CaseTypeDTO;
import lombok.Getter;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 患者类型
 *
 * @author liyanhong
 * @since 2020/2/27  11:10
 */
@Getter
public enum SuffererTypeEnum {

    all(0,"全部"),
    Close_contacts(1, "密接"),
    Heat(2, "发热"),
    Suspected(3, "疑似"),
    Confirmed(4, "确诊"),
    Discharged(5, "出院观察者"),
	Diagnosed_20_Days_Ago(6,"20日前的临床诊断病例"),
	Other(7,"其他");
	
	
    private Integer code;
    private String value;

    SuffererTypeEnum(Integer c, String v) {
        this.code = c;
        this.value = v;
    }

    public static String getValueByCode(Integer code) {

        for (SuffererTypeEnum en : SuffererTypeEnum.values()) {
            if (en.getCode().equals(code)) {
                return en.getValue();
            }
        }

        return StringUtils.EMPTY;
    }

    public static Integer getCodeByName(String name) {

        for (SuffererTypeEnum en : SuffererTypeEnum.values()) {
            if (en.getValue().equals(name)) {
                return en.getCode();
            }
        }

        return null;
    }

    public static List<CaseTypeDTO> getList(){
        List<CaseTypeDTO> list=new ArrayList<>();
        CaseTypeDTO caseTypeDTO=null;
        for (SuffererTypeEnum caseTypeEnum : values()) {
            caseTypeDTO=new CaseTypeDTO();
            caseTypeDTO.setCode(caseTypeEnum.code);
            caseTypeDTO.setName(caseTypeEnum.value);
            list.add(caseTypeDTO);
        }
        return list;
    }
}
