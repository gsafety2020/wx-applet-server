create table pest_user_authorize
(
    authorize_id           varchar(32)  not null  primary key comment '主键',
    mobile                 varchar(16) not null comment '手机号码',
    authorize_type         varchar(10) not null comment '权限类型，如query,update,delete等',
    remark                 varchar(255) null comment '备注',
    create_by              varchar(255) null comment '创建人',
    upate_by               varchar(255) null comment '更新人',
    create_time            datetime     null comment '创建时间',
    update_time            datetime     null comment '更新时间',
    is_delete              varchar(1)   not null comment '是否删除0：未删除  1.已删除'
);