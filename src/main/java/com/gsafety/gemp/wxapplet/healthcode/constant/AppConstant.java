package com.gsafety.gemp.wxapplet.healthcode.constant;

/**
 * @author Administrator
 * @Title: App
 * @ProjectName wuhan-ncov
 * @Description: TODO
 * @date 2020/2/120:20
 */
public class AppConstant {

    /**
     * 默认当前页面为第一页
     */
    public static final int DEFAULT_CURRENT_PAGE = 0;

    /**
     * 默认每页10条数据
     */
    public static final int DEAULT_PAGE_SIZE = 10;

    /**
     * 行政区划编号为000000
     */
    public static final String DEFAULT_DIM_ZERO = "000000";


    /**
     * 行政区划编号为0
     */
    public static final String DIM_ZERO = "0";

    public static final String DIM_ChINA_CODE = "100000";

    public static String getDimCode(String areaCode){
        if(DEFAULT_DIM_ZERO.equals(areaCode)||AppConstant.DIM_ZERO.equals(areaCode)){
            areaCode=DIM_ChINA_CODE;
        }
        return areaCode;
    }

    /**
     * 疫情文件正常状态为5
     */
    public static final String NORMAL_FILE_STATUS = "5";

}
