package com.gsafety.gemp.wxapplet.healthcode.wrapper;

import org.springframework.beans.BeanUtils;

import com.gsafety.gemp.wxapplet.healthcode.contract.dto.PestHealthAuthorizeDTO;
import com.gsafety.gemp.wxapplet.healthcode.dao.po.PestHealthAuthorizePO;

/**
 * 
* @ClassName: PestHealthAuthorizeWrapper 
* @Description: 类型转换
* @author luoxiao
* @date 2020年2月26日 下午10:08:32 
*
 */
public class PestHealthAuthorizeWrapper {

	public static PestHealthAuthorizeDTO poToDto(PestHealthAuthorizePO po){
		PestHealthAuthorizeDTO dto = new PestHealthAuthorizeDTO();
		BeanUtils.copyProperties(po, dto);
		return dto;
	}
}
