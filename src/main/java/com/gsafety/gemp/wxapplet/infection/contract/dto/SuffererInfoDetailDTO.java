package com.gsafety.gemp.wxapplet.infection.contract.dto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author dusiwei
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SuffererInfoDetailDTO {

    @ApiModelProperty(value = "姓名")
    private String suffererName;

    @ApiModelProperty(value = "身份证号")
    private String idNo;

    @ApiModelProperty(value = "性别")
    private String gender;

    @ApiModelProperty(value = "年龄")
    private String age;

    @ApiModelProperty(value = "电话")
    private String telephone;

    @ApiModelProperty(value = "病患类型")
    private String suffererType;

    @ApiModelProperty(value = "位置")
    private String location;
    @ApiModelProperty(value = "感染人数")
    private Integer closeContactNum;
    @ApiModelProperty(value = "感染率")
    private BigDecimal infectionRate;

}
