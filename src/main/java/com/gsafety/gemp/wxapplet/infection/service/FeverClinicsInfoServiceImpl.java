package com.gsafety.gemp.wxapplet.infection.service;

import com.gsafety.gemp.wxapplet.infection.contract.dto.FeverClinicsInfoDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.FeverClinicsInfoPageDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.PointHospitalInfoDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.PointHospitalInfoPageDTO;
import com.gsafety.gemp.wxapplet.infection.contract.iface.FeverClinicsInfoService;
import com.gsafety.gemp.wxapplet.infection.dao.FeverClinicsInfoDao;
import com.gsafety.gemp.wxapplet.infection.dao.po.FeverClinicsInfoPO;
import com.gsafety.gemp.wxapplet.infection.dao.po.PointHospitalInfoPO;
import com.gsafety.gemp.wxapplet.infection.wrapper.FeverClinicsInfoWrapper;
import com.gsafety.gemp.wxapplet.infection.wrapper.PointHospitalInfoWrapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class FeverClinicsInfoServiceImpl implements FeverClinicsInfoService {

    @Resource
    private FeverClinicsInfoDao feverClinicsInfoDao;

    @Override
    public Page<FeverClinicsInfoDTO> search(FeverClinicsInfoPageDTO dto, PageRequest pageRequest) {
        Specification<FeverClinicsInfoPO> spec = concatSpecification(dto);
        Page<FeverClinicsInfoPO> poPage = feverClinicsInfoDao.findAll(spec, pageRequest);
        Page<FeverClinicsInfoDTO> PageDto= FeverClinicsInfoWrapper.toPageDTO(poPage);
        return PageDto;
    }

    @Override
    public FeverClinicsInfoDTO findOne(String id) {
        Optional<FeverClinicsInfoPO> optional = feverClinicsInfoDao.findById(id);
        return optional.isPresent() ? FeverClinicsInfoWrapper.toDTO(optional.get()) : null;
    }

    private Specification<FeverClinicsInfoPO> concatSpecification(FeverClinicsInfoPageDTO dto){
        Specification<FeverClinicsInfoPO> spec = new Specification<FeverClinicsInfoPO>(){

            @Override
            public Predicate toPredicate(Root<FeverClinicsInfoPO> root, CriteriaQuery<?> query,
                                         CriteriaBuilder cb) {
                List<Predicate> predicates = new ArrayList<>(); //所有的断言
                
                if(dto != null){
                    if(org.apache.commons.lang.StringUtils.isNotEmpty(dto.getSuffererName())){
                        Predicate suffererNameLike = cb.like(root.get("suffererName"), "%"+dto.getSuffererName().trim()+"%");
                        predicates.add(suffererNameLike);
                    }
                    if(org.apache.commons.lang.StringUtils.isNotEmpty(dto.getIdNo())){
                        Predicate suffererCardLike = cb.like(root.get("idNo"), "%"+dto.getIdNo().trim()+"%");
                        predicates.add(suffererCardLike);
                    }
                    if(org.apache.commons.lang.StringUtils.isNotEmpty(dto.getTelphone())){
                        Predicate suffererTelLike = cb.like(root.get("telphone"), "%"+dto.getTelphone().trim()+"%");
                        predicates.add(suffererTelLike);
                    }

                }
                return cb.and(predicates.toArray(new Predicate[predicates.size()]));
            }

        };
        return spec;
    }
}
