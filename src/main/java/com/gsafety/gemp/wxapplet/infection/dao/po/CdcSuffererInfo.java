package com.gsafety.gemp.wxapplet.infection.dao.po;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "cdc_sufferer_info", schema = "wx_applet", catalog = "")
public class CdcSuffererInfo {
    private String id;
    private String cardId;
    private String cardCode;
    private String cardStatus;
    private String suffererName;
    private String suffererParent;
    private String idNo;
    private String sex;
    private String birthday;
    private String age;
    private String workUnit;
    private String telphone;
    private String location;
    private String addressCode;
    private String detailedAddress;
    private String occupation;
    private String caseType;
    private String caseType2;
    private String onsetDate;
    private String caseDate;
    private String deadthDate;
    private String diseaseName;
    private String beforeRevisionName;
    private String beforeRevisionCaseDate;
    private String fjbrDate;
    private String fillInDoctor;
    private String fillInDate;
    private String reportUnitCode;
    private String reportUnit;
    private String unitType;
    private String reportFillInDate;
    private String reportFillInUser;
    private String reportFillInUnit;
    private String districtReviewDate;
    private String cityReviewDate;
    private String provinceReviewDate;
    private String reviewStatus;
    private String reportRevisionDate;
    private String revisionFinalJudgmentDate;
    private String finalJudgmentDeathDate;
    private String revisionUser;
    private String revisionUserUnit;
    private String deleteDate;
    private String deleteUser;
    private String deleteUserUnit;
    private String deleteReason;
    private String rmark;
    private String clinicalSeverity;
    private String referralType;
    private String receiveOrg;
    private String importDate;
    private Date creatTime;
    private Date updateTime;
    private String dataDate;


    @Id
    @Column(name = "ID", nullable = false, length = 32)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "CARD_ID", nullable = true, length = 32)
    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    @Basic
    @Column(name = "CARD_CODE", nullable = true, length = 32)
    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    @Basic
    @Column(name = "CARD_STATUS", nullable = true, length = 20)
    public String getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(String cardStatus) {
        this.cardStatus = cardStatus;
    }

    @Basic
    @Column(name = "SUFFERER_NAME", nullable = true, length = 32)
    public String getSuffererName() {
        return suffererName;
    }

    public void setSuffererName(String suffererName) {
        this.suffererName = suffererName;
    }

    @Basic
    @Column(name = "SUFFERER_PARENT", nullable = true, length = 32)
    public String getSuffererParent() {
        return suffererParent;
    }

    public void setSuffererParent(String suffererParent) {
        this.suffererParent = suffererParent;
    }

    @Basic
    @Column(name = "ID_NO", nullable = true, length = 18)
    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    @Basic
    @Column(name = "SEX", nullable = true, length = 10)
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Basic
    @Column(name = "BIRTHDAY", nullable = true, length = 10)
    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    @Basic
    @Column(name = "AGE", nullable = true)
    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    @Basic
    @Column(name = "WORK_UNIT", nullable = true, length = 200)
    public String getWorkUnit() {
        return workUnit;
    }

    public void setWorkUnit(String workUnit) {
        this.workUnit = workUnit;
    }

    @Basic
    @Column(name = "TELPHONE", nullable = true, length = 20)
    public String getTelphone() {
        return telphone;
    }

    public void setTelphone(String telphone) {
        this.telphone = telphone;
    }

    @Basic
    @Column(name = "LOCATION", nullable = true, length = 50)
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Basic
    @Column(name = "ADDRESS_CODE", nullable = true, length = 32)
    public String getAddressCode() {
        return addressCode;
    }

    public void setAddressCode(String addressCode) {
        this.addressCode = addressCode;
    }

    @Basic
    @Column(name = "DETAILED_ADDRESS", nullable = true, length = 200)
    public String getDetailedAddress() {
        return detailedAddress;
    }

    public void setDetailedAddress(String detailedAddress) {
        this.detailedAddress = detailedAddress;
    }

    @Basic
    @Column(name = "OCCUPATION", nullable = true, length = 100)
    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    @Basic
    @Column(name = "CASE_TYPE", nullable = true, length = 20)
    public String getCaseType() {
        return caseType;
    }

    public void setCaseType(String caseType) {
        this.caseType = caseType;
    }

    @Basic
    @Column(name = "CASE_TYPE2", nullable = true, length = 20)
    public String getCaseType2() {
        return caseType2;
    }

    public void setCaseType2(String caseType2) {
        this.caseType2 = caseType2;
    }

    @Basic
    @Column(name = "ONSET_DATE", nullable = true, length = 10)
    public String getOnsetDate() {
        return onsetDate;
    }

    public void setOnsetDate(String onsetDate) {
        this.onsetDate = onsetDate;
    }

    @Basic
    @Column(name = "CASE_DATE", nullable = true, length = 19)
    public String getCaseDate() {
        return caseDate;
    }

    public void setCaseDate(String caseDate) {
        this.caseDate = caseDate;
    }

    @Basic
    @Column(name = "DEADTH_DATE", nullable = true, length = 10)
    public String getDeadthDate() {
        return deadthDate;
    }

    public void setDeadthDate(String deadthDate) {
        this.deadthDate = deadthDate;
    }

    @Basic
    @Column(name = "DISEASE_NAME", nullable = true, length = 50)
    public String getDiseaseName() {
        return diseaseName;
    }

    public void setDiseaseName(String diseaseName) {
        this.diseaseName = diseaseName;
    }

    @Basic
    @Column(name = "BEFORE_REVISION_NAME", nullable = true, length = 50)
    public String getBeforeRevisionName() {
        return beforeRevisionName;
    }

    public void setBeforeRevisionName(String beforeRevisionName) {
        this.beforeRevisionName = beforeRevisionName;
    }

    @Basic
    @Column(name = "BEFORE_REVISION_CASE_DATE", nullable = true, length = 19)
    public String getBeforeRevisionCaseDate() {
        return beforeRevisionCaseDate;
    }

    public void setBeforeRevisionCaseDate(String beforeRevisionCaseDate) {
        this.beforeRevisionCaseDate = beforeRevisionCaseDate;
    }

    @Basic
    @Column(name = "FJBR_DATE", nullable = true, length = 19)
    public String getFjbrDate() {
        return fjbrDate;
    }

    public void setFjbrDate(String fjbrDate) {
        this.fjbrDate = fjbrDate;
    }

    @Basic
    @Column(name = "FILL_IN_DOCTOR", nullable = true, length = 50)
    public String getFillInDoctor() {
        return fillInDoctor;
    }

    public void setFillInDoctor(String fillInDoctor) {
        this.fillInDoctor = fillInDoctor;
    }

    @Basic
    @Column(name = "FILL_IN_DATE", nullable = true, length = 19)
    public String getFillInDate() {
        return fillInDate;
    }

    public void setFillInDate(String fillInDate) {
        this.fillInDate = fillInDate;
    }

    @Basic
    @Column(name = "REPORT_UNIT_CODE", nullable = true, length = 20)
    public String getReportUnitCode() {
        return reportUnitCode;
    }

    public void setReportUnitCode(String reportUnitCode) {
        this.reportUnitCode = reportUnitCode;
    }

    @Basic
    @Column(name = "REPORT_UNIT", nullable = true, length = 200)
    public String getReportUnit() {
        return reportUnit;
    }

    public void setReportUnit(String reportUnit) {
        this.reportUnit = reportUnit;
    }

    @Basic
    @Column(name = "UNIT_TYPE", nullable = true, length = 10)
    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    @Basic
    @Column(name = "REPORT_FILL_IN_DATE", nullable = true, length = 19)
    public String getReportFillInDate() {
        return reportFillInDate;
    }

    public void setReportFillInDate(String reportFillInDate) {
        this.reportFillInDate = reportFillInDate;
    }

    @Basic
    @Column(name = "REPORT_FILL_IN_USER", nullable = true, length = 50)
    public String getReportFillInUser() {
        return reportFillInUser;
    }

    public void setReportFillInUser(String reportFillInUser) {
        this.reportFillInUser = reportFillInUser;
    }

    @Basic
    @Column(name = "REPORT_FILL_IN_UNIT", nullable = true, length = 200)
    public String getReportFillInUnit() {
        return reportFillInUnit;
    }

    public void setReportFillInUnit(String reportFillInUnit) {
        this.reportFillInUnit = reportFillInUnit;
    }

    @Basic
    @Column(name = "DISTRICT_REVIEW_DATE", nullable = true, length = 19)
    public String getDistrictReviewDate() {
        return districtReviewDate;
    }

    public void setDistrictReviewDate(String districtReviewDate) {
        this.districtReviewDate = districtReviewDate;
    }

    @Basic
    @Column(name = "CITY_REVIEW_DATE", nullable = true, length = 19)
    public String getCityReviewDate() {
        return cityReviewDate;
    }

    public void setCityReviewDate(String cityReviewDate) {
        this.cityReviewDate = cityReviewDate;
    }

    @Basic
    @Column(name = "PROVINCE_REVIEW_DATE", nullable = true, length = 19)
    public String getProvinceReviewDate() {
        return provinceReviewDate;
    }

    public void setProvinceReviewDate(String provinceReviewDate) {
        this.provinceReviewDate = provinceReviewDate;
    }

    @Basic
    @Column(name = "REVIEW_STATUS", nullable = true, length = 10)
    public String getReviewStatus() {
        return reviewStatus;
    }

    public void setReviewStatus(String reviewStatus) {
        this.reviewStatus = reviewStatus;
    }

    @Basic
    @Column(name = "REPORT_REVISION_DATE", nullable = true, length = 19)
    public String getReportRevisionDate() {
        return reportRevisionDate;
    }

    public void setReportRevisionDate(String reportRevisionDate) {
        this.reportRevisionDate = reportRevisionDate;
    }

    @Basic
    @Column(name = "REVISION_FINAL_JUDGMENT_DATE", nullable = true, length = 19)
    public String getRevisionFinalJudgmentDate() {
        return revisionFinalJudgmentDate;
    }

    public void setRevisionFinalJudgmentDate(String revisionFinalJudgmentDate) {
        this.revisionFinalJudgmentDate = revisionFinalJudgmentDate;
    }

    @Basic
    @Column(name = "FINAL_JUDGMENT_DEATH_DATE", nullable = true, length = 19)
    public String getFinalJudgmentDeathDate() {
        return finalJudgmentDeathDate;
    }

    public void setFinalJudgmentDeathDate(String finalJudgmentDeathDate) {
        this.finalJudgmentDeathDate = finalJudgmentDeathDate;
    }

    @Basic
    @Column(name = "REVISION_USER", nullable = true, length = 50)
    public String getRevisionUser() {
        return revisionUser;
    }

    public void setRevisionUser(String revisionUser) {
        this.revisionUser = revisionUser;
    }

    @Basic
    @Column(name = "REVISION_USER_UNIT", nullable = true, length = 200)
    public String getRevisionUserUnit() {
        return revisionUserUnit;
    }

    public void setRevisionUserUnit(String revisionUserUnit) {
        this.revisionUserUnit = revisionUserUnit;
    }

    @Basic
    @Column(name = "DELETE_DATE", nullable = true, length = 19)
    public String getDeleteDate() {
        return deleteDate;
    }

    public void setDeleteDate(String deleteDate) {
        this.deleteDate = deleteDate;
    }

    @Basic
    @Column(name = "DELETE_USER", nullable = true, length = 50)
    public String getDeleteUser() {
        return deleteUser;
    }

    public void setDeleteUser(String deleteUser) {
        this.deleteUser = deleteUser;
    }

    @Basic
    @Column(name = "DELETE_USER_UNIT", nullable = true, length = 200)
    public String getDeleteUserUnit() {
        return deleteUserUnit;
    }

    public void setDeleteUserUnit(String deleteUserUnit) {
        this.deleteUserUnit = deleteUserUnit;
    }

    @Basic
    @Column(name = "DELETE_REASON", nullable = true, length = 100)
    public String getDeleteReason() {
        return deleteReason;
    }

    public void setDeleteReason(String deleteReason) {
        this.deleteReason = deleteReason;
    }

    @Basic
    @Column(name = "RMARK", nullable = true, length = 200)
    public String getRmark() {
        return rmark;
    }

    public void setRmark(String rmark) {
        this.rmark = rmark;
    }

    @Basic
    @Column(name = "CLINICAL_SEVERITY", nullable = true, length = 50)
    public String getClinicalSeverity() {
        return clinicalSeverity;
    }

    public void setClinicalSeverity(String clinicalSeverity) {
        this.clinicalSeverity = clinicalSeverity;
    }

    @Basic
    @Column(name = "REFERRAL_TYPE", nullable = true, length = 10)
    public String getReferralType() {
        return referralType;
    }

    public void setReferralType(String referralType) {
        this.referralType = referralType;
    }

    @Basic
    @Column(name = "RECEIVE_ORG", nullable = true, length = 100)
    public String getReceiveOrg() {
        return receiveOrg;
    }

    public void setReceiveOrg(String receiveOrg) {
        this.receiveOrg = receiveOrg;
    }

    @Basic
    @Column(name = "IMPORT_DATE", nullable = true, length = 10)
    public String getImportDate() {
        return importDate;
    }

    public void setImportDate(String importDate) {
        this.importDate = importDate;
    }

    @Basic
    @Column(name = "CREAT_TIME", nullable = true)
    public Date getCreatTime() {
        return creatTime;
    }

    public void setCreatTime(Date creatTime) {
        this.creatTime = creatTime;
    }

    @Basic
    @Column(name = "UPDATE_TIME", nullable = true)
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Basic
    @Column(name = "DATA_DATE", nullable = true)
    public String getDataDate() {
        return dataDate;
    }

    public void setDataDate(String dataDate) {
        this.dataDate = dataDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CdcSuffererInfo that = (CdcSuffererInfo) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(cardId, that.cardId) &&
                Objects.equals(cardCode, that.cardCode) &&
                Objects.equals(cardStatus, that.cardStatus) &&
                Objects.equals(suffererName, that.suffererName) &&
                Objects.equals(suffererParent, that.suffererParent) &&
                Objects.equals(idNo, that.idNo) &&
                Objects.equals(sex, that.sex) &&
                Objects.equals(birthday, that.birthday) &&
                Objects.equals(age, that.age) &&
                Objects.equals(workUnit, that.workUnit) &&
                Objects.equals(telphone, that.telphone) &&
                Objects.equals(location, that.location) &&
                Objects.equals(addressCode, that.addressCode) &&
                Objects.equals(detailedAddress, that.detailedAddress) &&
                Objects.equals(occupation, that.occupation) &&
                Objects.equals(caseType, that.caseType) &&
                Objects.equals(caseType2, that.caseType2) &&
                Objects.equals(onsetDate, that.onsetDate) &&
                Objects.equals(caseDate, that.caseDate) &&
                Objects.equals(deadthDate, that.deadthDate) &&
                Objects.equals(diseaseName, that.diseaseName) &&
                Objects.equals(beforeRevisionName, that.beforeRevisionName) &&
                Objects.equals(beforeRevisionCaseDate, that.beforeRevisionCaseDate) &&
                Objects.equals(fjbrDate, that.fjbrDate) &&
                Objects.equals(fillInDoctor, that.fillInDoctor) &&
                Objects.equals(fillInDate, that.fillInDate) &&
                Objects.equals(reportUnitCode, that.reportUnitCode) &&
                Objects.equals(reportUnit, that.reportUnit) &&
                Objects.equals(unitType, that.unitType) &&
                Objects.equals(reportFillInDate, that.reportFillInDate) &&
                Objects.equals(reportFillInUser, that.reportFillInUser) &&
                Objects.equals(reportFillInUnit, that.reportFillInUnit) &&
                Objects.equals(districtReviewDate, that.districtReviewDate) &&
                Objects.equals(cityReviewDate, that.cityReviewDate) &&
                Objects.equals(provinceReviewDate, that.provinceReviewDate) &&
                Objects.equals(reviewStatus, that.reviewStatus) &&
                Objects.equals(reportRevisionDate, that.reportRevisionDate) &&
                Objects.equals(revisionFinalJudgmentDate, that.revisionFinalJudgmentDate) &&
                Objects.equals(finalJudgmentDeathDate, that.finalJudgmentDeathDate) &&
                Objects.equals(revisionUser, that.revisionUser) &&
                Objects.equals(revisionUserUnit, that.revisionUserUnit) &&
                Objects.equals(deleteDate, that.deleteDate) &&
                Objects.equals(deleteUser, that.deleteUser) &&
                Objects.equals(deleteUserUnit, that.deleteUserUnit) &&
                Objects.equals(deleteReason, that.deleteReason) &&
                Objects.equals(rmark, that.rmark) &&
                Objects.equals(clinicalSeverity, that.clinicalSeverity) &&
                Objects.equals(referralType, that.referralType) &&
                Objects.equals(receiveOrg, that.receiveOrg) &&
                Objects.equals(importDate, that.importDate) &&
                Objects.equals(creatTime, that.creatTime) &&
                Objects.equals(updateTime, that.updateTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cardId, cardCode, cardStatus, suffererName, suffererParent, idNo, sex, birthday, age, workUnit, telphone, location, addressCode, detailedAddress, occupation, caseType, caseType2, onsetDate, caseDate, deadthDate, diseaseName, beforeRevisionName, beforeRevisionCaseDate, fjbrDate, fillInDoctor, fillInDate, reportUnitCode, reportUnit, unitType, reportFillInDate, reportFillInUser, reportFillInUnit, districtReviewDate, cityReviewDate, provinceReviewDate, reviewStatus, reportRevisionDate, revisionFinalJudgmentDate, finalJudgmentDeathDate, revisionUser, revisionUserUnit, deleteDate, deleteUser, deleteUserUnit, deleteReason, rmark, clinicalSeverity, referralType, receiveOrg, importDate, creatTime, updateTime);
    }
}
