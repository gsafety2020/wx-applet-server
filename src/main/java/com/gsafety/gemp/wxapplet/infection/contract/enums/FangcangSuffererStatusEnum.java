package com.gsafety.gemp.wxapplet.infection.contract.enums;

/**
 * @author dusiwei
 */
public enum FangcangSuffererStatusEnum {

    TYPE_ZAICANG("1","在舱"),
    TYPE_ZHUANGYUAN("2","转院"),
    TYPE_CHUYUAN("3","出院");

    private String code;
    private String name;
    FangcangSuffererStatusEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static String getNameByCode(String code) {
        for (FangcangSuffererStatusEnum statusEnum : values()) {
            if (statusEnum.code.equals(code)){
                return statusEnum.name;
            }
        }
        return null;
    }

    public static String getCodeByName(String name) {
        for (FangcangSuffererStatusEnum statusEnum : values()) {
            if (statusEnum.name.equals(name)){
                return statusEnum.code;
            }
        }
        return null;
    }

}
