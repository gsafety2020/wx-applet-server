package com.gsafety.gemp.wxapplet.infection.contract.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FeverClinicsInfoDTO {

    @ApiModelProperty(value = "主键")
    private String id;

    /**发热门诊医院*/
    @ApiModelProperty(value = "发热门诊医院")
    private String hospital;

    /**患者姓名*/
    @ApiModelProperty(value = "患者姓名")
    private String suffererName;

    /**性别(1-男,2-女)*/
    @ApiModelProperty(value = "性别(1-男,2-女)")
    private String gender;

    /**年龄*/
    @ApiModelProperty(value = "年龄")
    private String age;

    /**身份证号码*/
    @ApiModelProperty(value = "身份证号码")
    private String idNo;

    /**联系电话*/
    @ApiModelProperty(value = "联系电话")
    private String telphone;

    /**居住地址*/
    @ApiModelProperty(value = "居住地址")
    private String address;

    /**主要症状*/
    @ApiModelProperty(value = "发热门诊医院")
    private String syptom;

    /** 就诊时间 **/
    @ApiModelProperty(value = "就诊时间")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date consultationTime;

    /**患者来源*/
    @ApiModelProperty(value = "患者来源")
    private String source;

    /**患者去向*/
    @ApiModelProperty(value = "患者去向")
    private String direction;

    /** 创建时间**/
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /** 更新时间**/
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    /** 备注**/
    @ApiModelProperty(value = "备注")
    private String note;
}
