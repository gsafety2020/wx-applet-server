package com.gsafety.gemp.wxapplet.healthcode.service;

import java.util.Calendar;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.gsafety.gemp.wxapplet.healthcode.constant.PestHealthRecordStatus;
import com.gsafety.gemp.wxapplet.healthcode.contract.service.PestHealthRecordService;
import com.gsafety.gemp.wxapplet.healthcode.dao.PestHealthAuthorizeDao;
import com.gsafety.gemp.wxapplet.healthcode.dao.PestHealthRecordDao;
import com.gsafety.gemp.wxapplet.healthcode.dao.po.PestHealthAuthorizePO;
import com.gsafety.gemp.wxapplet.healthcode.dao.po.PestHealthRecordPO;
import com.gsafety.gemp.wxapplet.utils.UUIDUtils;

/**
 * 
* @ClassName: PestHealthRecordServiceImpl 
* @Description: 打卡记录服务实现层
* @author luoxiao
* @date 2020年2月26日 下午5:27:06 
*
 */
@Service("pestHealthRecordService")
public class PestHealthRecordServiceImpl implements PestHealthRecordService{

	@Autowired
	private PestHealthRecordDao pestHealthRecordDao;
	
	@Autowired
	private PestHealthAuthorizeDao pestHealthAuthorizeDao;
	
	@Override
	public void saveChockInRecordInfo(String authorizeId,String healthCodeId) {
		if(StringUtils.isEmpty(authorizeId)){
			throw new RuntimeException("授权主键为空!");
		}
		if(StringUtils.isEmpty(healthCodeId)){
			throw new RuntimeException("健康码设备为空!");
		}
		Optional<PestHealthAuthorizePO> optional =  pestHealthAuthorizeDao.findById(authorizeId);
		if(!optional.isPresent()){
			throw new RuntimeException("授权不存在!");
		}
		//判断二维码信息是否存在
		PestHealthAuthorizePO authorizePo = optional.get();
		PestHealthRecordPO po = new PestHealthRecordPO();
		po.setHealthCodeId(healthCodeId);
		po.setAuthorizeId(authorizeId);
		po.setRecordId(UUIDUtils.getUUID());
		po.setIsDelete(PestHealthRecordStatus.DeleteEnum.NOT_DELETE.getCode());
		po.setPostDate(Calendar.getInstance().getTime());
		po.setCreateTime(Calendar.getInstance().getTime());
		po.setUpdateTime(Calendar.getInstance().getTime());
		handlerPestHealthRecordPO(authorizePo,po);
		pestHealthRecordDao.saveAndFlush(po);
	}
	
	private void handlerPestHealthRecordPO(PestHealthAuthorizePO authorizePo,PestHealthRecordPO po){
		po.setDeviceId(authorizePo.getDeviceId());
		po.setDeviceName(authorizePo.getDeviceName());
		po.setDeviceType(authorizePo.getDeviceType());
		po.setLatitude(authorizePo.getLatitude());
		po.setLongitude(authorizePo.getLongitude());
		po.setAreaCode(authorizePo.getAreaCode());
		po.setAreaName(authorizePo.getAreaName());
		po.setAddress(authorizePo.getAddress());
	}
}
