package com.gsafety.gemp.wxapplet.infection.wrapper;

import com.gsafety.gemp.wxapplet.infection.contract.dto.SiteNameDTO;
import com.gsafety.gemp.wxapplet.infection.dao.po.SiteInfoPO;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lzx
 * @version 1.0
 * @date 2020/2/28 1:23
 */
public class KeyAssignmentWrapper {

    /**
     *
     * 该方法用于实现...的功能
     * @author xlq
     * @since 2020年2月9日 下午7:38:28
     * @param po
     * @return
     */
    public static SiteNameDTO toDTO(SiteInfoPO po) {
        SiteNameDTO dto = new SiteNameDTO();
        BeanUtils.copyProperties(po, dto);
        return dto;
    }

    /**
     * 查询隔离地点
     * @param records
     * @return
     */
    public static List<SiteNameDTO> toDTOList(List<SiteInfoPO> records){
        List<SiteNameDTO> list = new ArrayList<>();
        records.stream().forEach(record -> list.add(toDTO(record)));
        return list;
    }

}
