package com.gsafety.gemp.wxapplet.infection.dao.po;

import lombok.Builder;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "cdc_sufferer_trace", schema = "wx_applet", catalog = "")
public class CdcSuffererTrace {
    private String id;
    private String suffererIdNo;
    private String suffererName;
    private String hospitalName;
    private String changeType;
    private String changeInfo;
    private String remark;
    private String changeTime;
    private Date createTime;
    private String sourceType;

    private String hospitalId;




    @Id
    @Column(name = "ID", nullable = false, length = 32)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "SUFFERER_ID_NO", nullable = true, length = 20)
    public String getSuffererIdNo() {
        return suffererIdNo;
    }

    public void setSuffererIdNo(String suffererIdNo) {
        this.suffererIdNo = suffererIdNo;
    }

    @Basic
    @Column(name = "SUFFERER_NAME", nullable = true, length = 20)
    public String getSuffererName() {
        return suffererName;
    }

    public void setSuffererName(String suffererName) {
        this.suffererName = suffererName;
    }

    @Basic
    @Column(name = "HOSPITAL_NAME", nullable = true, length = 100)
    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    @Basic
    @Column(name = "CHANGE_TYPE", nullable = true, length = 1)
    public String getChangeType() {
        return changeType;
    }

    public void setChangeType(String changeType) {
        this.changeType = changeType;
    }

    @Basic
    @Column(name = "CHANGE_INFO", nullable = true, length = 50)
    public String getChangeInfo() {
        return changeInfo;
    }

    public void setChangeInfo(String changeInfo) {
        this.changeInfo = changeInfo;
    }

    @Basic
    @Column(name = "REMARK", nullable = true, length = 100)
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Basic
    @Column(name = "CHANGE_TIME", nullable = true)
    public String getChangeTime() {
        return changeTime;
    }

    public void setChangeTime(String changeTime) {
        this.changeTime = changeTime;
    }

    @Basic
    @Column(name = "CREATE_TIME", nullable = true)
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


    @Basic
    @Column(name = "SOURCE_TYPE", nullable = true)
    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    @Basic
    @Column(name = "HOSPITAL_ID", nullable = true)
    public String getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }





    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CdcSuffererTrace that = (CdcSuffererTrace) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(suffererIdNo, that.suffererIdNo) &&
                Objects.equals(suffererName, that.suffererName) &&
                Objects.equals(hospitalName, that.hospitalName) &&
                Objects.equals(changeType, that.changeType) &&
                Objects.equals(changeInfo, that.changeInfo) &&
                Objects.equals(remark, that.remark) &&
                Objects.equals(changeTime, that.changeTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, suffererIdNo, suffererName, hospitalName, changeType, changeInfo, remark, changeTime);
    }
}
