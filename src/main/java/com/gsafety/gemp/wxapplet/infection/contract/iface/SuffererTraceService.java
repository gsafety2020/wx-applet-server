package com.gsafety.gemp.wxapplet.infection.contract.iface;

import java.util.List;
import java.util.Map;

import com.gsafety.gemp.wxapplet.infection.contract.dto.TraceTJJDTO;
import com.gsafety.gemp.wxapplet.infection.dao.po.SuffererTracePO;

public interface SuffererTraceService {
	
	//取最大每个患者最大的sequence_no的值
	Map<String, SuffererTracePO> getSuffererIdSequenceNo();
	
	/**
	 * 
	* @Title: trackTJJSufferer 
	* @Description: 统计局溯源
	* @param @param suffererId
	* @param @return    设定文件 
	* @return JSONObject    返回类型 
	* @throws
	 */
	Map<String,Object> trackTJJSufferer(String suffererId);
}
