/**
 * 
 */
package com.gsafety.gemp.wxapplet.client.sns;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.ResponseEntity;

import com.gsafety.gemp.wxapplet.client.DefaultClient;
import com.gsafety.gemp.wxapplet.client.Request;
import com.gsafety.gemp.wxapplet.client.Response;
import com.gsafety.gemp.wxapplet.client.exception.DefaultClientException;



/**
 * 微信snsclient重构
 * 
 * @author yangbo
 *
 */
public class SnsClient extends DefaultClient {

	/**
	 * 
	 */
	private static final int DEFAULT_HASHMAP_SIZE = 16;

	@Override
	protected String getUrl() {
		return "https://api.weixin.qq.com/sns/{method}";
	}

	@Override
	public <T extends Response> T doGet(Request<T> request) throws DefaultClientException {
		Class<String> responseType = String.class;
		Map<String, Object> urlVariables = new HashMap<>(DEFAULT_HASHMAP_SIZE);
		urlVariables.put("method", request.getMethodScope());
		String url = getUrl();
		if (null != request.getRequestMessage()) {
			url += "?" + request.getRequestMessage();
		}

		ResponseEntity<String> entity = getRestTemplate().getForEntity(url, responseType, urlVariables);
		if (entity.getStatusCode().is2xxSuccessful()) {
			return fromJson(entity.getBody(), request.getResponseClass());
		} else {
			throw new DefaultClientException();
		}
	}
}