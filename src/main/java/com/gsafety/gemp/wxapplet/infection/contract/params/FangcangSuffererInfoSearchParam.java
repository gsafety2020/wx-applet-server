package com.gsafety.gemp.wxapplet.infection.contract.params;

import com.gsafety.gemp.wxapplet.infection.contract.common.PageReq;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author dusiwei
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FangcangSuffererInfoSearchParam  extends PageReq implements Serializable {

    @ApiModelProperty(value = "姓名")
    private String suffererName;

    @ApiModelProperty(value = "身份证号")
    private String idNo;

    @ApiModelProperty(value = "联系电话")
    private String telephone;

    @ApiModelProperty(value = "状态：1在舱,2转院,3出院")
    private String suffererStatus;

    @ApiModelProperty(value = "入院时间,yyyy-MM-dd")
    private String admittedTime;

}
