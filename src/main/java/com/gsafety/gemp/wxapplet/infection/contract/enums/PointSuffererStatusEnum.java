package com.gsafety.gemp.wxapplet.infection.contract.enums;

import com.gsafety.gemp.wxapplet.infection.contract.dto.PointCommonDTO;

import java.util.ArrayList;
import java.util.List;

public enum PointSuffererStatusEnum {

    ALL("0","全部"),
    IN("1","当前在院"),
    OUT("2","治愈出院"),
    TURN_OUT("3","转出"),
    DEATH("4","死亡");

    private String code;
    private String name;
    PointSuffererStatusEnum(String code,String name){
        this.code = code;
        this.name = name;
    }

    public String getCode(){
        return this.code;
    }

    public static String getNameByCode(String code) {
        for (PointSuffererStatusEnum dataTypeEnum : values()) {
            if (dataTypeEnum.code.equals(code)){
                return dataTypeEnum.name;
            }
        }
        return null;
    }

    public static List<PointCommonDTO> getList(){
        List<PointCommonDTO> list=new ArrayList<>();
        PointCommonDTO pointCommonDTO=null;
        for (PointSuffererStatusEnum commonEnum : values()) {
            PointCommonDTO commonDTO=new PointCommonDTO();
            commonDTO.setCode(commonEnum.code);
            commonDTO.setName(commonEnum.name);
            list.add(commonDTO);
        }
        return list;
    }
}
