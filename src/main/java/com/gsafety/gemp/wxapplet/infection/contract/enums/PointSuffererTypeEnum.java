package com.gsafety.gemp.wxapplet.infection.contract.enums;

import com.gsafety.gemp.wxapplet.infection.contract.dto.PointCommonDTO;

import java.util.ArrayList;
import java.util.List;

public enum PointSuffererTypeEnum {

    ALL("0","全部"),
    DIAGNOSIS("1","确诊"),
    SUSPECT("2","疑似");

    private String code;
    private String name;
    PointSuffererTypeEnum(String code,String name){
        this.code = code;
        this.name = name;
    }

    public static String getNameByCode(String code) {
        for (PointSuffererTypeEnum dataTypeEnum : values()) {
            if (dataTypeEnum.code.equals(code)){
                return dataTypeEnum.name;
            }
        }
        return null;
    }

    public static List<PointCommonDTO> getList(){
        List<PointCommonDTO> list=new ArrayList<>();
        PointCommonDTO pointCommonDTO=null;
        for (PointSuffererTypeEnum commonEnum : values()) {
            PointCommonDTO commonDTO=new PointCommonDTO();
            commonDTO.setCode(commonEnum.code);
            commonDTO.setName(commonEnum.name);
            list.add(commonDTO);
        }
        return list;
    }
}
