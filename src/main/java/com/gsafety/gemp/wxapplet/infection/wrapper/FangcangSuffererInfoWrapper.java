package com.gsafety.gemp.wxapplet.infection.wrapper;

import com.gsafety.gemp.wxapplet.infection.contract.dto.FangcangSuffererInfoDTO;
import com.gsafety.gemp.wxapplet.infection.contract.enums.FangcangSuffererStatusEnum;
import com.gsafety.gemp.wxapplet.infection.dao.po.FangcangSuffererInfoAllPO;
import com.gsafety.gemp.wxapplet.infection.dao.po.FangcangSuffererInfoPO;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dusiwei
 */
public class FangcangSuffererInfoWrapper {

    private FangcangSuffererInfoWrapper() {
        super();
    }

    public static FangcangSuffererInfoDTO toDTO(FangcangSuffererInfoPO po) {
        FangcangSuffererInfoDTO dto = new FangcangSuffererInfoDTO();
        BeanUtils.copyProperties(po, dto);
        if (StringUtils.isNotEmpty(po.getSuffererStatus())) {
            dto.setSuffererStatusName(FangcangSuffererStatusEnum.getNameByCode(po.getSuffererStatus()));
        }
        return dto;
    }

    public static List<FangcangSuffererInfoDTO> toDTOList(List<FangcangSuffererInfoPO> records) {
        List<FangcangSuffererInfoDTO> list = new ArrayList<>();
        records.stream().forEach(record -> list.add(toDTO(record)));
        return list;
    }

    public static Page<FangcangSuffererInfoDTO> toPageDTO(Page<FangcangSuffererInfoPO> page){
        List<FangcangSuffererInfoDTO> content = toDTOList(page.getContent());
        return new PageImpl<>(content, page.getPageable(), page.getTotalElements());
    }

    public static FangcangSuffererInfoDTO allToDTO(FangcangSuffererInfoAllPO po) {
        FangcangSuffererInfoDTO dto = new FangcangSuffererInfoDTO();
        BeanUtils.copyProperties(po, dto);
        if (StringUtils.isNotEmpty(po.getSuffererStatus())) {
            dto.setSuffererStatusName(FangcangSuffererStatusEnum.getNameByCode(po.getSuffererStatus()));
        }
        return dto;
    }

    public static List<FangcangSuffererInfoDTO> allToDTOList(List<FangcangSuffererInfoAllPO> records) {
        List<FangcangSuffererInfoDTO> list = new ArrayList<>();
        records.stream().forEach(record -> list.add(allToDTO(record)));
        return list;
    }

    public static Page<FangcangSuffererInfoDTO> allToPageDTO(Page<FangcangSuffererInfoAllPO> page){
        List<FangcangSuffererInfoDTO> content = allToDTOList(page.getContent());
        return new PageImpl<>(content, page.getPageable(), page.getTotalElements());
    }
}
