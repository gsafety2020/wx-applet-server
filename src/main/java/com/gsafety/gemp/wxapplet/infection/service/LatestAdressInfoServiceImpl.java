package com.gsafety.gemp.wxapplet.infection.service;

import cn.hutool.core.collection.CollectionUtil;
import com.gsafety.gemp.common.result.Result;
import com.gsafety.gemp.wxapplet.infection.contract.iface.LatestAdressInfoService;
import com.gsafety.gemp.wxapplet.infection.dao.*;
import com.gsafety.gemp.wxapplet.infection.dao.po.*;
import com.gsafety.gemp.wxapplet.utils.UUIDUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class LatestAdressInfoServiceImpl implements LatestAdressInfoService {

    @Autowired
    private CdcSuffererInfoDao cdcSuffererInfoDao;
    @Autowired
    private SuffererInfoDao suffererInfoDao;
    @Autowired
    private LatestAdressInfoDao latestAdressInfoDao;
    @Autowired
    private FangcangSuffererInfoDao fangcangSuffererInfoDao;
    @Autowired
    private PointHospitalInfoDao pointHospitalInfoDao;

    /**
     * 详情查询时修改地址和时间
     * @param idNo
     */
    @Override
    @Transactional
    public void updateAdressInfo(String idNo){
//        if(StringUtils.isNotBlank(idNo)){
//            LatestAdressInfoPO po = latestAdressInfoDao.findByIdNo(idNo);
//            if(null != po) {
//                Date date = new Date();
//                po.setUpdateTime(date);
//                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                //定点
//                List<PointHospitalInfoPO> pointHospitalInfoPOList = pointHospitalInfoDao.findAllByIdNoOrderByMoveIntoTimeDesc(idNo);
//                PointHospitalInfoPO pointHospitalInfoPO = null;
//                if(CollectionUtil.isNotEmpty(pointHospitalInfoPOList)){
//                    pointHospitalInfoPO = pointHospitalInfoPOList.get(0);
//                }
//                //方舱
//                List<FangcangSuffererInfoPO> fangcangSuffererInfoPOList = fangcangSuffererInfoDao.findAllByIdNoOrderByAdmittedTimeDesc(idNo);
//                FangcangSuffererInfoPO fangcangSuffererInfoPO = null;
//                if (CollectionUtil.isNotEmpty(fangcangSuffererInfoPOList)) {
//                    fangcangSuffererInfoPO = fangcangSuffererInfoPOList.get(0);
//                }
//                //隔离点
//                SuffererInfoPO suffererInfoPO = null;
//                List<SuffererInfoPO> suffererInfoPOList = suffererInfoDao.findAllBySuffererCardOrderByMoveIntoTimeDesc(idNo);
//                if (CollectionUtil.isNotEmpty(suffererInfoPOList)) {
//                    suffererInfoPO = suffererInfoPOList.get(0);
//                }
//                //三处位置都不存在
//                if (null == suffererInfoPO && null == fangcangSuffererInfoPO && null == pointHospitalInfoPO) {
//                    po.setAdress("");
//                    po.setDateTime(null);
//                    latestAdressInfoDao.saveAndFlush(po);
//                    return;
//                }
//                // 1:定点；2：隔离点；3：方舱
//                //判断定点医院数据
//                if (null != pointHospitalInfoPO) {
//                    //判断治愈出院时间
//                    Date dischargeTime = pointHospitalInfoPO.getDischargeTime();
//                    //如果治愈出院时间为空表示其还在医院,则记录数据
//                    if (null == dischargeTime) {
//                        po.setAdress(pointHospitalInfoPO.getHospitalName());
//                        Date moveIntoTime = pointHospitalInfoPO.getMoveIntoTime();
//                        if (null != moveIntoTime) {
//                            po.setDateTime(sdf.format(moveIntoTime));
//                        } else {
//                            po.setDateTime(null);
//                        }
//                        latestAdressInfoDao.saveAndFlush(po);
//                        return;
//                    }
//                }
//                //判断方舱医院数据
//                if (null != fangcangSuffererInfoPO) { //治愈出院时间不为空则表示出院，则查询下一个方舱数据
//                    String status = fangcangSuffererInfoPO.getSuffererStatus();
//                    //表明在方舱医院
//                    if ("1".equals(status)) {
//                        po.setAdress(fangcangSuffererInfoPO.getHospitalName());
//                        String admittedTime = fangcangSuffererInfoPO.getAdmittedTime();
//                        if(StringUtils.isNotBlank(admittedTime)){
//                            String newAdmittedTime = admittedTime.replaceAll(" ", "");
//                            if(newAdmittedTime.length() < 9){
//                                newAdmittedTime = newAdmittedTime.substring(0,4)+"-"+newAdmittedTime.substring(4,6)+"-"+newAdmittedTime.substring(6,8);
//                            }
//                            po.setDateTime(newAdmittedTime + " 00:00:00");
//                        }else{
//                            po.setDateTime("");
//                        }
//                        latestAdressInfoDao.saveAndFlush(po);
//                        return;
//                    }
//                }
//                //判断隔离点数据
//                if (null != suffererInfoPO) {
//                    Date turnOutTime = suffererInfoPO.getTurnOutTime();
//                    //如果隔离点转出时间为空,表明其还在隔离点
//                    if (null == turnOutTime) {
//                        po.setAdress(suffererInfoPO.getCurSiteName());
//                        Date moveIntoTime = suffererInfoPO.getMoveIntoTime();
//                        if (null != moveIntoTime) {
//                            po.setDateTime(sdf.format(moveIntoTime));
//                        } else {
//                            po.setDateTime(null);
//                        }
//                    } else { //隔离点转出时间不为空,表明其已经转出
//                        po.setAdress(suffererInfoPO.getTurnOutToPlace());
//                        po.setDateTime(null);
//                    }
//                    latestAdressInfoDao.saveAndFlush(po);
//                    return;
//                }
//            }
//        }
    }

    private String getCurrentFormatDate(){
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(calendar.DATE, -1);
        date = calendar.getTime();
        SimpleDateFormat format= new SimpleDateFormat("yyyy-MM-dd");
        String dateString = format.format(date);
        return dateString;
    }


    /**
     *
     * @param prevDate
     * @return
     */
    @Override
    @Transactional
    public Result<String> excuteDataCompare(String prevDate){
//        List<CdcSuffererInfo> cdcSuffererInfoList = cdcSuffererInfoDao.findAllByDataDate(prevDate);
//        Date date = new Date();
//        LatestAdressInfoPO po;
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        if(CollectionUtil.isNotEmpty(cdcSuffererInfoList)){
//            for(CdcSuffererInfo cdcInfo : cdcSuffererInfoList){
//                String idNo = cdcInfo.getIdNo();
//                if(StringUtils.isNotBlank(idNo)) {
//                    po = latestAdressInfoDao.findByIdNo(idNo);
//                    if (null == po) {
//                        po = new LatestAdressInfoPO();
//                        po.setId(UUIDUtils.getUUID());
//                    }
//                    po.setIdNo(idNo);
//                    po.setUpdateTime(date);
//                    //定点
//                    List<PointHospitalInfoPO> pointHospitalInfoPOList = pointHospitalInfoDao.findAllByIdNoOrderByMoveIntoTimeDesc(idNo);
//                    PointHospitalInfoPO pointHospitalInfoPO = null;
//                    if(CollectionUtil.isNotEmpty(pointHospitalInfoPOList)){
//                        pointHospitalInfoPO = pointHospitalInfoPOList.get(0);
//                    }
//                    //方舱
//                    List<FangcangSuffererInfoPO> fangcangSuffererInfoPOList = fangcangSuffererInfoDao.findAllByIdNoOrderByAdmittedTimeDesc(idNo);
//                    FangcangSuffererInfoPO fangcangSuffererInfoPO = null;
//                    if (CollectionUtil.isNotEmpty(fangcangSuffererInfoPOList)) {
//                        fangcangSuffererInfoPO = fangcangSuffererInfoPOList.get(0);
//                    }
//                    //隔离点
//                    SuffererInfoPO suffererInfoPO = null;
//                    List<SuffererInfoPO> suffererInfoPOList = suffererInfoDao.findAllBySuffererCardOrderByMoveIntoTimeDesc(idNo);
//                    if (CollectionUtil.isNotEmpty(suffererInfoPOList)) {
//                        suffererInfoPO = suffererInfoPOList.get(0);
//                    }
//                    //三处位置都不存在
//                    if (null == suffererInfoPO && null == fangcangSuffererInfoPO && null == pointHospitalInfoPO) {
//                        po.setAdress("");
//                        po.setDateTime(null);
//                        latestAdressInfoDao.saveAndFlush(po);
//                        continue;
//                    }
//                    // 1:定点；2：隔离点；3：方舱
//                    //判断定点医院数据
//                    if (null != pointHospitalInfoPO) {
//                        //判断治愈出院时间
//                        Date dischargeTime = pointHospitalInfoPO.getDischargeTime();
//                        //如果治愈出院时间为空表示其还在医院,则记录数据
//                        if (null == dischargeTime) {
//                            po.setAdress(pointHospitalInfoPO.getHospitalName());
//                            Date moveIntoTime = pointHospitalInfoPO.getMoveIntoTime();
//                            if (null != moveIntoTime) {
//                                po.setDateTime(sdf.format(moveIntoTime));
//                            } else {
//                                po.setDateTime(null);
//                            }
//                            latestAdressInfoDao.saveAndFlush(po);
//                            continue;
//                        }
//                    }
//                    //判断方舱医院数据
//                    if (null != fangcangSuffererInfoPO) { //治愈出院时间不为空则表示出院，则查询下一个方舱数据
//                        String status = fangcangSuffererInfoPO.getSuffererStatus();
//                        //表明在方舱医院
//                        if ("1".equals(status)) {
//                            po.setAdress(fangcangSuffererInfoPO.getHospitalName());
//                            String admittedTime = fangcangSuffererInfoPO.getAdmittedTime();
//                            if(StringUtils.isNotBlank(admittedTime)){
//                                String newAdmittedTime = admittedTime.replaceAll(" ", "");
//                                if(newAdmittedTime.length() < 9){
//                                    newAdmittedTime = newAdmittedTime.substring(0,4)+"-"+newAdmittedTime.substring(4,6)+"-"+newAdmittedTime.substring(6,8);
//                                }
//                                po.setDateTime(newAdmittedTime + " 00:00:00");
//                            }else{
//                                po.setDateTime("");
//                            }
//                            latestAdressInfoDao.saveAndFlush(po);
//                            continue;
//                        }
//                    }
//                    //判断隔离点数据
//                    if (null != suffererInfoPO) {
//                        Date turnOutTime = suffererInfoPO.getTurnOutTime();
//                        //如果隔离点转出时间为空,表明其还在隔离点
//                        if (null == turnOutTime) {
//                            po.setAdress(suffererInfoPO.getCurSiteName());
//                            Date moveIntoTime = suffererInfoPO.getMoveIntoTime();
//                            if (null != moveIntoTime) {
//                                po.setDateTime(sdf.format(moveIntoTime));
//                            } else {
//                                po.setDateTime(null);
//                            }
//                        } else { //隔离点转出时间不为空,表明其已经转出
//                            po.setAdress(suffererInfoPO.getTurnOutToPlace());
//                            po.setDateTime(null);
//                        }
//                        latestAdressInfoDao.saveAndFlush(po);
//                        continue;
//                    }
//                }else{
//                    continue;
//                }
//            }
//        }
        return Result.success("操作成功");
    }




}
