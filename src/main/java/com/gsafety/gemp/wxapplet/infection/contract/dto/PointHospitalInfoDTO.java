package com.gsafety.gemp.wxapplet.infection.contract.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PointHospitalInfoDTO {

    @Id
    @Column(name = "id")
    @ApiModelProperty(value = "主键 身份证+yyyyddmm")
    private String id;

    /**患者姓名*/
    @Column(name = "SUFFERER_NAME")
    @ApiModelProperty(value = "患者姓名")
    private String suffererName;

    /**性别(0-女，1-男)*/
    @Column(name = "sex")
    @ApiModelProperty(value = "性别(0-女，1-男)")
    private String gender;

    /**年龄*/
    @Column(name = "age")
    @ApiModelProperty(value = "年龄")
    private String age;

    /**身份证号码*/
    @Column(name = "idNo")
    @ApiModelProperty(value = "身份证号码")
    private String idNo;

    /**地址*/
    @ApiModelProperty(value = "地址")
    private String detailed_address;

    /**联系电话*/
    @ApiModelProperty(value = "联系电话")
    private String telphone;

    /**入住时间*/
    @ApiModelProperty(value = "入住时间")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date moveIntoTime;

    /**类型(1-确诊，2-疑似)*/
    @ApiModelProperty(value = "类型(1-确诊，2-疑似)")
    private String suffererType;

    /**病情(1-重症，2-危重，99-其他)*/
    @ApiModelProperty(value = "病情(1-重症，2-危重，99-其他)")
    private String illnessState;

    /**状态(1-当前在院，2-治愈出院，3-转出，4-死亡)*/
    @ApiModelProperty(value = "状态(1-当前在院，2-治愈出院，3-转出，4-死亡)")
    private String suffererStatus;

    /** 治愈出院时间**/
    @ApiModelProperty(value = "治愈出院时间")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date dischargeTime;

    /** 死亡时间**/
    @ApiModelProperty(value = "死亡时间")
    private Date deathDate;

    /** 医院名称**/
    @ApiModelProperty(value = "医院名称")
    private String hospitalName;

    /** 创建时间**/
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /** 更新时间**/
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;
}
