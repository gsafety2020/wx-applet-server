package com.gsafety.gemp.wxapplet.infection.dao;

import com.gsafety.gemp.wxapplet.infection.dao.po.FangcangSuffererInfoPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author dusiwei
 */
public interface FangcangSuffererInfoDao extends JpaRepository<FangcangSuffererInfoPO, String>,
        JpaSpecificationExecutor<FangcangSuffererInfoPO> {
    FangcangSuffererInfoPO findByIdNo(String idoNo);

    List<FangcangSuffererInfoPO> findAllByIdNoOrderByAdmittedTimeDesc(String idoNo);

    List<FangcangSuffererInfoPO> findAllByIdNoOrderByDataTimeDesc(String idoNo);

    List<FangcangSuffererInfoPO> findBySuffererNameOrderByCreateTimeDesc(String suffererName);

    List<FangcangSuffererInfoPO> findByDataTime(String dataDate);

}

