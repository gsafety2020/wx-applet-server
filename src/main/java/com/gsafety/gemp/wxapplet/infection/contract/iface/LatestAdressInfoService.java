package com.gsafety.gemp.wxapplet.infection.contract.iface;

import com.gsafety.gemp.common.result.Result;

public interface LatestAdressInfoService {

    void updateAdressInfo(String idNo);

    Result<String> excuteDataCompare(String prevDate);
}
