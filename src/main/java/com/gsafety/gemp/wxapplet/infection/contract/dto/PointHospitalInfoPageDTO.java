package com.gsafety.gemp.wxapplet.infection.contract.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PointHospitalInfoPageDTO {

    @ApiModelProperty(value= "患者姓名")
    private String suffererName;

    @ApiModelProperty(value = "身份证号")
    private String idNo;

    @ApiModelProperty(value = "患者电话")
    private String telphone;

    @ApiModelProperty(value = "类型")
    private String suffererType;

    @ApiModelProperty(value = "病情")
    private String illnessState;

    @ApiModelProperty(value = "状态")
    private String suffererStatus;

    @ApiModelProperty(value = "页码,从0开始")
    private Integer page;

    @ApiModelProperty(value = "每页行数")
    private Integer size;
}
