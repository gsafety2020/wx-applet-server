package com.gsafety.gemp.wxapplet.infection.service.spec;

import com.gsafety.gemp.wxapplet.infection.dao.po.CdcSuffererInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;

import java.util.List;

public class CdcSuffererSpecifications {

    private CdcSuffererSpecifications() {
        super();
    }

    public static final String FIELD_IMPORT_DATE = "importDate";
    public static final String FIELD_DEATH_DATE = "deadthDate";
    public static final String FIELD_CASE_TYPE = "caseType";

    public static Specification<CdcSuffererInfo> importDateEqual(String importDate) {
        if (StringUtils.isEmpty(importDate)) {
            return null;
        }
        return (root, query, cb) -> cb.equal(root.get(FIELD_IMPORT_DATE),importDate);
    }

    public static Specification<CdcSuffererInfo> deathDateNotNull() {
        return (root, query, cb) -> root.get(FIELD_DEATH_DATE).isNotNull();
    }

    public static Specification<CdcSuffererInfo> deathDateNotEmpty() {
        return (root, query, cb) -> cb.not(cb.equal(root.get(FIELD_DEATH_DATE),""));
    }

    public static Specification<CdcSuffererInfo> caseTypeIn(List<String> types) {
        if (CollectionUtils.isEmpty(types)) {
            return null;
        }
        return (root, query, cb) -> root.get(FIELD_CASE_TYPE).in(types);
    }

}
