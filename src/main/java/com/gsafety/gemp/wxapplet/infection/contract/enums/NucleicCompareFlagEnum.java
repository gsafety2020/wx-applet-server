package com.gsafety.gemp.wxapplet.infection.contract.enums;

import com.gsafety.gemp.wxapplet.infection.contract.dto.PointCommonDTO;

import java.util.ArrayList;
import java.util.List;

public enum NucleicCompareFlagEnum {
    COMPARE_FLAG_0("0","等于"),
    COMPARE_FLAG_1("1","小于"),
    COMPARE_FLAG_2("2","大于");

    private String code;
    private String name;
    NucleicCompareFlagEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public String getCode(){
        return this.code;
    }

    public static String getNameByCode(String code) {
        for (NucleicCompareFlagEnum dataTypeEnum : values()) {
            if (dataTypeEnum.code.equals(code)){
                return dataTypeEnum.name;
            }
        }
        return null;
    }

    public static List<PointCommonDTO> getList(){
        List<PointCommonDTO> list=new ArrayList<>();
        PointCommonDTO pointCommonDTO=null;
        for (NucleicCompareFlagEnum commonEnum : values()) {
            PointCommonDTO commonDTO=new PointCommonDTO();
            commonDTO.setCode(commonEnum.code);
            commonDTO.setName(commonEnum.name);
            list.add(commonDTO);
        }
        return list;
    }
}
