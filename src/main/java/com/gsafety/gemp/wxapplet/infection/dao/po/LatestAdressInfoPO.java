package com.gsafety.gemp.wxapplet.infection.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "latest_adress_info")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LatestAdressInfoPO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "ID_NO")
    private String idNo;

    @Column(name = "ADRESS")
    private String adress;

    @Column(name = "DATE_TIME")
    private String dateTime;

    @Column(name = "UPDATE_TIME")
    private Date updateTime;
}
