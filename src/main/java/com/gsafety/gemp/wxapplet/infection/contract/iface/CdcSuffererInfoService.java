package com.gsafety.gemp.wxapplet.infection.contract.iface;

import com.gsafety.gemp.wxapplet.healthcode.contract.dto.Result;
import com.gsafety.gemp.wxapplet.infection.contract.dto.CdcSuffererInfoDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.CdcSuffererInfoPageDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface CdcSuffererInfoService {


    Result importExcel(MultipartFile file,String dataDate);

    void bakYesterdayData();

    /**
     * 通过关键字查询患者姓名/手机号/身份证，返回匹配的记录数据
     *
     *
     * @return
     */
    Page<CdcSuffererInfoDTO> searchWebSuffererInfo(CdcSuffererInfoPageDTO dto, PageRequest pageRequest);


    /**
     * 导出excel
     * @param response
     * @param request
     * @throws Exception
     */
    void export(HttpServletResponse response, HttpServletRequest request) throws Exception;

    /**
     * 数据是否存在
     * @param id
     * @return
     */
    boolean existsById(String id);

    /**
     * 根据id查询
     * @param id
     * @return
     */
    CdcSuffererInfoDTO findOne(String id);
}
