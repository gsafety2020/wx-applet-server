package com.gsafety.gemp.wxapplet.infection.quartz;

import com.gsafety.gemp.wxapplet.infection.contract.iface.CloseContactInfoService;
import com.gsafety.gemp.wxapplet.infection.contract.iface.TraceService;
import com.gsafety.gemp.wxapplet.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Calendar;

@Configuration
@EnableScheduling
@Slf4j
public class CdcSuffererQuartz {


    @Autowired
    private CloseContactInfoService closeContactInfoService;

    @Autowired
    private TraceService traceService;

    /**
     * 每个小时更新密接人员当前位置
     */
//    @Scheduled(cron = "	0 0 * * * ? ")
    private void updateCurLocation(){
        log.info("密接人员当前位置更新任务开始 [{}]", Calendar.getInstance().getTime().toString());
        Long beginTime = System.currentTimeMillis();
        String yesterday = DateUtils.getYesterdayString();
        closeContactInfoService.syncCurLocation(yesterday);
        Long endTime = System.currentTimeMillis();
        log.info("密接人员当前位置更新任务结束 [{}]",Calendar.getInstance().getTime().toString());
        log.info("总计花费时间：[time:{}]",endTime-beginTime);

    }

    /**
     * 每个小时生成其他数据源的流水
     */
//    @Scheduled(cron = "	0 0 1 * * ? ")
    private void generateOtherTrace(){
        log.info("方舱定点医院流水生成任务开始 [{}]", Calendar.getInstance().getTime().toString());
        Long beginTime = System.currentTimeMillis();
//        String yesterday = DateUtils.getYesterdayString();
        String yesterday = DateUtils.getBeforeYesterdayString();
        traceService.generatePointTrace(yesterday);
        traceService.generateFangCangTrace(yesterday);
        traceService.generateSiteTrace(yesterday);
        Long endTime = System.currentTimeMillis();
        log.info("方舱定点医院流水生成任务结束 [{}]",Calendar.getInstance().getTime().toString());
        log.info("总计花费时间：[time:{}]",endTime-beginTime);
    }



    @Scheduled(cron = "	0 0 */4 * * ? ")
    private void genrateCloseContactIdNo(){
        log.info("生成密接人员身份证号任务开始 [{}]", Calendar.getInstance().getTime().toString());
        Long beginTime = System.currentTimeMillis();
        closeContactInfoService.genrateCloseContactIdNo();
        Long endTime = System.currentTimeMillis();
        log.info("生成密接人员身份证号任务结束 [{}]",Calendar.getInstance().getTime().toString());
        log.info("总计花费时间：[time:{}]",endTime-beginTime);
    }


}
