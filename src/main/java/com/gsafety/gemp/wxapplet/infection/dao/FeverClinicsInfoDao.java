package com.gsafety.gemp.wxapplet.infection.dao;

import com.gsafety.gemp.wxapplet.infection.dao.po.FeverClinicsInfoPO;
import com.gsafety.gemp.wxapplet.infection.dao.po.PointHospitalInfoPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface FeverClinicsInfoDao  extends JpaRepository<FeverClinicsInfoPO, String>,
        JpaSpecificationExecutor<FeverClinicsInfoPO> {

    List<FeverClinicsInfoPO> findByDataDate(String dataDate);

    List<FeverClinicsInfoPO> findByIdNoOrderByDataDateDesc(String idNo);

    List<FeverClinicsInfoPO> findBySuffererName(String suffererName);

}
