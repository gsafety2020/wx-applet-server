package com.gsafety.gemp.wxapplet.infection.wrapper;

import com.gsafety.gemp.wxapplet.infection.contract.dto.PointHospitalInfoDTO;
import com.gsafety.gemp.wxapplet.infection.contract.enums.PointGenderEnum;
import com.gsafety.gemp.wxapplet.infection.contract.enums.PointIllnessStateEnum;
import com.gsafety.gemp.wxapplet.infection.contract.enums.PointSuffererStatusEnum;
import com.gsafety.gemp.wxapplet.infection.contract.enums.PointSuffererTypeEnum;
import com.gsafety.gemp.wxapplet.infection.dao.po.PointHospitalInfoPO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.ArrayList;
import java.util.List;

public class PointHospitalInfoWrapper {

    public static PointHospitalInfoDTO toDTO(PointHospitalInfoPO po) {
        PointHospitalInfoDTO dto = new PointHospitalInfoDTO();
        BeanUtils.copyProperties(po, dto);
        if(StringUtils.isNotBlank(po.getSuffererType())){
            dto.setSuffererType(PointSuffererTypeEnum.getNameByCode(po.getSuffererType()));
        }
        if(StringUtils.isNotBlank(po.getIllnessState())){
            dto.setIllnessState(PointIllnessStateEnum.getNameByCode(po.getIllnessState()));
        }
        if(StringUtils.isNotBlank(po.getSuffererStatus())){
            dto.setSuffererStatus(PointSuffererStatusEnum.getNameByCode(po.getSuffererStatus()));
        }
        if(StringUtils.isNotBlank(po.getGender())){
            dto.setGender(PointGenderEnum.getNameByCode(po.getGender()));
        }
        return dto;
    }

    public static List<PointHospitalInfoDTO> toDTOList(List<PointHospitalInfoPO> records) {
        List<PointHospitalInfoDTO> list = new ArrayList<>();
        records.stream().forEach(record -> list.add(toDTO(record)));
        return list;
    }

    public static Page<PointHospitalInfoDTO> toPageDTO(Page<PointHospitalInfoPO> page){
        List<PointHospitalInfoDTO> content = toDTOList(page.getContent());
        return new PageImpl<>(content, page.getPageable(), page.getTotalElements());
    }

}
