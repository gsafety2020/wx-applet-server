/**
 * 
 */
package com.gsafety.gemp.wxapplet.healthcode.specification;

import com.gsafety.gemp.wxapplet.healthcode.dao.po.PestHealthCodePO;
import com.gsafety.gemp.wxapplet.healthcode.dao.po.PestWechatUserPO;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.jpa.domain.Specification;

/**
 * 该类用于微信用户信息列表搜索
 *
 * @author wangwenhai
 *
 * @since 2020年2月27日 下午3:38:20
 *
 */
public class PestWechatUserSpecifications {

	/**
	 * 昵称字段
	 */
	public static final String FIELD_NICK_NAME = "nickName";





	private PestWechatUserSpecifications() {
		super();
	}

	/**
	 * 
	 * nickNameLike 昵称搜索条件
	 *
	 * @author wangwenhai
	 * 
	 * @since 2020年2月27日 下午3:50:16
	 *
	 * @param nickName
	 * @return
	 */
	public static Specification<PestWechatUserPO> nickNameLike(String nickName) {

		return (root, query, builer) ->{
			if(StringUtils.isEmpty(nickName)){
				return null;
			}
			return builer.like(root.get(FIELD_NICK_NAME), "%" + nickName + "%");
		};
	}





}
