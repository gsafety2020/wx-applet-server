package com.gsafety.gemp.wxapplet.infection.contract.iface;

import com.gsafety.gemp.wxapplet.infection.contract.dto.PointHospitalInfoDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.PointHospitalInfoPageDTO;
import com.gsafety.gemp.wxapplet.infection.dao.po.PointHospitalInfoPO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public interface PointHospitalInfoService {

    Page<PointHospitalInfoDTO> search(PointHospitalInfoPageDTO dto,  PageRequest pageRequest);

    /**
     * 定点医院疫情人员详情
     * @param id
     * @return
     */
    PointHospitalInfoPO findOne(String id);
}
