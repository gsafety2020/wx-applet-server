package com.gsafety.gemp.wxapplet.healthcode.contract.service;

import java.util.List;

import com.gsafety.gemp.wxapplet.healthcode.contract.dto.CodeBasDistrictDTO;
import com.gsafety.gemp.wxapplet.healthcode.contract.dto.PestHealthCodeDTO;
import com.gsafety.gemp.wxapplet.healthcode.contract.dto.PestHealthCodeSaveDTO;
import com.gsafety.gemp.wxapplet.healthcode.contract.dto.PestWechatUserDTO;
import com.gsafety.gemp.wxapplet.healthcode.contract.dto.in.PestHealthCodeReportDTO;
import com.gsafety.gemp.wxapplet.healthcode.contract.params.DimQueryParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 
* @ClassName: PestHealthCodeService 
* @Description: 健康码服务层
* @author luoxiao
* @date 2020年2月21日 下午4:46:11 
*
 */
public interface PestHealthCodeService {

	/**
	 * 
	* @Title: getObjectByDistrictCodes 
	* @Description: 通过地区编码查询对象
	* @param @param districtCodes
	* @param @return    设定文件 
	* @return List<CodeBasDistrictDTO>    返回类型 
	* @throws
	 */
	List<CodeBasDistrictDTO> getObjectByDistrictCodes(List<String> districtCodes);
	
	/**
	 * 
	* @Title: getObjectByParentCode 
	* @Description: 根据父编码查询对象
	* @param @param districtCode
	* @param @return    设定文件 
	* @return List<CodeBasDistrictDTO>    返回类型 
	* @throws
	 */
	List<CodeBasDistrictDTO> getObjectByParentCode();
	
	/**
	 * 
	* @Title: saveUpdate 
	* @Description: 健康码信息填写
	* @param @param dto    设定文件 
	* @return void    返回类型 
	* @throws
	 */
	PestHealthCodeSaveDTO saveUpdate(PestHealthCodeReportDTO dto); 
	
	/**
	 * 
	* @Title: existData 
	* @Description: 通过healthCodeId查询用户在该地区得健康码
	* @param @param areaCode
	* @param @param sessionKey
	* @param @return    设定文件 
	* @return Boolean    返回类型 
	* @throws
	 */
	PestHealthCodeDTO getHealthCodeById(String healthCodeId);
	
	/**
	 * 
	* @Title: getEffectiveHealthCodeByOpenIdAndAreaCode 
	* @Description:通过openId和areaCode查询没有删除的健康码信息
	* @param @param openId
	* @param @param areaCode
	* @param @return    设定文件 
	* @return PestHealthCodeDTO    返回类型 
	* @throws
	 */
	PestHealthCodeDTO getEffectiveHealthCodeByOpenIdAndAreaCode(String openId,String areaCode);
	
	/**
	 * 
	* @Title: removeHealthCode 
	* @Description: 移除该用户在该地区的健康码
	* @param @param healthCodeId    设定文件 
	* @return void    返回类型 
	* @throws
	 */
	void removeHealthCode(String healthCodeId);

	/**
	 * 查询健康码列表
	 * @param queryParam
	 * @param sort
	 * @return
	 */
	Page<PestHealthCodeDTO> findHealthCodePage(DimQueryParam queryParam, Pageable sort);

	/**
	 * 保存健康码
	 * @param dto
	 */
    void save(PestHealthCodeDTO dto);


}
