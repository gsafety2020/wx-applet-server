package com.gsafety.gemp.wxapplet.infection.contract.iface;

import com.gsafety.gemp.wxapplet.infection.contract.dto.*;
import com.gsafety.gemp.wxapplet.infection.dao.po.NucleicDetectionInfoPO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface NucleicDetectionInfoService {

    Page<NucleicDetectionInfoDTO> search(NucleicDetectionInfoPageDTO dto, PageRequest pageRequest);

    /**
     * 核酸检测患者信息详情
     * @param id
     * @return
     */
    NucleicDetectionInfoPO findOne(String id);

    void deteCtionNumUpdate();

    List<PointCommonDTO> detectionNumList();
}
