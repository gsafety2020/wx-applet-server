package com.gsafety.gemp.wxapplet.infection.contract.enums;

import com.gsafety.gemp.wxapplet.infection.contract.dto.CaseTypeDTO;

import java.util.ArrayList;
import java.util.List;

public enum ClinicalSeverityEnum {

    TYPE_ALL("0","全部"),
    TYPE_NORMAL("1","普通型"),
    TYPE_LIGHT("2","轻型"),
    TYPE_CRITICAL("3","危重型"),
    TYPE_SEVERE("4","无症状感染者"),
    TYPE_ASYMPTOMATIC("5","重型"),
    TYPE_OTHER("6","其它");

    private String code;
    private String name;
    ClinicalSeverityEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static String getNameByCode(String code) {
        for (ClinicalSeverityEnum caseTypeEnum : values()) {
            if (caseTypeEnum.code.equals(code)){
                return caseTypeEnum.name;
            }
        }
        return null;
    }

    public static String getCodeByName(String name) {
        for (ClinicalSeverityEnum caseTypeEnum : values()) {
            if (caseTypeEnum.name.equals(name)){
                return caseTypeEnum.code;
            }
        }
        return null;
    }

    public static List<CaseTypeDTO> getList(){
        List<CaseTypeDTO> list=new ArrayList<>();
        CaseTypeDTO caseTypeDTO=null;
        for (ClinicalSeverityEnum caseTypeEnum : values()) {
            caseTypeDTO=new CaseTypeDTO();
            caseTypeDTO.setCode(Integer.parseInt(caseTypeEnum.code));
            caseTypeDTO.setName(caseTypeEnum.name);
            list.add(caseTypeDTO);
        }
        return list;
    }
}
