package com.gsafety.gemp.wxapplet.infection.contract.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("患者详情")
public class SuffererWebDetailDTO extends SuffererBaseDTO{
	  
    /** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = -3913161114785035316L;

	@JsonProperty("sufferer_type")
    @ApiModelProperty(value = "病人类型：1.密接人员；2.发热患者；3.疑似患者；4.确诊; 5.其他;")
    private Integer suffererType;
    
    @JsonProperty("sufferer_type_time")
    @ApiModelProperty(value = "患者类型时间记录")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd hh:mm")
    private Date suffererTypeTime;

    @JsonProperty("sufferer_status")
    @ApiModelProperty(value = "管理状态状态：1.隔离点在管（进入）；2.在院治疗（进入）；3.治愈出院（离开）；4.病亡（离开）；5.转出（离开）；6.其他；")
    private Integer suffererStatus;
    
    @JsonProperty("sufferer_status_time")
    @ApiModelProperty(value = "管理状态时间记录")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd hh:mm")
    private Date suffererStatusTime;

    @JsonProperty("sufferer_card")
    @ApiModelProperty(value = "身份证号")
    private String suffererCard;
    
    @JsonProperty("illness_state")
    @ApiModelProperty(value = "病情. 1.治愈；2死亡；3.危重 4.重症 5.其他")
    private Integer illnessState;
    
    @JsonProperty("illness_state_time")
    @ApiModelProperty(value = "病情情况时间记录")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd hh:mm")
    private Date illnessStateTime;
    
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd hh:mm")
	@ApiModelProperty(value = "病情时间")
	private Date illnessBeginTime;
    
    @JsonProperty("illness_name")
    @ApiModelProperty(value = "病情名称")
    private String  illnessName;
    
    @JsonProperty("sufferer_status_name")
    @ApiModelProperty(value = "管理状态名称")
    private String  suffererStatusName;
    
    @JsonProperty("sufferer_type_name")
    @ApiModelProperty(value = "患者类型名称")
    private String  suffererTypeName;

    @JsonProperty("cur_site_name")
    @ApiModelProperty(value = "当前位置")
    private String curSiteName;
    
    @JsonProperty("cur_site_id")
    @ApiModelProperty(value = "当前所在隔离点或医院ID")
    private String curSiteId;

    @JsonProperty("update_time")
    @ApiModelProperty(value = "更新时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date updateTime;

}
