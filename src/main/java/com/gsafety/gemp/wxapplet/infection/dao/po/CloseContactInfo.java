package com.gsafety.gemp.wxapplet.infection.dao.po;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "close_contact_info", schema = "wx_applet", catalog = "")
public class CloseContactInfo {
    private String id;
    private String suffererIdno;
    private String suffererName;
    private String ccName;
    private String ccSex;
    private Integer ccAge;
    private String ccRelation;
    private String ccTelphone;
    private String ccAddress;
    private String ccExposeTime;
    private String remark;
    private String dataDate;
    private String ccCurLocation;
    private String ccIdno;
    private Timestamp createTime;

    @Id
    @Column(name = "ID", nullable = false, length = 32)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "SUFFERER_IDNO", nullable = true, length = 20)
    public String getSuffererIdno() {
        return suffererIdno;
    }

    public void setSuffererIdno(String suffererIdno) {
        this.suffererIdno = suffererIdno;
    }

    @Basic
    @Column(name = "SUFFERER_NAME", nullable = true, length = 20)
    public String getSuffererName() {
        return suffererName;
    }

    public void setSuffererName(String suffererName) {
        this.suffererName = suffererName;
    }

    @Basic
    @Column(name = "CC_NAME", nullable = true, length = 20)
    public String getCcName() {
        return ccName;
    }

    public void setCcName(String ccName) {
        this.ccName = ccName;
    }

    @Basic
    @Column(name = "CC_SEX", nullable = true, length = 10)
    public String getCcSex() {
        return ccSex;
    }

    public void setCcSex(String ccSex) {
        this.ccSex = ccSex;
    }

    @Basic
    @Column(name = "CC_AGE", nullable = true)
    public Integer getCcAge() {
        return ccAge;
    }

    public void setCcAge(Integer ccAge) {
        this.ccAge = ccAge;
    }

    @Basic
    @Column(name = "CC_RELATION", nullable = true, length = 100)
    public String getCcRelation() {
        return ccRelation;
    }

    public void setCcRelation(String ccRelation) {
        this.ccRelation = ccRelation;
    }

    @Basic
    @Column(name = "CC_TELPHONE", nullable = true, length = 20)
    public String getCcTelphone() {
        return ccTelphone;
    }

    public void setCcTelphone(String ccTelphone) {
        this.ccTelphone = ccTelphone;
    }

    @Basic
    @Column(name = "CC_ADDRESS", nullable = true, length = 100)
    public String getCcAddress() {
        return ccAddress;
    }

    public void setCcAddress(String ccAddress) {
        this.ccAddress = ccAddress;
    }

    @Basic
    @Column(name = "CC_EXPOSE_TIME", nullable = true, length = 20)
    public String getCcExposeTime() {
        return ccExposeTime;
    }

    public void setCcExposeTime(String ccExposeTime) {
        this.ccExposeTime = ccExposeTime;
    }

    @Basic
    @Column(name = "REMARK", nullable = true, length = 255)
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Basic
    @Column(name = "DATA_DATE", nullable = true, length = 10)
    public String getDataDate() {
        return dataDate;
    }

    public void setDataDate(String dataDate) {
        this.dataDate = dataDate;
    }

    @Basic
    @Column(name = "CC_CUR_LOCATION", nullable = true, length = 255)
    public String getCcCurLocation() {
        return ccCurLocation;
    }

    public void setCcCurLocation(String ccCurLocation) {
        this.ccCurLocation = ccCurLocation;
    }

    @Basic
    @Column(name = "CC_IDNO", nullable = true, length = 30)
    public String getCcIdno() {
        return ccIdno;
    }

    public void setCcIdno(String ccIdno) {
        this.ccIdno = ccIdno;
    }

    @Basic
    @Column(name = "CREATE_TIME", nullable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CloseContactInfo that = (CloseContactInfo) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(suffererIdno, that.suffererIdno) &&
                Objects.equals(suffererName, that.suffererName) &&
                Objects.equals(ccName, that.ccName) &&
                Objects.equals(ccSex, that.ccSex) &&
                Objects.equals(ccAge, that.ccAge) &&
                Objects.equals(ccRelation, that.ccRelation) &&
                Objects.equals(ccTelphone, that.ccTelphone) &&
                Objects.equals(ccAddress, that.ccAddress) &&
                Objects.equals(ccExposeTime, that.ccExposeTime) &&
                Objects.equals(remark, that.remark) &&
                Objects.equals(dataDate, that.dataDate) &&
                Objects.equals(ccCurLocation, that.ccCurLocation) &&
                Objects.equals(createTime, that.createTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, suffererIdno, suffererName, ccName, ccSex, ccAge, ccRelation, ccTelphone, ccAddress, ccExposeTime, remark, dataDate, ccCurLocation, createTime);
    }
}
