package com.gsafety.gemp.wxapplet.infection.contract.enums;

import com.gsafety.gemp.wxapplet.utils.IntegerCodeEnum;
import lombok.Getter;
import org.apache.commons.lang.StringUtils;

/**
 * 患者状态
 *
 * @author liyanhong
 * @since 2020/2/27  11:12
 */
@Getter
public enum SuffererStatusEnum implements IntegerCodeEnum{

    Isolate(1, "隔离点在管"),
    InHospital(2, "在院治疗"),
    Cured(3, "治愈出院"),
    Death(4, "病亡"),
    Turnout(5, "转出"),
    Other(6, "其他");

    private Integer code;
    private String value;

    SuffererStatusEnum(Integer c, String v) {
        this.code = c;
        this.value = v;
    }

    public static String getValueByCode(Integer code) {

        for (SuffererStatusEnum en : SuffererStatusEnum.values()) {
            if (en.getCode().equals(code)) {
                return en.getValue();
            }
        }

        return StringUtils.EMPTY;
    }
}
