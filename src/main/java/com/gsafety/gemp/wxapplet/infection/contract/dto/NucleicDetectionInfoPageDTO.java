package com.gsafety.gemp.wxapplet.infection.contract.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NucleicDetectionInfoPageDTO {

    @ApiModelProperty(value= "患者姓名")
    private String suffererName;

    @ApiModelProperty(value = "身份证号")
    private String idNo;

    @ApiModelProperty(value = "送检日期")
    @Temporal(TemporalType.DATE)
    private Date sendSampleTime;

    @ApiModelProperty(value = "检测结果")
    private String detectionResult;

    @ApiModelProperty(value = "隔离点类型")
    private String isolationType;

    @ApiModelProperty(value = "检测次数")
    private String detectionNum;

    @ApiModelProperty(value = "比较标志")
    private String compareFlag;

    @ApiModelProperty(value = "页码,从0开始")
    private Integer page;

    @ApiModelProperty(value = "每页行数")
    private Integer size;
}
