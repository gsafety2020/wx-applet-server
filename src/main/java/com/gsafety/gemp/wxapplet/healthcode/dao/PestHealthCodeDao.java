package com.gsafety.gemp.wxapplet.healthcode.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gsafety.gemp.wxapplet.healthcode.dao.po.PestHealthCodePO;



/**
 * 
* @ClassName: PestHealthCodeDao 
* @Description: 健康码Dao层
* @author luoxiao
* @date 2020年2月21日 下午4:44:12 
*
 */
public interface PestHealthCodeDao extends JpaRepository<PestHealthCodePO, String>,
JpaSpecificationExecutor<PestHealthCodePO> {

	PestHealthCodePO findByHealthCodeId(String healthCodeId);
	
	PestHealthCodePO findByOpenIdAndAreaCodeAndIsDelete(String openId,String areaCode,String isDelete);
}
