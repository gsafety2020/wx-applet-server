package com.gsafety.gemp.wxapplet.healthcode.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gsafety.gemp.wxapplet.healthcode.dao.po.PestHealthAuthorizePO;


/**
 * 
* @ClassName: PestHealthAuthorizeDao 
* @Description: 扫码授权数据层
* @author luoxiao
* @date 2020年2月26日 下午5:23:28 
*
 */
public interface PestHealthAuthorizeDao extends JpaRepository<PestHealthAuthorizePO, String>,
JpaSpecificationExecutor<PestHealthAuthorizePO> {

	PestHealthAuthorizePO  findByDeviceIdAndIsDelete(String deviceId,String isDelete);
}
