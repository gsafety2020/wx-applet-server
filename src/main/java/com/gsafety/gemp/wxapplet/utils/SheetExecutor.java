package com.gsafety.gemp.wxapplet.utils;

import com.gsafety.gemp.common.excel.ExcelUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author dusiwei
 */
public class SheetExecutor<T> {

    private Class<T> clazz;

    public SheetExecutor(Class<T> clazz) {
        this.clazz = clazz;
    }

    public void setSheet(Sheet sheet, List<T> list) {
        setSheet(sheet,list,0,0);
    }

    public void setSheet(Sheet sheet, List<T> list,int start,int end) {
        if (org.apache.commons.collections4.CollectionUtils.isEmpty(list)) {
            return;
        }
        Map<Integer, String> excelTitleMap = ExcelUtils.getExcelTitleMap(sheet, start, end);

        Field[] fields = this.clazz.getDeclaredFields();
        Map<Integer, Field> fieldMap = ExcelUtils.getFieldMap(fields, excelTitleMap);
        if (null!=fieldMap && !fieldMap.isEmpty()) {
            for (int i = 0; i < list.size(); i++) {
                Row row = sheet.createRow(i + end + 1);
                Iterator<Map.Entry<Integer, Field>> iterator = fieldMap.entrySet().iterator();

                while(iterator.hasNext()) {
                    Map.Entry<Integer, Field> entry = iterator.next();
                    Integer colIndex = entry.getKey();
                    Field field = entry.getValue();
                    field.setAccessible(true);
                    Cell cell = row.createCell(colIndex);
                    T t = list.get(i);
                    Object object = null;
                    try {
                        object = field.get(t);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                    String value = String.valueOf(object == null ? "" : object);
                    cell.setCellValue(value);
                }
            }
        }
    }

}
