package com.gsafety.gemp.wxapplet.healthcode.contract.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.gsafety.gemp.wxapplet.healthcode.contract.dto.PestWechatUserDTO;
import com.gsafety.gemp.wxapplet.healthcode.contract.params.DimQueryParam;
import com.gsafety.gemp.wxapplet.healthcode.contract.dto.PestWechatUserDTO;
import com.gsafety.gemp.wxapplet.healthcode.controller.WechatAuthController.WechatUserinfo;
import com.gsafety.gemp.wxapplet.healthcode.dao.po.PestWechatUserPO;

/**
 * 
* @ClassName: PestWechatUserService 
* @Description: 微信用户服务层
* @author luoxiao
* @date 2020年2月26日 上午12:04:22 
*
 */
public interface PestWechatUserService {

	/**
	 * 
	* @Title: saveUpdate 
	* @Description: 保存用户
	* @param @param po    设定文件 
	* @return void    返回类型 
	* @throws
	 */
	void saveUpdate(PestWechatUserPO po);
	
	/**
	 * 
	* @Title: authorizeUser 
	* @Description: 用户授权
	* @param @param po    设定文件 
	* @return void    返回类型 
	* @throws
	 */
	void authorizeUser(PestWechatUserPO po,WechatUserinfo userInfo);
	
	/**
	 * 
	* @Title: getPestWechatUserById 
	* @Description: 通过主键获取详细信息
	* @param @param openId
	* @param @return    设定文件 
	* @return PestWechatUserPO    返回类型 
	* @throws
	 */
	PestWechatUserPO getPestWechatUserById(String openId);

	/**
	 * 查询微信用户信息
	 * @param queryParam
	 * @param pageable
	 * @return
	 */
	Page<PestWechatUserDTO> findWechatInfoPage(DimQueryParam queryParam, Pageable pageable);

	/**
	 *
	 * 同步用户登录信息到user
	 *
	 * @author yangbo
	 *
	 * @since 2020年2月27日 下午12:06:15
	 *
	 * @param dto
	 */
	void recordLogin(PestWechatUserDTO dto);
}
