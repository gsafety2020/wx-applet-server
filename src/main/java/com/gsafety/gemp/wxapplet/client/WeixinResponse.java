/**
 * 
 */
package com.gsafety.gemp.wxapplet.client;

/**
 * @author lenovo
 *
 */
public interface WeixinResponse {

	/**
	 * 返回错误码
	 * 
	 * @return
	 */
	public int getErrcode();

	/**
	 * 返回错误信息
	 * 
	 * @return
	 */
	public String getErrmsg();
}