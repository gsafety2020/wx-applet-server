package com.gsafety.gemp.wxapplet.infection.service;

import com.gsafety.gemp.wxapplet.infection.contract.iface.UserService;
import com.gsafety.gemp.wxapplet.infection.dao.SiteInfoDao;
import com.gsafety.gemp.wxapplet.infection.dao.UserDao;
import com.gsafety.gemp.wxapplet.infection.dao.po.UserPO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.transaction.Transactional;

/**
 * 病患追溯信息服务实现类
 * @author zxiao
 * @since 2020/2/27  10:44
 */
@Service("userService")
public class UserServiceImpl implements UserService {

	@Resource
	private UserDao userDao;

	@Override
	public UserPO findUser(String username) {
		return userDao.findByUsername(username);
	}

    @Override
	@Transactional(rollbackOn = Exception.class)
    public void updateToken(UserPO user) {
		userDao.save(user);
	}

	@Override
	public UserPO findById(String id) {
		return userDao.getOne(id);
	}

}
