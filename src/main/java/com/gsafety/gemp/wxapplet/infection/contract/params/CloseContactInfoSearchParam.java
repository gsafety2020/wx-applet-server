package com.gsafety.gemp.wxapplet.infection.contract.params;


import com.gsafety.gemp.wxapplet.infection.contract.common.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description="cdc新增病患查询条件")
public class CloseContactInfoSearchParam extends PageReq implements Serializable {
    @ApiModelProperty(value = "患者身份证")
    private String suffererIdNo;
//    @ApiModelProperty(value = "数据日期 yyyy-MM-dd")
//    private String dataDate;
}
