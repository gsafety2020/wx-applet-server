package com.gsafety.gemp.wxapplet.infection.contract.params;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description="cdc病患数据导入参数")
public class CdcSuffererImportParam {

    @ApiModelProperty(value = "数据日期 yyyyMMdd")
    private String dataDate;
}
