package com.gsafety.gemp.wxapplet.healthcode.factory;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.gsafety.gemp.wxapplet.healthcode.controller.WechatAuthController.WechatUserinfo;
import com.gsafety.gemp.wxapplet.utils.RedisUtils;

@Component
public class RedisCacheWechatUserManager implements WechatUserManager{

	@Autowired
	private RedisUtils resutUtils;
	
	@Value("${static.wechart.user.cache.timeout}")
	private Long timeout;
	
	private static final String[] INGORE_FIELD = new String[] { "openid", "sessionKey" };
	
	@Override
	public void setUser(String sessionKey, WechatUserinfo userInfo) {
		WechatUserinfo existsInfo =(WechatUserinfo)resutUtils.get(sessionKey);
		if(existsInfo != null){
			BeanUtils.copyProperties(userInfo, existsInfo, INGORE_FIELD);
			if(timeout != null){
				resutUtils.set(sessionKey, existsInfo, timeout);
			}else{
				resutUtils.set(sessionKey, existsInfo);
			}
		}else{
			if(timeout != null){
				resutUtils.set(sessionKey, userInfo, timeout);
			}else{
				resutUtils.set(sessionKey, userInfo);
			}
		}
	}

	@Override
	public WechatUserinfo getUser(String sessionKey) {
		WechatUserinfo userInfo =(WechatUserinfo)resutUtils.get(sessionKey);
		if(timeout != null){
			resutUtils.set(sessionKey, userInfo, timeout);
		}else{
			resutUtils.set(sessionKey, userInfo);
		}
		return userInfo;
	}

	@Override
	public String touchSessionKey(String nickname) {
		// TODO Auto-generated method stub
		return null;
	}

}
