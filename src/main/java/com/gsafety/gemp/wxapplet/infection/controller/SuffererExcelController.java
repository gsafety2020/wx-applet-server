package com.gsafety.gemp.wxapplet.infection.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.gsafety.gemp.common.result.Result;
import com.gsafety.gemp.wxapplet.infection.contract.dto.SuffererInfoWebPageDTO;
import com.gsafety.gemp.wxapplet.infection.contract.iface.SuffererExcelService;
import com.gsafety.gemp.wxapplet.infection.contract.iface.SuffererInfoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;


@Api(value = "统计局数据导入导出", tags = {"统计局数据导入导出"})
@RestController
@RequestMapping("/infectionweb")
@Slf4j
public class SuffererExcelController {

    @Autowired
    private SuffererInfoService suffererInfoService;
    
    @Autowired
    private SuffererExcelService suffererExcelService;

    @ApiOperation(value = "患者信息导出")
    @PostMapping(value = "/sufferer/export/v1")
    public void infoExport(@RequestBody SuffererInfoWebPageDTO dto, HttpServletResponse response, HttpServletRequest request) throws Exception {
        suffererInfoService.export(dto,response,request);
    }

    @ApiOperation(value = "患者信息导入")
    @PostMapping(value = "/sufferer/import/v1", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    Result<String> importAsylumAreaInfo(@RequestParam(value = "file") MultipartFile file) {
         return suffererInfoService.importExcel(file);
    }
    
    @ApiOperation(value = "统计局数据对比")
    @PostMapping(value = "/sufferer/trace/v1")
    public Result<String> compare() {
    	try{
    		suffererExcelService.compareBakSuffererInfoTrace();
    		return Result.success("数据对比成功,流水数据已生成!");
    	}catch(Exception e){
    		log.error("数据对比失败!",e);
    		return Result.error("数据对比失败!");
    	}
   }
    
}
