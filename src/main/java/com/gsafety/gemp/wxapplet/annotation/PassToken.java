package com.gsafety.gemp.wxapplet.annotation;

import java.lang.annotation.*;

/**
 * 用来跳过验证的
 * 修改人： <br>
 * 修改时间： <br>
 * 修改备注： <br>
 * </p>
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface PassToken {

}
