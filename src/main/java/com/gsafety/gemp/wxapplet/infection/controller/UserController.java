package com.gsafety.gemp.wxapplet.infection.controller;

import cn.hutool.core.util.ObjectUtil;
import com.gsafety.gemp.wxapplet.annotation.PassToken;
import com.gsafety.gemp.wxapplet.healthcode.contract.dto.Result;
import com.gsafety.gemp.wxapplet.infection.contract.dto.UserDTO;
import com.gsafety.gemp.wxapplet.infection.contract.iface.UserService;
import com.gsafety.gemp.wxapplet.infection.dao.po.UserPO;
import com.gsafety.gemp.wxapplet.utils.JWTUtils;
import com.gsafety.gemp.wxapplet.web.UserContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 用户接口
 *
 * @author zxaio
 * @since 2020/2/27  10:41
 */
@Api(value = "用户Web接口", tags = {"用户Web接口"})
@RestController
@RequestMapping("/sys")
@Slf4j
public class UserController {

    @Resource
    private UserService userService;

    @ApiOperation(value = "登录")
    @PostMapping("/login")
    @PassToken
    public Result login(@RequestBody UserDTO dto) {
        UserPO user = userService.findUser(dto.getUsername());
        if (ObjectUtil.isNull(user)) {
            return new Result().fail("用户名不存在！");
        }

        if (!user.checkPwd(dto.getPwd())) {
            return new Result().fail("密码错误！");
        }

        user.setToken(JWTUtils.create(user));
        userService.updateToken(user);

        return new Result().success(user);
    }

    @ApiOperation(value = "注销")
    @PostMapping("/logout")
    @ApiImplicitParams({ @ApiImplicitParam(paramType = "header", dataType = "String", name = "token", value = "token标记", required = true) })
    public Result logout() {
        UserPO user = UserContext.getUser();
        user.setToken(null);
        userService.updateToken(user);

        UserContext.setUser(null);

        return new Result().success("注销成功");
    }
    

}
