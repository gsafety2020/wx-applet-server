package com.gsafety.gemp.wxapplet.infection.contract.params;


import com.gsafety.gemp.wxapplet.infection.contract.common.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author fanlx
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description="流水查询参数")
public class TraceSearchParam extends PageReq implements Serializable {
    @ApiModelProperty("身份证号")
    private String idNo;
    @ApiModelProperty("患者姓名")
    private String suffererName;
}
