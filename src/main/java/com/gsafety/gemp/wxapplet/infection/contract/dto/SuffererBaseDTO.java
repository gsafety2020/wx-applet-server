package com.gsafety.gemp.wxapplet.infection.contract.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 患者基本信息
 *
 * @author liyanhong
 * @since 2020/2/27  14:17
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("患者基本信息")
public class SuffererBaseDTO implements Serializable {

    private static final long serialVersionUID = -7033143140164839217L;

    @JsonProperty("id")
    @ApiModelProperty(value = "患者ID")
    private String id;

    @JsonProperty("sufferer_name")
    @ApiModelProperty("姓名")
    private String suffererName;

    @JsonProperty("sufferer_age")
    @ApiModelProperty(value = "年龄")
    private String suffererAge;

    @JsonProperty("sufferer_gender")
    @ApiModelProperty(value = "性别")
    private String suffererGender;

    @JsonProperty("sufferer_tel")
    @ApiModelProperty(value = "电话")
    private String suffererTel;
}
