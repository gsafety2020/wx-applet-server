package com.gsafety.gemp.wxapplet.infection.contract.iface;

import com.gsafety.gemp.wxapplet.infection.contract.dto.CdcSuffererInfoDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.CdcSuffererTraceDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.CdcSuffererTracesDTO;
import com.gsafety.gemp.wxapplet.infection.contract.params.CdcSuffererNewlySearchParam;
import com.gsafety.gemp.wxapplet.infection.contract.params.TraceSearchParam;
import org.springframework.data.domain.Page;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface CdcSuffererTraceService {


    CdcSuffererTraceDTO getTraceList(String suffererId);

    CdcSuffererTraceDTO getTraceList(String suffererId,String suffererName);

    Integer statisticsNewly(String dataDate,String type);

    Page<CdcSuffererInfoDTO> getNewlyList(CdcSuffererNewlySearchParam dto);

    void statExport(HttpServletResponse response, HttpServletRequest request, String date) throws Exception;

    void traceExport(String suffererIdNo, String suffererName, HttpServletResponse response, HttpServletRequest request) throws Exception;

    Page<CdcSuffererTracesDTO> getTracePage(TraceSearchParam dto);
}
