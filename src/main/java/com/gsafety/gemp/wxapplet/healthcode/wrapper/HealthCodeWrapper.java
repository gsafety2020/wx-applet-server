/**
 * 
 */
package com.gsafety.gemp.wxapplet.healthcode.wrapper;

import com.gsafety.gemp.wxapplet.healthcode.contract.dto.PestHealthCodeDTO;
import com.gsafety.gemp.wxapplet.healthcode.dao.po.PestHealthCodePO;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * 该类用于将返回结果转成DTO。
 *
 * @author wangwenhai
 *
 * @since 2020年2月25日 下午1:09:51
 *
 */
public class HealthCodeWrapper {

	/**
	 *
	 */
	private HealthCodeWrapper() {
		super();
	}

	/**
	 * 
	 * 该方法用于实现单个PO转DTO
	 *
	 * @author wangwenhai
	 * 
	 * @since 2020年2月13日 下午4:08:08
	 *
	 * @param po
	 * @return
	 */
	public static PestHealthCodeDTO toDTO(PestHealthCodePO po) {
		PestHealthCodeDTO dto = PestHealthCodeDTO.builder().build();
		BeanUtils.copyProperties(po, dto);
		String color=po.getHealthCodeColor();
		if("red".equals(color)){
			dto.setHealthCodeColor("红色");
		} else if("green".equals(color)){
			dto.setHealthCodeColor("绿色");
		} else if("yellow".equals(color)){
			dto.setHealthCodeColor("黄色");
		}
		return dto;
	}

	/**
	 * 
	 * 该方法用于实现list po转DTO
	 *
	 * @author wangwenhai
	 * 
	 * @since 2020年2月13日 下午4:09:53
	 *
	 * @param records
	 * @return
	 */
	public static List<PestHealthCodeDTO> toDTOList(List<PestHealthCodePO> records) {
		List<PestHealthCodeDTO> list = new ArrayList<>();
		records.stream().forEach(record -> list.add(toDTO(record)));
		return list;
	}

	/**
	 *
	 * 该方法用于实现page po转DTO
	 *
	 * @author wangwenhai
	 *
	 * @since 2020年2月13日 下午4:09:53
	 *
	 * @param page
	 * @return
	 */
	public static Page<PestHealthCodeDTO> toPageDTO(Page<PestHealthCodePO> page){
		List<PestHealthCodeDTO> content = toDTOList(page.getContent());
		return new PageImpl<>(content, page.getPageable(), page.getTotalElements());
	}

	public static PestHealthCodePO dtoToPo(PestHealthCodeDTO dto) {
		PestHealthCodePO po = new PestHealthCodePO();
		BeanUtils.copyProperties(dto, po);
		String color=dto.getHealthCodeColor();
		if("红色".equals(color)){
			po.setHealthCodeColor("red");
		} else if("绿色".equals(color)){
			po.setHealthCodeColor("green");
		} else if("黄色".equals(color)){
			po.setHealthCodeColor("yellow");
		}
		return po;
	}
}
