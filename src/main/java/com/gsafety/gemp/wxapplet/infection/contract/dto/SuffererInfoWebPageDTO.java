package com.gsafety.gemp.wxapplet.infection.contract.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("WEB端口分页查询DTO")
public class SuffererInfoWebPageDTO {

	@ApiModelProperty(value = "患者姓名")
	private String suffererName;
	
	@ApiModelProperty(value = "身份证号")
	private String suffererCard;
	
	@ApiModelProperty(value = "患者电话")
	private String suffererTel;
	
	@ApiModelProperty(value = "病人类型：1.密接人员；2.发热患者；3.疑似患者；4.确诊; 5.其他;")
	private Integer suffererType;
	
	@ApiModelProperty(value = "病患状态：1.隔离点在管（进入）；2.在院治疗（进入）；3.治愈出院（离开）；4.病亡（离开）；5.转出（离开）；6.其他；")
	private Integer suffererStatus;
	
	@ApiModelProperty(value = "病情. 1.治愈；2死亡；3.危重 4.重症 5.其他")
	private Integer illnessState;
	
	@ApiModelProperty(value = "页码,从0开始")
	private Integer page;
	
	@ApiModelProperty(value = "每页行数")
	private Integer size;
}
