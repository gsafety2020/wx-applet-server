package com.gsafety.gemp.wxapplet.infection.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.gsafety.gemp.wxapplet.infection.contract.dto.TraceTJJDTO;
import com.gsafety.gemp.wxapplet.infection.contract.enums.SuffererTraceTypeEnum;
import com.gsafety.gemp.wxapplet.infection.contract.iface.SuffererTraceService;
import com.gsafety.gemp.wxapplet.infection.dao.SuffererInfoDao;
import com.gsafety.gemp.wxapplet.infection.dao.SuffererTraceDao;
import com.gsafety.gemp.wxapplet.infection.dao.po.SuffererInfoPO;
import com.gsafety.gemp.wxapplet.infection.dao.po.SuffererTracePO;



@Service("suffererTraceService")
public class SuffererTraceServiceImpl implements SuffererTraceService {

	@Autowired
	private SuffererTraceDao suffererTraceDao;
	
	@Autowired
	private SuffererInfoDao suffererInfoDao;
	
	@Override
	public Map<String, SuffererTracePO> getSuffererIdSequenceNo() {
		List<SuffererTracePO> results = suffererTraceDao.findAll();
		Map<String, SuffererTracePO> map =results.parallelStream().collect(Collectors.groupingBy(SuffererTracePO::getSuffererId,
				Collectors.collectingAndThen(
			    Collectors.reducing(( c1,  c2) -> c1.getSequenceNo() > c2.getSequenceNo() ? c1 : c2),Optional::get))); 
		return map;
	}

	@Override
	public Map<String,Object> trackTJJSufferer(String suffererId) {
		if(StringUtils.isEmpty(suffererId)){
			throw new RuntimeException("患者Id为空!");
		}
		Map<String,Object> resultMap = new HashMap<String, Object>();
		List<SuffererTracePO> traces = suffererTraceDao.findBySuffererId(suffererId);
		Map<Integer,List<SuffererTracePO>> map = null;
		List<TraceTJJDTO> result = null;
		if(!CollectionUtils.isEmpty(traces)){
			map = traces.stream().collect(Collectors.groupingBy(SuffererTracePO::getSequenceNo));
			result = covertMapToTJJJson(map);
		}
		Optional<SuffererInfoPO> optional =  suffererInfoDao.findById(suffererId);
		String username = "";
		if(optional.isPresent()){
			username = optional.get().getSuffererName();
		}
		resultMap.put("name",username);
		resultMap.put("list", result);
		return resultMap;
	}
	
	private List<TraceTJJDTO> covertMapToTJJJson(Map<Integer,List<SuffererTracePO>> map){
		TraceTJJDTO tjj = null;
		List<TraceTJJDTO> lists = new ArrayList<>();
		Map<Integer,List<SuffererTracePO>> sortMap = map.entrySet().stream()  
	    .sorted(Map.Entry.comparingByKey())
	    .collect(
	        Collectors.toMap(
	          Map.Entry::getKey, 
	          Map.Entry::getValue,
	          (oldVal, newVal) -> oldVal,
	          LinkedHashMap::new
	        )
	    );
		for(Entry<Integer,List<SuffererTracePO>> entry : sortMap.entrySet()){
			int i = 0;
			for(SuffererTracePO po : entry.getValue()){
				tjj = new TraceTJJDTO();
				if(i == 0){
					tjj.setSiteName(po.getSiteName());
				}
				tjj.setStatusName(SuffererTraceTypeEnum.getValueByCode(po.getTraceType()));
				tjj.setChangeTime(po.getChangeTime());
				if(SuffererTraceTypeEnum.ROLL_OUT.getCode() == po.getTraceType()){
					tjj.setName("状态");
				}else if(SuffererTraceTypeEnum.SHIFT_IN.getCode() == po.getTraceType()){
					tjj.setName("状态");
				}else{
					tjj.setName("病情类型");
				}
				i++;
				lists.add(tjj);
			}		
		}
		return lists;
	}

}