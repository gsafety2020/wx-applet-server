package com.gsafety.gemp.wxapplet.infection.controller;

import com.gsafety.gemp.common.result.Result;
import com.gsafety.gemp.wxapplet.infection.contract.iface.LatestAdressInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(value = "临时查询位置接口", tags = {"临时查询位置接口"})
@RestController
@RequestMapping("/infection/adress/latest")
public class LatestAdressInfoController {

    @Autowired
    private LatestAdressInfoService latestAdressInfoService;

    @ApiOperation(value="临时查询位置接口")
    @PostMapping("/list")
    public Result<String> list(@RequestParam(value="prevDate") String prevDate){
        return latestAdressInfoService.excuteDataCompare(prevDate);
    }

}
