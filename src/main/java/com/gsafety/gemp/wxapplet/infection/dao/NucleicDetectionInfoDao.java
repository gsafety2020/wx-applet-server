package com.gsafety.gemp.wxapplet.infection.dao;

import com.gsafety.gemp.wxapplet.infection.dao.po.NucleicDetectionInfoPO;
import com.gsafety.gemp.wxapplet.infection.dao.po.PointHospitalInfoPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Date;
import java.util.List;

public interface NucleicDetectionInfoDao extends JpaRepository<NucleicDetectionInfoPO, String>,
        JpaSpecificationExecutor<NucleicDetectionInfoPO> {

    List<NucleicDetectionInfoPO> findByIsolationTypeIn(String[] type);

    List<NucleicDetectionInfoPO> findBySendSampleTime(Date time);

    List<NucleicDetectionInfoPO> findByIdNoIsNotNullOrderBySendSampleTime();

    List<NucleicDetectionInfoPO> findByIdNoIsNullOrderBySendSampleTime();

    List<NucleicDetectionInfoPO> findByIdNoIsNotNullAndDetectionNumIsNotNullOrderByDetectionNum();
}
