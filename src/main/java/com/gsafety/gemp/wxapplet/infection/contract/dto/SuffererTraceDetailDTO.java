package com.gsafety.gemp.wxapplet.infection.contract.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("病患溯源明细")
public class SuffererTraceDetailDTO {

    @ApiModelProperty(value = "定点医院/隔离点名称")
    private String siteName;

    @ApiModelProperty(value = "收治/类型/状态/病情")
    private String status ;

    @ApiModelProperty(value = "数据")
    private String info ;

    @ApiModelProperty(value = "时间")
    private Date time;
}
