package com.gsafety.gemp.wxapplet.infection.contract.iface;

import com.gsafety.gemp.wxapplet.infection.contract.dto.FangcangSuffererInfoDTO;
import com.gsafety.gemp.wxapplet.infection.contract.params.FangcangSuffererInfoSearchParam;
import org.springframework.data.domain.Page;

/**
 * @author dusiwei
 */
public interface FangcangSuffererInfoService {

    /**
     * 分页列表
     * @param dto
     * @return
     */
    Page<FangcangSuffererInfoDTO> findPage(FangcangSuffererInfoSearchParam dto);

    /**
     * 根据id查询
     * @param id
     * @return
     */
    FangcangSuffererInfoDTO findOne(String id);

    /**
     * 数据是否存在
     * @param id
     * @return
     */
    boolean existsById(String id);

}
