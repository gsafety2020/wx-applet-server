package com.gsafety.gemp.wxapplet.infection.contract.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("转出位置")
public class SiteInfoSelectDTO implements Serializable{

	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;

	@JsonProperty("id")
    @ApiModelProperty(value = "位置ID")
	private String id;
	
	@JsonProperty("siteName")
    @ApiModelProperty(value = "位置名称")
	private String siteName;
}
