package com.gsafety.gemp.wxapplet.healthcode.constant;

/**
 * 
* @ClassName: PestHealthAuthorizeStatus 
* @Description: 授权信息枚举类 
* @author luoxiao
* @date 2020年2月26日 下午5:33:26 
*
 */
public class PestHealthAuthorizeStatus {

	public enum DeleteEnum{
		
		NOT_DELETE("0","未删除"),DELETED("1","已删除");
		
		private String code;
		
		private String value;

		private DeleteEnum(String code, String value) {
			this.code = code;
			this.value = value;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}
}
