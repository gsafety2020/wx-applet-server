package com.gsafety.gemp.wxapplet.infection.dao;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gsafety.gemp.wxapplet.infection.dao.po.SuffererTracePO;

/**
 * 
* @ClassName: SuffererTraceDao 
* @Description: 状态变更记录DAO
* @author luoxiao
* @date 2020年3月1日 下午4:41:44 
*
 */
public interface SuffererTraceDao extends JpaRepository<SuffererTracePO, String>,
JpaSpecificationExecutor<SuffererTracePO>{


    List<SuffererTracePO> findBySuffererIdOrderByChangeTime(String suffererId);

    @Transactional
    void deleteByCreateTimeBetween(Date beginTime,Date endTime);
    
    int countByCreateTimeBetween(Date beginTime,Date endTime);
    
    List<SuffererTracePO> findBySuffererId(String suffererId);

    int countByTraceTypeAndChangeTimeBetween(Integer code, Date dateToStringBeginOrEnd, Date dateToStringBeginOrEnd1);

    List<SuffererTracePO> findByTraceTypeAndChangeTimeBetween(Integer suffererType, Date dateToStringBeginOrEnd, Date dateToStringBeginOrEnd1);
}
