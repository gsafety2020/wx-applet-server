package com.gsafety.gemp.wxapplet.infection.contract.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

/**
 * Author: huangll Written on 18/3/13.
 */

@Getter
@Setter
public class PageReq {

    @ApiModelProperty(value = "当前页数")
    private int nowPage = 1;

    @ApiModelProperty(value = "每页条数")
    private int pageSize = 20;

    @ApiModelProperty(hidden = true)
    public Pageable getPageable() {
        return PageRequest.of(nowPage - 1, pageSize);
    }

    @ApiModelProperty(hidden = true)
    public Pageable getPageable(Sort.Order order) {
        return PageRequest.of(nowPage - 1, pageSize, Sort.by(order));
    }

    @ApiModelProperty(hidden = true)
    public Pageable getPageable(List<Sort.Order> orders) {
        return PageRequest.of(nowPage - 1, pageSize, Sort.by(orders));
    }

}
