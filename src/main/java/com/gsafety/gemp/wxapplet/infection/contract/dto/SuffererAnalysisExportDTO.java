package com.gsafety.gemp.wxapplet.infection.contract.dto;

import com.gsafety.gemp.common.excel.ExcelAttribute;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author dusiwei
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SuffererAnalysisExportDTO {

    @ExcelAttribute(name="姓名")
    private String suffererName;

    @ExcelAttribute(name="性别")
    private String suffererGender;

    @ExcelAttribute(name = "年龄")
    private Float suffererAge;

    @ExcelAttribute(name = "身份证号")
    private String suffererCard;

    @ExcelAttribute(name = "电话")
    private String suffererTel;

    private Date moveIntoTime;

    @ExcelAttribute(name = "收治日期")
    private String moveIntoTimeStr;

    private Integer suffererStatus;

    @ExcelAttribute(name = "当前状态")
    private String suffererStatusName;

    @ExcelAttribute(name = "隔离点位置")
    private String curSiteName;

}
