package com.gsafety.gemp.wxapplet.infection.service;

import com.gsafety.gemp.wxapplet.infection.contract.dto.SiteNameDTO;
import com.gsafety.gemp.wxapplet.infection.contract.iface.KeyAssignmentService;
import com.gsafety.gemp.wxapplet.infection.dao.KeyAssignmentDao;
import com.gsafety.gemp.wxapplet.infection.dao.po.SiteInfoPO;
import com.gsafety.gemp.wxapplet.infection.wrapper.KeyAssignmentWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author lzx
 * @version 1.0
 * @date 2020/2/28 1:10
 */
@Service
public class KeyAssignmentImpl  implements KeyAssignmentService {

    @Autowired
    private KeyAssignmentDao keyAssignmentDao;

    @Override
    public List<SiteNameDTO> findSiteName() {
        List<SiteInfoPO> records = this.keyAssignmentDao.findAll();
        return KeyAssignmentWrapper.toDTOList(records);
    }
}
