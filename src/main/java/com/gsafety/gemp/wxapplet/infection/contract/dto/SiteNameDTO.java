package com.gsafety.gemp.wxapplet.infection.contract.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lzx
 * @version 1.0
 * @date 2020/2/28 1:52
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SiteNameDTO {
    @ApiModelProperty(value = "ID")
    private String id;
    @ApiModelProperty(value = "观察点名称")
    private String siteName;
}
