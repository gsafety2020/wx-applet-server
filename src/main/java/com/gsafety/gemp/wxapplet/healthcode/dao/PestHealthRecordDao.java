package com.gsafety.gemp.wxapplet.healthcode.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gsafety.gemp.wxapplet.healthcode.dao.po.PestHealthRecordPO;

/**
 * 
* @ClassName: PestHealthRecordDao 
* @Description: 打卡记录数层
* @author luoxiao
* @date 2020年2月26日 下午5:24:05 
*
 */
public interface PestHealthRecordDao extends JpaRepository<PestHealthRecordPO, String>,
JpaSpecificationExecutor<PestHealthRecordPO> {

}
