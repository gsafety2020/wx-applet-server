package com.gsafety.gemp.wxapplet.infection.contract.enums;

import com.gsafety.gemp.wxapplet.infection.contract.dto.CaseTypeDTO;

import java.util.ArrayList;
import java.util.List;

public enum  CaseTypeEnum {

    TYPE_ALL("0","全部"),
    TYPE_LINCHUANG("1","临床诊断"),
    TYPE_QUEZHEN("2","确诊"),
    TYPE_YANGXING("3","阳性检测"),
    TYPE_YISHI("4","疑似");

    private String code;
    private String name;
    CaseTypeEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static String getNameByCode(String code) {
        for (CaseTypeEnum caseTypeEnum : values()) {
            if (caseTypeEnum.code.equals(code)){
                return caseTypeEnum.name;
            }
        }
        return null;
    }

    public static String getCodeByName(String name) {
        for (CaseTypeEnum caseTypeEnum : values()) {
            if (caseTypeEnum.name.equals(name)){
                return caseTypeEnum.code;
            }
        }
        return null;
    }

    public static List<CaseTypeDTO> getList(){
        List<CaseTypeDTO> list=new ArrayList<>();
        CaseTypeDTO caseTypeDTO=null;
        for (CaseTypeEnum caseTypeEnum : values()) {
            caseTypeDTO=new CaseTypeDTO();
            caseTypeDTO.setCode(Integer.parseInt(caseTypeEnum.code));
            caseTypeDTO.setName(caseTypeEnum.name);
            list.add(caseTypeDTO);
        }
        return list;
    }

}
