package com.gsafety.gemp.wxapplet.infection.controller;

import cn.hutool.core.util.ObjectUtil;
import com.gsafety.gemp.wxapplet.healthcode.contract.dto.Result;
import com.gsafety.gemp.wxapplet.infection.contract.dto.NucleicDetectionInfoPageDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.PointCommonDTO;
import com.gsafety.gemp.wxapplet.infection.contract.enums.NucleicCompareFlagEnum;
import com.gsafety.gemp.wxapplet.infection.contract.enums.NucleicDetectionResultEnum;
import com.gsafety.gemp.wxapplet.infection.contract.enums.NucleicIsolationTypeEnum;
import com.gsafety.gemp.wxapplet.infection.contract.iface.NucleicDetectionInfoService;
import com.gsafety.gemp.wxapplet.infection.dao.po.NucleicDetectionInfoPO;
import com.gsafety.gemp.wxapplet.infection.wrapper.NucleicDetectionInfoWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(value = "核酸检测信息接口", tags = {"核酸检测信息接口"})
@RestController
@RequestMapping("/infection/nucleic/detection")
@Slf4j
public class NucleicDetectionInfoController {

    @Autowired
    private NucleicDetectionInfoService nucleicDetectionInfoService;

    @ApiOperation(value="核酸检测信息分页查询接口")
    @PostMapping("/list")
    public Result list(@RequestBody NucleicDetectionInfoPageDTO dto){
        if(dto.getPage() == null || dto.getPage() <= 0){
            dto.setPage(0);
        }else{
            dto.setPage(dto.getPage() - 1);
        }

        if(dto.getSize() == null || dto.getSize() < 1){
            dto.setSize(10);
        }
        Sort sort = Sort.by(Sort.Direction.DESC, "updateTime");
        PageRequest pageRequest = PageRequest.of(dto.getPage(), dto.getSize(),sort);
        return new Result().success(nucleicDetectionInfoService.search(dto,pageRequest));
    }

    @ApiOperation(value="核酸检测信息信息详情接口")
    @GetMapping("/detail")
    public Result detail(@ApiParam(value = "患者ID") @RequestParam String id){
        NucleicDetectionInfoPO po = nucleicDetectionInfoService.findOne(id);
        if (ObjectUtil.isNull(po)) {
            return new Result().fail("数据查找失败");
        }

        return new Result().success(NucleicDetectionInfoWrapper.toDTO(po));
    }

    /**
     *
     * @param
     * @return
     */
    @ApiOperation(value = "检测结果下拉框")
    @GetMapping(value = "/findListByDetectionResult")
    public Result findListByDetectionResult(){
        List<PointCommonDTO> dto= NucleicDetectionResultEnum.getList();
        return new Result().success(dto);
    }

    /**
     *
     * @param
     * @return
     */
    @ApiOperation(value = "隔离点类型下拉框")
    @GetMapping(value = "/findListByIsolationType")
    public Result findListByIsolationType(){
        List<PointCommonDTO> dto= NucleicIsolationTypeEnum.getList();
        return new Result().success(dto);
    }

    @ApiOperation(value="检测次数更新")
    @PostMapping("/detectionNumUpdate")
    public void detectionNumUpdate(){
        nucleicDetectionInfoService.deteCtionNumUpdate();
    }

    @ApiOperation(value = "检测次数下拉框")
    @GetMapping(value = "/detectionNumList")
    public Result detectionNumList(){
        List<PointCommonDTO> dto= nucleicDetectionInfoService.detectionNumList();
        return new Result().success(dto);
    }

    @ApiOperation(value = "比较标志下拉框")
    @GetMapping(value = "/compareFlagList")
    public Result compareFlagList(){
        List<PointCommonDTO> dto= NucleicCompareFlagEnum.getList();
        return new Result().success(dto);
    }
}
