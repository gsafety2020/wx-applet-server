package com.gsafety.gemp.wxapplet.infection.contract.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author liyanhong
 * @since 2020/2/27  14:51
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("患者状态编辑对象")
public class SuffererEditDTO implements Serializable {
    private static final long serialVersionUID = -111515308796679611L;

    @JsonProperty("id")
    @ApiModelProperty(value = "患者ID")
    private String id;

    @JsonProperty("sufferer_type")
    @ApiModelProperty(value = "患者类型")
    private Integer suffererType;

    @JsonProperty("sufferer_status")
    @ApiModelProperty(value = "患者状态")
    private Integer suffererStatus;

    @JsonProperty("cur_site_name")
    @ApiModelProperty(value = "当前位置")
    private String curSiteName;

    @JsonProperty("cur_site_id")
    @ApiModelProperty(value = "当前位置ID")
    private String curSiteId;

    @JsonProperty("update_by")
    @ApiModelProperty(value = "操作员的openid")
    private String updateBy;

    @JsonProperty("session_key")
    @ApiModelProperty(value = "用户sessionkey")
    private String sessionKey;
    
    @JsonProperty("illness_state")
    @ApiModelProperty(value = "病情. 1.治愈；2死亡；3.危重 4.重症 5.其他")
    private Integer illnessState;
    
    @JsonProperty("remark")
    @ApiModelProperty(value = "备注")
    private String remark;
}
