package com.gsafety.gemp.wxapplet.infection.wrapper;

import com.gsafety.gemp.wxapplet.healthcode.constant.PestWechatUserStatus;
import com.gsafety.gemp.wxapplet.infection.contract.dto.CdcSuffererInfoDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.CdcSuffererInfoStatExportDTO;
import com.gsafety.gemp.wxapplet.infection.contract.enums.CaseTypeEnum;
import com.gsafety.gemp.wxapplet.infection.contract.enums.ClinicalSeverityEnum;
import com.gsafety.gemp.wxapplet.infection.dao.po.CdcSuffererInfo;
import com.gsafety.gemp.wxapplet.infection.dao.po.CdcSuffererInfoAll;
import com.gsafety.gemp.wxapplet.utils.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.math.BigDecimal;
import java.util.*;

public class CdcSuffererInfoWrapper {

    private static Calendar calendar1900 = new GregorianCalendar(1900,0,-1);

    private final static String POINT = ".";

    public static CdcSuffererInfo toPO(CdcSuffererInfoDTO dto) {
        CdcSuffererInfo po = new CdcSuffererInfo();
        BeanUtils.copyProperties(dto,po);
        return format(po);
    }


    private static CdcSuffererInfo format(CdcSuffererInfo po){

        if(StringUtils.isNotEmpty(po.getCardId())){
            po.setCardId(po.getCardId().replace("'",""));
        }

        if(StringUtils.isNotEmpty(po.getIdNo())){
            po.setIdNo(po.getIdNo().replace("'",""));
        }

        po.setCaseType(CaseTypeEnum.getCodeByName(po.getCaseType()));

        po.setSex(PestWechatUserStatus.GenderEnum.getCodeByName(po.getSex()));

        po.setClinicalSeverity(ClinicalSeverityEnum.getCodeByName(po.getClinicalSeverity()));

        po.setDeadthDate(dateFormat(po.getDeadthDate()));

        po.setBeforeRevisionCaseDate(dateFormat(po.getBeforeRevisionCaseDate()));

        po.setFjbrDate(dateFormat(po.getFjbrDate()));

        po.setReportRevisionDate(dateFormat(po.getReportRevisionDate()));

        po.setFinalJudgmentDeathDate(dateFormat(po.getFinalJudgmentDeathDate()));

        po.setDeleteDate(dateFormat(po.getDeleteDate()));

        po.setBirthday(dateFormat(po.getBirthday()));

        po.setCityReviewDate(dateFormat(po.getCityReviewDate()));

        po.setProvinceReviewDate(dateFormat(po.getProvinceReviewDate()));

        po.setDistrictReviewDate(dateFormat(po.getDistrictReviewDate()));

        po.setRevisionFinalJudgmentDate(dateFormat(po.getRevisionFinalJudgmentDate()));

        po.setFillInDate(dateFormat(po.getFillInDate()));

        po.setReportFillInDate(dateFormat(po.getReportFillInDate()));

        po.setCaseDate(dateFormat(po.getCaseDate()));

        po.setOnsetDate(dateFormat(po.getOnsetDate()));

        return po;
    }

    public static List<CdcSuffererInfo> toPoList(List<CdcSuffererInfoDTO> dtos){
        List<CdcSuffererInfo> list = new ArrayList<>();
        for (CdcSuffererInfoDTO dto : dtos) {
            CdcSuffererInfo po = toPO(dto);
            list.add(po);
        }
        return list;
    }

    private static String dateFormat(String  date){
        if(StringUtils.isEmpty(date) || POINT.equals(date)){
            return null;
        }
        return DateUtils.format(DateUtils.addDays(calendar1900.getTime(),new BigDecimal(date)), "yyyy-MM-dd HH:mm:ss");
    }

    public static CdcSuffererInfoDTO toDTO(CdcSuffererInfo po) {
        CdcSuffererInfoDTO dto = new CdcSuffererInfoDTO();
        BeanUtils.copyProperties(po, dto);
        if(null!=po.getCaseType()) {
            dto.setCaseType(CaseTypeEnum.getNameByCode(po.getCaseType()));
        }
        if(StringUtils.isNotBlank(po.getClinicalSeverity())) {
            dto.setClinicalSeverity(ClinicalSeverityEnum.getNameByCode(po.getClinicalSeverity()));
        }
        dto.setSex(PestWechatUserStatus.GenderEnum.getNameByCode(dto.getSex()));
        return dto;
    }

    public static CdcSuffererInfoDTO toDTO(CdcSuffererInfoAll po) {
        CdcSuffererInfoDTO dto = new CdcSuffererInfoDTO();
        BeanUtils.copyProperties(po, dto);
        if(null!=po.getCaseType()) {
            dto.setCaseType(CaseTypeEnum.getNameByCode(po.getCaseType()));
        }
        if(StringUtils.isNotBlank(po.getClinicalSeverity())) {
            dto.setClinicalSeverity(ClinicalSeverityEnum.getNameByCode(po.getClinicalSeverity()));
        }
        dto.setSex(PestWechatUserStatus.GenderEnum.getNameByCode(dto.getSex()));
        return dto;
    }

    public static List<CdcSuffererInfoDTO> toDTOList(List<CdcSuffererInfo> records) {
        List<CdcSuffererInfoDTO> list = new ArrayList<>();
        records.stream().forEach(record -> list.add(toDTO(record)));
        return list;
    }

    public static Page<CdcSuffererInfoDTO> toPageDTO(Page<CdcSuffererInfo> page){
        List<CdcSuffererInfoDTO> content = toDTOList(page.getContent());
        return new PageImpl<>(content, page.getPageable(), page.getTotalElements());
    }

    public static CdcSuffererInfoStatExportDTO toStatExportDTO(CdcSuffererInfo record) {
        CdcSuffererInfoStatExportDTO dto = new CdcSuffererInfoStatExportDTO();
        BeanUtils.copyProperties(record,dto);
        if (StringUtils.isNotEmpty(record.getDeadthDate())) {
            dto.setStatus("死亡");
        } else if (StringUtils.isNotEmpty(record.getReferralType())) {
            dto.setStatus("已转诊");
        } else {
            dto.setStatus("在院治疗");
        }
        return dto;
    }

}
