/**
 * 
 */
package com.gsafety.gemp.wxapplet.healthcode.factory;

import com.gsafety.gemp.wxapplet.utils.PropertiesUtils;
import com.gsafety.gemp.wxapplet.utils.SpringContextUtil;

/**
 * 该类用于...，用于实现...等能力。
 *
 * @author yangbo
 *
 * @since 2020年2月14日 下午8:55:59
 *
 */
public class WechatUserManagerFactory {

	/**
	 * 
	 */
	private static WechatUserManager UNIQUE; 
	
	/**选择缓存存储策略  local表示本地策略   redis表示redis策略*/
	private static String strategy = PropertiesUtils.get("/application.properties", "static.wechart.user.redis.cache.selected");
	/**
	 * 
	 */
	private WechatUserManagerFactory() {
		super();
	}

	/**
	 * 
	 * 该方法用于实现...的功能
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月14日 下午8:56:18
	 *
	 * @return
	 */
	public static WechatUserManager getInstance() {
		if(null == UNIQUE) {
			if(CacheStrategyEnum.LOCAL.getCode().equals(strategy)){
				UNIQUE = new LocalCacheWechatUserManager();
			}else if(CacheStrategyEnum.REDIS.getCode().equals(strategy)){
				UNIQUE = SpringContextUtil.getBean(RedisCacheWechatUserManager.class);
			}else{
				throw new RuntimeException("缓存策略不存在!");
			}
			
		}
		return UNIQUE;
	}
}
