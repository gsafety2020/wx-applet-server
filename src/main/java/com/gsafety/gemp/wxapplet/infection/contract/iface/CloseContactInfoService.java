package com.gsafety.gemp.wxapplet.infection.contract.iface;


import com.gsafety.gemp.wxapplet.infection.contract.dto.CloseContactInfoDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.CloseContactTreeDTO;
import com.gsafety.gemp.wxapplet.infection.contract.dto.SuffererInfoDetailDTO;
import com.gsafety.gemp.wxapplet.infection.contract.params.CloseContactInfoSearchParam;
import com.gsafety.gemp.wxapplet.infection.contract.params.SuperSpreaderSearchParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

/**
 * @author fanlx
 */
public interface CloseContactInfoService {

    PageImpl findPage(CloseContactInfoSearchParam param);

    void syncCurLocation(String dataDate);

    void genrateCloseContactIdNo();

    Page<SuffererInfoDetailDTO> findSuperSpreader(SuperSpreaderSearchParam param);

     List<CloseContactTreeDTO> findNetList(String idCard);
}
