package com.gsafety.gemp.wxapplet.infection.contract.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 
* @ClassName: SiteStaffRelationStatus 
* @Description: 人员站点隔离群关系枚举类
* @author luoxiao
* @date 2020年3月1日 下午11:44:31 
*
 */
public class SiteStaffRelationStatus {

	@Getter
	@AllArgsConstructor
	public enum GroupSiteEnum{
		
		ALL("all","全部"),OTHER("other","其他");
		
		private String code;
		
		private String value;
	}
}
